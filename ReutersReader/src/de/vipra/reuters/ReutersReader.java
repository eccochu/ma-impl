package de.vipra.reuters;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import de.fuberlin.inf.agcsw.cer.models.Article;
import de.fuberlin.inf.agcsw.cer.models.ReutersArticleWithFrames;

public class ReutersReader {

	public static final ObjectMapper mapper = new ObjectMapper();

	static {
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.enable(Feature.ALLOW_COMMENTS);
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		List<ReutersArticleWithFrames> articles = new ArrayList<>();

		try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(
				new FileInputStream("/home/eike/Downloads/articles-annotated-0-0.5-withframes.ser")))) {
			articles = (List<ReutersArticleWithFrames>) ois.readObject();
			System.out.println(articles.size());
			List<Article> articles2 = new ArrayList<>(articles.size());
			for (ReutersArticleWithFrames a : articles)
				articles2.add(new Article(a));
			mapper.writeValue(new File("/home/eike/Downloads/articles.json"), articles2);
		}
	}

}
