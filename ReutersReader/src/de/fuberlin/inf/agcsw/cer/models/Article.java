package de.fuberlin.inf.agcsw.cer.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Article {

	@JsonIgnore
	private static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	
	@JsonIgnore
	private ReutersArticleWithFrames article;
	
	public Article(ReutersArticleWithFrames a) {
		article = a;
	}
	
	public String getTitle() {
		return article.getTitle();
	}
	
	public String getText() {
		return article.getText();
	}
	
	public String getDate() {
		return DATE_FORMAT.format(article.getDate());
	}

}
