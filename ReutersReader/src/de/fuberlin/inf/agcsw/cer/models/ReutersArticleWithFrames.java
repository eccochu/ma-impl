package de.fuberlin.inf.agcsw.cer.models;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.fuberlin.inf.agcsw.cer.models.framenet.Frame;
import de.fuberlin.inf.agcsw.dbpedia.annotation.models.SpotlightAnnotation;

public class ReutersArticleWithFrames implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String dateString;
	private Date date;
	private String title;
	private String text;
	private String uuid;
	private SpotlightAnnotation annotation;
	private List<Frame> frames = new ArrayList<>();

	private static DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SS");

	public ReutersArticleWithFrames() {
	}

	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void parseDate() {
		try {
			date = DATE_FORMAT.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public SpotlightAnnotation getSpotlightAnnotation() {
		return annotation;
	}

	public void setSpotlightAnnotation(SpotlightAnnotation annotation) {
		this.annotation = annotation;
	}

	public List<Frame> getFramenetFrames() {
		return frames;
	}

	public void setFramenetFrames(List<Frame> frames) {
		this.frames = frames;
	}

	public void addFrames(List<Frame> frames) {
		this.frames.addAll(frames);
	}

	public static ReutersArticleWithFrames cloneFromOldArticle(ReutersArticle reutersArticle) {
		ReutersArticleWithFrames res = new ReutersArticleWithFrames();
		res.setSpotlightAnnotation(reutersArticle.getAnnotation());
		res.setDate(reutersArticle.getDate());
		res.setDateString(reutersArticle.getDateString());
		res.setText(reutersArticle.getText());
		res.setTitle(reutersArticle.getTitle());
		res.setUuid(reutersArticle.getUuid());

		return res;
	}
}
