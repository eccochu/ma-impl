package de.fuberlin.inf.agcsw.cer.models;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.fuberlin.inf.agcsw.dbpedia.annotation.models.SpotlightAnnotation;

/**
 * Created by wojlukas on 2/2/16.
 */
public class ReutersArticle implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String dateString;
	private Date date;
	private String title;
	private String text;
	private String uuid;
	private SpotlightAnnotation annotation;

	private static DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SS");

	public ReutersArticle() {
	}

	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void parseDate() {
		try {
			date = DATE_FORMAT.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public SpotlightAnnotation getAnnotation() {
		return annotation;
	}

	public void setAnnotation(SpotlightAnnotation annotation) {
		this.annotation = annotation;
	}
}