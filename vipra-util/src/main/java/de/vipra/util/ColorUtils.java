package de.vipra.util;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ColorUtils {

	/**
	 * @see {@link #generateRandomColor(Long)}
	 */
	public static Color generateRandomColor() {
		return generateRandomColor(null);
	}

	/**
	 * Generate a random color, using an optional seed.
	 *
	 * @param seed
	 *            optional random seed, null for default seed
	 * @return generated random color
	 */
	public static Color generateRandomColor(final Long seed) {
		final Random random = seed != null ? new Random(seed) : new Random();
		final int red = (random.nextInt(256) + 255) / 2;
		final int green = (random.nextInt(256) + 255) / 2;
		final int blue = (random.nextInt(256) + 255) / 2;
		final Color color = new Color(red, green, blue);
		return color;
	}

	/**
	 * @see {@link #generateRandomColors(int, Long)}
	 */
	public static List<Color> generateRandomColors(final int number) {
		return generateRandomColors(number, null);
	}

	/**
	 * Generate random colors, using an optional seed.
	 *
	 * @param number
	 *            how many colors to generate
	 * @param seed
	 *            optional random seed, null for default seed
	 * @return generated random colors
	 */
	public static List<Color> generateRandomColors(final int number, final Long seed) {
		final Random random = seed != null ? new Random(seed) : new Random();
		final List<Color> colors = new ArrayList<>(number);
		for (int i = 0; i < number; i++) {
			final int red = (random.nextInt(256) + 255) / 2;
			final int green = (random.nextInt(256) + 255) / 2;
			final int blue = (random.nextInt(256) + 255) / 2;
			colors.add(new Color(red, green, blue));
		}
		return colors;
	}

	/**
	 * Turns a Color object into a hexadecimal string of format #ABCDEF.
	 *
	 * @param color
	 *            Color to transform into hex string
	 * @return hex string
	 */
	public static String toHexString(final Color color) {
		return String.format("#%02X%02X%02X", color.getRed(), color.getGreen(), color.getBlue());
	}

	/**
	 * Parses a hexadecimal string into a Color object.
	 *
	 * @param hex
	 *            hexadecimal string
	 * @return Color object
	 */
	public static Color fromHexString(final String hex) {
		return Color.decode(hex);
	}

}
