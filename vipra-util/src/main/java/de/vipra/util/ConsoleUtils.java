package de.vipra.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Color;

public class ConsoleUtils {

	public static enum Level {
		INFO,
		WARN,
		ERROR
	};

	public static final String PATH_T = "├";
	public static final String PATH_L = "└";

	private static boolean silent = false;
	private static int pad = 5;
	private static int lastLineLength = 0;
	private static File logFile = null;
	private static PrintStream logWriter = null;

	public static void setSilent(final boolean s) {
		silent = s;
	}

	public static void setLogFile(final File file) throws IOException {
		logFile = file;
		logWriter = new PrintStream(new FileOutputStream(logFile, true), true);
	}

	private static int print(final PrintStream ps, final boolean newLine, final String msg) {
		int result = 0;
		if (!silent) {
			if (newLine)
				ps.println(msg);
			else
				ps.print(msg);
			if (msg != null) {
				lastLineLength = msg.length();
				result = lastLineLength;
			}
		}
		if (logWriter != null)
			logWriter.println(msg.replaceAll("\u001B\\[[;\\d]*[ -/]*[@-~]", "").replaceAll("\\r", ""));
		return result;
	}

	private static String label(final String label) {
		return StringUtils.pad(label, pad);
	}

	public static int print(final Level level, final String msg) {
		switch (level) {
			case INFO:
				return info(msg);
			case WARN:
				return warn(msg);
			case ERROR:
				return error(msg);
		}
		return 0;
	}

	public static void clearLine() {
		if (lastLineLength > 0) {
			System.out.print(StringUtils.repeat(" ", lastLineLength) + "\r");
			lastLineLength = 0;
		}
	}

	public static void flush() {
		System.out.flush();
	}

	public static int info(final String msg) {
		return print(System.out, true, label("INFO") + " - " + msg);
	}

	public static int infoNOLF(final String msg) {
		return print(System.out, false, label("INFO") + " - " + msg);
	}

	public static int warn(final String msg) {
		return print(System.out, true, label("WARN") + " - " + msg);
	}

	public static int warnNOLF(final String msg) {
		return print(System.out, false, label("WARN") + " - " + msg);
	}

	public static int error(final String msg) {
		return print(System.out, true, label("ERROR") + " - " + Ansi.ansi().fg(Color.RED).a(msg).reset());
	}

	public static int errorNOLF(final String msg) {
		return print(System.out, false, label("ERROR") + " - " + Ansi.ansi().fg(Color.RED).a(msg).reset());
	}

	public static int error(final Throwable t) {
		return error(t.getMessage());
	}

	public static int print(final String msg) {
		return print(System.out, true, msg);
	}

	public static int printNOLF(final String msg) {
		return print(System.out, false, msg);
	}

	public static double readDouble(String message, final Double def, final Double min, final Double max, final boolean showBounds) {
		final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		if (showBounds)
			message += " (" + (min != null ? min : "-∞") + ".." + (max != null ? max : "∞") + ")";
		if (def != null)
			message += " [" + def + "]";
		message += ": ";
		while (true) {
			infoNOLF(message);
			try {
				final String line = in.readLine();
				if (line.isEmpty() && def != null)
					return def;
				final double d = Double.parseDouble(line);
				if (min != null && d < min)
					continue;
				if (max != null && d > max)
					continue;
				return d;
			} catch (final NumberFormatException e) {} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public static double readDouble(final String message, final Double def, final Double min, final Double max) {
		return readDouble(message, def, min, max, true);
	}

	public static double readDouble(final String message, final Double def) {
		return readDouble(message, def, null, null, true);
	}

	public static double readDouble(final String message) {
		return readDouble(message, null, null, null, false);
	}

	public static int readInt(String message, final Integer def, final Integer min, final Integer max, final boolean showBounds) {
		final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		if (showBounds)
			message += " (" + (min != null ? min : "-∞") + ".." + (max != null ? max : "∞") + ")";
		if (def != null)
			message += " [" + def + "]";
		message += ": ";
		while (true) {
			infoNOLF(message);
			try {
				final String line = in.readLine();
				if (line.isEmpty() && def != null)
					return def;
				final int i = Integer.parseInt(line);
				if (min != null && i < min)
					continue;
				if (max != null && i > max)
					continue;
				return i;
			} catch (final NumberFormatException e) {} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public static int readInt(final String message, final Integer def, final Integer min, final Integer max) {
		return readInt(message, def, min, max, true);
	}

	public static int readInt(final String message, final Integer def) {
		return readInt(message, def, null, null, true);
	}

	public static int readInt(final String message) {
		return readInt(message, null, null, null, false);
	}

	public static String readString(String message, final String def, final boolean canBeEmpty) {
		final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		message += ": ";
		while (true) {
			infoNOLF(message);
			try {
				final String line = in.readLine();
				if (line.isEmpty()) {
					if (def != null)
						return def;
					if (!canBeEmpty)
						continue;
				}
				return line;
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public static String readString(final String message, final String def) {
		return readString(message, def, false);
	}

	public static String readString(final String message) {
		return readString(message, null, false);
	}

	public static <T extends Enum<?>> T readEnum(final Class<T> enumClass, String message, final T def) {
		final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		if (def != null)
			message += " [" + def + "]";
		message += ": ";
		while (true) {
			infoNOLF(message);
			try {
				final String line = in.readLine();
				final T value = EnumUtils.searchEnum(enumClass, line);
				if (value != null)
					return value;
				if (def != null)
					return def;
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public static boolean readBoolean(String message, final Boolean def) {
		final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		if (def != null)
			message += " [" + def + "]";
		message += ": ";
		while (true) {
			infoNOLF(message);
			try {
				final String line = in.readLine().toLowerCase().trim();
				if (line.equals("true") || line.equals("yes") || line.equals("1") || line.equals("on") || line.equals("enable")
						|| line.equals("enabled"))
					return true;
				if (line.equals("false") || line.equals("no") || line.equals("0") || line.equals("off") || line.equals("disable")
						|| line.equals("disabled"))
					return false;
				if (def != null)
					return def;
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public static boolean readBoolean(final String message) {
		return readBoolean(message, null);
	}

	public static String positionString(final int current, final int max) {
		String positionStr = "(" + StringUtils.pad(Integer.toString(current), Integer.toString(max).length(), " ", true) + "/" + max + ")";
		if (max > 1) {
			if (current < max)
				positionStr = " " + PATH_T + " " + positionStr;
			else
				positionStr = " " + PATH_L + " " + positionStr;
		} else
			positionStr = " " + PATH_L + " " + positionStr;
		return positionStr;
	}

}
