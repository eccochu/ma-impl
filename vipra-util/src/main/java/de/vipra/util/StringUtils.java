package de.vipra.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

	public static String ellipsize(final String input, final int maxLength) {
		final String ellip = "...";
		if (input == null || input.length() <= maxLength || input.length() < ellip.length()) {
			return input;
		}
		return input.substring(0, maxLength - ellip.length()).concat(ellip);
	}

	public static String join(final Iterable<String> it, final String separator, final boolean reverse) {
		final Iterator<String> iter = it.iterator();
		if (iter.hasNext()) {
			final StringBuilder sb = new StringBuilder(iter.next());
			while (iter.hasNext()) {
				sb.append(separator).append(iter.next());
			}
			return sb.toString();
		}
		return "";
	}

	public static String join(final Iterable<String> it) {
		return join(it, " ", false);
	}

	public static String join(final Iterable<String> it, final String separator) {
		return join(it, separator, false);
	}

	public static String join(final String[] arr) {
		return join(arr, " ", false);
	}

	public static String join(final String[] arr, final String separator) {
		return join(arr, separator, false);
	}

	public static String join(final String[] arr, final String separator, final boolean reverse) {
		return join(Arrays.asList(arr), separator, reverse);
	}

	public static String timeString(long nanos, final boolean showMillis, final boolean compactHMS, final boolean onlyHMS) {
		final List<String> parts = new ArrayList<>(6);

		if (!onlyHMS) {
			final long days = TimeUnit.NANOSECONDS.toDays(nanos);
			if (days > 0) {
				parts.add(days + "d");
				nanos -= TimeUnit.DAYS.toNanos(days);
			}
		}

		final long hours = TimeUnit.NANOSECONDS.toHours(nanos);
		if (hours > 0 || compactHMS) {
			if (compactHMS) {
				parts.add(StringUtils.padNumber(hours, 2));
				parts.add(":");
			} else if (hours > 0)
				parts.add(hours + "h");
			nanos -= TimeUnit.HOURS.toNanos(hours);
		}

		final long minutes = TimeUnit.NANOSECONDS.toMinutes(nanos);
		if (minutes > 0 || compactHMS) {
			if (compactHMS) {
				parts.add(StringUtils.padNumber(minutes, 2));
				parts.add(":");
			} else if (minutes > 0)
				parts.add(minutes + "m");
			nanos -= TimeUnit.MINUTES.toNanos(minutes);

		}

		final long seconds = TimeUnit.NANOSECONDS.toSeconds(nanos);
		if (seconds > 0 || compactHMS) {
			if (compactHMS)
				parts.add(StringUtils.padNumber(seconds, 2));
			else if (seconds > 0)
				parts.add(seconds + "s");
			nanos -= TimeUnit.SECONDS.toNanos(seconds);
		}

		if (showMillis && !onlyHMS) {
			final long millis = TimeUnit.NANOSECONDS.toMillis(nanos);
			if (millis > 0 || compactHMS) {
				if (compactHMS) {
					parts.add(".");
					parts.add(StringUtils.padNumber(millis, 4));
				} else
					parts.add(millis + "ms");
			}
		}

		if (parts.size() == 0) {
			parts.add("0ms");
		}

		return StringUtils.join(parts);
	}

	public static String timeString(final long nanos, final boolean showMillis) {
		return timeString(nanos, showMillis, false, false);
	}

	public static String timeString(final long nanos) {
		return timeString(nanos, true, false, false);
	}

	public static String padNumber(final long number, final int length) {
		String lc = Long.toString(number);
		while (lc.length() < length) {
			lc = "0" + lc;
		}
		return lc;
	}

	public static String padNumber(final int number, final int length) {
		return padNumber((long) number, length);
	}

	/**
	 * Turns byte counts into human readable strings. Taken from
	 * https://stackoverflow.com/questions/3758606
	 *
	 * @param bytes
	 *            number of bytes
	 * @param si
	 *            true to use SI units
	 * @return formatted string
	 */
	public static String humanReadableByteCount(final long bytes, final boolean si) {
		final int unit = si ? 1000 : 1024;
		if (bytes < unit)
			return bytes + " B";
		final int exp = (int) (Math.log(bytes) / Math.log(unit));
		final String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
		return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}

	public static String humanReadableByteCount(final long bytes) {
		return humanReadableByteCount(bytes, true);
	}

	private static Pattern firstNumberPattern = Pattern.compile("[0-9]+");

	public static Integer getFirstNumber(final String in) {
		final Matcher m = firstNumberPattern.matcher(in);
		if (m.find()) {
			try {
				return Integer.parseInt(m.group());
			} catch (final NumberFormatException e) {}
		}
		return null;
	}

	public static String quantity(final Number qty, final String singular, final String plural) {
		if (qty.intValue() == 1)
			return singular;
		return plural;
	}

	public static String quantity(final Number qty, final String singular) {
		return quantity(qty, singular, singular + "s");
	}

	public static String[] getFields(String fields) {
		if (fields == null)
			return null;
		fields = fields.trim();
		if (fields.length() == 0)
			return null;
		return fields.split(",");
	}

	public static String capitalize(final String in) {
		if (in == null || in.length() == 0)
			return in;
		return in.substring(0, 1).toUpperCase() + in.substring(1);
	}

	public static String decapitalize(final String in) {
		if (in == null || in.length() == 0)
			return in;
		return in.substring(0, 1).toLowerCase() + in.substring(1);
	}

	public static String camelToDashCase(final String in) {
		if (in == null || in.length() == 0)
			return in;
		return in.replaceAll("([A-Z])", "-$1").toLowerCase();
	}

	public static String dashToCamelCase(final String in) {
		if (in == null || in.length() == 0)
			return in;
		final StringBuilder sb = new StringBuilder();
		final String[] parts = in.split("-");
		sb.append(parts[0]);
		for (int i = 1; i < parts.length; i++)
			sb.append(StringUtils.capitalize(parts[i]));
		return sb.toString();
	}

	public static String repeat(final String pattern, int count) {
		final StringBuilder sb = new StringBuilder();
		while (count-- > 0)
			sb.append(pattern);
		return sb.toString();
	}

	public static String pad(final String str, final int length, final String pad, final boolean left) {
		if (str.length() >= length)
			return str;
		final StringBuilder sb = new StringBuilder();
		if (!left)
			sb.append(str);
		for (int i = 0; i < length - str.length(); i++)
			sb.append(pad);
		if (left)
			sb.append(str);
		return sb.toString();
	}

	public static String pad(final String str, final int length, final String pad) {
		return pad(str, length, pad, false);
	}

	public static String pad(final String str, final int length) {
		return pad(str, length, " ", false);
	}

	public static String pad(final String str, final int length, final boolean left) {
		return pad(str, length, " ", left);
	}

	public static final Pattern WORD_PATTERN = Pattern.compile("[a-zA-Z]+");

	public static boolean isWord(final String str) {
		return WORD_PATTERN.matcher(str).matches();
	}

}
