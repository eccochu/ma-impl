package de.vipra.util.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Reference;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.vipra.util.MongoUtils;
import de.vipra.util.StringUtils;
import de.vipra.util.an.QueryIgnore;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(value = "topics", noClassnameStored = true)
@Indexes({ @Index("name"), @Index("-name"), @Index("articlesCount"), @Index("-articlesCount"), @Index("created"), @Index("-created"),
		@Index("modified"), @Index("-modified") })
public class TopicFull implements Model<ObjectId>, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id = new ObjectId();

	@Reference
	@QueryIgnore(multi = true)
	private TopicModel topicModel;

	private String name;

	@Reference
	@QueryIgnore(multi = true)
	private List<Sequence> sequences;

	@Embedded
	@QueryIgnore(multi = true)
	private List<TopicWord> words;

	@Embedded
	@QueryIgnore(multi = true)
	private List<TopicShare> similarTopics;

	@QueryIgnore(multi = true)
	private Double avgRelevance;

	@QueryIgnore(multi = true)
	private Double varRelevance;

	@QueryIgnore(multi = true)
	private Double risingRelevance;

	@QueryIgnore(multi = true)
	private Double fallingRelevance;

	@QueryIgnore(multi = true)
	private Double risingDecayRelevance;

	private Integer articlesCount;

	private String color;

	@QueryIgnore(multi = true)
	private Date created;

	@QueryIgnore(multi = true)
	private Date modified;

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(final ObjectId id) {
		this.id = id;
	}

	public void setId(final String id) {
		this.id = MongoUtils.objectId(id);
	}

	public TopicModel getTopicModel() {
		return topicModel;
	}

	public void setTopicModel(final TopicModel topicModel) {
		this.topicModel = topicModel;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public List<Sequence> getSequences() {
		return sequences;
	}

	public void setSequences(final List<Sequence> sequences) {
		this.sequences = sequences;
	}

	public List<TopicWord> getWords() {
		return words;
	}

	public void setWords(final List<TopicWord> words) {
		this.words = words;
	}

	public List<TopicShare> getSimilarTopics() {
		return similarTopics;
	}

	public void setSimilarTopics(final List<TopicShare> similarTopics) {
		this.similarTopics = similarTopics;
	}

	public Double getAvgRelevance() {
		return avgRelevance;
	}

	public void setAvgRelevance(final Double avgRelevance) {
		this.avgRelevance = avgRelevance;
	}

	public Double getVarRelevance() {
		return varRelevance;
	}

	public void setVarRelevance(final Double varRelevance) {
		this.varRelevance = varRelevance;
	}

	public Double getRisingRelevance() {
		return risingRelevance;
	}

	public void setRisingRelevance(final Double risingRelevance) {
		this.risingRelevance = risingRelevance;
	}

	public Double getFallingRelevance() {
		return fallingRelevance;
	}

	public void setFallingRelevance(final Double fallingRelevance) {
		this.fallingRelevance = fallingRelevance;
	}

	public Double getRisingDecayRelevance() {
		return risingDecayRelevance;
	}

	public void setRisingDecayRelevance(final Double risingDecayRelevance) {
		this.risingDecayRelevance = risingDecayRelevance;
	}

	public Integer getArticlesCount() {
		return articlesCount;
	}

	public void setArticlesCount(final Integer articlesCount) {
		this.articlesCount = articlesCount;
	}

	public String getColor() {
		return color;
	}

	public void setColor(final String color) {
		this.color = color;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(final Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(final Date modified) {
		this.modified = modified;
	}

	@PrePersist
	public void prePersist() {
		modified = new Date();
		if (created == null)
			created = modified;
	}

	public static String getNameFromWords(final int wordsNum, final List<SequenceWord> words) {
		String name = null;
		if (words != null && words.size() > 0) {
			final int size = Math.min(wordsNum, words.size());
			final List<String> topWords = new ArrayList<>(size);
			for (int i = 0; i < size; i++) {
				topWords.add(words.get(i).getWord());
			}
			name = StringUtils.join(topWords);
		}
		return name;
	}

	@Override
	public boolean equals(final Object o) {
		if (o == null)
			return false;
		if (!(o instanceof TopicFull))
			return false;
		final TopicFull a = (TopicFull) o;
		if (id == null)
			return a.getId() == null;
		return id.equals(a.getId());
	}

	@Override
	public int hashCode() {
		if (id == null)
			return super.hashCode();
		return id.hashCode();
	}

	@Override
	public String toString() {
		return "TopicFull [id=" + id + ", model=" + topicModel + ", name=" + name + ", sequences=" + sequences + ", avgRelevance=" + avgRelevance
				+ ", varRelevance=" + varRelevance + ", risingRelevance=" + risingRelevance + ", fallingRelevance=" + fallingRelevance
				+ ", risingDecayRelevance=" + risingDecayRelevance + ", created=" + created + ", modified=" + modified + "]";
	}

}
