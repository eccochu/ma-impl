package de.vipra.util.model;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Embedded
public class ArticleStats implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long wordCount;

	private Long uniqueWordCount;

	private Long processedWordCount;

	private Double reductionRatio;

	public Long getWordCount() {
		return wordCount;
	}

	public void setWordCount(final Long wordCount) {
		this.wordCount = wordCount;
	}

	public Long getUniqueWordCount() {
		return uniqueWordCount;
	}

	public void setUniqueWordCount(final Long uniqueWordCount) {
		this.uniqueWordCount = uniqueWordCount;
	}

	public Long getProcessedWordCount() {
		return processedWordCount;
	}

	public void setProcessedWordCount(final Long processedWordCount) {
		this.processedWordCount = processedWordCount;
	}

	public Double getReductionRatio() {
		return reductionRatio;
	}

	public void setReductionRatio(final Double reductionRatio) {
		this.reductionRatio = reductionRatio;
	}

	@Override
	public String toString() {
		return "ArticleStats [wordCount=" + wordCount + ", uniqueWordCount=" + uniqueWordCount + ", processedWordCount=" + processedWordCount
				+ ", reductionRatio=" + reductionRatio + "]";
	}

}