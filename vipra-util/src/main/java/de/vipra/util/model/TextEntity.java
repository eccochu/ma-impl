package de.vipra.util.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.annotations.Embedded;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.vipra.util.StringUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
@Embedded
public class TextEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	private String entity;

	private Boolean isHypernym;

	private Boolean isWord;

	private String url;

	private List<String> types;

	public TextEntity() {}

	public TextEntity(final String entity, final String url) {
		this.entity = entity;
		this.url = url;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(final String entity) {
		this.entity = entity;
		isWord = StringUtils.isWord(entity);
	}

	public Boolean getIsHypernym() {
		return isHypernym;
	}

	public void setIsHypernym(final Boolean isHypernym) {
		this.isHypernym = isHypernym;
	}

	public Boolean getIsWord() {
		return isWord;
	}

	public void setIsWord(final Boolean isWord) {
		this.isWord = isWord;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public List<String> getTypes() {
		return types;
	}

	public void setTypes(final List<String> types) {
		this.types = types;
	}

	public String realEntity() {
		return TextEntityFull.realEntity(url);
	}

	public List<String> entityWithTypes() {
		final List<String> entityWithTypes = new ArrayList<>(types.size() + 1);
		entityWithTypes.add(entity.toLowerCase());
		for (final String type : types)
			entityWithTypes.add(type.toLowerCase());
		return entityWithTypes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entity == null) ? 0 : entity.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final TextEntity other = (TextEntity) obj;
		if (entity == null) {
			if (other.entity != null)
				return false;
		} else if (!entity.equals(other.entity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TextEntity [entity=" + entity + ", url=" + url + ", types=" + types + "]";
	}

}
