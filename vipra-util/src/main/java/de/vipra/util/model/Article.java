package de.vipra.util.model;

import java.io.Serializable;
import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(value = "articles", noClassnameStored = true)
@Indexes({ @Index("title"), @Index("-title"), @Index("topicsCount"), @Index("-topicsCount") })
public class Article implements Model<ObjectId>, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id;

	private String title;

	private Date date;

	private Integer topicsCount;

	public Article() {}

	public Article(final ObjectId id) {
		this.id = id;
	}

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(final ObjectId id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(final Date date) {
		this.date = date;
	}

	public Integer getTopicsCount() {
		return topicsCount;
	}

	public void setTopicsCount(final Integer topicsCount) {
		this.topicsCount = topicsCount;
	}

	@Override
	public boolean equals(final Object o) {
		if (o == null)
			return false;
		if (!(o instanceof Article))
			return false;
		final Article a = (Article) o;
		if (id == null)
			return a.getId() == null;
		return id.equals(a.getId());
	}

	@Override
	public int hashCode() {
		if (id == null)
			return super.hashCode();
		return id.hashCode();
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + "]";
	}

}
