package de.vipra.util.model;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Reference;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Embedded
public class SimilarArticle implements Comparable<SimilarArticle>, Serializable {

	private static final long serialVersionUID = 1L;

	@Reference(ignoreMissing = true)
	private Article article;

	private double divergence;

	public Article getArticle() {
		return article;
	}

	public void setArticle(final Article article) {
		this.article = article;
	}

	public double getDivergence() {
		return divergence;
	}

	public void setDivergence(final double divergence) {
		this.divergence = divergence;
	}

	@Override
	public int compareTo(final SimilarArticle o) {
		return Double.compare(divergence, o.getDivergence());
	}

	@Override
	public String toString() {
		return "SimilarArticle [article=" + article + ", divergence=" + divergence + "]";
	}

}
