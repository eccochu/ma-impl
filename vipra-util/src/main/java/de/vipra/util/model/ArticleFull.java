package de.vipra.util.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.vipra.util.Constants;
import de.vipra.util.MongoUtils;
import de.vipra.util.NestedMap;
import de.vipra.util.StringUtils;
import de.vipra.util.an.ElasticIndex;
import de.vipra.util.an.QueryIgnore;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(value = "articles", noClassnameStored = true)
@Indexes({ @Index("title"), @Index("-title"), @Index("date"), @Index("-date"), @Index("topicsCount"), @Index("-topicsCount"), @Index("created"),
		@Index("-created"), @Index("modified"), @Index("-modified") })
public class ArticleFull implements Model<ObjectId>, Serializable {

	private static final long serialVersionUID = 1L;

	public static final Logger log = LoggerFactory.getLogger(ArticleFull.class);

	@Id
	private ObjectId id = new ObjectId();

	@ElasticIndex("title")
	private String title;

	@QueryIgnore(multi = true)
	private String text;

	@QueryIgnore(all = true)
	private String[] processedText;

	private String url;

	@ElasticIndex("date")
	private Date date;

	@QueryIgnore(multi = true)
	@Embedded
	private Window window;

	@Reference
	@QueryIgnore(multi = true)
	private TopicModel topicModel;

	@Embedded
	@QueryIgnore(multi = true)
	private List<TopicShare> topics;

	private Integer topicsCount;

	@Embedded
	@QueryIgnore(multi = true)
	private List<SimilarArticle> similarArticles;

	@Embedded
	@QueryIgnore(all = true)
	private List<ArticleWord> words;

	@Embedded
	@QueryIgnore(all = true)
	private List<TextEntityCount> entities;

	@Embedded
	@QueryIgnore(multi = true)
	private ArticleStats stats;

	@QueryIgnore(multi = true)
	private Date created;

	@QueryIgnore(multi = true)
	private Date modified;

	@Transient
	private NestedMap meta;

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(final ObjectId id) {
		this.id = id;
	}

	public void setId(final String id) {
		this.id = MongoUtils.objectId(id);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	@ElasticIndex("excerpt")
	public String serializeExcerpt() {
		return serializeExcerpt(Constants.EXCERPT_LENGTH);
	}

	public String serializeExcerpt(final int length) {
		return StringUtils.ellipsize(text.replaceAll("<[^>]*>", ""), length);
	}

	@ElasticIndex("text")
	public String serializeText() {
		return StringUtils.join(processedText);
	}

	public String[] getProcessedText() {
		return processedText;
	}

	public void setProcessedText(final String[] processedText) {
		this.processedText = processedText;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(final Date date) {
		this.date = date;
	}

	public Window getWindow() {
		return window;
	}

	public void setWindow(final Window window) {
		this.window = window;
	}

	public void setDate(final String date) {
		final SimpleDateFormat df = new SimpleDateFormat(Constants.DATETIME_FORMAT);
		try {
			setDate(df.parse(date));
		} catch (final ParseException e) {
			log.error("could not parse date " + date);
		}
	}

	public TopicModel getTopicModel() {
		return topicModel;
	}

	public void setTopicModel(final TopicModel topicModel) {
		this.topicModel = topicModel;
	}

	public List<TopicShare> getTopics() {
		return topics;
	}

	public void setTopics(final List<TopicShare> topics) {
		this.topics = topics;
		topicsCount = topics == null ? 0 : topics.size();
	}

	public Integer getTopicsCount() {
		return topicsCount;
	}

	@ElasticIndex("topics")
	public String[] serializeTopics() {
		final List<TopicShare> refs = getTopics();
		if (refs == null)
			return new String[0];
		final List<String> topics = new ArrayList<>(refs.size());
		for (final TopicShare ref : refs) {
			final Topic topic = ref.getTopic();
			if (topic != null)
				topics.add(topic.getName());
		}
		return topics.toArray(new String[topics.size()]);
	}

	public List<SimilarArticle> getSimilarArticles() {
		return similarArticles;
	}

	public void setSimilarArticles(final List<SimilarArticle> similarArticles) {
		this.similarArticles = similarArticles;
	}

	public List<ArticleWord> getWords() {
		return words;
	}

	public void setWords(final List<ArticleWord> words) {
		this.words = words;
	}

	public List<TextEntityCount> getEntities() {
		return entities;
	}

	public void setEntities(final List<TextEntityCount> entities) {
		this.entities = entities;
	}

	public ArticleStats getStats() {
		return stats;
	}

	public void setStats(final ArticleStats stats) {
		this.stats = stats;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(final Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(final Date modified) {
		this.modified = modified;
	}

	public NestedMap getMeta() {
		return meta;
	}

	public void setMeta(final NestedMap meta) {
		this.meta = meta;
	}

	public void addMeta(final String key, final Object value) {
		if (meta == null)
			meta = new NestedMap();
		meta.put(key, value);
	}

	public String[] entities() {
		final List<String> strEntities = new ArrayList<>(entities.size());
		for (final TextEntityCount textEntity : entities)
			strEntities.add(textEntity.getEntity().getEntity());
		return strEntities.toArray(new String[strEntities.size()]);
	}

	public String[] types() {
		final List<String> strTypes = new ArrayList<>(entities.size() * 3);
		for (final TextEntityCount textEntity : entities)
			strTypes.addAll(textEntity.getEntity().getTypes());
		return strTypes.toArray(new String[strTypes.size()]);
	}

	public String[] hypernyms() {
		final List<String> strHypernyms = new ArrayList<>(entities.size());
		for (final TextEntityCount textEntity : entities)
			if (textEntity.getEntity().getIsHypernym())
				strHypernyms.add(textEntity.getEntity().getEntity());
		return strHypernyms.toArray(new String[strHypernyms.size()]);
	}

	@PrePersist
	public void prePersist() {
		modified = new Date();
		if (created == null)
			created = modified;
	}

	@Override
	public boolean equals(final Object o) {
		if (o == null)
			return false;
		if (!(o instanceof ArticleFull))
			return false;
		final ArticleFull a = (ArticleFull) o;
		if (id == null)
			return a.getId() == null;
		return id.equals(a.getId());
	}

	@Override
	public int hashCode() {
		if (id == null)
			return super.hashCode();
		return id.hashCode();
	}

	@Override
	public String toString() {
		return "ArticleFull [id=" + id + ", title=" + title + ", text=" + text + ", processedText=" + Arrays.toString(processedText) + ", url=" + url
				+ ", date=" + date + ", topics=" + topics + ", similarArticles=" + similarArticles + ", stats=" + stats + ", created=" + created
				+ ", modified=" + modified + ", meta=" + meta + "]";
	}

}