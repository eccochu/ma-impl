package de.vipra.util.model;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Embedded
public class SequenceWord implements Comparable<SequenceWord>, Serializable {

	private static final long serialVersionUID = 1L;

	private String word;

	private Double probability;

	public SequenceWord() {}

	public SequenceWord(final String word, final Double probability) {
		this.word = word;
		this.probability = probability;
	}

	public String getWord() {
		return word;
	}

	public void setWord(final String word) {
		this.word = word;
	}

	public Double getProbability() {
		return probability;
	}

	public void setProbability(final Double probability) {
		this.probability = probability;
	}

	@Override
	public int compareTo(final SequenceWord o) {
		final int c1 = Double.compare(probability, o.getProbability());
		return c1 == 0 ? word.compareTo(o.getWord()) : c1;
	}

	@Override
	public String toString() {
		return "SequenceWord [word=" + word + ", probability=" + probability + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final SequenceWord other = (SequenceWord) obj;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}

}
