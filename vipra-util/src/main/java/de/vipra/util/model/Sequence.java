package de.vipra.util.model;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(value = "sequences", noClassnameStored = true)
@Indexes({ @Index("relevance"), @Index("-relevance"), @Index("relevanceChange"), @Index("-relevanceChange") })
public class Sequence implements Model<ObjectId>, Comparable<Sequence>, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id = new ObjectId();

	@Embedded
	private Window window;

	private Double relevance;

	private Double relevanceChange;

	public Sequence() {}

	public Sequence(final ObjectId id) {
		this.id = id;
	}

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(final ObjectId id) {
		this.id = id;
	}

	public Window getWindow() {
		return window;
	}

	public void setWindow(final Window window) {
		this.window = window;
	}

	public Double getRelevance() {
		return relevance;
	}

	public void setRelevance(final Double relevance) {
		this.relevance = relevance;
	}

	public Double getRelevanceChange() {
		return relevanceChange;
	}

	public void setRelevanceChange(final Double relevanceChange) {
		this.relevanceChange = relevanceChange;
	}

	@Override
	public int compareTo(final Sequence o) {
		return window.compareTo(o.getWindow());
	}

	@Override
	public String toString() {
		return "Sequence [id=" + id + ", window=" + window + ", relevance=" + relevance + ", relevanceChange=" + relevanceChange + "]";
	}

}
