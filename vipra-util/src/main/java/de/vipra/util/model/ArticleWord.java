package de.vipra.util.model;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Embedded
public class ArticleWord implements Comparable<ArticleWord>, Serializable {

	private static final long serialVersionUID = 1L;

	private String word;

	private Integer count;

	public ArticleWord() {}

	public ArticleWord(final String word, final int count) {
		// TODO NPE
		this.word = word.toLowerCase().trim();
		this.count = count;
	}

	public String getWord() {
		return word;
	}

	public void setWord(final String word) {
		this.word = word;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(final Integer count) {
		this.count = count;
	}

	public String aTag(final String word) {
		return aTag(this.word, word);
	}

	public static String aTag(final String word, final String label) {
		return "<w=" + word + ">" + label + "</a>";
	}

	@Override
	public int compareTo(final ArticleWord o) {
		return count.compareTo(o.getCount());
	}

	@Override
	public String toString() {
		return "ArticleWord [word=" + word + ", count=" + count + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ArticleWord other = (ArticleWord) obj;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}

}
