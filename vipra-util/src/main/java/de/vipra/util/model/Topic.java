package de.vipra.util.model;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.vipra.util.MongoUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
@SuppressWarnings("serial")
@Entity(value = "topics", noClassnameStored = true)
@Indexes({ @Index("name"), @Index("-name"), @Index("articlesCount"), @Index("-articlesCount") })
public class Topic implements Model<ObjectId>, Serializable {

	@Id
	private ObjectId id;

	private String name;

	private Integer articlesCount;

	private String color;

	public Topic() {}

	public Topic(final ObjectId id) {
		this.id = id;
	}

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(final ObjectId id) {
		this.id = id;
	}

	public void setId(final String id) {
		this.id = MongoUtils.objectId(id);
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public int getArticlesCount() {
		return articlesCount;
	}

	public void setArticlesCount(final int articlesCount) {
		this.articlesCount = articlesCount;
	}

	public String getColor() {
		return color;
	}

	public void setColor(final String color) {
		this.color = color;
	}

	@Override
	public boolean equals(final Object o) {
		if (o == null)
			return false;
		if (!(o instanceof Topic))
			return false;
		final Topic a = (Topic) o;
		if (id == null)
			return a.getId() == null;
		return id.equals(a.getId());
	}

	@Override
	public int hashCode() {
		if (id == null)
			return super.hashCode();
		return id.hashCode();
	}

	@Override
	public String toString() {
		return "Topic [id=" + id + ", name=" + name + "]";
	}

}
