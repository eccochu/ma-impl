package de.vipra.util.model;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import org.apache.commons.io.FileUtils;
import org.mongodb.morphia.annotations.Embedded;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import de.vipra.util.Config;
import de.vipra.util.Constants;
import de.vipra.util.Constants.WindowResolution;

@JsonIgnoreProperties(ignoreUnknown = true)
@Embedded
public class TopicModelConfig implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String FILE_NAME = "config.json";

	private String name;
	private String group;
	private String description;
	private int kTopics = Constants.K_TOPICS;
	private int kTopWords = Constants.K_TOP_WORDS;
	private int dynamicMinIterations = Constants.DYNAMIC_MIN_ITER;
	private int dynamicMaxIterations = Constants.DYNAMIC_MAX_ITER;
	private int staticIterations = Constants.STATIC_ITER;
	private int topicAutoNamingWords = Constants.TOPIC_AUTO_NAMING_WORDS;
	private int maxSimilarDocuments = Constants.MAX_SIMILAR_DOCUMENTS;
	private int maxSimilarTopics = Constants.MAX_SIMILAR_TOPICS;
	private int documentMinimumLength = Constants.DOCUMENT_MIN_LENGTH;
	private int documentMinimumWordFrequency = Constants.DOCUMENT_MIN_WORD_FREQ;
	private int spotlightSupport = Constants.SPOTLIGHT_SUPPORT;
	private double spotlightConfidence = Constants.SPOTLIGHT_CONFIDENCE;
	private double minTopicShare = Constants.MIN_TOPIC_SHARE;
	private double minRelativeProbability = Constants.MIN_RELATIVE_PROB;
	private double risingDecayLambda = Constants.RISING_DECAY_LAMBDA;
	private double maxSimilarDocumentsDivergence = Constants.MAX_SIMILAR_DOCUMENTS_DIVERGENCE;
	private double maxSimilarTopicsDivergence = Constants.MAX_SIMILAR_TOPICS_DIVERGENCE;
	private boolean processorUseText = Constants.PROCESSOR_USE_TEXT;
	private boolean processorUseEntities = Constants.PROCESSOR_USE_ENTITIES;
	private boolean processorUseEntityTypes = Constants.PROCESSOR_USE_ENTITY_TYPES;
	private boolean processorUseHypernyms = Constants.PROCESSOR_USE_HYPERNYMS;
	private boolean queryEntityDescriptions = Constants.QUERY_ENTITY_DESCRIPTIONS;
	private WindowResolution windowResolution = Constants.WINDOW_RESOLUTION;

	public TopicModelConfig() {}

	public TopicModelConfig(final TopicModelConfig topicModelConfig) {
		kTopics = topicModelConfig.getkTopics();
		kTopWords = topicModelConfig.getkTopWords();
		dynamicMinIterations = topicModelConfig.getDynamicMinIterations();
		dynamicMaxIterations = topicModelConfig.getDynamicMaxIterations();
		staticIterations = topicModelConfig.getStaticIterations();
		topicAutoNamingWords = topicModelConfig.getTopicAutoNamingWords();
		maxSimilarDocuments = topicModelConfig.getMaxSimilarDocuments();
		maxSimilarTopics = topicModelConfig.getMaxSimilarTopics();
		documentMinimumLength = topicModelConfig.getDocumentMinimumLength();
		documentMinimumWordFrequency = topicModelConfig.getDocumentMinimumWordFrequency();
		spotlightSupport = topicModelConfig.getSpotlightSupport();
		spotlightConfidence = topicModelConfig.getSpotlightConfidence();
		minTopicShare = topicModelConfig.getMinTopicShare();
		minRelativeProbability = topicModelConfig.getMinRelativeProbability();
		risingDecayLambda = topicModelConfig.getRisingDecayLambda();
		maxSimilarDocumentsDivergence = topicModelConfig.getMaxSimilarDocumentsDivergence();
		maxSimilarTopicsDivergence = topicModelConfig.getMaxSimilarTopicsDivergence();
		processorUseText = topicModelConfig.isProcessorUseText();
		processorUseEntities = topicModelConfig.isProcessorUseEntities();
		processorUseEntityTypes = topicModelConfig.isProcessorUseEntityTypes();
		processorUseHypernyms = topicModelConfig.isProcessorUseHypernyms();
		queryEntityDescriptions = topicModelConfig.isQueryEntityDescriptions();
		windowResolution = topicModelConfig.getWindowResolution();
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(final String group) {
		this.group = group;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public int getkTopics() {
		return kTopics;
	}

	public void setkTopics(final int kTopics) {
		this.kTopics = kTopics;
	}

	public int getkTopWords() {
		return kTopWords;
	}

	public void setkTopWords(final int kTopWords) {
		this.kTopWords = kTopWords;
	}

	public int getDynamicMinIterations() {
		return dynamicMinIterations;
	}

	public void setDynamicMinIterations(final int dynamicMinIterations) {
		this.dynamicMinIterations = dynamicMinIterations;
	}

	public int getDynamicMaxIterations() {
		return dynamicMaxIterations;
	}

	public void setDynamicMaxIterations(final int dynamicMaxIterations) {
		this.dynamicMaxIterations = dynamicMaxIterations;
	}

	public int getStaticIterations() {
		return staticIterations;
	}

	public void setStaticIterations(final int staticIterations) {
		this.staticIterations = staticIterations;
	}

	public int getTopicAutoNamingWords() {
		return topicAutoNamingWords;
	}

	public void setTopicAutoNamingWords(final int topicAutoNamingWords) {
		this.topicAutoNamingWords = topicAutoNamingWords;
	}

	public int getDocumentMinimumLength() {
		return documentMinimumLength;
	}

	public void setDocumentMinimumLength(final int documentMinimumLength) {
		this.documentMinimumLength = documentMinimumLength;
	}

	public int getDocumentMinimumWordFrequency() {
		return documentMinimumWordFrequency;
	}

	public void setDocumentMinimumWordFrequency(final int documentMinimumWordFrequency) {
		this.documentMinimumWordFrequency = documentMinimumWordFrequency;
	}

	public int getMaxSimilarDocuments() {
		return maxSimilarDocuments;
	}

	public void setMaxSimilarDocuments(final int maxSimilarDocuments) {
		this.maxSimilarDocuments = maxSimilarDocuments;
	}

	public int getMaxSimilarTopics() {
		return maxSimilarTopics;
	}

	public void setMaxSimilarTopics(final int maxSimilarTopics) {
		this.maxSimilarTopics = maxSimilarTopics;
	}

	public int getSpotlightSupport() {
		return spotlightSupport;
	}

	public void setSpotlightSupport(final int spotlightSupport) {
		this.spotlightSupport = spotlightSupport;
	}

	public double getSpotlightConfidence() {
		return spotlightConfidence;
	}

	public void setSpotlightConfidence(final double spotlightConfidence) {
		this.spotlightConfidence = spotlightConfidence;
	}

	public double getMinTopicShare() {
		return minTopicShare;
	}

	public void setMinTopicShare(final double minTopicShare) {
		this.minTopicShare = minTopicShare;
	}

	public double getMinRelativeProbability() {
		return minRelativeProbability;
	}

	public void setMinRelativeProbability(final double minRelativeProbability) {
		this.minRelativeProbability = minRelativeProbability;
	}

	public double getRisingDecayLambda() {
		return risingDecayLambda;
	}

	public void setRisingDecayLambda(final double risingDecayLambda) {
		this.risingDecayLambda = risingDecayLambda;
	}

	public double getMaxSimilarDocumentsDivergence() {
		return maxSimilarDocumentsDivergence;
	}

	public void setMaxSimilarDocumentsDivergence(final double maxSimilarDocumentsDivergence) {
		this.maxSimilarDocumentsDivergence = maxSimilarDocumentsDivergence;
	}

	public double getMaxSimilarTopicsDivergence() {
		return maxSimilarTopicsDivergence;
	}

	public void setMaxSimilarTopicsDivergence(final double maxSimilarTopicsDivergence) {
		this.maxSimilarTopicsDivergence = maxSimilarTopicsDivergence;
	}

	public boolean isProcessorUseText() {
		return processorUseText;
	}

	public void setProcessorUseText(final boolean processorUseText) {
		this.processorUseText = processorUseText;
	}

	public boolean isProcessorUseEntities() {
		return processorUseEntities;
	}

	public void setProcessorUseEntities(final boolean processorUseEntities) {
		this.processorUseEntities = processorUseEntities;
	}

	public boolean isProcessorUseEntityTypes() {
		return processorUseEntityTypes;
	}

	public void setProcessorUseEntityTypes(final boolean processorUseEntityTypes) {
		this.processorUseEntityTypes = processorUseEntityTypes;
	}

	public boolean isProcessorUseHypernyms() {
		return processorUseHypernyms;
	}

	public void setProcessorUseHypernyms(final boolean processorUseHypernyms) {
		this.processorUseHypernyms = processorUseHypernyms;
	}

	public boolean isQueryEntityDescriptions() {
		return queryEntityDescriptions;
	}

	public void setQueryEntityDescriptions(final boolean queryEntityDescriptions) {
		this.queryEntityDescriptions = queryEntityDescriptions;
	}

	public WindowResolution getWindowResolution() {
		return windowResolution;
	}

	public void setWindowResolution(final WindowResolution windowResolution) {
		this.windowResolution = windowResolution;
	}

	public File getModelDir(final File dataDir) {
		return new File(dataDir, name);
	}

	public void saveToFile(final File modelDir) throws JsonGenerationException, JsonMappingException, IOException {
		final File file = new File(modelDir, FILE_NAME);
		final String value = Config.mapper.writeValueAsString(this);
		FileUtils.write(file, value, Constants.FILEBASE_ENCODING, false);
	}

	public static TopicModelConfig readFromFile(final File modelDir) throws JsonParseException, JsonMappingException, IOException {
		final File file = new File(modelDir, FILE_NAME);
		if (file.exists())
			return Config.mapper.readValue(file, TopicModelConfig.class);
		return new TopicModelConfig();
	}

	@Override
	public String toString() {
		return "[window=" + windowResolution + ", k=" + kTopics + ", iter=" + staticIterations + "/" + dynamicMinIterations + "-"
				+ dynamicMaxIterations + "] " + (group != null ? group : "");
	}

	public String toPrettyString() {
		return " kTopics: " + kTopics + "\n kTopWords: " + kTopWords + "\n dynamicMinIterations: " + dynamicMinIterations
				+ "\n dynamicMaxIterations: " + dynamicMaxIterations + "\n staticIterations: " + staticIterations + "\n topicAutoNamingWords: "
				+ topicAutoNamingWords + "\n maxSimilarDocuments: " + maxSimilarDocuments + "\n documentMinimumLength: " + documentMinimumLength
				+ "\n documentMinimumWordFrequency: " + documentMinimumWordFrequency + "\n spotlightSupport: " + spotlightSupport
				+ "\n spotlightConfidence: " + spotlightConfidence + "\n minTopicShare: " + minTopicShare + "\n minRelativeProbability: "
				+ minRelativeProbability + "\n risingDecayLambda: " + risingDecayLambda + "\n maxSimilarDocumentsDivergence: "
				+ maxSimilarDocumentsDivergence + "\n maxSimilarTopicsDivergence: " + maxSimilarTopicsDivergence + "\n processor use text: "
				+ processorUseText + "\n processor use entities: " + processorUseEntities + "\n processor use entity types: "
				+ processorUseEntityTypes + "\n processor use hypernyms: " + processorUseHypernyms + "\n query entity descriptions: "
				+ queryEntityDescriptions + "\n windowResolution: " + windowResolution;
	}

}
