package de.vipra.util.model;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Embedded
public class TextEntityCount implements Comparable<TextEntityCount>, Serializable {

	private static final long serialVersionUID = 1L;

	@Embedded
	private TextEntity entity;

	private Integer count;

	public TextEntity getEntity() {
		return entity;
	}

	public void setEntity(final TextEntity entity) {
		this.entity = entity;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(final Integer count) {
		this.count = count;
	}

	@Override
	public int compareTo(final TextEntityCount o) {
		return count.compareTo(o.getCount());
	}

}
