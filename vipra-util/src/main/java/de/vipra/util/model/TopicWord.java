package de.vipra.util.model;

import java.io.Serializable;
import java.util.List;

import org.mongodb.morphia.annotations.Embedded;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Embedded
public class TopicWord implements Comparable<TopicWord>, Serializable {

	private static final long serialVersionUID = 1L;

	private String word;

	private List<Double> sequenceProbabilities;

	private List<Double> sequenceProbabilitiesChange;

	public String getWord() {
		return word;
	}

	public void setWord(final String word) {
		this.word = word;
	}

	public List<Double> getSequenceProbabilities() {
		return sequenceProbabilities;
	}

	public void setSequenceProbabilities(final List<Double> sequenceProbabilities) {
		this.sequenceProbabilities = sequenceProbabilities;
	}

	public List<Double> getSequenceProbabilitiesChange() {
		return sequenceProbabilitiesChange;
	}

	public void setSequenceProbabilitiesChange(final List<Double> sequenceProbabilitiesChange) {
		this.sequenceProbabilitiesChange = sequenceProbabilitiesChange;
	}

	@Override
	public int compareTo(final TopicWord o) {
		return sequenceProbabilities.get(0).compareTo(o.getSequenceProbabilities().get(0));
	}

}
