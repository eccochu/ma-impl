package de.vipra.util.model;

import java.io.Serializable;
import java.util.Date;

import org.mongodb.morphia.annotations.Embedded;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.vipra.util.Constants.WindowResolution;

@JsonIgnoreProperties(ignoreUnknown = true)
@Embedded
public class Window implements Serializable, Comparable<Window> {

	private static final long serialVersionUID = 1L;

	private Date startDate;

	private Date endDate;

	private WindowResolution windowResolution;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(final Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(final Date endDate) {
		this.endDate = endDate;
	}

	public WindowResolution getWindowResolution() {
		return windowResolution;
	}

	public void setWindowResolution(final WindowResolution windowResolution) {
		this.windowResolution = windowResolution;
	}

	@Override
	public int compareTo(final Window o) {
		return startDate.compareTo(o.getStartDate());
	}

}
