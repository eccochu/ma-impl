package de.vipra.util.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Reference;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.vipra.util.an.QueryIgnore;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(value = "sequences", noClassnameStored = true)
@Indexes({ @Index("relevance"), @Index("-relevance"), @Index("relevanceChange"), @Index("-relevanceChange"), @Index("created"), @Index("-created"),
		@Index("modified"), @Index("-modified") })
public class SequenceFull implements Model<ObjectId>, Comparable<SequenceFull>, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id = new ObjectId();

	@Reference
	@QueryIgnore(multi = true)
	private TopicModel topicModel;

	@Embedded
	private Window window;

	private Double relevance;

	private Double relevanceChange;

	@Reference
	@QueryIgnore(multi = true)
	private Topic topic;

	@Embedded
	@QueryIgnore(multi = true)
	private List<SequenceWord> words;

	@QueryIgnore(multi = true)
	private Date created;

	@QueryIgnore(multi = true)
	private Date modified;

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(final ObjectId id) {
		this.id = id;
	}

	public TopicModel getTopicModel() {
		return topicModel;
	}

	public void setTopicModel(final TopicModel topicModel) {
		this.topicModel = topicModel;
	}

	public Window getWindow() {
		return window;
	}

	public void setWindow(final Window window) {
		this.window = window;
	}

	public Double getRelevance() {
		return relevance;
	}

	public void setRelevance(final Double relevance) {
		this.relevance = relevance;
	}

	public Double getRelevanceChange() {
		return relevanceChange;
	}

	public void setRelevanceChange(final Double relevanceChange) {
		this.relevanceChange = relevanceChange;
	}

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(final Topic topic) {
		this.topic = topic;
	}

	public List<SequenceWord> getWords() {
		return words;
	}

	public void setWords(final List<SequenceWord> words) {
		this.words = words;
	}

	@Override
	public int compareTo(final SequenceFull o) {
		return window.compareTo(o.getWindow());
	}

	@Override
	public String toString() {
		return "SequenceFull [id=" + id + ", window=" + window + ", relevance=" + relevance + ", relevanceChange=" + relevanceChange + ", topic="
				+ topic + ", words=" + words + "]";
	}

	@PrePersist
	public void prePersist() {
		modified = new Date();
		if (created == null)
			created = modified;
	}

}
