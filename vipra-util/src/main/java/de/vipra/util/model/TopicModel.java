package de.vipra.util.model;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(value = "topicmodels", noClassnameStored = true)
public class TopicModel implements Model<ObjectId>, Comparable<TopicModel>, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id;

	public TopicModel() {}

	public TopicModel(final ObjectId id) {
		this.id = id;
	}

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(final ObjectId id) {
		this.id = id;
	}

	@Override
	public int compareTo(final TopicModel o) {
		return id.compareTo(o.getId());
	};

}
