package de.vipra.util.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.vipra.util.an.QueryIgnore;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(value = "topicmodels", noClassnameStored = true)
public class TopicModelFull implements Model<ObjectId>, Comparable<TopicModelFull>, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id = new ObjectId();

	private String name;

	private String group;

	private Long topicCount;

	private Long articleCount;

	private Long wordCount;

	private Long entityCount;

	private Long windowCount;

	private Date lastGenerated;

	private Date lastIndexed;

	private Long colorSeed;

	@Embedded
	@QueryIgnore(multi = true)
	private List<Window> windows;

	@Embedded
	@QueryIgnore(multi = true)
	private TopicModelConfig modelConfig;

	@QueryIgnore(multi = true)
	private Date created;

	@QueryIgnore(multi = true)
	private Date modified;

	public TopicModelFull() {}

	public TopicModelFull(final String name) {
		this.name = name;
	}

	public TopicModelFull(final String name, final TopicModelConfig modelConfig) {
		setName(name);
		this.modelConfig = modelConfig;
	}

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(final ObjectId id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		if (name != null && name.contains(":")) {
			final String[] parts = name.split(":");
			group = parts[0];
			this.name = parts[1];
		} else
			this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(final String group) {
		this.group = group;
	}

	public Long getTopicCount() {
		return topicCount;
	}

	public void setTopicCount(final Long topicCount) {
		this.topicCount = topicCount;
	}

	public Long getArticleCount() {
		return articleCount;
	}

	public void setArticleCount(final Long articleCount) {
		this.articleCount = articleCount;
	}

	public Long getWordCount() {
		return wordCount;
	}

	public void setWordCount(final Long wordCount) {
		this.wordCount = wordCount;
	}

	public Long getEntityCount() {
		return entityCount;
	}

	public void setEntityCount(final Long entityCount) {
		this.entityCount = entityCount;
	}

	public Long getWindowCount() {
		return windowCount;
	}

	public void setWindowCount(final Long windowCount) {
		this.windowCount = windowCount;
	}

	public TopicModelConfig getModelConfig() {
		return modelConfig;
	}

	public void setModelConfig(final TopicModelConfig modelConfig) {
		this.modelConfig = modelConfig;
	}

	public Date getLastGenerated() {
		return lastGenerated;
	}

	public void setLastGenerated(final Date lastGenerated) {
		this.lastGenerated = lastGenerated;
	}

	public Date getLastIndexed() {
		return lastIndexed;
	}

	public void setLastIndexed(final Date lastIndexed) {
		this.lastIndexed = lastIndexed;
	}

	public Long getColorSeed() {
		return colorSeed;
	}

	public void setColorSeed(final Long colorSeed) {
		this.colorSeed = colorSeed;
	}

	public List<Window> getWindows() {
		return windows;
	}

	public void setWindows(final List<Window> windows) {
		this.windows = windows;
		windowCount = (long) (windows != null ? windows.size() : 0);
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(final Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(final Date modified) {
		this.modified = modified;
	}

	@Override
	public int compareTo(final TopicModelFull o) {
		return id.compareTo(o.getId());
	}

	@PrePersist
	public void prePersist() {
		modified = new Date();
		if (created == null)
			created = modified;
	}

}
