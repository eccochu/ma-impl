package de.vipra.util.model;

public enum TopicModelState {
	NONE,
	MODELING,
	INDEXING
}
