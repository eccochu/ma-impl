package de.vipra.util.model;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Reference;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Embedded
public class TopicShare implements Comparable<TopicShare>, Serializable {

	private static final long serialVersionUID = 1L;

	@Reference(ignoreMissing = true)
	private Topic topic;

	private Double share;

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(final Topic topic) {
		this.topic = topic;
	}

	public Double getShare() {
		return share;
	}

	public void setShare(final Double share) {
		this.share = share;
	}

	@Override
	public int compareTo(final TopicShare arg0) {
		return (int) (share - arg0.getShare());
	}

	@Override
	public String toString() {
		return "TopicRef [topic=" + topic + ", share=" + share + "]";
	}

}
