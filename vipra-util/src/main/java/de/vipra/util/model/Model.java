package de.vipra.util.model;

import java.io.Serializable;

public interface Model<IdType> extends Serializable {

	IdType getId();

	void setId(IdType id);

}