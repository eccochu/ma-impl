package de.vipra.util.model;

import java.io.Serializable;
import java.util.Date;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.annotations.Reference;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.vipra.util.Constants.WindowResolution;
import de.vipra.util.an.QueryIgnore;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(value = "windows", noClassnameStored = true)
@Indexes({ @Index("name"), @Index("-name") })
public class WindowFull implements Model<String>, Serializable, Comparable<WindowFull> {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private String name;

	private Date startDate;

	private Date endDate;

	private WindowResolution windowResolution;

	@QueryIgnore(multi = true)
	@Reference
	private TopicModel topicModel;

	public WindowFull() {}

	public WindowFull(final Window window, final TopicModel topicModel) {
		name = Long.toString(window.getStartDate().getTime());
		startDate = window.getStartDate();
		endDate = window.getEndDate();
		windowResolution = window.getWindowResolution();
		this.topicModel = topicModel;
		id = name + "-" + topicModel.getId();
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(final Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(final Date endDate) {
		this.endDate = endDate;
	}

	public WindowResolution getWindowResolution() {
		return windowResolution;
	}

	public void setWindowResolution(final WindowResolution windowResolution) {
		this.windowResolution = windowResolution;
	}

	public TopicModel getTopicModel() {
		return topicModel;
	}

	public void setTopicModel(final TopicModel topicModel) {
		this.topicModel = topicModel;
	}

	@Override
	public int compareTo(final WindowFull o) {
		return startDate.compareTo(o.getStartDate());
	}
}
