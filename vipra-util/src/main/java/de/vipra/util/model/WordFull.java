package de.vipra.util.model;

import java.io.Serializable;
import java.util.Date;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Reference;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.vipra.util.an.QueryIgnore;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(value = "words", noClassnameStored = true)
@Indexes({ @Index("word"), @Index("-word") })
public class WordFull implements Model<String>, Comparable<WordFull>, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private String word;

	@QueryIgnore(multi = true)
	private Boolean isEntity;

	@Reference
	@QueryIgnore(multi = true)
	private TopicModel topicModel;

	@QueryIgnore(multi = true)
	private Date created;

	@QueryIgnore(multi = true)
	private Date modified;

	public WordFull() {}

	public WordFull(final String word, final TopicModel topicModel) {
		this.word = word.toLowerCase().trim();
		this.topicModel = topicModel;
		id = this.word + "-" + topicModel.getId();
		isEntity = false;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(final String id) {
		this.id = id;
	}

	public String getWord() {
		return word;
	}

	public void setWord(final String word) {
		this.word = word;
	}

	public Boolean getIsEntity() {
		return isEntity;
	}

	public void setIsEntity(final Boolean isEntity) {
		this.isEntity = isEntity;
	}

	public TopicModel getTopicModel() {
		return topicModel;
	}

	public void setTopicModel(final TopicModel topicModel) {
		this.topicModel = topicModel;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(final Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(final Date modified) {
		this.modified = modified;
	}

	@Override
	public int compareTo(final WordFull o) {
		return word.compareTo(o.getWord());
	}

	@PrePersist
	public void prePersist() {
		modified = new Date();
		if (created == null)
			created = modified;
	}

}
