package de.vipra.util.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Reference;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.vipra.util.Constants;
import de.vipra.util.StringUtils;
import de.vipra.util.an.QueryIgnore;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(value = "textentities", noClassnameStored = true)
@Indexes({ @Index("url"), @Index("-url"), @Index("created"), @Index("-created"), @Index("modified"), @Index("-modified"), @Index("entity"),
		@Index("-entity") })
public class TextEntityFull implements Model<String>, Serializable, Comparable<TextEntityFull> {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private String entity;

	@QueryIgnore(multi = true)
	private String description;

	@QueryIgnore(multi = true)
	private Boolean isHypernym;

	@QueryIgnore(multi = true)
	private Boolean isWord;

	@Reference
	@QueryIgnore(multi = true)
	private TopicModel topicModel;

	private String url;

	@QueryIgnore(multi = true)
	private List<String> types;

	@QueryIgnore(multi = true)
	private Date created;

	@QueryIgnore(multi = true)
	private Date modified;

	public TextEntityFull() {}

	public TextEntityFull(final TextEntity textEntity, final TopicModel topicModel) {
		entity = realEntity(textEntity.getUrl());
		url = textEntity.getUrl();
		types = textEntity.getTypes();
		isHypernym = textEntity.getIsHypernym();
		isWord = StringUtils.isWord(entity);
		this.topicModel = topicModel;
		id = entity.toLowerCase() + "-" + topicModel.getId();
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(final String id) {
		this.id = id;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(final String entity) {
		this.entity = entity;
		isWord = StringUtils.isWord(entity);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public Boolean getIsHypernym() {
		return isHypernym;
	}

	public void setIsHypernym(final Boolean isHypernym) {
		this.isHypernym = isHypernym;
	}

	public Boolean getIsWord() {
		return isWord;
	}

	public void setIsWord(final Boolean isWord) {
		this.isWord = isWord;
	}

	public TopicModel getTopicModel() {
		return topicModel;
	}

	public void setTopicModel(final TopicModel topicModel) {
		this.topicModel = topicModel;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public List<String> getTypes() {
		return types;
	}

	public void setTypes(final List<String> types) {
		this.types = types;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(final Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(final Date modified) {
		this.modified = modified;
	}

	public String aTag() {
		return aTag(entity);
	}

	public String aTag(final String entity) {
		return "<e=" + this.entity + ">" + entity + "</a>";
	}

	public String realEntity() {
		return realEntity(url);
	}

	public static String realEntity(final String url) {
		final Pattern p = Pattern.compile("resource/\\.*(.*)");
		final Matcher m = p.matcher(url);
		if (!m.find())
			return null;
		return m.group(1).replaceAll(Constants.REGEX_ENTITY_DISALLOWED, "_").replaceAll("_+", "_").replaceAll("^_+|_+$", "").trim();
	}

	@Override
	public int compareTo(final TextEntityFull arg0) {
		return entity.compareTo(arg0.getEntity());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entity == null) ? 0 : entity.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final TextEntityFull other = (TextEntityFull) obj;
		if (entity == null) {
			if (other.entity != null)
				return false;
		} else if (!entity.equals(other.entity))
			return false;
		return true;
	}

	@PrePersist
	public void prePersist() {
		if (isHypernym == null)
			isHypernym = false;
		if (isWord == null)
			isWord = false;
		modified = new Date();
		if (created == null)
			created = modified;
	}

}
