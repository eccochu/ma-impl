package de.vipra.util;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.logging.MorphiaLoggerFactory;
import org.mongodb.morphia.logging.slf4j.SLF4JLoggerImplFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;

import de.vipra.util.ex.ConfigException;

public class Mongo {

	public static final Logger log = LoggerFactory.getLogger(Mongo.class);

	static {
		MorphiaLoggerFactory.registerLogger(SLF4JLoggerImplFactory.class);
	}

	private static Mongo instance;

	private final MongoClient client;
	private final Morphia morphia;
	private final Datastore datastore;

	private Mongo(final Config config) throws ConfigException {
		final String host = config.getDatabaseHost();
		final Integer port = config.getDatabasePort();
		final String databaseName = config.getDatabaseName();

		if (host == null || port == null || databaseName == null) {
			log.error("host/port/dbname missing in configuration");
			throw new ConfigException("host/port/dbname missing in configuration");
		}

		final MongoClientOptions options = MongoClientOptions.builder().connectTimeout(10000).build();

		client = new MongoClient(host + ":" + port, options);

		morphia = new Morphia();
		datastore = morphia.createDatastore(client, databaseName);

		morphia.mapPackage("de.vipra.util.model");
		datastore.ensureIndexes();
		datastore.ensureCaps();
	}

	public MongoClient getClient() {
		return client;
	}

	public Morphia getMorphia() {
		return morphia;
	}

	public Datastore getDatastore() {
		return datastore;
	}

	public static Mongo getInstance(final Config config) throws ConfigException {
		if (instance == null) {
			instance = new Mongo(config);
		}
		return instance;
	}

}
