package de.vipra.util;

import java.util.Calendar;

/**
 * Calendar utils to be used with java default calendar implementation.
 */
public class CalendarUtils {

	public static final int getSemester(final Calendar c) {
		return c.get(Calendar.MONTH) < 6 ? 0 : 6;
	}

	public static final int getTrimester(final Calendar c) {
		final int month = c.get(Calendar.MONTH);
		if (month < 4)
			return 0;
		if (month < 8)
			return 4;
		return 8;
	}

	/**
	 * Returns the quarter of the passed calendar. Months are turned into
	 * quarters of 4: Jan-Mar: 0, Apr-Jun: 1, Jul-Sep: 2, Oct:Dec: 3.
	 *
	 * @param c
	 *            the calendar to be used
	 * @return the quarter of the calendar month
	 */
	public static final int getQuarter(final Calendar c) {
		final int month = c.get(Calendar.MONTH);
		if (month < 3)
			return 0;
		if (month < 6)
			return 3;
		if (month < 9)
			return 6;
		return 9;
	}

	/**
	 * Returns the start month of the quarter of a calendar date. Ranges from
	 * 0-3, because Java is weird.
	 *
	 * @param c
	 *            the calendar of which to return the start month of its quarter
	 * @return the start month of the quarter of the calendar
	 */
	public static final int getQuarterStart(final Calendar c) {
		switch (c.get(Calendar.MONTH)) {
			case 0:
			case 1:
			case 2:
				return 0;
			case 3:
			case 4:
			case 5:
				return 3;
			case 6:
			case 7:
			case 8:
				return 6;
			case 9:
			case 10:
			case 11:
				return 9;
		}
		return 0;
	}

	/**
	 * Returns the end month of the quarter of a calendar date. Ranges from 0-3,
	 * because Java is weird.
	 *
	 * @param c
	 *            the calendar of which to return the end month of its quarter
	 * @return the end month of the quarter of the calendar
	 */
	public static final int getQuarterEnd(final Calendar c) {
		switch (c.get(Calendar.MONTH)) {
			case 0:
			case 1:
			case 2:
				return 2;
			case 3:
			case 4:
			case 5:
				return 5;
			case 6:
			case 7:
			case 8:
				return 8;
			case 9:
			case 10:
			case 11:
				return 11;
		}
		return 0;
	}

}
