package de.vipra.util.an;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The QueryIgnore annotation allows ignoring annotated fields on single, multi,
 * and all resource requests. Example: ignore large fields on multi query to
 * reduce payload size.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface QueryIgnore {

	public boolean single() default false;

	public boolean multi() default false;

	public boolean all() default false;

}
