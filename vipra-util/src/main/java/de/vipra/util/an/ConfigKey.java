package de.vipra.util.an;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The ConfigKey field annotates configuration values. The key itself is the
 * properties file key under which the value will be stored in the configuration
 * file.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ConfigKey {

	public String value() default "";

}
