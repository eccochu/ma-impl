package de.vipra.util.an;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The UpdateIgnore annotation marks fields to be ignored when doing a partial
 * entity update. Annotated fields will not be included in the update. Fields
 * without this annotation will be used for updating, fields with null values
 * will unset database fields.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface UpdateIgnore {

}
