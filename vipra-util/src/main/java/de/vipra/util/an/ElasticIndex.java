package de.vipra.util.an;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The ElasticIndex annotation marks fields for use when creating and updating
 * ElasticSearch indexes.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface ElasticIndex {

	public String value() default "";

}
