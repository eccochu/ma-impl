package de.vipra.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class CountMap<T> {

	private final Map<T, Integer> map;

	public CountMap() {
		this.map = new HashMap<>();
	}

	public CountMap(final Map<T, Integer> map) {
		this.map = new HashMap<>(map);
	}

	public CountMap(final int size) {
		this.map = new HashMap<>(size);
	}

	public void count(final T t) {
		count(t, 1);
	}

	public void count(final T t, final int add) {
		final Integer count = map.get(t);
		if (count == null)
			map.put(t, add);
		else
			map.put(t, count + add);
	}

	public Set<Entry<T, Integer>> entrySet() {
		return map.entrySet();
	}

	public int size() {
		return map.size();
	}

	public Integer get(final T key) {
		return map.get(key);
	}

	public Set<T> keySet() {
		return map.keySet();
	}

	public boolean contains(final Object key) {
		return map.containsKey(key);
	}

	public void clear() {
		map.clear();
	}

}
