package de.vipra.util;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {

	public static String[] toStringArray(final Object[] array) {
		final String[] strings = new String[array.length];
		for (int i = 0; i < array.length; i++) {
			strings[i] = array[i].toString();
		}
		return strings;
	}

	public static <T> List<T> toList(final Iterable<T> it) {
		final List<T> list = new ArrayList<>();
		for (final T t : it)
			list.add(t);
		return list;
	}

}
