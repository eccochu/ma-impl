package de.vipra.util;

import java.io.File;
import java.io.IOException;

import de.vipra.util.ex.LockFileException;

public class LockFile {

	private final File file = FileUtils.getTempFile("vipra.lock");

	public void lock() throws LockFileException {
		if (isLocked())
			throw new LockFileException();
		try {
			file.createNewFile();
		} catch (final IOException e) {
			throw new LockFileException(e);
		}
	}

	public void unlock() throws LockFileException {
		try {
			file.delete();
		} catch (final SecurityException e) {
			throw new LockFileException(e);
		}
	}

	public boolean isLocked() {
		return file.exists();
	}

	public String getPath() {
		return file.getAbsolutePath();
	}

}
