package de.vipra.util.ex;

@SuppressWarnings("serial")
public class LockFileException extends Exception {

	public LockFileException() {}

	public LockFileException(final String msg) {
		super(msg);
	}

	public LockFileException(final Throwable t) {
		super(t);
	}

}
