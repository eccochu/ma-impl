package de.vipra.util.ex;

public class ConfigException extends Exception {

	private static final long serialVersionUID = 1L;

	public ConfigException(final String string) {
		super(string);
	}

	public ConfigException(final Throwable t) {
		super(t);
	}

}