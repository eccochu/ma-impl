package de.vipra.util.ex;

public class DatabaseException extends Exception {

	private static final long serialVersionUID = 1L;

	public DatabaseException(final String msg) {
		super(msg);
	}

	public DatabaseException(final Exception e) {
		super(e);
	}

}
