package de.vipra.util;

import java.util.Collection;
import java.util.List;

public class MathUtils {

	public static Double mean(final Collection<Long> numbers) {
		double sum = 0;
		for (final Long l : numbers)
			sum += l;
		return sum / numbers.size();
	}

	public static Double median(final List<Long> numbers) {
		final int size = numbers.size();
		final int middle = size / 2;
		if (size % 2 == 1) {
			return (double) numbers.get(middle);
		} else {
			return (numbers.get(middle - 1) + numbers.get(middle)) / 2.0;
		}
	}

	public static long mode(final List<Long> numbers) {
		long maxValue = 0;
		long maxCount = 0;

		for (int i = 0; i < numbers.size(); ++i) {
			int count = 0;
			for (int j = 0; j < numbers.size(); ++j) {
				if (numbers.get(j) == numbers.get(i))
					++count;
			}
			if (count > maxCount) {
				maxCount = count;
				maxValue = numbers.get(i);
			}
		}

		return maxValue;
	}

}
