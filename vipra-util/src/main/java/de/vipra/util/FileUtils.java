package de.vipra.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

public class FileUtils extends org.apache.commons.io.FileUtils {

	public static final boolean isJAR;

	static {
		final String classResource = FileUtils.class.getResource("FileUtils.class").toString();
		isJAR = classResource.startsWith("jar:");
	}

	/**
	 * If this method is run from within a runnable jar, returns the runnable
	 * jar file, else null
	 *
	 * @return jar file or null
	 */
	public static File getJAR() {
		if (isJAR)
			try {
				return new File(FileUtils.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
			} catch (final URISyntaxException e) {
				e.printStackTrace();
			}
		return null;
	}

	/**
	 * returns a File object, relative to this class. This is execution
	 * sensitive. If this method is called from within a runnable jar file, the
	 * file path is relative to the jar file.
	 *
	 * @param relPath
	 *            the relative path to the file
	 * @return found file or null
	 */
	public static File getFile(final String relPath) {
		try {
			final File thisFile = new File(FileUtils.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
			return new File(thisFile.getParent(), relPath);
		} catch (final URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Reads a file and returns a list of lines
	 *
	 * @param file
	 *            the file to be read
	 * @return list of lines
	 * @throws IOException
	 */
	public static List<String> readFile(final File file) throws IOException {
		return Files.readAllLines(Paths.get(file.getAbsolutePath()), Constants.FILEBASE_ENCODING);
	}

	/**
	 * Returns a resource file. Resource files are stored in various locations,
	 * depending on execution schema (direct execution, runnable jar etc.)
	 *
	 * @param name
	 *            name of resource
	 * @Param clazz class to use to retrieve resource
	 * @return resource or null
	 */
	public static InputStream getResource(String name, final Class<?> clazz) {
		while (name.startsWith("/"))
			name = name.substring(1);
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
		if (is == null) {
			if (clazz != null)
				is = clazz.getResourceAsStream(name);
			else
				is = Config.class.getResourceAsStream(name);
		}
		if (isJAR && !name.startsWith("resources") && getResource("resources/", clazz) != null)
			return getResource("resources/" + name, clazz);
		else
			return is;
	}

	/**
	 * @see {@link FileUtils#getResource(String, Class)}
	 */
	public static InputStream getResource(final String name) {
		return getResource(name, null);
	}

	/**
	 * Iterates the lines of a file. Closes the reader automatically when it
	 * hits the end of the file
	 *
	 * @param file
	 *            the file to be read
	 * @return iterator over file lines
	 * @throws FileNotFoundException
	 */
	public static Iterator<String> iterateFileLines(final File file) throws FileNotFoundException {
		return (new Iterator<String>() {

			private BufferedReader reader;
			private Boolean next;
			private String nextLine;

			@Override
			public boolean hasNext() {
				if (next == null) {
					nextLine = null;
					try {
						nextLine = reader.readLine();
					} catch (final IOException e1) {
						e1.printStackTrace();
					}
					next = nextLine != null;
					if (!next)
						try {
							reader.close();
						} catch (final IOException e) {
							e.printStackTrace();
						}
				}
				return next;
			}

			@Override
			public String next() {
				if (next == null) {
					try {
						return reader.readLine();
					} catch (final IOException e1) {
						e1.printStackTrace();
					}
				}
				return nextLine;
			}

			public Iterator<String> init(final File file) throws FileNotFoundException {
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
				return this;
			}

		}).init(file);
	}

	public static File getTempFile(final String fileName) {
		return new File(PathUtils.tempDir(), fileName);
	}

	public static File createTempFile(final String fileName) throws IOException {
		final File file = getTempFile(fileName);
		file.createNewFile();
		return file;
	}

}
