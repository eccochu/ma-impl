package de.vipra.util;

public class CountMatrix<T, U> extends Matrix<T, U, Integer> {

	public void count(final T t, final U u) {
		Integer i = get(t, u);
		if (i == null)
			i = 1;
		else
			i++;
		put(t, u, i);
	}

}
