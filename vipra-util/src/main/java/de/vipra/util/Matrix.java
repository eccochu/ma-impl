package de.vipra.util;

import java.util.HashMap;
import java.util.Map;

public class Matrix<T, U, V> {

	private final Map<T, Map<U, V>> rowMap;
	private final Map<U, Map<T, V>> colMap;

	private int startRowSize = 10;
	private int startColSize = 10;

	public Matrix() {
		rowMap = new HashMap<>();
		colMap = new HashMap<>();
	}

	public Matrix(final int rowSize, final int colSize) {
		rowMap = new HashMap<>(rowSize);
		colMap = new HashMap<>(colSize);
		startRowSize = rowSize;
		startColSize = colSize;
	}

	public V put(final T t, final U u, final V v) {
		Map<U, V> row = rowMap.get(t);
		Map<T, V> col = colMap.get(u);
		V oldV = null;
		if (row == null) {
			row = new HashMap<>(startRowSize);
			rowMap.put(t, row);
		} else {
			oldV = row.get(u);
		}
		if (col == null) {
			col = new HashMap<>(startColSize);
			colMap.put(u, col);
		}
		row.put(u, v);
		col.put(t, v);
		return oldV;
	}

	public V get(final T t, final U u) {
		final Map<U, V> subMap = rowMap.get(t);
		if (subMap == null)
			return null;
		return subMap.get(u);
	}

	public int size() {
		int size = 0;
		for (final Map<U, V> subMap : rowMap.values())
			size += subMap.size();
		return size;
	}

	public Map<U, V> row(final T t) {
		return rowMap.get(t);
	}

	public Map<T, V> col(final U u) {
		return colMap.get(u);
	}

}
