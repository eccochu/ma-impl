package de.vipra.util;

import java.net.MalformedURLException;

public class URLUtils {

	public static String concat(String url, final String path) throws MalformedURLException {
		if (url.endsWith("/")) {
			if (path.startsWith("/"))
				url += path.substring(1);
			else
				url += path;
		} else {
			if (path.startsWith("/"))
				url += path;
			else
				url += "/" + path;
		}

		return url;
	}

}
