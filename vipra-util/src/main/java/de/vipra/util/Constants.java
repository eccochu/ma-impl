package de.vipra.util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

public class Constants {

	/*
	 * FILEBASE
	 */

	public static final String FILEBASE_DIR = "vipra";
	public static final Charset FILEBASE_ENCODING = StandardCharsets.UTF_8;
	public static final String LINE_SEP = System.lineSeparator();

	/**
	 * Buffer used while importing files into the database and filebase in # of
	 * articles.
	 */
	public static final int IMPORT_BUFFER_MAX = 1000;

	/*
	 * FILES
	 */

	public static final String CONFIG_FILE = "config.json";
	public static final String BUILDNUMBER_FILE = "buildNumber.properties";
	public static final String LOG_FILE = "vipra.log";
	public static final String HYPERNYMS_FILE = "hypernyms.map";

	/*
	 * DATABASE
	 */

	public static final String DATABASE_HOST = "127.0.0.1";
	public static final int DATABASE_PORT = 27017;
	public static final String DATABASE_NAME = "test";

	/*
	 * ELASTICSEARCH
	 */
	public static final String ES_HOST = "127.0.0.1";
	public static final int ES_PORT = 9300;

	/**
	 * Topic boost parameter. Boosts topic importance in queries. Default 4.
	 */
	public static final int ES_BOOST_TOPICS = 4;

	/**
	 * Title boost parameter. Boosts title importance in queries. Default 2.
	 */
	public static final int ES_BOOST_TITLES = 2;

	/*
	 * TOPIC MODELING
	 */

	/**
	 * The number of words to be used to generate a topic name. The top n words
	 * (sorted by probability) are used to generate a name for unnamed topics.
	 * Default 4.
	 */
	public static final int TOPIC_AUTO_NAMING_WORDS = 4;

	/**
	 * Number of topics to discover with topic modeling, if the selected topic
	 * modeling library supports this parameter. Default 20.
	 */
	public static final int K_TOPICS = 20;

	/**
	 * Number of top words per sequence and topic
	 */
	public static final int K_TOP_WORDS = 20;

	/**
	 * This value is a weight to the rising decay caulculation of topic
	 * relevances. The higher this value, the more focus is put on later
	 * sequences containing more recent documents. Default 0.0.
	 */
	public static final double RISING_DECAY_LAMBDA = 0.0;

	/**
	 * Minimum topic share for an article. Topics with a smaller share are
	 * ignored.
	 */
	public static final double MIN_TOPIC_SHARE = 0.01;

	/**
	 * Minimum probability of words. Words with lower probability are ignored.
	 * Default 0.01.
	 */
	public static final double MIN_RELATIVE_PROB = 0.01;

	/**
	 * Maximum number of similar documents for each document. Default 10.
	 */
	public static final int MAX_SIMILAR_DOCUMENTS = 10;

	/**
	 * Maximum number of similar topics for each topic. Default 10.
	 */
	public static final int MAX_SIMILAR_TOPICS = 10;

	/**
	 * Maximum divergence between a document and similar documents. Lower values
	 * mean more similar documents (less divergence). Default 0.25.
	 */
	public static final double MAX_SIMILAR_DOCUMENTS_DIVERGENCE = 0.25;

	/**
	 * Maximum divergence between a topic and similar topics. Lower values mean
	 * more similar topics (less divergence). Default 0.25.
	 */
	public static final double MAX_SIMILAR_TOPICS_DIVERGENCE = 0.25;

	/**
	 * Dynamic minimum iterations. Used for dynamic topic modeling. Default 100.
	 */
	public static final int DYNAMIC_MIN_ITER = 100;

	/**
	 * Dynamic maximum iterations. Used for dynamic topic modeling. Default
	 * 1000.
	 */
	public static final int DYNAMIC_MAX_ITER = 1000;

	/**
	 * Static iterations. Used for static topic modeling. Default 100.
	 */
	public static final int STATIC_ITER = 200;

	/**
	 * Minumum number of words per document. Default 10.
	 */
	public static final int DOCUMENT_MIN_LENGTH = 10;

	/**
	 * Minimum word frequency for a word to be accepted. Default 5.
	 */
	public static final int DOCUMENT_MIN_WORD_FREQ = 5;

	/**
	 * Minimum number of dbpedia inlinks for an entity annotation to be
	 * accepted. Default 20.
	 */
	public static final int SPOTLIGHT_SUPPORT = 100;

	/**
	 * Disambiguation confidence. Eliminates top n percent of inconfident
	 * annotations. Ranges from 0 to 1. Default 0.5.
	 */
	public static final double SPOTLIGHT_CONFIDENCE = 0.5;

	/**
	 * The dynamic topic modeling window resolution to be used. This value is
	 * only used, if the selected analyzer supports dynamic topic modeling. To
	 * find a list of available analyzers,
	 * {@link de.vipra.util.Constants.WindowResolution}.
	 */
	public static final WindowResolution WINDOW_RESOLUTION = WindowResolution.YEAR;

	/**
	 * Processor modes for transforming text as topic model generation
	 * preparation.
	 */
	public static final boolean PROCESSOR_USE_TEXT = true;
	public static final boolean PROCESSOR_USE_ENTITIES = true;
	public static final boolean PROCESSOR_USE_ENTITY_TYPES = true;
	public static final boolean PROCESSOR_USE_HYPERNYMS = true;

	/**
	 * True to query entity descriptions from dbpedia upon entity detection.
	 */
	public static final boolean QUERY_ENTITY_DESCRIPTIONS = true;

	/**
	 * Stopwords list. Extensive list of stopwords used to clean imported
	 * articles of the most common words before topic modeling is applied.
	 */
	public static final List<String> STOPWORDS = Arrays.asList("'ll", "'ve", "'re", "'s", "'t", "'d", "a", "a's", "able", "about", "above", "abst",
			"accordance", "according", "accordingly", "across", "act", "actually", "added", "adj", "affected", "affecting", "affects", "after",
			"afterwards", "again", "against", "ah", "ain't", "aint", "all", "allow", "allows", "almost", "alone", "along", "already", "also",
			"although", "always", "am", "among", "amongst", "amoungst", "amount", "an", "and", "announce", "another", "any", "anybody", "anyhow",
			"anymore", "anyone", "anything", "anyway", "anyways", "anywhere", "apart", "apparently", "appear", "appreciate", "appropriate",
			"approximately", "are", "area", "areas", "aren", "aren't", "arent", "arise", "around", "as", "aside", "ask", "asked", "asking", "asks",
			"associated", "at", "auth", "available", "away", "awfully", "b", "back", "backed", "backing", "backs", "be", "became", "because",
			"become", "becomes", "becoming", "been", "before", "beforehand", "began", "begin", "beginning", "beginnings", "begins", "behind", "being",
			"beings", "believe", "below", "beside", "besides", "best", "better", "between", "beyond", "big", "bill", "biol", "both", "bottom",
			"brief", "briefly", "but", "by", "c", "c'mon", "c's", "ca", "call", "came", "can", "can't", "cannot", "cant", "case", "cases", "cause",
			"causes", "certain", "certainly", "changes", "clear", "clearly", "cmon", "co", "com", "come", "comes", "computer", "con", "concerning",
			"consequently", "consider", "considering", "contain", "containing", "contains", "corresponding", "could", "couldn't", "couldnt", "course",
			"cry", "cs", "currently", "d", "date", "de", "definitely", "describe", "described", "despite", "detail", "did", "didn't", "didnt",
			"differ", "different", "differently", "do", "does", "doesn't", "doesnt", "doing", "don't", "done", "dont", "down", "downed", "downing",
			"downs", "downwards", "due", "during", "e", "each", "early", "ed", "edu", "effect", "eg", "eight", "eighty", "either", "eleven", "else",
			"elsewhere", "empty", "end", "ended", "ending", "ends", "enough", "entirely", "especially", "et", "et-al", "etc", "even", "evenly",
			"ever", "every", "everybody", "everyone", "everything", "everywhere", "ex", "exactly", "example", "except", "f", "face", "faces", "fact",
			"facts", "far", "felt", "few", "ff", "fifteen", "fifth", "fify", "fill", "find", "finds", "fire", "first", "five", "fix", "followed",
			"following", "follows", "for", "former", "formerly", "forth", "forty", "found", "four", "from", "front", "full", "fully", "further",
			"furthered", "furthering", "furthermore", "furthers", "g", "gave", "general", "generally", "get", "gets", "getting", "give", "given",
			"gives", "giving", "go", "goes", "going", "gone", "good", "goods", "got", "gotten", "great", "greater", "greatest", "greetings", "group",
			"grouped", "grouping", "groups", "h", "had", "hadn't", "hadnt", "happens", "hardly", "has", "hasn't", "hasnt", "have", "haven't",
			"havent", "having", "he", "he'd", "he'll", "he's", "hed", "hello", "help", "hence", "her", "here", "here's", "hereafter", "hereby",
			"herein", "heres", "hereupon", "hers", "herse", "herself", "hes", "hi", "hid", "high", "higher", "highest", "him", "himse", "himself",
			"his", "hither", "home", "hopefully", "how", "how's", "howbeit", "however", "hundred", "i", "i'd", "i'll", "i'm", "i've", "id", "ie",
			"if", "ignored", "ill", "im", "immediate", "immediately", "importance", "important", "in", "inasmuch", "inc", "indeed", "index",
			"indicate", "indicated", "indicates", "information", "inner", "insofar", "instead", "interest", "interested", "interesting", "interests",
			"into", "invention", "inward", "is", "isn't", "isnt", "it", "it'd", "it'll", "it's", "itd", "itll", "its", "itse", "itself", "ive", "j",
			"just", "k", "keep", "keeps", "kept", "kg", "kind", "km", "knew", "know", "known", "knows", "l", "large", "largely", "last", "lately",
			"later", "latest", "latter", "latterly", "least", "less", "lest", "let", "let's", "lets", "like", "liked", "likely", "line", "little",
			"long", "longer", "longest", "look", "looking", "looks", "ltd", "m", "made", "mainly", "make", "makes", "making", "man", "many", "may",
			"maybe", "me", "mean", "means", "meantime", "meanwhile", "member", "members", "men", "merely", "mg", "might", "mill", "million", "mine",
			"miss", "ml", "more", "moreover", "most", "mostly", "move", "mr", "mrs", "much", "mug", "must", "mustn't", "my", "myse", "myself", "n",
			"na", "name", "namely", "nay", "nd", "near", "nearly", "necessarily", "necessary", "need", "needed", "needing", "needs", "neither",
			"never", "nevertheless", "new", "newer", "newest", "next", "nine", "ninety", "no", "nobody", "non", "none", "nonetheless", "noone", "nor",
			"normally", "nos", "not", "noted", "nothing", "novel", "now", "nowhere", "number", "numbers", "o", "obtain", "obtained", "obviously",
			"of", "off", "often", "oh", "ok", "okay", "old", "older", "oldest", "omitted", "on", "once", "one", "ones", "only", "onto", "open",
			"opened", "opening", "opens", "or", "ord", "order", "ordered", "ordering", "orders", "other", "others", "otherwise", "ought", "our",
			"ours", "ourselves", "out", "outside", "over", "overall", "owing", "own", "p", "page", "pages", "part", "parted", "particular",
			"particularly", "parting", "parts", "past", "per", "perhaps", "place", "placed", "places", "please", "plus", "point", "pointed",
			"pointing", "points", "poorly", "possible", "possibly", "potentially", "pp", "predominantly", "present", "presented", "presenting",
			"presents", "presumably", "previously", "primarily", "probably", "problem", "problems", "promptly", "proud", "provides", "put", "puts",
			"q", "que", "quickly", "quite", "qv", "r", "ran", "rather", "rd", "re", "readily", "really", "reasonably", "recent", "recently", "ref",
			"refs", "regarding", "regardless", "regards", "related", "relatively", "research", "respectively", "resulted", "resulting", "results",
			"right", "room", "rooms", "run", "s", "said", "same", "saw", "say", "saying", "says", "sec", "second", "secondly", "seconds", "section",
			"see", "seeing", "seem", "seemed", "seeming", "seems", "seen", "sees", "self", "selves", "sensible", "sent", "serious", "seriously",
			"seven", "several", "shall", "shan't", "she", "she'd", "she'll", "she's", "shed", "shes", "should", "shouldn't", "shouldnt", "show",
			"showed", "showing", "shown", "showns", "shows", "side", "sides", "significant", "significantly", "similar", "similarly", "since",
			"sincere", "six", "sixty", "slightly", "small", "smaller", "smallest", "so", "some", "somebody", "somehow", "someone", "somethan",
			"something", "sometime", "sometimes", "somewhat", "somewhere", "soon", "sorry", "specifically", "specified", "specify", "specifying",
			"state", "states", "still", "stop", "strongly", "sub", "substantially", "successfully", "such", "such as", "sufficiently", "suggest",
			"sup", "sure", "system", "t", "t's", "take", "taken", "taking", "tell", "ten", "tends", "th", "than", "thank", "thanks", "thanx", "that",
			"that'll", "that's", "that've", "thats", "the", "their", "theirs", "them", "themselves", "then", "thence", "there", "there'll", "there's",
			"there've", "thereafter", "thereby", "thered", "therefore", "therein", "thereof", "therere", "theres", "thereto", "thereupon", "these",
			"they", "they'd", "they'll", "they're", "they've", "theyd", "theyll", "theyre", "theyve", "thick", "thin", "thing", "things", "think",
			"thinks", "third", "this", "thorough", "thoroughly", "those", "thou", "though", "thoughh", "thought", "thoughts", "thousand", "three",
			"throug", "through", "throughout", "thru", "thus", "til", "tip", "to", "today", "together", "too", "took", "top", "toward", "towards",
			"tried", "tries", "truly", "try", "trying", "ts", "turn", "turned", "turning", "turns", "twelve", "twenty", "twice", "two", "u", "un",
			"under", "unfortunately", "unless", "unlike", "unlikely", "until", "unto", "up", "upon", "ups", "us", "use", "used", "useful", "usefully",
			"usefulness", "uses", "using", "usually", "uucp", "v", "value", "various", "very", "via", "viz", "vol", "vols", "vs", "w", "want",
			"wanted", "wanting", "wants", "was", "wasn't", "wasnt", "way", "ways", "we", "we'd", "we'll", "we're", "we've", "wed", "welcome", "well",
			"wells", "went", "were", "weren't", "werent", "weve", "what", "what'll", "what's", "whatever", "whats", "when", "when's", "whence",
			"whenever", "where", "where's", "whereafter", "whereas", "whereby", "wherein", "wheres", "whereupon", "wherever", "whether", "which",
			"while", "whim", "whither", "who", "who'll", "who's", "whod", "whoever", "whole", "whom", "whomever", "whos", "whose", "why", "why's",
			"widely", "will", "willing", "wish", "with", "within", "without", "won't", "wonder", "wont", "words", "work", "worked", "working",
			"works", "world", "would", "wouldn't", "wouldnt", "www", "x", "y", "year", "years", "yes", "yet", "you", "you'd", "you'll", "you're",
			"you've", "youd", "youll", "young", "younger", "youngest", "your", "youre", "yours", "yourself", "yourselves", "youve", "z", "zero");

	/**
	 * Disallowed chars for words in processed text segments. This regular
	 * expression is used to strip text of characters that should not be
	 * processed.
	 */
	public static final String CHARS_DISALLOWED = "[^a-zA-Z0-9 ]";

	/**
	 * Regular expression to find and remove email addresses from text.
	 */
	public static final String REGEX_EMAIL = "[^\\s]*@[^\\s]*";

	/**
	 * Regular expressiong to find and remove urls from text.
	 */
	public static final String REGEX_URL = "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)";

	/**
	 * Regular expression to find and remove numbers from text.
	 */
	public static final String REGEX_NUMBER = "\\b\\w*\\d+\\w*\\b";

	/**
	 * Regular expression to find and remove single char words.
	 */
	public static final String REGEX_SINGLECHAR = "\\b\\w\\b";

	/**
	 * Regular expression to match forbidden characters in an entity name.
	 */
	public static final String REGEX_ENTITY_DISALLOWED = "[/\\.]+";

	/*
	 * INDEX
	 */

	/**
	 * The length of the text excerpt used for indexing and displaying text in
	 * search results.
	 */
	public static final int EXCERPT_LENGTH = 500;

	/*
	 * OTHER
	 */

	/**
	 * The global date time format. Will be used for conversion from and to
	 * database and frontend dates.
	 */
	public static final String DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

	/**
	 * Describes the window size, when using dynamic topic modeling
	 */
	public static enum WindowResolution {
		YEAR,
		SEMESTER,
		TRIMESTER,
		QUARTER,
		MONTH,
		WEEK,
		DAY,
		HOUR,
		MINUTE,
		SECOND;

		public Date startDate(final Date date) {
			final Calendar in = new GregorianCalendar();
			in.setTime(date);
			final Calendar out = new GregorianCalendar();
			out.setTimeZone(TimeZone.getTimeZone("GMT"));
			out.setTime(date);
			out.set(Calendar.MILLISECOND, out.getActualMinimum(Calendar.MILLISECOND));
			switch (this) {
				case YEAR:
					out.set(Calendar.MONTH, out.getActualMinimum(Calendar.MONTH));
				case MONTH:
					out.set(Calendar.DAY_OF_MONTH, out.getActualMinimum(Calendar.DAY_OF_MONTH));
				case SEMESTER:
				case TRIMESTER:
				case QUARTER:
				case DAY:
					out.set(Calendar.HOUR_OF_DAY, out.getActualMinimum(Calendar.HOUR_OF_DAY));
				case HOUR:
					out.set(Calendar.MINUTE, out.getActualMinimum(Calendar.MINUTE));
				case MINUTE:
					out.set(Calendar.SECOND, out.getActualMinimum(Calendar.SECOND));
				default:
					break;
			}
			switch (this) {
				case SEMESTER:
					out.set(Calendar.MONTH, CalendarUtils.getSemester(in));
					out.set(Calendar.DAY_OF_MONTH, out.getActualMaximum(Calendar.DAY_OF_MONTH));
					break;
				case TRIMESTER:
					out.set(Calendar.MONTH, CalendarUtils.getTrimester(in));
					out.set(Calendar.DAY_OF_MONTH, out.getActualMaximum(Calendar.DAY_OF_MONTH));
					break;
				case QUARTER:
					out.set(Calendar.MONTH, CalendarUtils.getQuarter(in));
					out.set(Calendar.DAY_OF_MONTH, out.getActualMaximum(Calendar.DAY_OF_MONTH));
					break;
				case WEEK:
					out.set(Calendar.WEEK_OF_YEAR, in.get(Calendar.WEEK_OF_YEAR));
					out.set(Calendar.DAY_OF_WEEK, out.getFirstDayOfWeek());
					break;
				default:
					break;
			}
			return out.getTime();
		}

		public Date endDate(final Date date) {
			final Calendar in = new GregorianCalendar();
			in.setTime(date);
			final Calendar out = new GregorianCalendar();
			out.setTime(date);
			out.setTimeZone(TimeZone.getTimeZone("GMT"));
			out.set(Calendar.MILLISECOND, out.getActualMaximum(Calendar.MILLISECOND));
			switch (this) {
				case YEAR:
					out.set(Calendar.MONTH, out.getActualMaximum(Calendar.MONTH));
				case MONTH:
					out.set(Calendar.DAY_OF_MONTH, out.getActualMaximum(Calendar.DAY_OF_MONTH));
				case SEMESTER:
				case TRIMESTER:
				case QUARTER:
				case DAY:
					out.set(Calendar.HOUR_OF_DAY, out.getActualMaximum(Calendar.HOUR_OF_DAY));
				case HOUR:
					out.set(Calendar.MINUTE, out.getActualMaximum(Calendar.MINUTE));
				case MINUTE:
					out.set(Calendar.SECOND, out.getActualMaximum(Calendar.SECOND));
				default:
					break;
			}
			switch (this) {
				case SEMESTER:
					out.set(Calendar.MONTH, CalendarUtils.getSemester(in) + 5);
					out.set(Calendar.DAY_OF_MONTH, out.getActualMaximum(Calendar.DAY_OF_MONTH));
					break;
				case TRIMESTER:
					out.set(Calendar.MONTH, CalendarUtils.getTrimester(in) + 3);
					out.set(Calendar.DAY_OF_MONTH, out.getActualMaximum(Calendar.DAY_OF_MONTH));
					break;
				case QUARTER:
					out.set(Calendar.MONTH, CalendarUtils.getQuarter(in) + 2);
					out.set(Calendar.DAY_OF_MONTH, out.getActualMaximum(Calendar.DAY_OF_MONTH));
					break;
				case WEEK:
					out.set(Calendar.WEEK_OF_YEAR, in.get(Calendar.WEEK_OF_YEAR));
					out.set(Calendar.DAY_OF_WEEK, in.getFirstDayOfWeek());
					out.add(Calendar.DAY_OF_YEAR, 6);
					break;
				default:
					break;
			}
			return out.getTime();
		}
	}

}
