package de.vipra.util;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Timer class for nano time measurements using {@link System#nanoTime()}.
 * Advanced methods for creating multiple named sequential time frames (laps)
 * and collective output
 */
public class Timer {

	private long firstStart;
	private long start;
	private Map<String, Long> laps;

	public Timer() {
		restart();
	}

	/**
	 * restart the timer (reset everything) and return the start time
	 *
	 * @return start time
	 */
	public long restart() {
		firstStart = start = System.nanoTime();
		laps = new LinkedHashMap<>();
		return start;
	}

	/**
	 * Return the current time since start or last lap, whichever is closer.
	 *
	 * @return time since start or last lap
	 */
	public long current() {
		return System.nanoTime() - start;
	}

	/**
	 * Create a named lap and reset the lap start time.
	 *
	 * @param name
	 *            Name of lap
	 * @return lap time
	 */
	public long lap(final String name) {
		final long lap = System.nanoTime() - start;
		start = System.nanoTime();
		laps.put(name, lap);
		return lap;
	}

	/**
	 * Get total time since start or last reset, ignoring laps
	 *
	 * @return total time
	 */
	public long total() {
		return System.nanoTime() - firstStart;
	}

	@Override
	public String toString() {
		String out = null;
		if (laps != null && laps.size() > 0) {
			final StringBuilder sb = new StringBuilder();
			for (final Entry<String, Long> e : laps.entrySet()) {
				sb.append(", ").append(e.getKey()).append(": ").append(StringUtils.timeString(e.getValue()));
			}
			out = sb.toString().substring(2);
		} else {
			out = super.toString();
		}
		return out;
	}

}
