package de.vipra.util.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import org.mongodb.morphia.query.Query;

public class QueryBuilder {

	public static enum CriterionType {
		EQUALS,
		EQUALS_IGNORECASE,
		LESSERTHAN,
		LESSERTHANEQUAL,
		GREATERTHAN,
		GREATERTHANEQUAL,
		STARTSWITH,
		STARTSWITH_IGNORECASE,
		ENDSWITH,
		ENDSWITH_IGNORECASE,
		CONTAINS,
		CONTAINS_IGNORECASE,
		IN,
		REGEX
	};

	public static class Criterion {
		public final String field;
		public final CriterionType type;
		public final Object value;

		public Criterion(final String field, final CriterionType type, final Object value) {
			this.field = field;
			this.type = type;
			this.value = value;
		}

		public void apply(final Query<?> query) {
			switch (type) {
				case EQUALS:
					query.field(field).equal(value);
					break;
				case EQUALS_IGNORECASE:
					query.field(field).equalIgnoreCase(value);
					break;
				case LESSERTHAN:
					query.field(field).lessThan(value);
					break;
				case LESSERTHANEQUAL:
					query.field(field).lessThanOrEq(value);
					break;
				case GREATERTHAN:
					query.field(field).greaterThan(value);
					break;
				case GREATERTHANEQUAL:
					query.field(field).greaterThanOrEq(value);
					break;
				case STARTSWITH:
					query.field(field).startsWith((String) value);
					break;
				case STARTSWITH_IGNORECASE:
					query.field(field).startsWithIgnoreCase((String) value);
					break;
				case ENDSWITH:
					query.field(field).endsWith((String) value);
					break;
				case ENDSWITH_IGNORECASE:
					query.field(field).endsWithIgnoreCase((String) value);
					break;
				case CONTAINS:
					query.field(field).contains((String) value);
					break;
				case CONTAINS_IGNORECASE:
					query.field(field).containsIgnoreCase((String) value);
					break;
				case IN:
					query.field(field).in((Iterable<?>) value);
					break;
				case REGEX:
					query.filter(field, value);
			}
		}
	}

	private Integer skip;
	private Integer limit;
	private String sortBy;
	private final Set<Criterion> criteria;
	private String[] fields;
	private boolean include;
	private boolean allFields;

	private QueryBuilder() {
		criteria = new HashSet<>();
	}

	public static QueryBuilder builder() {
		return new QueryBuilder();
	}

	public QueryBuilder skip(final Integer skip) {
		if (skip == null || skip >= 0)
			this.skip = skip;
		return this;
	}

	public QueryBuilder limit(final Integer limit) {
		if (limit == null || limit >= 0)
			this.limit = limit;
		return this;
	}

	public QueryBuilder sortBy(final String sortBy) {
		if (sortBy == null || !sortBy.isEmpty())
			this.sortBy = sortBy;
		return this;
	}

	public QueryBuilder eq(final String field, final Object value) {
		return criterion(field, CriterionType.EQUALS, value);
	}

	public QueryBuilder eq(final String field, final Object value, final boolean ignoreCase) {
		return criterion(field, ignoreCase ? CriterionType.EQUALS_IGNORECASE : CriterionType.EQUALS, value);
	}

	public QueryBuilder lt(final String field, final Object value) {
		return criterion(field, CriterionType.LESSERTHAN, value);
	}

	public QueryBuilder lte(final String field, final Object value) {
		return criterion(field, CriterionType.LESSERTHANEQUAL, value);
	}

	public QueryBuilder gt(final String field, final Object value) {
		return criterion(field, CriterionType.GREATERTHAN, value);
	}

	public QueryBuilder gte(final String field, final Object value) {
		return criterion(field, CriterionType.GREATERTHANEQUAL, value);
	}

	public QueryBuilder startsWith(final String field, final String value, final boolean ignoreCase) {
		return criterion(field, ignoreCase ? CriterionType.STARTSWITH_IGNORECASE : CriterionType.STARTSWITH, value);
	}

	public QueryBuilder endsWith(final String field, final String value, final boolean ignoreCase) {
		return criterion(field, ignoreCase ? CriterionType.ENDSWITH_IGNORECASE : CriterionType.ENDSWITH, value);
	}

	public QueryBuilder contains(final String field, final String value, final boolean ignoreCase) {
		return criterion(field, ignoreCase ? CriterionType.CONTAINS_IGNORECASE : CriterionType.CONTAINS, value);
	}

	public QueryBuilder in(final String field, final Iterable<?> values) {
		return criterion(field, CriterionType.IN, values);
	}

	public QueryBuilder regex(final String field, final Pattern pattern) {
		return criterion(field, CriterionType.REGEX, pattern);
	}

	private QueryBuilder criterion(final String field, final CriterionType type, final Object value) {
		if (field != null && !field.isEmpty() && value != null)
			criteria.add(new Criterion(field, type, value));
		return this;
	}

	public QueryBuilder allFields() {
		allFields = true;
		fields = null;
		return this;
	}

	public QueryBuilder fields(final String... fields) {
		return fields(true, fields);
	}

	public QueryBuilder fields(final boolean include, final String... fields) {
		if (fields != null && fields.length > 0) {
			if (this.fields != null && this.fields.length > 0 && include != this.include) {
				final Set<String> a = new HashSet<>(Arrays.asList(this.fields));
				a.removeAll(new HashSet<>(Arrays.asList(fields)));
				if (a.isEmpty()) {
					this.fields = null;
				} else {
					this.fields = a.toArray(new String[a.size()]);
				}
			} else {
				this.include = include;
				for (final String field : fields) {
					if (field.equalsIgnoreCase("_all")) {
						allFields = true;
						this.fields = null;
						return this;
					}
				}
				this.fields = fields;
			}
		}
		return this;
	}

	public Integer getSkip() {
		return skip;
	}

	public Integer getLimit() {
		return limit;
	}

	public String getSortBy() {
		if (sortBy == null)
			return null;
		return sortBy.startsWith("+") ? sortBy.substring(1) : sortBy;
	}

	public Set<Criterion> getCriteria() {
		return criteria;
	}

	public boolean isInclude() {
		return include;
	}

	public String[] getFields() {
		return fields;
	}

	public boolean isAllFields() {
		return allFields;
	}

	public Query<?> build(final Query<?> query) {
		return build(query, false);
	}

	public Query<?> build(final Query<?> query, final boolean counting) {
		if (!counting && getSkip() != null && getSkip() > 0)
			query.offset(getSkip());
		if (!counting && getLimit() != null && getLimit() > 0)
			query.limit(getLimit());
		if (!counting && getSortBy() != null)
			query.order(getSortBy());
		if (getCriteria() != null)
			for (final Criterion criterion : getCriteria())
				criterion.apply(query);
		if (getFields() != null) {
			final String[] fields = getFields();
			if (isInclude()) {
				query.retrievedFields(true, fields);
			} else {
				query.retrievedFields(false, fields);
			}
		}
		return query;
	}

}