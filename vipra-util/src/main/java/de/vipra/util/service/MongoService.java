package de.vipra.util.service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import de.vipra.util.Config;
import de.vipra.util.ListUtils;
import de.vipra.util.Mongo;
import de.vipra.util.an.QueryIgnore;
import de.vipra.util.an.UpdateIgnore;
import de.vipra.util.ex.ConfigException;
import de.vipra.util.ex.DatabaseException;
import de.vipra.util.model.Model;

public class MongoService<Type extends Model<IdType>, IdType> {

	private final Datastore datastore;
	private final Class<Type> clazz;
	private final String[] ignoredFieldsSingleQuery;
	private final String[] ignoredFieldsMultiQuery;
	private final Map<String, Field> updateFields;

	public MongoService(final Mongo mongo, final Class<Type> clazz) {
		this.datastore = mongo.getDatastore();
		this.clazz = clazz;

		final List<String> ignoreSingle = new ArrayList<>();
		final List<String> ignoreMulti = new ArrayList<>();
		final Field[] fields = clazz.getDeclaredFields();
		this.updateFields = new HashMap<>(fields.length);
		for (final Field field : fields) {
			if (!field.isAccessible())
				field.setAccessible(true);

			final UpdateIgnore ui = field.getDeclaredAnnotation(UpdateIgnore.class);
			if (ui == null)
				this.updateFields.put(field.getName(), field);

			final QueryIgnore qi = field.getDeclaredAnnotation(QueryIgnore.class);
			if (qi != null) {
				if (qi.single() || qi.all())
					ignoreSingle.add(field.getName());
				if (qi.multi() || qi.all())
					ignoreMulti.add(field.getName());
			}
		}
		this.ignoredFieldsSingleQuery = ignoreSingle.toArray(new String[ignoreSingle.size()]);
		this.ignoredFieldsMultiQuery = ignoreMulti.toArray(new String[ignoreMulti.size()]);
	}

	public Type getSingle(final IdType id, final String... fields) throws DatabaseException {
		return getSingle(id, QueryBuilder.builder().fields(true, fields));
	}

	public Type getSingle(final IdType id, final QueryBuilder builder) throws DatabaseException {
		final Query<Type> query = datastore.createQuery(clazz).field("_id").equal(id);

		if (builder != null) {
			if (builder.getFields() != null) {
				final String[] fields = builder.getFields();
				if (builder.isInclude()) {
					query.retrievedFields(true, fields);
				} else {
					query.retrievedFields(false, fields);
				}
			} else if (!builder.isAllFields() && ignoredFieldsSingleQuery.length > 0) {
				query.retrievedFields(false, ignoredFieldsSingleQuery);
			}
		} else if (ignoredFieldsSingleQuery.length > 0) {
			query.retrievedFields(false, ignoredFieldsSingleQuery);
		}
		final Type t = query.get();
		return t;
	}

	public Type getSingle(final QueryBuilder builder) throws DatabaseException {
		final Query<Type> query = datastore.createQuery(clazz);
		builder.build(query);
		final Type t = query.get();
		return t;
	}

	public List<Type> getMultiple(final Integer skip, final Integer limit, final String sortBy, final String... fields) {
		return getMultiple(QueryBuilder.builder().skip(skip).limit(limit).sortBy(sortBy).fields(true, fields));
	}

	public List<Type> getMultiple(final QueryBuilder builder) {
		final Query<Type> query = datastore.createQuery(clazz);

		if (builder != null) {
			builder.build(query);
			if (builder.getFields() == null && !builder.isAllFields() && ignoredFieldsMultiQuery.length > 0) {
				query.retrievedFields(false, ignoredFieldsMultiQuery);
			}
		} else if (ignoredFieldsMultiQuery.length > 0) {
			query.retrievedFields(false, ignoredFieldsMultiQuery);
		}
		final List<Type> list = query.asList();
		return list;
	}

	public List<Type> getAll(final String... fields) {
		return getMultiple(QueryBuilder.builder().fields(true, fields));
	}

	public Iterator<Type> getAllIterator(final QueryBuilder builder) {
		final Query<Type> query = datastore.createQuery(clazz);

		if (builder != null) {
			builder.build(query);
			if (builder.getFields() == null && !builder.isAllFields() && ignoredFieldsMultiQuery.length > 0) {
				query.retrievedFields(false, ignoredFieldsMultiQuery);
			}
		} else if (ignoredFieldsMultiQuery.length > 0) {
			query.retrievedFields(false, ignoredFieldsMultiQuery);
		}
		return query.iterator();
	}

	public Type createSingle(final Type t) throws DatabaseException {
		if (t == null)
			throw new DatabaseException(new NullPointerException("entity is null"));

		datastore.save(t);
		return t;
	}

	public List<Type> createMultiple(final Iterable<Type> t) throws DatabaseException {
		if (t == null)
			throw new DatabaseException(new NullPointerException("entities are null"));
		final List<Type> list = ListUtils.toList(t);

		datastore.save(list);
		return list;
	}

	public long deleteSingle(final IdType id) throws DatabaseException {
		if (id == null)
			throw new DatabaseException(new NullPointerException("id is null"));

		final int deleted = datastore.delete(clazz, id).getN();
		return deleted;
	}

	public long deleteMultiple(final Iterable<IdType> ids) throws DatabaseException {
		if (ids == null)
			throw new DatabaseException(new NullPointerException("ids are null"));

		final int deleted = datastore.delete(clazz, ids).getN();
		return deleted;
	}

	public long deleteMultiple(final QueryBuilder builder) throws DatabaseException {
		final Query<Type> query = datastore.createQuery(clazz);
		if (builder != null)
			builder.build(query);

		final int deleted = datastore.delete(query).getN();
		return deleted;
	}

	public void replaceSingle(final Type t) throws DatabaseException {
		if (t == null)
			throw new DatabaseException(new NullPointerException("entity is null"));
		if (t.getId() == null)
			throw new DatabaseException(new NullPointerException("entity has no id"));

		datastore.save(t);
	}

	public void replaceMultiple(final Iterable<Type> ts) throws DatabaseException {
		if (ts == null)
			throw new DatabaseException(new NullPointerException("entities are null"));

		datastore.save(ts);
	}

	public void updateSingle(final Type t, final boolean upsert, final String... fields) throws DatabaseException {
		if (t == null)
			throw new DatabaseException(new NullPointerException("entity is null"));
		if (t.getId() == null)
			throw new DatabaseException(new NullPointerException("entity has no id"));

		if (fields == null || fields.length == 0) {
			replaceSingle(t);
		} else {
			final Query<Type> query = datastore.createQuery(clazz).field("_id").equal(t.getId());
			final UpdateOperations<Type> ops = datastore.createUpdateOperations(clazz);
			boolean noChanges = true;
			for (final String fieldName : fields) {
				if (!this.updateFields.containsKey(fieldName))
					throw new DatabaseException("field unknown: " + fieldName);
				final Field field = this.updateFields.get(fieldName);
				if (field == null)
					continue;
				try {
					final Object value = field.get(t);
					if (value == null)
						ops.unset(fieldName);
					else
						ops.set(fieldName, value);
					noChanges = false;
				} catch (IllegalArgumentException | IllegalAccessException e) {
					throw new DatabaseException(e);
				}
			}
			if (!noChanges)
				datastore.update(query, ops, upsert);
		}
	}

	public void updateSingle(final Type t, final String... fields) throws DatabaseException {
		updateSingle(t, false, fields);
	}

	public void updateMultiple(final Iterable<Type> ts, final String fieldName, final Object value) throws DatabaseException {
		final List<IdType> ids = new ArrayList<>();
		for (final Type t : ts)
			ids.add(t.getId());
		final Query<Type> query = datastore.createQuery(clazz).field("_id").in(ids);
		final UpdateOperations<Type> ops = datastore.createUpdateOperations(clazz);
		if (!this.updateFields.containsKey(fieldName))
			throw new DatabaseException("field unknown: " + fieldName);
		final Field field = this.updateFields.get(fieldName);
		if (field == null)
			return;
		try {
			if (value == null)
				ops.unset(fieldName);
			else
				ops.set(fieldName, value);
		} catch (final IllegalArgumentException e) {
			throw new DatabaseException(e);
		}
		datastore.update(query, ops, false);
	}

	public void drop() {
		datastore.getCollection(clazz).drop();
	}

	public long count(final QueryBuilder builder) {
		if (builder == null)
			return datastore.getCount(clazz);

		final Query<Type> query = datastore.createQuery(clazz);
		builder.build(query, true);

		return datastore.getCount(query);
	}

	public static <Type extends Model<IdType>, IdType> MongoService<Type, IdType> getDatabaseService(final Config config, final Class<Type> clazz)
			throws ConfigException {
		final Mongo mongo = config.getMongo();
		return new MongoService<>(mongo, clazz);
	}

	public static void dropDatabase(final Config config) throws ConfigException {
		config.getMongo().getClient().dropDatabase(config.getDatabaseName());
	}

}
