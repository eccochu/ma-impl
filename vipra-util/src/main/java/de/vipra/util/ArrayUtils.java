package de.vipra.util;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayUtils {

	/**
	 * Finds the maximum column values in a matrix of double values
	 *
	 * @param values
	 *            the double matrix to scan
	 * @return an array of maximum values per column
	 */
	public static double[] findColMaximum(final double[][] values) {
		final int rows = values.length;
		final int cols = values[0].length;
		final double[] maximum = new double[cols];
		Arrays.fill(maximum, Integer.MIN_VALUE);
		for (int row = 0; row < rows; row++)
			for (int col = 0; col < cols; col++)
				if (values[row][col] > maximum[col])
					maximum[col] = values[row][col];
		return maximum;
	}

	/**
	 * Jensen Shannon Divergence to measure similarity between two probability
	 * distributions.
	 *
	 * @param p1
	 *            left distribution
	 * @param p2
	 *            right distribution
	 * @return divergence
	 */
	public static double jsDivergence(final double[] p1, final double[] p2) {
		assert p1.length == p2.length;
		final double[] avg = new double[p1.length];
		for (int i = 0; i < p1.length; i++)
			avg[i] = (p1[i] + p2[i]) / 2.0;
		return (klDivergence(p1, avg) + klDivergence(p2, avg)) / 2.0;
	}

	/**
	 * Kullback Leibler Divergence to measure similarity between two probability
	 * distributions.
	 *
	 * @param p1
	 *            left distribution
	 * @param p2
	 *            right distribution
	 * @return divergence
	 */
	public static double klDivergence(final double[] p1, final double[] p2) {
		assert p1.length == p2.length;
		double result = 0;
		for (int i = 0; i < p1.length; i++) {
			if (p1[i] == 0 || p2[i] == 0)
				continue;
			result += p1[i] * Math.log(p1[i] / p2[i]);
		}
		return result / Math.log(2);
	}

	/**
	 * Concatenate two arrays
	 *
	 * @param a
	 *            left array
	 * @param b
	 *            right array
	 * @return left array + right array concatenated
	 */
	public static <T> T[] addAll(final T[] a, final T[] b) {
		final int aLen = a.length;
		final int bLen = b.length;

		@SuppressWarnings("unchecked")
		final T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen);
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);

		return c;
	}

}
