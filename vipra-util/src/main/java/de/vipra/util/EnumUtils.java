package de.vipra.util;

public class EnumUtils {

	/**
	 * Finds an enum value by its name, ignoring case.
	 *
	 * @param enumeration
	 *            Enum to be searched
	 * @param search
	 *            Enum value to be searched
	 * @return the found enum value, or null
	 */
	public static <T extends Enum<?>> T searchEnum(final Class<T> enumeration, final String search) {
		for (final T each : enumeration.getEnumConstants()) {
			if (each.name().compareToIgnoreCase(search) == 0) {
				return each;
			}
		}
		return null;
	}

}
