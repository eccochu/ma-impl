package de.vipra.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BuildInfo {

	private static Properties properties;

	private static Properties getProperties() {
		if (properties == null) {
			final InputStream inputStream = FileUtils.getResource("buildNumber.properties");
			properties = new Properties();
			try {
				properties.load(inputStream);
			} catch (final IOException e) {
				throw new RuntimeException("Failed to read properties file", e);
			} finally {
				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (final IOException e) {
						// Ignore
					}
				}
			}
		}

		return properties;
	}

	private final String gitTag;
	private final String gitSHA1;
	private final String version;
	private final String buildDate;

	public BuildInfo() {
		final Properties props = getProperties();
		gitTag = props.getProperty("git-tag");
		gitSHA1 = props.getProperty("git-sha-1");
		version = props.getProperty("version");
		buildDate = props.getProperty("builddate");
	}

	public String getGitTag() {
		return gitTag;
	}

	public String getGitSHA1() {
		return gitSHA1;
	}

	public String getVersion() {
		return version;
	}

	public String getBuildDate() {
		return buildDate;
	}

}