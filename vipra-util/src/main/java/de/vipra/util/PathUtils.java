package de.vipra.util;

import java.io.File;

public class PathUtils {

	public static File appDataDir() {
		final String os = System.getProperty("os.name").toUpperCase();
		File base = null;
		if (os.contains("WIN")) {
			base = new File(System.getProperty("APPDATA"));
		} else if (os.contains("MAC")) {
			base = new File(System.getProperty("user.home") + File.separator + "Library" + File.separator + "ApplicationSupport");
		} else {
			base = new File(System.getProperty("user.home") + File.separator + ".local" + File.separator + "share");
		}
		return base;
	}

	public static File appConfigDir() {
		final String os = System.getProperty("os.name").toUpperCase();
		File base = null;
		if (os.contains("WIN")) {
			base = new File(System.getProperty("APPDATA"));
		} else if (os.contains("MAC")) {
			base = new File(System.getProperty("user.home") + File.separator + "Library" + File.separator + "ApplicationSupport");
		} else {
			base = new File(System.getProperty("user.home") + File.separator + ".config");
		}
		return base;
	}

	public static File tempDir() {
		return new File(System.getProperty("java.io.tmpdir"));
	}

}
