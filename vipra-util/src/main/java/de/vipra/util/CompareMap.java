package de.vipra.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CompareMap<T extends Comparable<T>> {

	private final Map<Object, T> map = new HashMap<>();

	public void add(final T t) {
		final T old = map.get(t);
		if (old == null || t.compareTo(old) > 0)
			map.put(t, t);
	}

	public void addAll(final Collection<T> ts) {
		for (final T t : ts)
			add(t);
	}

	public Collection<T> values() {
		return map.values();
	}

}
