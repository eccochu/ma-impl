package de.vipra.util;

import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;

import java.util.ArrayList;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

public class MongoUtils {

	public static Bson getSorts(final String sortBy) {
		if (sortBy == null)
			return null;
		final String[] sortKeys = sortBy.split(",");
		final ArrayList<Bson> sorts = new ArrayList<>(sortKeys.length);
		for (String sort : sortKeys) {
			if (sort.startsWith("-")) {
				sorts.add(descending(sort.substring(1)));
			} else if (sort.startsWith("+")) {
				sort = sort.substring(1);
			}
			sorts.add(ascending(sort));
		}
		return orderBy(sorts);
	}

	public static ObjectId objectId(final String id) {
		try {
			return new ObjectId(id);
		} catch (final IllegalArgumentException e) {
			return null;
		}
	}

}
