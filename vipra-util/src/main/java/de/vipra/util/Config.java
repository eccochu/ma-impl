package de.vipra.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import de.vipra.util.ex.ConfigException;
import de.vipra.util.model.Model;
import de.vipra.util.model.TopicModelConfig;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.service.MongoService;

public class Config {

	public static final Logger log = LoggerFactory.getLogger(Config.class);
	public static final ObjectMapper mapper = new ObjectMapper();

	public static String configString = null;

	static {
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.enable(Feature.ALLOW_COMMENTS);
	}

	private static Config instance;

	private String dataDirectory = getGenericDataDir().getAbsolutePath();
	private String databaseHost = Constants.DATABASE_HOST;
	private int databasePort = Constants.DATABASE_PORT;
	private String databaseName = Constants.DATABASE_NAME;
	private String elasticSearchHost = Constants.ES_HOST;
	private int elasticSearchPort = Constants.ES_PORT;
	private TopicModelConfig modelConfigTemplate = new TopicModelConfig();
	private String spotlightUrl;
	private String dtmPath;

	@JsonIgnore
	private Map<String, TopicModelConfig> topicModelConfigs;

	public String getDatabaseHost() {
		return databaseHost;
	}

	public void setDatabaseHost(final String databaseHost) {
		this.databaseHost = databaseHost;
	}

	public int getDatabasePort() {
		return databasePort;
	}

	public void setDatabasePort(final int databasePort) {
		this.databasePort = databasePort;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(final String databaseName) {
		this.databaseName = databaseName;
	}

	public String getElasticSearchHost() {
		return elasticSearchHost;
	}

	public void setElasticSearchHost(final String elasticSearchHost) {
		this.elasticSearchHost = elasticSearchHost;
	}

	public int getElasticSearchPort() {
		return elasticSearchPort;
	}

	public void setElasticSearchPort(final int elasticSearchPort) {
		this.elasticSearchPort = elasticSearchPort;
	}

	public boolean isSpotlightEnabled() {
		return spotlightUrl != null && !spotlightUrl.isEmpty();
	}

	public String getSpotlightUrl() {
		return spotlightUrl;
	}

	public void setSpotlightUrl(final String spotlightUrl) {
		this.spotlightUrl = spotlightUrl;
	}

	public String getDtmPath() {
		return dtmPath;
	}

	public void setDtmPath(final String dtmPath) {
		this.dtmPath = dtmPath;
	}

	public Map<String, TopicModelConfig> getTopicModelConfigs() {
		return topicModelConfigs;
	}

	public Collection<TopicModelConfig> getTopicModelConfigs(final String[] names) throws Exception {
		final List<TopicModelConfig> topicModelConfigs = new ArrayList<>(names.length);
		for (final String name : names) {
			if (name.equalsIgnoreCase("all"))
				return this.topicModelConfigs.values();
			topicModelConfigs.add(getTopicModelConfig(name));
		}
		return topicModelConfigs;
	}

	public TopicModelConfig getTopicModelConfig(final String name) throws Exception {
		final TopicModelConfig modelConfig = topicModelConfigs.get(name);
		if (modelConfig == null)
			throw new Exception("unknown model: " + name);
		return modelConfig;
	}

	public void setTopicModelConfig(final TopicModelConfig config) {
		topicModelConfigs.put(config.getName(), config);
	}

	public void deleteTopicModelConfig(final String name) {
		topicModelConfigs.remove(name);
	}

	private void setTopicModelConfigs(final Map<String, TopicModelConfig> topicModelConfigs) {
		this.topicModelConfigs = topicModelConfigs;
	}

	public void clearTopicModelConfigs() {
		topicModelConfigs.clear();
	}

	public void setDataDirectory(final String dataDirectory) {
		this.dataDirectory = dataDirectory;
	}

	public TopicModelConfig getModelConfigTemplate() {
		return modelConfigTemplate;
	}

	public void setModelConfigTemplate(final TopicModelConfig modelConfigTemplate) {
		this.modelConfigTemplate = modelConfigTemplate;
	}

	/**
	 * Returns the data directory used for topic modeling and configuration
	 * storage
	 *
	 * @return the data directory to be used
	 * @throws ConfigException
	 */
	public File getDataDirectory() throws ConfigException {
		final File dataDir;
		if (dataDirectory != null && !dataDirectory.isEmpty())
			dataDir = new File(dataDirectory);
		else
			dataDir = getGenericDataDir();

		if (!dataDir.exists()) {
			if (!dataDir.mkdirs()) {
				throw new ConfigException("could not create data directory: " + dataDir.getAbsolutePath());
			}
		}

		return dataDir;
	}

	/**
	 * Returns a generic data directory, if none is configured.
	 *
	 * @return generic data directory
	 */
	public static File getGenericDataDir() {
		final File base = PathUtils.appDataDir();
		return new File(base, Constants.FILEBASE_DIR);
	}

	/**
	 * Returns a generic config directory, if none is configured.
	 *
	 * @return generic config directory
	 */
	public static File getGenericConfigDir() {
		final File base = PathUtils.appConfigDir();
		return new File(base, Constants.FILEBASE_DIR);
	}

	public static File getGenericStatsDir() {
		final File dir = new File(getGenericLogDir(), "vipra-stats");
		if (!dir.exists())
			dir.mkdirs();
		return dir;
	}

	public static File getGenericLogDir() {
		return PathUtils.tempDir();
	}

	/**
	 * Returns a generic logging file
	 *
	 * @return generic logging file
	 */
	public static File getGenericLogFile() {
		final File base = getGenericLogDir();
		return new File(base, Constants.LOG_FILE);
	}

	/**
	 * Returns a representation of the used mongodb connection
	 *
	 * @return mongo connection
	 * @throws ConfigException
	 */
	public Mongo getMongo() throws ConfigException {
		return Mongo.getInstance(this);
	}

	/**
	 * Create a database service abstraction to interact with the database.
	 *
	 * @param clazz
	 *            the DAO class, extending Model
	 * @return the database service
	 * @throws ConfigException
	 */
	public <Type extends Model<IdType>, IdType> MongoService<Type, IdType> getDatabaseService(final Class<Type> clazz) throws ConfigException {
		return MongoService.getDatabaseService(this, clazz);
	}

	public static Config getConfig() throws ConfigException {
		return getConfig(configString, true);
	}

	public static Config getConfig(final boolean readModels) throws ConfigException {
		return getConfig(configString, readModels);
	}

	public static Config getConfig(final String configStr, final boolean readModels) throws ConfigException {
		if (instance == null) {
			try {
				InputStream in = null;
				final File configDir = getGenericConfigDir();
				if (!configDir.exists()) {
					configDir.mkdirs();
				}

				final List<File> files = new ArrayList<>();

				// custom config file or name
				if (configStr != null) {
					files.addAll(Arrays.asList(new File(configStr), new File(configDir, configStr), new File(configDir, configStr + ".json")));
				}

				// config from environment
				final String configPath = System.getenv("VIPRA_CONFIG");
				if (configPath != null && configPath.length() > 0) {
					files.addAll(Arrays.asList(new File(configPath), new File(configPath, Constants.CONFIG_FILE)));
				}

				// config from generic config dir
				final File configFile = new File(configDir, Constants.CONFIG_FILE);
				files.add(configFile);

				// check locations
				for (final File file : files) {
					if (file.exists() && file.isFile() && file.canRead()) {
						in = new FileInputStream(file);
						break;
					}
				}

				// config from source
				if (in == null) {
					in = FileUtils.getResource(Constants.CONFIG_FILE);
				}

				if (in == null) {
					log.error("config file input stream is null");
					throw new ConfigException("could not find any config file");
				}

				// load config
				final String config = IOUtils.toString(in);

				// write generic config
				if (configDir.exists() && !configFile.exists())
					org.apache.commons.io.FileUtils.write(configFile, config);

				instance = mapper.readValue(config, Config.class);

				if (instance == null)
					throw new ConfigException("could not read configuration");

				if (readModels)
					instance.rereadTopicModelConfigs();
			} catch (final IOException e) {
				throw new ConfigException(e);
			}
		}

		return instance;
	}

	public void rereadTopicModelConfigs() throws ConfigException {
		if (instance == null)
			throw new ConfigException("config not initialized");

		// read topic model configs
		final MongoService<TopicModelFull, ObjectId> dbTopicModels = MongoService.getDatabaseService(instance, TopicModelFull.class);
		final List<TopicModelFull> topicModels = dbTopicModels.getAll("_all");
		final Map<String, TopicModelConfig> topicModelConfigs = new HashMap<>(topicModels.size());
		for (final TopicModelFull topicModel : topicModels)
			topicModelConfigs.put(topicModel.getName(), topicModel.getModelConfig());
		setTopicModelConfigs(topicModelConfigs);
	}

	public String validModelName(String name) throws ConfigException {
		name = name.trim().toLowerCase();
		if (name.equals("all"))
			return "invalid model name: 'all' is a special name and cannot be used.";
		for (final Entry<String, TopicModelConfig> entry : topicModelConfigs.entrySet())
			if (entry.getValue().getName().toLowerCase().equals(name))
				return "model with name '" + name + "' already exists.";
		if (new File(getDataDirectory(), name).exists())
			return "model with name '" + name + "' already exists.";
		return null;
	}

}
