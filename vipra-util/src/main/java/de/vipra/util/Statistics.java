package de.vipra.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class Statistics {

	public static class RunInfo {
		private final String name;
		private final List<Long> times;
		private final Double mean;
		private final Double median;
		private final Long mode;

		public RunInfo(final String name, final Collection<Long> times) {
			this.name = name;
			this.times = new ArrayList<>(times.size());
			try {
				for (final Long time : times)
					this.times.add(time);
			} catch (final Exception e) {
				e.printStackTrace();
			}

			mean = MathUtils.mean(times);
			median = MathUtils.median(this.times);
			mode = MathUtils.mode(this.times);
		}

		public String getName() {
			return name;
		}

		public List<Long> getTimes() {
			return times;
		}

		public Double getMean() {
			return mean;
		}

		public Double getMedian() {
			return median;
		}

		public Long getMode() {
			return mode;
		}
	}

	private long startTime;
	private long stopTime;
	private long runTime;
	private List<RunInfo> runInfo;

	@JsonIgnore
	private final MultiMap<String, Long> times = new MultiMap<>(true);

	@JsonIgnore
	private final Map<String, Long> running = new HashMap<>();

	public Statistics() {
		begin();
	}

	public String name() {
		return "run-" + startTime;
	}

	public void begin() {
		startTime = System.nanoTime();
	}

	public void finish(final String path) throws JsonGenerationException, JsonMappingException, IOException {
		stopAll();
		stopTime = System.nanoTime();
		runTime = stopTime - startTime;
		generateStats();
		save(path);
	}

	public void generateStats() {
		runInfo = new ArrayList<>();
		for (final Map.Entry<String, Collection<Long>> entry : times.entrySet()) {
			runInfo.add(new RunInfo(entry.getKey(), entry.getValue()));
		}
	}

	public void save(final String path) throws JsonGenerationException, JsonMappingException, IOException {
		File target;
		if (path != null && !path.isEmpty()) {
			final File file = new File(path);
			if (file.exists()) {
				if (file.isDirectory())
					target = new File(file, name() + ".json");
				else
					target = file;
			} else {
				target = file;
			}
		} else {
			target = new File(Config.getGenericStatsDir(), name() + ".json");
		}
		Config.mapper.writeValue(target, this);
	}

	public void start(final String name) {
		running.put(name, System.nanoTime());
	}

	public void stop(final String name) {
		final long curr = System.nanoTime();
		final Long start = running.remove(name);
		if (start != null) {
			times.put(name, curr - start);
		}
	}

	public void stopAll() {
		final long curr = System.nanoTime();
		for (final Map.Entry<String, Long> run : running.entrySet())
			times.put(run.getKey(), curr - run.getValue());
		running.clear();
	}

	public void stop(final String... names) {
		final long curr = System.nanoTime();
		for (final String name : names) {
			final Long v = running.remove(name);
			if (v != null)
				times.put(name, curr - v);
		}
	}

	public void next(final String stopName, final String startName) {
		final long curr = System.nanoTime();
		final Long start = running.remove(stopName);
		if (start != null) {
			times.put(stopName, curr - start);
		}

		running.put(startName, curr);
	}

	/*
	 * getters
	 */

	public long getStartTime() {
		return startTime;
	}

	public long getStopTime() {
		return stopTime;
	}

	public long getRunTime() {
		return runTime;
	}

	public List<RunInfo> getRunInfo() {
		return runInfo;
	}

}
