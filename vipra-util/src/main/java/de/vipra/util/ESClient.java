package de.vipra.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

/**
 * ElasticSearch client to generate elastisearch transport client connections.
 */
public abstract class ESClient {

	private static TransportClient client;

	/**
	 * get a elasticsearch transport client
	 *
	 * @param config
	 *            application configuration singleton
	 * @return elasticsearch transport client
	 * @throws UnknownHostException
	 */
	public static TransportClient getClient(final Config config) throws UnknownHostException {
		if (client == null) {
			client = TransportClient.builder().build().addTransportAddress(
					new InetSocketTransportAddress(InetAddress.getByName(config.getElasticSearchHost()), config.getElasticSearchPort()));
		}
		return client;
	}

}
