package de.vipra.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import de.vipra.util.an.ElasticIndex;

public class ESSerializer<T> {

	private final Set<Entry<String, Field>> fields;
	private final Set<Entry<String, Method>> methods;

	public ESSerializer(final Class<T> clazz) {
		final Map<String, Field> foundFields = new HashMap<>();
		final Map<String, Method> foundMethods = new HashMap<>();

		for (final Field field : clazz.getDeclaredFields()) {
			if (!Modifier.isStatic(field.getModifiers())) {
				if (!field.isAccessible())
					field.setAccessible(true);

				final ElasticIndex ei = field.getDeclaredAnnotation(ElasticIndex.class);
				if (ei == null)
					continue;

				foundFields.put(ei.value(), field);
			}
		}

		for (final Method method : clazz.getDeclaredMethods()) {
			if (!Modifier.isStatic(method.getModifiers()) && method.getParameterCount() == 0) {
				if (!method.isAccessible())
					method.setAccessible(true);

				final ElasticIndex ei = method.getDeclaredAnnotation(ElasticIndex.class);
				if (ei == null)
					continue;

				foundMethods.put(ei.value(), method);
			}
		}

		this.fields = foundFields.entrySet();
		this.methods = foundMethods.entrySet();
	}

	public Map<String, Object> serialize(final T t) {
		final Map<String, Object> values = new HashMap<>();
		for (final Entry<String, Field> e : fields) {
			try {
				values.put(e.getKey(), e.getValue().get(t));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				throw new RuntimeException("could not serialize field value " + e.getKey(), e1);
			}
		}
		for (final Entry<String, Method> m : methods) {
			try {
				values.put(m.getKey(), m.getValue().invoke(t));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
				throw new RuntimeException("could not serialize method value " + m.getKey(), e1);
			}
		}
		return values;
	}

}
