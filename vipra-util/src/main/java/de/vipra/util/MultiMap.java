package de.vipra.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Supplier;

public class MultiMap<K, V> {

	private final Map<K, Collection<V>> map;
	private final Supplier<Collection<V>> supplier;

	public MultiMap() {
		this(false);
	}

	public MultiMap(final boolean duplicates) {
		if (duplicates)
			supplier = () -> new ArrayList<>();
		else
			supplier = () -> new HashSet<>();
		this.map = new HashMap<>();
	}

	public MultiMap(final Supplier<Collection<V>> supplier) {
		this.supplier = supplier;
		this.map = new HashMap<>();
	}

	public void put(final K key, final V value) {
		Collection<V> c = map.get(key);
		if (c == null)
			c = supplier.get();
		c.add(value);
		map.put(key, c);
	}

	public void put(final K key, final Collection<V> values) {
		Collection<V> c = map.get(key);
		if (c == null)
			c = supplier.get();
		c.addAll(values);
		map.put(key, c);
	}

	public Collection<V> get(final K key) {
		return map.get(key);
	}

	public Set<Entry<K, Collection<V>>> entrySet() {
		return map.entrySet();
	}

	public int size() {
		return map.size();
	}

}
