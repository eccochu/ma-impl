package de.vipra.util;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Nested map. This map is a rough implementation of JSON objects, featuring map
 * nesting. Currently not supporting array notation. Can work with arbitrary
 * field delimiters.
 *
 * Default field delimiter is . (dot). Accessing fields is the same as in JSON:
 * some.nested.object. Array notation is not implemented: some.nested[3].array
 */
public class NestedMap {

	private String delimiter = Pattern.quote(".");
	private Map<String, Object> map = new HashMap<>();

	public NestedMap() {}

	public NestedMap(final String delimiter) {
		this.delimiter = Pattern.quote(delimiter);
	}

	@JsonCreator
	public NestedMap(final Map<String, Object> map) {
		this.map = map;
	}

	@JsonValue
	public Map<String, Object> getMap() {
		return map;
	}

	@SuppressWarnings("unchecked")
	public void put(final String key, final Object value) {
		Map<String, Object> current = map;
		final String[] parts = key.split(delimiter);
		final String name = parts.length > 0 ? parts[parts.length - 1] : key;

		for (int i = 0; i < parts.length - 1; i++) {
			final Object o = current.get(parts[i]);
			if (o == null || !(o instanceof Map)) {
				final HashMap<String, Object> newMap = new HashMap<>();
				current.put(parts[i], newMap);
				current = newMap;
			} else {
				current = (Map<String, Object>) o;
			}
		}

		current.put(name, value);
	}

}
