#!/bin/bash

#######################################################################################
# CONFIGURATION

# enable/disable (1/0) to clean before building
CLEAN=0

# enable/disable (1/0) project building
BUILD_DTM=1
BUILD_VIPRA_UTIL=1
BUILD_VIPRA_CMD=1
BUILD_VIPRA_UI=1
BUILD_VIPRA_BACKEND=1

# set to 1 to deploy backend after build completed
DEPLOY_BACKEND_AFTER_BUILD=1
# copy backend to docker
DEPLOY_BACKEND_TO_DOCKER=1
# copy backend to vagrant
DEPLOY_BACKEND_TO_VAGRANT=0
# copy exploded WAR instead of *.war file
DEPLOY_BACKEND_EXPLODED_WAR=1
# copy to ROOT, overwrite context path
DEPLOY_BACKEND_TO_ROOT=1

# set to 1 to deploy frontend after build completed
DEPLOY_FRONTEND_AFTER_BUILD=1
# copy frontend to docker
DEPLOY_FRONTEND_TO_DOCKER=1
# copy frontend to vagrant
DEPLOY_FRONTEND_TO_VAGRANT=0

#######################################################################################

DIR="$(dirname "$(readlink -f "$0")")"
VERSION=$(git describe)
VERSION_LONG=$(git describe --long)

WAR_FILE="vipra.war"
VIPRA_BACKEND="$DIR/vipra-backend"
VIPRA_CMD="$DIR/vipra-cmd"
VIPRA_UI="$DIR/vipra-ui"
VIPRA_UTIL="$DIR/vipra-util"
DOCKER_WEBAPPS="$DIR/docker/webapps"
VAGRANT_WEBAPPS="$DIR/vm/webapps"
DOCKER_WEBROOT="$DIR/docker/webroot"
VAGRANT_WEBROOT="$DIR/vm/webroot"
WAR_PATH="$DIR/vipra-backend/target/$WAR_FILE"
EXPLODED_WAR_PATH="$DIR/vipra-backend/target/vipra"
UI_PATH="$DIR/vipra-ui/public"
LOG="$DIR/build.log"
rm -f $LOG

if [ $# -gt 0 ]; then
        BUILD_DTM=0
        BUILD_VIPRA_UTIL=0
        BUILD_VIPRA_CMD=0
        BUILD_VIPRA_UI=0
        BUILD_VIPRA_BACKEND=0

        while test $# -gt 0
        do
                case "$1" in
                        dtm) BUILD_DTM=1
                        ;;
                        util) BUILD_VIPRA_UTIL=1
                        ;;
                        cmd) BUILD_VIPRA_CMD=1
                        ;;
                        ui) BUILD_VIPRA_UI=1
                        ;;
                        backend) BUILD_VIPRA_BACKEND=1
                        ;;
                        clean) CLEAN=1
                        ;;
                esac
                shift
        done
fi

if [ $CLEAN -eq 1 ]; then
        rm -rf $VIPRA_BACKEND/target
        rm -rf $VIPRA_CMD/target
        rm -rf $VIPRA_UI/public
        rm -rf $VIPRA_UTIL/target
        rm -rf $DOCKER_WEBAPPS/*
        rm -rf $DOCKER_WEBROOT/*
        rm -rf $VAGRANT_WEBAPPS/*
        rm -rf $VAGRANT_WEBROOT/*
        touch $DOCKER_WEBAPPS/.gitkeep
        touch $DOCKER_WEBROOT/.gitkeep
        touch $VAGRANT_WEBAPPS/.gitkeep
        touch $VAGRANT_WEBROOT/.gitkeep
fi

# check commands

if [ $BUILD_DTM -eq 1 ]; then
        MAKE=$(command -v make 2>/dev/null) || { echo >&2 "make not found"; exit 1; }
fi

if [ $BUILD_VIPRA_UTIL -eq 1 ] || [ $BUILD_VIPRA_CMD -eq 1 ] || [ $BUILD_VIPRA_BACKEND -eq 1 ]; then
        JAVA=$(command -v java 2>/dev/null) || { echo >&2 "java not found"; exit 1; }
        MVN=$(command -v mvn 2>/dev/null) || { echo >&2 "mvn not found"; exit 1; }
fi

if [ $BUILD_VIPRA_UI -eq 1 ]; then
        NODE=$(command -v node 2>/dev/null) || { echo >&2 "node not found"; exit 1; }
        NPM=$(command -v npm 2>/dev/null) || { echo >&2 "npm not found"; exit 1; }
        BOWER=$(command -v bower 2>/dev/null) || { echo >&2 "bower not found"; exit 1; }
        GULP=$(command -v gulp 2>/dev/null) || { echo >&2 "gulp not found"; exit 1; }
fi

# insert build number

echo "git.tag=$VERSION" > $DIR/build.properties
echo "git.longtag=$VERSION_LONG" >> $DIR/build.properties

# build dtm

if [ $BUILD_DTM -eq 1 ]; then
        echo "compiling dtm" | tee -a $LOG
        cd $DIR/dtm_release/dtm
        make >> $LOG 2>&1
        if [ $? -ne 0 ]; then
                echo "error"
                exit 1
        fi
        cd $DIR
fi

# build vipra-util

if [ $BUILD_VIPRA_UTIL -eq 1 ]; then
        echo "compiling vipra-util" | tee -a $LOG
        $MVN -f $DIR/vipra-util/pom.xml install >> $LOG 2>&1
        if [ $? -ne 0 ]; then
        	echo "error"
        	exit 1
        fi
fi

# build vipra-cmd

if [ $BUILD_VIPRA_CMD -eq 1 ]; then
        echo "compiling vipra-cmd" | tee -a $LOG
        $MVN -f $DIR/vipra-cmd/pom.xml package >> $LOG 2>&1
        if [ $? -ne 0 ]; then
                echo "error"
                exit 1
        fi
fi

# build vipra-ui

if [ $BUILD_VIPRA_UI -eq 1 ]; then
        echo "compiling vipra-ui" | tee -a $LOG
        cd $DIR/vipra-ui
        ./build.sh >> $LOG 2>&1
        if [ $? -ne 0 ]; then
                echo "error"
                exit 1
        fi
        cd $DIR

        SRC=$UI_PATH/*

        if [ $DEPLOY_FRONTEND_TO_DOCKER -eq 1 ]; then
                rm -rf $DOCKER_WEBROOT/*
                cp -r $SRC $DOCKER_WEBROOT
                touch $DOCKER_WEBROOT/.gitkeep
        fi

        if [ $DEPLOY_FRONTEND_TO_VAGRANT -eq 1 ]; then
                rm -rf $VAGRANT_WEBROOT/*
                cp -r $SRC $VAGRANT_WEBROOT
                touch $VAGRANT_WEBROOT/.gitkeep
        fi
fi

# build vipra-backend

if [ $BUILD_VIPRA_BACKEND -eq 1 ]; then
        echo "compiling vipra-backend" | tee -a $LOG
        $MVN -f $DIR/vipra-backend/pom.xml package >> $LOG 2>&1
        $MVN -f $DIR/vipra-backend/pom.xml compile war:exploded >> $LOG 2>&1
        if [ $? -ne 0 ]; then
                echo "error"
                exit 1
        fi

        SRC=$WAR_PATH
        TARGET=$WAR_FILE
        
        if [ $DEPLOY_BACKEND_TO_ROOT -eq 1 ]; then
                TARGET="ROOT.war"
        fi

        if [ $DEPLOY_BACKEND_EXPLODED_WAR -eq 1 ]; then
                SRC=$EXPLODED_WAR_PATH
                TARGET="vipra"
                if [ $DEPLOY_BACKEND_TO_ROOT -eq 1 ]; then
                        TARGET="ROOT"
                fi
        fi

        if [ $DEPLOY_BACKEND_TO_DOCKER -eq 1 ]; then
                rm -rf $DOCKER_WEBAPPS/*
                cp -r $SRC $DOCKER_WEBAPPS/$TARGET
                touch $DOCKER_WEBAPPS/.gitkeep
        fi

        if [ $DEPLOY_BACKEND_TO_VAGRANT -eq 1 ]; then
                rm -rf $VAGRANT_WEBAPPS/*
                cp -r $SRC $VAGRANT_WEBAPPS/$TARGET
                touch $VAGRANT_WEBAPPS/.gitkeep
        fi
fi

echo "complete"
exit $?