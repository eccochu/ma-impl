#!/bin/bash

DIR="$(dirname "$(readlink -f "$0")")"

cd $DIR

sudo rm -rf /opt/vipra
sudo mkdir -p /opt/vipra
sudo cp -r ./* /opt/vipra/
sudo ln -sf /opt/vipra/vipra /bin/vipra

cd $OLDPWD

echo "vipra installed to /opt/vipra"
echo "use 'vipra' command to access vipra, test system with 'vipra -t'"
echo "REQUIRED: install libgsl-dev and manually compile dtm: 'sudo make -C /opt/vipra/dtm_release/dtm'"