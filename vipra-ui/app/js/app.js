/******************************************************************************
 * Vipra Application
 * Main application file
 ******************************************************************************/
/* globals angular, $, Alerter */
(function() {

  "use strict";

  var app = angular.module('vipra.app', [
    'ngResource',
    'ngSanitize',
    'ngAnimate',
    'ui.router',
    'cfp.hotkeys',
    'angulartics',
    'angulartics.google.analytics',
    'angular-loading-bar',
    'vipra.controllers',
    'vipra.directives',
    'vipra.factories',
    'vipra.templates'
  ]);

  app.config(['$locationProvider', '$stateProvider', '$urlRouterProvider', '$httpProvider', 'cfpLoadingBarProvider',
    function($locationProvider, $stateProvider, $urlRouterProvider, $httpProvider, cfpLoadingBarProvider) {

      $locationProvider.html5Mode(true);
      $urlRouterProvider.otherwise('/');

      // states

      $stateProvider.state('index', {
        url: '/?q',
        templateUrl: 'html/index.html',
        controller: 'IndexController',
        reloadOnSearch: false
      });

      $stateProvider.state('about', {
        url: '/about',
        templateUrl: 'html/about.html',
        controller: 'AboutController'
      });

      $stateProvider.state('network', {
        url: '/network?type&id',
        templateUrl: 'html/network.html',
        controller: 'NetworkController'
      });

      $stateProvider.state('explorer', {
        url: '/explorer',
        templateUrl: 'html/explorer.html',
        controller: 'ExplorerController'
      });

      // states: articles

      $stateProvider.state('articles', {
        url: '/articles',
        templateUrl: 'html/articles/index.html',
        controller: 'ArticlesIndexController',
        reloadOnSearch: false
      });

      $stateProvider.state('articles.show', {
        url: '/:id',
        templateUrl: 'html/articles/show.html',
        controller: 'ArticlesShowController'
      });

      $stateProvider.state('articles.show.entities', {
        url: '/entities',
        templateUrl: 'html/articles/entities.html',
        controller: 'ArticlesEntitiesController'
      });

      $stateProvider.state('articles.show.words', {
        url: '/words',
        templateUrl: 'html/articles/words.html',
        controller: 'ArticlesWordsController'
      });

      // states: topics

      $stateProvider.state('topics', {
        url: '/topics',
        templateUrl: 'html/topics/index.html',
        controller: 'TopicsIndexController',
        reloadOnSearch: false
      });

      $stateProvider.state('topics.show', {
        url: '/:id',
        templateUrl: 'html/topics/show.html',
        controller: 'TopicsShowController'
      });

      $stateProvider.state('topics.show.sequences', {
        url: '/sequences',
        templateUrl: 'html/topics/sequences.html',
        controller: 'TopicsSequencesController'
      });

      $stateProvider.state('topics.show.articles', {
        url: '/articles',
        templateUrl: 'html/topics/articles.html',
        controller: 'TopicsArticlesController',
        reloadOnSearch: false
      });

      // states: words

      $stateProvider.state('words', {
        url: '/words',
        templateUrl: 'html/words/index.html',
        controller: 'WordsIndexController',
        reloadOnSearch: false
      });

      $stateProvider.state('words.show', {
        url: '/:id',
        templateUrl: 'html/words/show.html',
        controller: 'WordsShowController'
      });

      $stateProvider.state('words.show.topics', {
        url: '/topics',
        templateUrl: 'html/words/topics.html',
        controller: 'WordsTopicsController'
      });

      $stateProvider.state('words.show.articles', {
        url: '/articles',
        templateUrl: 'html/words/articles.html',
        controller: 'WordsArticlesController'
      });

      // states: entities

      $stateProvider.state('entities', {
        url: '/entities',
        templateUrl: 'html/entities/index.html',
        controller: 'EntitiesIndexController',
        reloadOnSearch: false
      });

      $stateProvider.state('entities.show', {
        url: '/:id',
        templateUrl: 'html/entities/show.html',
        controller: 'EntitiesShowController'
      });

      $stateProvider.state('entities.show.articles', {
        url: '/articles',
        templateUrl: 'html/entities/articles.html',
        controller: 'EntitiesArticlesController',
        reloadOnSearch: false
      });

      // states: slides

      $stateProvider.state('slides', {
        url: '/slides',
        templateUrl: 'html/slides.html',
        controller: 'SlidesController'
      });

      // states: errors

      $stateProvider.state('error', {
        url: '/{code:[4-5][0-9]{2}}',
        templateUrl: 'html/error.html',
        controller: 'ErrorsController'
      });

      // http interceptors

      $httpProvider.interceptors.push(function($q, $rootScope) {
        var requestIncrement = function(config) {
          $rootScope.loading.requests = ++$rootScope.loading.requests || 1;
          $rootScope.loading[config.method] = ++$rootScope.loading[config.method] || 1;
          $rootScope.loading.any = true;
        };

        var requestDecrement = function(config) {
          $rootScope.loading.requests = Math.max(--$rootScope.loading.requests, 0);
          $rootScope.loading[config.method] = Math.max(--$rootScope.loading[config.method], 0);
          $rootScope.loading.any = $rootScope.loading.requests > 0;
        };

        return {
          request: function(config) {
            requestIncrement(config);
            return config;
          },

          requestError: function(rejection) {
            requestDecrement(rejection.config);
            return $q.reject(rejection);
          },

          response: function(response) {
            requestDecrement(response.config);
            return response;
          },

          responseError: function(rejection) {
            requestDecrement(rejection.config);
            if(rejection.status === -1) {
              // backend unavailable
              $rootScope.fatal = 'Backend unavailable';
            } else if (rejection.data) {
              if (angular.isArray(rejection.data)) {
                for (var i = 0; i < rejection.data.length; i++) {
                  var rd = rejection.data[i];
                  Alerter.showDanger(rd.title, rd.detail);
                }
              } else {
                Alerter.showDanger(rejection.data.title, rejection.data.detail);
              }
            }
            return $q.reject(rejection);
          }
        };
      });

      Alerter.setSettings({
        prepend: false,
        merge: true
      }, {
        fadeAfter: 10000
      });

      // disable loading bar spinner
      cfpLoadingBarProvider.includeSpinner = false;
    }
  ]);

  app.run(['$rootScope', '$state', '$interval', function($rootScope, $state, $interval) {

    $rootScope.loading = {};

    $rootScope.Vipra = window.Vipra;

    $rootScope.error = function(code) {
      $state.transitionTo('error', {
        code: code
      }, {
        location: 'replace'
      });
    };

    $rootScope.$on('$stateChangeError', function() {
      $rootScope.error(404);
    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState) {
      $rootScope.oldState = fromState;
      $rootScope.state = toState;
      Alerter.removeAlerts();
      window.scrollTo(0,0);
    });

    $rootScope.$on('$stateChangeStart', function() {
      $rootScope.alerts = [];
    });

    // http://www.mograblog.com/2015/10/angular-improve-website-performance.html
    // this cancels intervals on scope destroy
    var ScopeProt = Object.getPrototypeOf($rootScope);  
    ScopeProt.$interval = function(func, time){  
      var timer = $interval(func,time);  
      this.on('$destroy', function(){ $timeout.cancel(timer); });  
      return timer;  
    };  
  }]);

  $(document).on('change', '.btn-file :file', function() {
    var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  $(document).on('click','.navbar-collapse.in',function(e) {
    if($(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle') {
      $(this).collapse('hide');
    }
  });

  $(document).on('click', '#quicksearch', function() {
    if($(this).hasClass('search-collapsed'))
      $(this).removeClass('search-collapsed');
  });

  $(document).on('blur', '#quicksearch', function(e) {
    if(!this.value) {
      $(this).addClass('search-collapsed');
    }
  });

  // hold onto the drop down menu                                             
  var dropdownMenu;

  // and when you show it, move it to the body                                     
  $(window).on('show.bs.dropdown', function (e) {

    // grab the menu        
    dropdownMenu = $(e.target).find('.dropdown-menu');

    // detach it and append it to the body
    $('body').append(dropdownMenu.detach());

    // grab the new offset position
    var eTarget = $(e.target);
    var eOffset = eTarget.offset();
    var css = {
      'display': 'block',
      'top': eOffset.top + eTarget.outerHeight(),
      'left': eOffset.left,
      'right': 'auto'
    };

    if(dropdownMenu.hasClass('pull-left')) {
      css.left = 'auto';
      css.right = $(window).width() - (eOffset.left + eTarget.outerWidth());
    }

    // make sure to place it where it would normally go (this could be improved)
    dropdownMenu.css(css);
  });

  // and when you hide it, reattach the drop down, and hide it normally                                                   
  $(window).on('hide.bs.dropdown', function (e) {
    $(e.target).append(dropdownMenu.detach());
    dropdownMenu.hide();
  });

  $(window).on('resize', function() {
    if(dropdownMenu)
      dropdownMenu.hide();
  });

  $(document).ready(function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
      var input = $(this).parents('.input-group').find(':text'),
        log = numFiles > 1 ? numFiles + ' files selected' : label;

      if (input.length) {
        input.val(log);
      }
    });
  });

})();