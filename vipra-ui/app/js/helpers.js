/******************************************************************************
 * Vipra Application
 * Helpers & Polyfills
 ******************************************************************************/
/* globals Vipra, moment */
(function() {

  "use strict";

  window.Vipra = window.Vipra || {};

  /**
   * Helpers
   */

  Vipra.formatDate = function(date) {
    return date ? new Date(date).toLocaleDateString() : '';
  };

  Vipra.formatTime = function(date) {
    return date ? new Date(date).toLocaleTimeString() : '';
  };

  Vipra.formatDateTime = function(date) {
    return date ? new Date(date).toLocaleString() : '';
  };

  Vipra.toPercent = function(input, nums) {
    if (typeof input === 'undefined' || isNaN(input))
      return;
    if (typeof input !== 'number')
      input = parseInt(input, 10);
    return (input * 100).toFixed(isNaN(nums) ? 0 : nums);
  };

  Vipra.createInitial = function(text) {
    return '<span class="initial">' + text.substring(0, 1) + "</span>" + text.substring(1);
  };

  Vipra.randomId = function() {
    return 'id' + Math.random().toString(36).substring(7);
  };

  Vipra.windowLabel = function(date, res) {
    date = moment(date);
    var parts = [];
    switch (res) {
      case 'SECOND':
        parts.push(Vipra.zeroPad(date.second()));
        /* falls through */
      case 'MINUTE':
        parts.push(':');
        parts.push(Vipra.zeroPad(date.minute()));
        /* falls through */
      case 'HOUR':
        parts.push(':');
        parts.push(Vipra.zeroPad(date.hour()));
        /* falls through */
      case 'DAY':
        parts.push(' ');
        parts.push(Vipra.zeroPad(date.day() + 1));
        /* falls through */
      case 'MONTH':
        parts.push('-');
        parts.push(Vipra.zeroPad(date.month() + 1));
        /* falls through */
      case 'YEAR':
        parts.push('-');
        parts.push(date.year());
        break;
      case 'WEEK':
        parts.push('W' + date.week());
        parts.push(' ');
        parts.push(date.year());
        break;
      case 'SEMESTER':
        parts.push('S' + date.month() < 6 ? 1 : 2);
        parts.push(' ');
        parts.push(date.year());
        break;
      case 'TRIMESTER':
        var month = date.month();
        parts.push('T' + month < 4 ? 1 : month < 8 ? 2 : 3);
        parts.push(' ');
        parts.push(date.year());
        break;
      case 'QUARTER':
        parts.push('Q' + date.quarter());
        parts.push(' ');
        parts.push(date.year());
    }
    var rtrim = /^[\s\uFEFF\xA0:-]+|[\s\uFEFF\xA0:-]+$/g;
    return parts.reverse().join('').replace(rtrim, '');
  };

  Vipra.zeroPad = function(n) {
    return n < 10 ? '0' + n : n;
  };

  Vipra.statusMsg = function(status) {
    switch (parseInt(status)) {
      case 400:
        return 'Bad Request';
      case 401:
        return 'Unauthorized';
      case 402:
        return 'Payment Required';
      case 403:
        return 'Forbidden';
      case 404:
        return 'Not Found';
      case 405:
        return 'Method Not Allowed';
      case 406:
        return 'Not Acceptable';
      case 407:
        return 'Proxy Authentication Required';
      case 408:
        return 'Request Time-out';
      case 409:
        return 'Conflict';
      case 410:
        return 'Gone';
      case 411:
        return 'Length Required';
      case 412:
        return 'Precondition Failed';
      case 413:
        return 'Request Entity Too Large';
      case 414:
        return 'Request-URL Too Long';
      case 415:
        return 'Unsupported Media Type';
      case 416:
        return 'Requested range not satisfiable';
      case 417:
        return 'Expectation Failed';
      case 418:
        return 'I’m a teapot';
      case 420:
        return 'Policy Not Fulfilled';
      case 421:
        return 'Misdirected Request';
      case 422:
        return 'Unprocessable Entity';
      case 423:
        return 'Locked';
      case 424:
        return 'Failed Dependency';
      case 425:
        return 'Unordered Collection';
      case 426:
        return 'Upgrade Required';
      case 428:
        return 'Precondition Required';
      case 429:
        return 'Too Many Requests';
      case 431:
        return 'Request Header Fields Too Large';
      case 451:
        return 'Unavailable For Legal Reasons';
      case 500:
        return 'Internal Server Error';
      case 501:
        return 'Not Implemented';
      case 502:
        return 'Bad Gateway';
      case 503:
        return 'Service Unavailable';
      case 504:
        return 'Gateway Time-out';
      case 505:
        return 'HTTP Version not supported';
      case 506:
        return 'Variant Also Negotiates';
      case 508:
        return 'Loop Detected';
      case 509:
        return 'Bandwidth Limit Exceeded';
      case 510:
        return 'Not Extended';
      case 511:
        return 'Network Authentication Required';
      default:
        return 'Unknown Error Code';
    }
  };

  Vipra.joinResources = function(resources) {
    if(resources && resources.length) {
      var res = [];
      for(var i = 0; i < resources.length; i++)
        res.push('<a href="http://dbpedia.org/resource/' + resources[i] + '">' + resources[i] + '</a>');
      return res.join(', ');
    }
    return 'No types';
  }

  /**
   * Polyfills
   */

  if (typeof String.prototype.multiline === 'undefined')
    String.prototype.multiline = function(max) {
      return this.split(new RegExp("((?:\\w+ ){" + max + "})", "g")).filter(Boolean).join("\n");
    };

  if (typeof String.prototype.ellipsize === 'undefined')
    String.prototype.ellipsize = function(max) {
      return this.length <= max ? this : this.substring(0, max) + '...';
    };

  if (typeof String.prototype.startsWith === 'undefined')
    String.prototype.startsWith = function(start) {
      return this.lastIndexOf(start, 0) === 0;
    };

  if (typeof window.console === 'undefined')
    window.console = {
      log: function() {}
    };

})();