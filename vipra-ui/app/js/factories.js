/******************************************************************************
 * Vipra Application
 * Factories
 ******************************************************************************/
/* globals angular, Vipra, $ */
(function() {

  "use strict";

  var app = angular.module('vipra.factories', []);

  app.factory('$myResource', ['$resource', function($resource) {
    return function(url, paramDefaults, actions) {
      actions = angular.merge({}, {
        get: {
          method: 'GET',
          cache: true
        },
        save: {
          method: 'POST'
        },
        query: {
          method: 'GET',
          isArray: true,
          cache: true
        },
        remove: {
          method: 'DELETE'
        },
        delete: {
          method: 'DELETE'
        },
        update: {
          method: 'PUT'
        }
      }, actions);
      return $resource(url, paramDefaults, actions);
    };
  }]);

  app.factory('ArticleFactory', ['$myResource', function($myResource) {
    return $myResource(Vipra.config.restUrl + '/articles/:id', {}, {
      queryPOST: {
        method: 'POST',
        isArray: true,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        transformRequest: function (data) {
          var str = [];
          for (var d in data)
            if(data[d] !== undefined && data[d] !== null) {
              if(angular.isArray(data[d])) {
                for(var i = 0; i < data[d].length; i++)
                  str.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d][i]));
              } else {
                str.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
              }
            }
          return str.join('&');
        }
      }
    });
  }]);

  app.factory('TopicFactory', ['$myResource', function($myResource) {
    return $myResource(Vipra.config.restUrl + '/topics/:id', {}, {
      articles: {
        cache: true,
        isArray: true,
        url: Vipra.config.restUrl + '/topics/:id/articles'
      }
    });
  }]);

  app.factory('SequenceFactory', ['$myResource', function($myResource) {
    return $myResource(Vipra.config.restUrl + '/sequences/:id');
  }]);

  app.factory('SearchFactory', ['$myResource', function($myResource) {
    return $myResource(Vipra.config.restUrl + '/search', {}, {
      query: {
        cache: false
      }
    });
  }]);

  app.factory('InfoFactory', ['$myResource', function($myResource) {
    return $myResource(Vipra.config.restUrl + '/info');
  }]);

  app.factory('TopicModelFactory', ['$myResource', function($myResource) {
    return $myResource(Vipra.config.restUrl + '/topicmodels/:id', {}, {
      query: {
        cache: false
      }
    });
  }]);

  app.factory('WordFactory', ['$myResource', function($myResource) {
    return $myResource(Vipra.config.restUrl + '/words/:id');
  }]);

  app.factory('EntityFactory', ['$myResource', function($myResource) {
    return $myResource(Vipra.config.restUrl + '/entities/:id');
  }]);

  app.factory('WindowFactory', ['$myResource', function($myResource) {
    return $myResource(Vipra.config.restUrl + '/windows/:id');
  }]);

})();