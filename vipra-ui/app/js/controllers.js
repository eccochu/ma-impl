/******************************************************************************
 * Vipra Application
 * Controllers
 ******************************************************************************/
/* globals angular, Vipra, moment, vis, prompt, randomColor, Highcharts, $ */
(function() {

  "use strict";

  var app = angular.module('vipra.controllers', []);

  app.controller('RootController', ['$scope', '$rootScope', '$q', '$state', '$window', '$timeout', 'hotkeys', 'TopicModelFactory',
    function($scope, $rootScope, $q, $state, $window, $timeout, hotkeys, TopicModelFactory) {

      $scope.rootModels = {
        topicModel: null,
        search: null,
        title: 'Home',
        helpEnabled: false
      };

      var prevTopicModelDefer = $q.defer();

      $scope.rootModels.tmLoading = prevTopicModelDefer.promise;

      if (localStorage.tm) {
        $rootScope.loadingScreen = true;

        TopicModelFactory.get({
          id: localStorage.tm
        }, function(data) {
          $scope.rootModels.topicModel = data;
          $scope.loadingScreen = false;
          prevTopicModelDefer.resolve();
        }, function() {
          delete localStorage.tm;
          $scope.loadingScreen = false;
          prevTopicModelDefer.reject();
          $scope.chooseTopicModel();
        });
      } else {
        prevTopicModelDefer.reject();
      }

      $scope.queryTopicModels = function() {
        TopicModelFactory.query({
          fields: '_all'
        }, function(data) {
          var groups = {};
          var models = [];
          for(var i = 0; i < data.length; i++) {
            data[i].lastGeneratedString = data[i].lastGenerated ? Vipra.formatDateTime(data[i].lastGenerated) : 'never';
            data[i].lastIndexedString = data[i].lastIndexed ? Vipra.formatDateTime(data[i].lastIndexed) : 'never';
            if(data[i].group) {
              if(!groups.hasOwnProperty(data[i].group))
                groups[data[i].group] = [data[i]];
              else
                groups[data[i].group].push(data[i]);
            } else {
              models.push(data[i]);
            }
          }
          $scope.groups = groups;
          $scope.topicModels = models;
          $scope.topicModelsCount = data.length;
        });
      };

      $scope.chooseTopicModel = function(manual) {
        var fn = function() {
          $scope.queryTopicModels();
          $scope.rootModels.topicModelModalOpen = true;
          $('#topicModelModal').modal();
          if ($scope.rootModels.topicModel)
            $('.selected-model').focus();
          else
            $('.topic-model').first().focus();
        };
        if(manual)
          $scope.rootModels.tmLoading.finally(fn);
        else
          $scope.rootModels.tmLoading.then(null, fn);
      };

      $scope.changeTopicModel = function(topicModel) {
        $scope.rootModels.topicModel = topicModel;
        $('#topicModelModal').modal('hide');
        localStorage.tm = topicModel.id;
        if($state.current.name != 'index')
          $state.transitionTo('index');
      };

      $scope.showFeedbackModal = function() {
        $('#feedbackModal').modal();
        $('#feedbackIframe').attr('src', 'https://docs.google.com/forms/d/1RjXyGgw8F3v7QsfgOQKz0Koa2Dkr1wDNx3veRZr9I3o/viewform?embedded=true');
      };

      $scope.menubarSearch = function(query) {
        $state.transitionTo('index', {
          q: query
        });
      };

      $scope.checkTopicModel = function(state, callback) {
        if($scope.rootModels.topicModel)
          callback();
        else {
          $scope.rootModels.tmLoading.then(function() {
            callback();
          }, function() {
            $state.transitionTo('index');
          });
        }
      };

      $scope.goBack = function() {
        $window.history.back();
      };

      $scope.$on('$stateChangeSuccess', function() {
        $scope.rootModels.search = null;
      });

      hotkeys.add({
        combo: 's',
        description: 'Search for articles',
        callback: function($event) {
          if ($event.stopPropagation) $event.stopPropagation();
          if ($event.preventDefault) $event.preventDefault();
          if ($state.current.name === 'index')
            $('#searchBox').focus();
          else
            $('#quicksearch').removeClass('search-collapsed').focus().addClass('search-collapsed');
        }
      });

      hotkeys.add({
        combo: '1',
        description: 'Go to index',
        callback: function() {
          if ($scope.rootModels.topicModel && $state.current.name !== 'index')
            $state.transitionTo('index');
        }
      });

      hotkeys.add({
        combo: '2',
        description: 'Go to explorer',
        callback: function() {
          if ($scope.rootModels.topicModel && $state.current.name !== 'explorer')
            $state.transitionTo('explorer');
        }
      });

      hotkeys.add({
        combo: '3',
        description: 'Go to network',
        callback: function() {
          if ($scope.rootModels.topicModel && $state.current.name !== 'network')
            $state.transitionTo('network');
        }
      });

      hotkeys.add({
        combo: '4',
        description: 'Go to articles',
        callback: function() {
          if ($scope.rootModels.topicModel && $state.current.name !== 'articles')
            $state.transitionTo('articles');
        }
      });

      hotkeys.add({
        combo: '5',
        description: 'Go to topics',
        callback: function() {
          if ($scope.rootModels.topicModel && $state.current.name !== 'topics')
            $state.transitionTo('topics');
        }
      });

      hotkeys.add({
        combo: '6',
        description: 'Go to entities',
        callback: function() {
          if ($scope.rootModels.topicModel && $state.current.name !== 'entities')
            $state.transitionTo('entities');
        }
      });

      hotkeys.add({
        combo: '7',
        description: 'Go to words',
        callback: function() {
          if ($scope.rootModels.topicModel && $state.current.name !== 'words')
            $state.transitionTo('words');
        }
      });

      hotkeys.add({
        combo: 'h',
        description: 'Show help informations',
        callback: function() {
          $scope.showHelp();
        }
      });

      $scope.showCheatSheet = hotkeys.toggleCheatSheet;

      $scope.showHelp = function() {
        if($scope.rootModels.helpEnabled) {
          $('#helpModal').modal();
        }
      };

      $scope.showSearch = function() {
        $scope.searchExpanded = true;
      };
    }
  ]);

  /**
   * Index route
   */
  app.controller('IndexController', ['$scope', '$stateParams', '$location', '$timeout', 'ArticleFactory', 'TopicFactory', 'SearchFactory',
    function($scope, $stateParams, $location, $timeout, ArticleFactory, TopicFactory, SearchFactory) {
      $scope.rootModels.title = 'Home';

      if (!$scope.rootModels.topicModel)
        $scope.chooseTopicModel();

      $scope.searchResultsStep = 10;
      $scope.advancedSearchEnabled = true;
      $scope.search = $stateParams.q || $scope.search;

      $scope.$watch('rootModels.topicModel', function() {
        if(!$scope.rootModels.topicModel) return;

        ArticleFactory.query({
          topicModel: $scope.rootModels.topicModel.id,
          limit: 3,
          sort: '-created'
        }, function(data) {
          $scope.latestArticles = data;
        });

        TopicFactory.query({
          topicModel: $scope.rootModels.topicModel.id,
          limit: 3,
          sort: '-created'
        }, function(data) {
          $scope.latestTopics = data;
        });
      });

      $scope.$watchGroup(['search', 'rootModels.topicModel', 'rootModels.advFromDate', 'rootModels.advToDate'], function() {
        if ($scope.search && $scope.rootModels.topicModel) {
          $location.search('q', $scope.search);
          $scope.goSearch();
        } else {
          $location.search('q', null);
          $scope.searchResults = [];
        }
      });

      $scope.goSearch = function() {
        $scope.searching = true;
        $scope.skip = 0;
        $scope.searchResults = [];
        $scope.loadMoreResults();
      };

      $scope.loadMoreResults = function() {
        SearchFactory.query({
          topicModel: $scope.rootModels.topicModel.id,
          skip: $scope.skip,
          limit: $scope.searchResultsStep,
          query: $scope.search,
          from: $scope.rootModels.advFromDate ? $scope.rootModels.advFromDate.getTime() : null,
          to: $scope.rootModels.advToDate ? $scope.rootModels.advToDate.getTime() : null
        }, function(data, headers) {
          $scope.searching = false;
          $scope.searchResults.push.apply($scope.searchResults, data);
          $scope.skip += $scope.searchResultsStep;
          $scope.totalResults = headers("V-Total");
          $scope.noMoreResults = $scope.skip >= $scope.totalResults;
        }, function() {
          $scope.searching = false;
        });
      };
    }
  ]);

  /**
   * About route
   */
  app.controller('AboutController', ['$scope', 'InfoFactory',
    function($scope, InfoFactory) {
      $scope.rootModels.title = 'About';

      InfoFactory.get(function(data) {
        $scope.info = data;
        $scope.buildDate = Vipra.formatDateTime(moment($scope.info.app.builddate, 'YYMMDD_HHmm').toDate());
        $scope.startTime = Vipra.formatDateTime(moment($scope.info.vm.starttime, 'x').toDate());
        $scope.upTime = moment.duration($scope.info.vm.uptime).humanize();
      });
    }
  ]);

  /**
   * Network route
   */
  app.controller('NetworkController', ['$scope', '$state', '$stateParams', '$q', '$window', '$timeout', 'ArticleFactory', 'TopicFactory', 'WordFactory',
    function($scope, $state, $stateParams, $q, $window, $timeout, ArticleFactory, TopicFactory, WordFactory) {
      $scope.rootModels.title = 'Network';

      $scope.physicsEnabled = true;
      $scope.highlightEnabled = true;
      $scope.loadOnClick = true;

      var level2Color = 'rgba(130,130,130,0.8)';
      var level3Color = 'rgba(220,220,220,0.4)';

      var id = 0,
        ids = {},
        edges = {};

      $scope.colors = {
        articles: '#D4D8DF',
        words: '#FFFFFF'
      };

      $scope.nodes = new vis.DataSet();
      $scope.edges = new vis.DataSet();
      $scope.data = {
        nodes: $scope.nodes,
        edges: $scope.edges
      };

      $scope.options = {
        nodes: {
          font: {
            size: 14
          },
          shape: 'dot',
          borderWidth: 1
        },
        edges: {
          color: {
            highlight: '#f00'
          }
        },
        layout: {
          randomSeed: 1
        },
        physics: {
          barnesHut: {
            springConstant: 0.008,
            gravitationalConstant: -3500
          },
          timestep: 0.4,
          stabilization: {
            iterations: 100
          }
        },
        interaction: {
          tooltipDelay: 200,
          hideEdgesOnDrag: true
        }
      };

      $scope.shown = {
        articles: true,
        similararticles: false,
        topics: true,
        words: false
      };

      var newNode = function(title, type, show, dbid, color, shape, loader, size) {
        ids[dbid] = ++id;
        return {
          id: id,
          title: title,
          label: title.multiline(5).ellipsize(50),
          type: type,
          show: show,
          dbid: dbid,
          shape: shape || 'dot',
          loader: loader,
          size: size || 25,
          borderWidth: 0,
          origColor: {
            background: color,
            highlight: {
              background: color,
            }
          },
          color: {
            background: color,
            highlight: {
              background: color,
            }
          },
          font: {
            color: '#000'
          },
          shadow: {
            enabled: true
          }
        };
      };

      var newEdge = function(from, to) {
        return {
          id: from + '-' + to,
          from: from,
          to: to,
          selectionWidth: 2,
          color: {
            color: '#aaa',
            highlight: '#d9534f'
          }
        };
      };

      var topicNode = function(topic) {
        topic = topic.topic || topic;
        return newNode(topic.name, 'topic', 'topics.show', topic.id, topic.color, 'triangle', $scope.loadTopic, 35);
      };

      var articleNode = function(article) {
        return newNode(article.title, 'article', 'articles.show', article.id, $scope.colors.articles, 'square', $scope.loadArticle);
      };

      var wordNode = function(word) {
        return newNode(word.word, 'word', 'words.show', word.word, $scope.colors.words, 'box', $scope.loadWord);
      };

      var edgeExists = function(idA, idB) {
        if (idB < idA) {
          var tmp = idA;
          idA = idB;
          idB = tmp;
        }
        return edges.hasOwnProperty(idA + '-' + idB);
      };

      var addEdge = function(idA, idB) {
        if (idB < idA) {
          var tmp = idA;
          idA = idB;
          idB = tmp;
        }
        var id = idA + '-' + idB;
        edges[id] = 1;
        return id;
      };

      // construct new nodes
      var constructor = function(result, nodeFunction, node, props) {
        if (result) {
          var newNodes = [],
            newEdges = [],
            newEdgeIds = [];
          for (var i = 0; i < result.length; i++) {
            var current = result[i];
            if (ids.hasOwnProperty(current.id)) {
              if (node && edgeExists(ids[current.id], node.id))
                continue;
              if(node) {
                var edge = newEdge(ids[current.id], node.id);
                newEdges.push(edge);
                addEdge(edge.from, edge.to);
                newEdgeIds.push(edge.id);
              }
            } else {
              if(node) {
                if(props) {
                  newNodes.push(nodeFunction(current));
                } else {
                  newNodes.push(nodeFunction(current));
                }
                var edge = newEdge(id, node.id);
                newEdges.push(edge);
                addEdge(edge.from, edge.to);
                newEdgeIds.push(edge.id);
              } else {
                newNodes.push(nodeFunction(current));
              }
            }
          }
          if (newNodes.length)
            $scope.nodes.add(newNodes);
          if (newEdges.length)
            $scope.edges.add(newEdges);
          if (newEdgeIds.length)
            $scope.graph.selectEdges(newEdgeIds);
        }
      };

      // on node select
      var nodeLoader = null;
      $scope.select = function(props) {
        if(!$scope.loadOnClick) return;
        var node = $scope.nodes.get(props.nodes[0]);
        if (node) {
          nodeLoader = node.loader(node, props);
          if(nodeLoader) {
            nodeLoader.then(function() {
              node.loaded = true;
              $scope.nodes.update(node);
              if($scope.searchNodes)
                $scope.search();
              $scope.neighbourhoodHighlight(props);
            });
          }
        }
      };

      $scope.deselect = function() {
        $scope.$apply(function() {
          $scope.currentArticle = null;
        });
      };

      $scope.loadArticle = function(node, props) {
        var defer = $q.defer();
        ArticleFactory.get({
          id: node.dbid,
          fields: '_all'
        }, function(data) {
          $scope.currentArticle = data;
          var i;
          if (data.topics) {
            for (i = 0; i < data.topics.length; i++)
              data.topics[i] = data.topics[i].topic;
            constructor(data.topics, topicNode, node, props);
          }
          if (data.similarArticles && $scope.shown.similararticles) {
            var articles = [];
            for (i = 0; i < data.similarArticles.length; i++)
              articles.push(data.similarArticles[i].article);
            constructor(articles, articleNode, node, props);
          }
          defer.resolve();
        });
        return defer.promise;
      };

      $scope.loadTopic = function(node, props) {
        var defers = [];
        var d1 = $q.defer();
        d1.resolve();
        defers.push(d1.promise);
        if ($scope.shown.articles) {
          var d2 = $q.defer();
          defers.push(d2.promise);
          TopicFactory.articles({
            id: node.dbid,
            limit: 50
          }, function(data) {
            constructor(data, articleNode, node, props);
            d2.resolve();
          });
        }
        if ($scope.shown.words) {
          var d3 = $q.defer();
          defers.push(d3.promise);
          TopicFactory.get({
            id: node.dbid,
            limit: 50
          }, function(data) {
            constructor(data.words, wordNode, node, props);
            d3.resolve();
          });
        }
        var defer = $q.defer();
        $q.all([]).then(function() {
          defer.resolve();
        });
        return defer.promise;
      };

      $scope.loadWord = function(node, props) {
        var defers = [];
        var d1 = $q.defer();
        d1.resolve();
        defers.push(d1.promise);
        if ($scope.shown.articles) {
          var d2 = $q.defer();
          defers.push(d2.promise);
          ArticleFactory.query({
            word: node.dbid,
            topicModel: $scope.rootModels.topicModel.id,
            limit: 50
          }, function(data) {
            constructor(data, articleNode, node, props);
            d2.resolve();
          });
        }
        if ($scope.shown.topics) {
          var d3 = $q.defer();
          defers.push(d3.promise);
          TopicFactory.query({
            word: node.dbid,
            topicModel: $scope.rootModels.topicModel.id,
            limit: 50
          }, function(data) {
            constructor(data, topicNode, node, props);
            d3.resolve();
          });
        }
        var defer = $q.defer();
        $q.all([]).then(function() {
          defer.resolve();
        });
        return defer.promise;
      };

      // on node open
      $scope.open = function(props) {
        if(!props.nodes.length) return;
        var node = $scope.nodes.get(props.nodes[0]);

        $window.open($state.href(node.show, {
          id: node.dbid
        }, {
          absolute: true
        }), '_blank');
      };

      $scope.reset = function() {
        $state.go($state.current, {}, {
          reload: true
        });
      };

      $scope.fit = function() {
        $scope.graph.fit({
          animation: {
            offset: {x: 0, y: 0},
            duration: 1000,
            easingFunction: 'easeInOutQuad'
          }
        });
      };

      $scope.completeNetwork = function() {
        var nodes = $scope.nodes.get(),
          updates = [];
        for(var i = 0; i < nodes.length; i++)
          nodes[i].loader(nodes[i]);
        $scope.nodes.update(updates);
      };

      $scope.$watch('searchNodes', function() {
        $scope.search();
      });

      $scope.search = function() {
        var nodes = $scope.nodes.get();
        var i;
        var updates = [];
        if($scope.searchNodes) {
          for(i = 0; i < nodes.length; i++) {
            if(nodes[i].title.indexOf($scope.searchNodes) != -1) {
              updates.push({
                id: nodes[i].id,
                color: nodes[i].origColor,
                font: {
                  color: '#000'
                }
              });
            } else {
              updates.push({
                id: nodes[i].id,
                color: level3Color,
                font: {
                  color: level3Color
                }
              });
            }
          }
        } else {
          for(i = 0; i < nodes.length; i++) {
            updates.push({
              id: nodes[i].id,
              color: nodes[i].origColor,
              font: {
                color: '#000'
              }
            });
          }
        }

        $scope.data.nodes.update(updates);
      };

      var highlightActive = false;
      var lastParams = null;
      $scope.neighbourhoodHighlight = function(params, force) {
        if(typeof force === 'undefined' && !$scope.highlightEnabled) return;
        if(!highlightActive && $scope.searchNodes) return;
        var allNodes = $scope.nodes.get({returnType:"Object"});
        var nodeId;

        if (params && params.nodes.length > 0) {
          // if something is selected
          highlightActive = true;
          var i, j;
          var selectedNode = params.nodes[0];
          var degrees = 2;

          // mark all nodes as hard to read.
          for (nodeId in allNodes) {
            allNodes[nodeId].color = level3Color;
            if (allNodes[nodeId].hiddenLabel === undefined) {
              allNodes[nodeId].hiddenLabel = allNodes[nodeId].label;
              allNodes[nodeId].label = undefined;
            }
          }
          var connectedNodes = $scope.graph.getConnectedNodes(selectedNode);
          var allConnectedNodes = [];

          // get the second degree nodes
          for (i = 1; i < degrees; i++)
            for (j = 0; j < connectedNodes.length; j++)
              allConnectedNodes = allConnectedNodes.concat($scope.graph.getConnectedNodes(connectedNodes[j]));

          // all second degree nodes get a different color and their label back
          for (i = 0; i < allConnectedNodes.length; i++) {
            allNodes[allConnectedNodes[i]].color = level2Color;
            if (allNodes[allConnectedNodes[i]].hiddenLabel !== undefined) {
              allNodes[allConnectedNodes[i]].label = allNodes[allConnectedNodes[i]].hiddenLabel;
              allNodes[allConnectedNodes[i]].hiddenLabel = undefined;
            }
          }

          // all first degree nodes get their own color and their label back
          for (i = 0; i < connectedNodes.length; i++) {
            allNodes[connectedNodes[i]].color = allNodes[connectedNodes[i]].origColor;
            if (allNodes[connectedNodes[i]].hiddenLabel !== undefined) {
              allNodes[connectedNodes[i]].label = allNodes[connectedNodes[i]].hiddenLabel;
              allNodes[connectedNodes[i]].hiddenLabel = undefined;
            }
          }

          // the main node gets its own color and its label back.
          allNodes[selectedNode].color = allNodes[selectedNode].origColor;
          if (allNodes[selectedNode].hiddenLabel !== undefined) {
            allNodes[selectedNode].label = allNodes[selectedNode].hiddenLabel;
            allNodes[selectedNode].hiddenLabel = undefined;
          }
        } else {
          // nothing is selected
          highlightActive = false;
          for (nodeId in allNodes) {
            allNodes[nodeId].color = allNodes[nodeId].origColor;
            if (allNodes[nodeId].hiddenLabel !== undefined) {
              allNodes[nodeId].label = allNodes[nodeId].hiddenLabel;
              allNodes[nodeId].hiddenLabel = undefined;
            }
          }
        }

        // transform the object into an array
        var updateArray = [];
        for (nodeId in allNodes)
          if (allNodes.hasOwnProperty(nodeId))
            updateArray.push(allNodes[nodeId]);
        $scope.nodes.update(updateArray);

        if(!highlightActive && $scope.searchNodes)
          $scope.search();
      };

      $scope.$watch('highlightEnabled', function() {
        if(!$scope.highlightEnabled && highlightActive) {
          $scope.neighbourhoodHighlight(null, true);
        } else if($scope.highlightEnabled && !highlightActive && lastParams) {
          $scope.neighbourhoodHighlight(lastParams);
        }
      });

      $scope.togglePhysics = function(t) {
        if((typeof t !== 'undefined' && !t) || $scope.physicsEnabled) {
          $scope.graph.stopSimulation();
        } else {
          $scope.graph.startSimulation();
        }
        $scope.physicsEnabled = typeof t !== 'undefined' ? t : !$scope.physicsEnabled;
      };

      // create graph
      var container = document.getElementById("visgraph");
      $scope.graph = new vis.Network(container, $scope.data, $scope.options);
      $scope.graph.on('selectNode', $scope.select);
      $scope.graph.on('deselectNode', $scope.deselect);
      $scope.graph.on('doubleClick', $scope.open);
      $scope.graph.on('click', function(params) {
        lastParams = params;
        if(params.nodes.length <= 0) {
          $scope.neighbourhoodHighlight(params);
        }
      });
      $scope.graph.on('dragging', function() {
        if(!$scope.physicsEnabled) {
          $scope.$apply(function() {
            $scope.physicsEnabled = true;
          });
        }
      });

      if($stateParams.type) {
        // if the topic model is not set, the page was refreshed
        // set to true, id of current node decides topic model
        if (!$scope.rootModels.topicModel)
          $scope.rootModels.topicModel = true;

        // type is given, load node
        var factory;
        if ($stateParams.type === 'articles')
          factory = ArticleFactory;
        else if ($stateParams.type === 'topics')
          factory = TopicFactory;
        else if ($stateParams.type === 'word')
          factory = WordFactory;

        // get root node
        factory.get({
          id: $stateParams.id
        }, function(data) {
          $scope.rootNode = data;

          // add root node
          if ($stateParams.type === 'articles')
            $scope.nodes.add([articleNode(data)]);
          else if ($stateParams.type === 'topics')
            $scope.nodes.add([topicNode(data)]);
          else if ($stateParams.type === 'words')
            $scope.nodes.add([wordNode(data)]);
          ids[data.id] = id;

          // take topic model from node
          if (!angular.isObject($scope.rootModels.topicModel))
            $scope.rootModels.topicModel = data.topicModel;
        });
      } else {
        $scope.queryTopics = function() {
          if ($scope.shown.topics) {
            TopicFactory.query({
              topicModel: $scope.rootModels.topicModel.id
            }, function(data) {
              constructor(data, topicNode);
            });
          }
        };

        // page was reloaded, choose topic model
        if (!$scope.rootModels.topicModel)
          $state.transitionTo('index');
        else
          $scope.queryTopics();
      }
    }
  ]);

  /**
   * Explorer route
   */
  app.controller('ExplorerController', ['$scope', '$state', '$templateCache', '$timeout', 'TopicFactory',
    function($scope, $state, $templateCache, $timeout, TopicFactory) {
      $scope.rootModels.title = 'Explorer';

      $scope.checkTopicModel('explorer', function() {
        TopicFactory.query({
          fields: 'name,sequences,avgRelevance,varRelevance,risingRelevance,fallingRelevance,risingDecayRelevance,words,color',
          topicModel: $scope.rootModels.topicModel.id
        }, function(data) {
          $scope.topics = data;
          var colors = randomColor({
            count: $scope.topics.length
          });
          for (var i = 0, t; i < $scope.topics.length; i++) {
            t = $scope.topics[i];
            t.color = t.color || colors[i];
            avgMax = Math.max(t.avgRelevance, avgMax);
            varMax = Math.max(t.varRelevance, varMax);
            fallingMax = Math.max(t.fallingRelevance, fallingMax);
            risingMax = Math.max(t.risingRelevance, risingMax);
            risingDecayMax = Math.max(t.risingDecayRelevance, risingDecayMax);
            risingDecayMin = Math.max(t.risingDecayRelevance, risingDecayMin);
          }
          risingDecayMin = Math.abs(risingDecayMin);
          risingDecayMax = risingDecayMin + Math.abs(risingDecayMax);

          $scope.redrawGraph();
        });

        $scope.reflowCharts = function() {
          var topicHC = $('#topicRelChart').highcharts();
          if (topicHC) topicHC.reflow();
          var wordHC = $('#wordEvoChart').highcharts();
          if (wordHC) wordHC.reflow();
        };

        $timeout(function() {
          $('#outer').layout({
            applyDefaultStyles: true,
            south__size: '45%',
            center__childOptions: {
              applyDefaultStyles: true,
              center__onresize: function() {
                $scope.reflowCharts();
              },
              west: {
                size: 250
              }
            },
            south__childOptions: {
              applyDefaultStyles: true,
              center__onresize: function() {
                $scope.reflowCharts();
              },
              west: {
                size: 250
              }
            }
          });
          $scope.reflowCharts();
        });

        var avgMax = 0,
          varMax = 0,
          fallingMax = 0,
          risingMax = 0,
          risingDecayMax = 0,
          risingDecayMin = 0;

        $scope.explorerModels = {
          sorttopics: 'name',
          sortdir: false,
          seqstyle: 'absolute',
          chartstyle: 'areaspline',
          chartstack: 'none',
          articlesSort: 'title'
        };

        $scope.checkTopics = function(to) {
          var toggle = typeof to === 'undefined';
          for (var i = 0, t; i < $scope.topics.length; i++) {
            t = $scope.topics[i];
            t.selected = toggle ? !t.selected : to;
          }
          $scope.redrawGraph();
          $scope.changeSelectedTopics();
        };

        $scope.changeSelectedTopics = function() {
          if($scope.explorerModels.activeTopic && !$scope.explorerModels.activeTopic.selected) {
            delete $scope.explorerModels.activeTopic;
            delete $scope.explorerModels.activeSequence;
          }
          $scope.redrawGraph();
        };

        $scope.redrawGraph = function() {
          if (!$scope.topics) return;
          var series = [];

          // create series of selected topics
          for (var i = 0; i < $scope.topics.length; i++) {
            if ($scope.topics[i].selected) {
              var topic = $scope.topics[i],
                relevances = [];

              // data array with relevances and min/max
              for (var j = 0, sequence, relevance; j < topic.sequences.length; j++) {
                sequence = topic.sequences[j];
                relevance = $scope.explorerModels.seqstyle === 'relative' ? sequence.relevanceChange : sequence.relevance;
                relevances.push({
                  x: new Date(sequence.window.startDate).getTime(),
                  y: relevance,
                  sequence: sequence,
                  topic: topic
                });
              }

              series.push({
                id: topic.id,
                name: topic.name,
                data: relevances,
                color: topic.color,
                topic: topic,
                zIndex: i + 1
              });
            }
          }

          // highcharts configuration
          $scope.topicSeq = areaRelevanceChart(series, 'Topic Relevance', $scope.explorerModels.chartstyle,
            $scope.explorerModels.chartstack, $scope.pointSelected);
          $scope.topicsSelected = series.length;
        };

        $scope.topicCurrValue = function(topic) {
          var percent = 0;
          switch ($scope.explorerModels.sorttopics) {
            case 'avgRelevance':
              percent = topic.avgRelevance / avgMax;
              break;
            case 'varRelevance':
              percent = topic.varRelevance / varMax;
              break;
            case 'fallingRelevance':
              percent = topic.fallingRelevance / fallingMax;
              break;
            case 'risingRelevance':
              percent = topic.risingRelevance / risingMax;
              break;
            case 'risingDecayRelevance':
              percent = (topic.risingDecayRelevance + risingDecayMin) / risingDecayMax;
              break;
          }
          return (percent * 100) + '%';
        };

        $scope.highlightTopic = function(id, toggle) {
          if(toggle) {
            $('[data-id="' + id + '"]').addClass('highlight');
          } else {
            $('[data-id="' + id + '"]').removeClass('highlight');
          }

          if (!$scope.topicsSelected) return;
          var highcharts = $('#topicRelChart').highcharts();
          if (!highcharts) return;
          var series = highcharts.get(id);
          if (!series) return;

          if (toggle) {
            series.onMouseOver();
            series.group.zIndexOrig = series.group.zIndex;
            series.group.zIndexSetter(9999, 'zIndex');
            series.graph.attr('stroke', '#000000').attr('stroke-dasharray', '5,5');
          } else {
            series.onMouseOut();
            series.group.zIndexSetter(series.group.zIndexOrig, 'zIndex');
            series.graph.attr('stroke', series.color).attr('stroke-dasharray', '');
          }
        };

        $scope.resetZoom = function() {
          if (!$scope.topicsSelected) return;
          var highcharts = $('#topicRelChart').highcharts();
          if (!highcharts) return;

          highcharts.xAxis[0].setExtremes(null, null);
        };

        $scope.pointSelected = function(e) {
          $scope.$apply(function() {
            $scope.activateTopic(e.point.topic);
            $scope.activateSequence(e.point.sequence);
          });
        };

        $scope.activateTopic = function(topic) {
          $scope.explorerModels.activeTopic = topic;
        };

        $scope.activateSequence = function(seq) {
          $scope.explorerModels.activeSequence = seq;
        };

        $scope.clearSequence = function() {
          delete $scope.explorerModels.activeSequence;
          delete $scope.articles;
          var hc = $('#topicRelChart').highcharts();
          if(hc) {
            var selectedPoints = hc.getSelectedPoints();
            for(var i = 0; i < selectedPoints.length; i++) {
              selectedPoints[i].select(false);
            }
          }
        };

        $scope.sequenceChanged = function() {
          if(!$scope.explorerModels.activeTopic || !$scope.explorerModels.activeSequence) return;

          $scope.articles = [];
          $scope.showMoreArticles();
        };

        $scope.showMoreArticles = function(limit) {
          $scope.loadingArticles = true;
          TopicFactory.articles({
            skip: $scope.articles ? $scope.articles.length : 0,
            limit: typeof limit === 'undefined' ? 20 : limit,
            id: $scope.explorerModels.activeTopic.id,
            from: new Date($scope.explorerModels.activeSequence.window.startDate).getTime(),
            to: new Date($scope.explorerModels.activeSequence.window.endDate).getTime()
          }, function(data, headers) {
            $scope.articles.push.apply($scope.articles, data);
            $scope.articlesTotal = headers("V-Total");
            $scope.loadingArticles = false;
          }, function() {
            $scope.loadingArticles = false;
          });
        };

        $scope.showAllArticles = function() {
          $scope.showMoreArticles(0);
        };

        $scope.$watchGroup(['explorerModels.seqstyle', 'explorerModels.chartstyle', 'explorerModels.chartstack'], $scope.redrawGraph);

        $scope.$watch('explorerModels.sorttopics', function() {
          if (!$scope.topics) return;

          if ($scope.explorerModels.sorttopics === 'name') {
            $scope.explorerModels.sortdir = false;
          } else {
            $scope.explorerModels.sortdir = true;
          }

          $timeout(function() {
            for (var i = 0; i < $scope.topics.length; i++)
              $scope.topics[i].topicCurrValue = $scope.topicCurrValue($scope.topics[i]);
          }, 0);
        });

        $scope.$watch('explorerModels.activeTopic', function() {
          if(!$scope.explorerModels.activeTopic) {
            $scope.clearSequence();
            return;
          }

          $('.topic').removeClass('active-topic');
          $('[data-id="' + $scope.explorerModels.activeTopic.id + '"]').addClass('active-topic');

          // preselect some words
          if ($scope.explorerModels.activeTopic.words) {
            for (var i = 0; i < Math.min(3, $scope.explorerModels.activeTopic.words.length); i++)
              $scope.explorerModels.activeTopic.words[i].selected = true;
          }
        });

        $scope.$watch('explorerModels.activeSequence', function() {
          $scope.sequenceChanged();
        });
      });
    }
  ]);

  /****************************************************************************
   * Article Controllers
   ****************************************************************************/

  /**
   * Articles Index route
   */
  app.controller('ArticlesIndexController', ['$scope', '$state', 'ArticleFactory',
    function($scope, $state, ArticleFactory) {
      $scope.rootModels.title = 'Articles';

      $scope.checkTopicModel('articles', function() {
        $scope.articlesIndexModels = {
          sortkey: 'date',
          sortdir: true,
          page: 1,
          limit: 50,
          topics: []
        };

        $scope.reloadArticles = function() {
          $scope.loadingArticles = true;

          var topics = null;
          if($scope.articlesIndexModels.topics && $scope.articlesIndexModels.topics.length)
            topics = $scope.articlesIndexModels.topics;

          (topics ? ArticleFactory.queryPOST : ArticleFactory.query)({
            skip: ($scope.articlesIndexModels.page - 1) * $scope.articlesIndexModels.limit,
            limit: $scope.articlesIndexModels.limit,
            sort: ($scope.articlesIndexModels.sortdir ? '' : '-') + $scope.articlesIndexModels.sortkey,
            topicModel: $scope.rootModels.topicModel.id,
            char: $scope.articlesIndexModels.startChar,
            contains: $scope.articlesIndexModels.contains,
            from: $scope.articlesIndexModels.fromDate ? $scope.articlesIndexModels.fromDate.getTime() : null,
            to: $scope.articlesIndexModels.toDate ? $scope.articlesIndexModels.toDate.getTime() : null,
            topics: topics
          }, function(data, headers) {
            $scope.articles = data;
            $scope.articlesTotal = headers("V-Total");
            $scope.maxPage = Math.ceil($scope.articlesTotal / $scope.articlesIndexModels.limit);
            $scope.loadingArticles = false;
          }, function() {
            $scope.loadingArticles = false;
          });
        };

        $scope.$watchGroup(['articlesIndexModels.page', 'articlesIndexModels.sortkey', 'articlesIndexModels.sortdir', 'articlesIndexModels.fromDate', 'articlesIndexModels.toDate', 'articlesIndexModels.topics'], function() {
          $scope.reloadArticles();
        });

        $scope.$watchGroup(['articlesIndexModels.startChar', 'articlesIndexModels.contains'], function() {
          if($scope.articlesIndexModels.page !== 1)
            $scope.articlesIndexModels.page = 1;
          else
            $scope.reloadArticles();
        });
      });
    }
  ]);

  /**
   * Articles Show route
   */
  app.controller('ArticlesShowController', ['$scope', '$state', '$stateParams', '$timeout', 'ArticleFactory',
    function($scope, $state, $stateParams, $timeout, ArticleFactory) {
      $scope.rootModels.title = 'Article';

      $scope.articlesShowModels = {
        topicsSort: '-share',
        similarSort: '-share',
        wordsSort: '-count',
        entitiesSort: '-count'
      };

      ArticleFactory.get({
        id: $stateParams.id
      }, function(data) {
        $scope.article = data;
        $scope.article.text = $scope.prepareText($scope.article.text);
        $scope.rootModels.title = $scope.article.title;

        // calculate share from divergence
        if ($scope.article.similarArticles) {
          for (var articleIndex = 0; articleIndex < $scope.article.similarArticles.length; articleIndex++)
            $scope.article.similarArticles[articleIndex].share = Math.round(((1 - $scope.article.similarArticles[articleIndex].divergence) * 100));
        }

        // take topic model from article
        if (!angular.isObject($scope.rootModels.topicModel))
          $scope.rootModels.topicModel = data.topicModel;

        // calculate percentage share
        var topicShareSeries = [];

        if ($scope.article.topics) {
          var topics = $scope.article.topics,
            colors = randomColor({
              count: $scope.article.topics.length
            });

          for (var topicIndex = 0, d; topicIndex < topics.length; topicIndex++) {
            d = {
              name: topics[topicIndex].topic.name,
              y: topics[topicIndex].share,
              color: topics[topicIndex].topic.color || colors[topicIndex],
              id: topics[topicIndex].topic.id
            };

            topicShareSeries.push(d);
            $scope.article.topics[topicIndex].color = $scope.article.topics[topicIndex].color || colors[topicIndex];
          }
        }

        $timeout(function() {
          // highcharts data
          $scope.topicShare = topicShareChart([{
            name: 'Topic Share',
            colorByPoint: true,
            data: topicShareSeries
          }]);
        }, 0);
      }, function(e) {
        $scope.error(e.status);
      });

      $scope.prepareText = function(text) {
        var entityBase = $state.href('entities', {}, {absolute: true}),
          wordBase = $state.href('words', {}, {absolute: true});
        return text.replace(/<e=([^>]*)/g, '<a href="' + entityBase + '/$1"').replace(/<w=([^>]*)/g, '<a href="' + wordBase + '/$1"');
      };

      var topicShareChartElement = $('#topic-share');
      $scope.highlightSlice = function(id, toggle) {
        var highcharts = topicShareChartElement.highcharts();
        if (!highcharts) return;
        var point = highcharts.get(id);
        if (!point) return;

        if (toggle) {
          point.onMouseOver();
        } else {
          point.onMouseOut();
          highcharts.tooltip.hide();
        }
      };
    }
  ]);

  /**
   * Articles Entities route
   */
  app.controller('ArticlesEntitiesController', ['$scope', '$state', '$stateParams', 'ArticleFactory',
    function($scope, $state, $stateParams, ArticleFactory) {
      $scope.id = $stateParams.id;

      ArticleFactory.get({
        id: $stateParams.id,
        fields: 'entities'
      }, function(data) {
        $scope.allEntities = data.entities;
        $scope.showMoreEntities();
      });

      var entitiesCount = 0;
      $scope.showMoreEntities = function() {
        entitiesCount += 20;
        $scope.entities = $scope.allEntities.slice(0, entitiesCount);
        $scope.refreshEntityDistribution();
      };

      $scope.showAllEntities = function() {
        entitiesCount = $scope.allEntities.length;
        $scope.entities = $scope.allEntities;
        $scope.refreshEntityDistribution();
      };

      $scope.refreshEntityDistribution = function() {
        var series = [];
        if($scope.entities && $scope.entities.length) {
          for(var i = 0; i < $scope.entities.length; i++) {
            series.push({
              name: $scope.entities[i].entity.entity,
              data: [$scope.entities[i].count]
            });
          }
        }
        $scope.entityDistribution = itemCountChart(series, ['Entities']);
      };
    }
  ]);

  /**
   * Articles Words route
   */
  app.controller('ArticlesWordsController', ['$scope', '$state', '$stateParams', 'ArticleFactory',
    function($scope, $state, $stateParams, ArticleFactory) {
      $scope.id = $stateParams.id;

      ArticleFactory.get({
        id: $stateParams.id,
        fields: 'words'
      }, function(data) {
        $scope.allWords = data.words;
        $scope.showMoreWords();
      });

      var wordsCount = 0;
      $scope.showMoreWords = function() {
        wordsCount += 20;
        $scope.words = $scope.allWords.slice(0, wordsCount);
        $scope.refreshWordDistribution();
      };

      $scope.showAllWords = function() {
        wordsCount = $scope.allWords.length;
        $scope.words = $scope.allWords;
        $scope.refreshWordDistribution();
      };

      $scope.refreshWordDistribution = function() {
        var series = [];
        if($scope.words && $scope.words.length) {
          for(var i = 0; i < $scope.words.length; i++) {
            series.push({
              name: $scope.words[i].word,
              data: [$scope.words[i].count]
            });
          }
        }
        $scope.wordDistribution = itemCountChart(series, ['Words']);
      };
    }
  ]);

  /****************************************************************************
   * Topic Controllers
   ****************************************************************************/

  /**
   * Topics Index route
   */
  app.controller('TopicsIndexController', ['$scope', '$state', 'TopicFactory',
    function($scope, $state, TopicFactory) {
      $scope.rootModels.title = 'Topics';

      $scope.checkTopicModel('topics', function() {
        $scope.topicsIndexModels = {
          sortkey: 'name',
          sortdir: true,
          page: 1,
          limit: 50
        };

        $scope.reloadTopics = function() {
          $scope.loadingTopics = true;
          TopicFactory.query({
            topicModel: $scope.rootModels.topicModel.id,
            skip: ($scope.topicsIndexModels.page - 1) * $scope.topicsIndexModels.limit,
            limit: $scope.topicsIndexModels.limit,
            sort: ($scope.topicsIndexModels.sortdir ? '' : '-') + $scope.topicsIndexModels.sortkey,
            char: $scope.topicsIndexModels.startChar,
            contains: $scope.topicsIndexModels.contains
          }, function(data, headers) {
            $scope.topics = data;
            $scope.topicsTotal = headers("V-Total");
            $scope.maxPage = Math.ceil($scope.topicsTotal / $scope.topicsIndexModels.limit);
            $scope.loadingTopics = false;
          }, function() {
            $scope.loadingTopics = false;
          });
        };

        $scope.$watchGroup(['topicsIndexModels.page', 'topicsIndexModels.sortkey', 'topicsIndexModels.sortdir'], function() {
          $scope.reloadTopics();
        });

        $scope.$watchGroup(['topicsIndexModels.startChar', 'topicsIndexModels.contains'], function() {
          if($scope.topicsIndexModels.page !== 1)
            $scope.topicsIndexModels.page = 1;
          else
            $scope.reloadTopics();
        });
      });
    }
  ]);

  /**
   * Topics Show route
   */
  app.controller('TopicsShowController', ['$scope', '$state', '$stateParams', '$timeout', 'TopicFactory',
    function($scope, $state, $stateParams, $timeout, TopicFactory) {
      $scope.rootModels.title = 'Topic';

      $scope.topicsShowModels = {
        relSeqstyle: 'absolute',
        relChartstyle: 'areaspline',
        wordSeqstyle: 'absolute',
        wordChartstyle: 'spline',
        seqSortWords: '-probability'
      };

      TopicFactory.get({
        id: $stateParams.id
      }, function(data) {
        $scope.topic = data;
        $scope.rootModels.title = $scope.topic.name;

        // take topic model from topic
        if (!angular.isObject($scope.rootModels.topicModel))
          $scope.rootModels.topicModel = data.topicModel;

        // preselect some words
        if ($scope.topic.words) {
          for (var i = 0; i < Math.min(3, $scope.topic.words.length); i++)
            $scope.topic.words[i].selected = true;
        }

        // preselect first sequence
        if ($scope.topic.sequences && $scope.topic.sequences.length)
          $scope.topicsShowModels.sequence = $scope.topic.sequences[0];
        
        $scope.redrawRelevanceGraph();
      }, function(e) {
          $scope.error(e.status);
        });

      $scope.redrawRelevanceGraph = function() {
        if (!$scope.topic || !$scope.topic.sequences) return;
        var relevances = [];

        // create series
        for (var i = 0, sequence, relevance; i < $scope.topic.sequences.length; i++) {
          sequence = $scope.topic.sequences[i];
          relevance = $scope.topicsShowModels.relSeqstyle === 'relative' ? sequence.relevanceChange : sequence.relevance;
          relevances.push([new Date(sequence.window.startDate).getTime(), relevance]);
        }

        // highcharts configuration
        $scope.topicSeq = areaRelevanceChart([{
          name: $scope.topic.name,
          data: relevances
        }], 'Topic Relevance', $scope.topicsShowModels.relChartstyle);
      };

      $scope.resetRelZoom = function() {
        if (!$scope.topic) return;
        var highcharts = $('#topicRelChart').highcharts();
        if (!highcharts) return;

        highcharts.xAxis[0].setExtremes(null, null);
      };

      $scope.startRename = function() {
        $scope.origName = $scope.topic.name;
        $scope.isRename = true;
        $timeout(function() {
          document.getElementById('topicName').select();
        }, 0);
      };

      $scope.endRename = function(save) {
        delete $scope.renameErrors;
        if (save) {
          TopicFactory.update({
            id: $scope.topic.id
          }, $scope.topic, function(data) {
            $scope.topic = data;
            $scope.isRename = false;

            // if topic list of parent view is loaded, replace name by new name
            if ($scope.$parent.topics) {
              for (var i = 0, topic; i < $scope.$parent.topics.length; i++) {
                topic = $scope.$parent.topics[i];
                if (topic.id === data.id) {
                  break;
                }
              }
            }
          });
        } else {
          $scope.isRename = false;
          $scope.topic.name = $scope.origName;
        }
      };

      $scope.keyup = function($event) {
        if ($event.which === 13 || $event.which === 27) {
          $scope.endRename($event.which === 13);
          $event.preventDefault();
        }
      };

      $scope.$on('$stateChangeSuccess', function(e, toState) {
        if(toState.name === 'topics.show') {
          $timeout(function() {
            $scope.redrawRelevanceGraph();
          }, 100);
        }
      });
    }
  ]);

  /**
   * Topics Show Sequences route
   */
  app.controller('TopicsSequencesController', ['$scope', '$state', '$stateParams', 'TopicFactory', 'SequenceFactory',
    function($scope, $state, $stateParams, TopicFactory, SequenceFactory) {
      $scope.id = $stateParams.id;
      
      $scope.recalcSeqChange = function() {
        if (!$scope.sequence || !$scope.sequenceCompare) return;
        wordLoop:
          for (var i = 0, word; i < $scope.sequence.words.length; i++) {
            word = $scope.sequence.words[i];
            for (var j = 0, word2; j < $scope.sequenceCompare.words.length; j++) {
              word2 = $scope.sequenceCompare.words[j];
              if (word.word === word2.word) {
                word.change = word2.change = j - i;
                continue wordLoop;
              }
            }
            word.change = '-';
          }
      };

      $scope.closeCompare = function() {
        delete $scope.sequenceCompare;
        delete $scope.topicsShowModels.sequenceCompare;
      };

      $scope.$watch('topicsShowModels.relSeqstyle', $scope.redrawRelevanceGraph);
      $scope.$watch('topicsShowModels.relChartstyle', $scope.redrawRelevanceGraph);

      $scope.$watch('topicsShowModels.sequence', function() {
        if (!$scope.topicsShowModels.sequence) return;

        SequenceFactory.get({
          id: $scope.topicsShowModels.sequence.id
        }, function(data) {
          $scope.sequence = data;
          $scope.recalcSeqChange();
        });
      });

      $scope.$watch('topicsShowModels.sequenceCompare', function() {
        if (!$scope.topicsShowModels.sequenceCompare) return;

        SequenceFactory.get({
          id: $scope.topicsShowModels.sequenceCompare.id
        }, function(data) {
          $scope.sequenceCompare = data;
          $scope.recalcSeqChange();
        });
      });

      $('body').on('mouseleave', '.compare-row', function() {
        $('[word="' + $(this).attr('word') + '"]').removeClass('highlight');
      });

      $('body').on('mouseenter', '.compare-row', function() {
        $('[word="' + $(this).attr('word') + '"]').addClass('highlight');
      });
    }
  ]);

  /**
   * Topics Show Articles route
   */
  app.controller('TopicsArticlesController', ['$scope', '$stateParams', 'TopicFactory',
    function($scope, $stateParams, TopicFactory) {
      $scope.id = $stateParams.id;

      $scope.topicsArticlesModels = {
        sortkey: 'title',
        sortdir: true,
        page: 1,
        limit: 50
      };

      $scope.$watchGroup(['topicsArticlesModels.page', 'topicsArticlesModels.sortkey', 'topicsArticlesModels.sortdir'], function() {
        TopicFactory.articles({
          id: $stateParams.id,
          skip: ($scope.topicsArticlesModels.page - 1) * $scope.topicsArticlesModels.limit,
          limit: $scope.topicsArticlesModels.limit,
          sort: ($scope.topicsArticlesModels.sortdir ? '' : '-') + $scope.topicsArticlesModels.sortkey
        }, function(data, headers) {
          $scope.articles = data;
          $scope.articlesTotal = headers("V-Total");
          $scope.maxPage = Math.ceil($scope.articlesTotal / $scope.topicsArticlesModels.limit);
        }, function(e) {
          $scope.error(e.status);
        });
      });

    }
  ]);

  /****************************************************************************
   * Entity Controllers
   ****************************************************************************/

  /**
   * Entities Index route
   */
  app.controller('EntitiesIndexController', ['$scope', '$state', 'EntityFactory',
    function($scope, $state, EntityFactory) {
      $scope.rootModels.title = 'Entities';

      $scope.checkTopicModel('entities', function() {
        $scope.entitiesIndexModels = {
          sortkey: 'entity',
          sortdir: true,
          type: 1,
          page: 1,
          limit: 50
        };

        $scope.reloadEntities = function() {
          $scope.loadingEntities = true;

          var words = null;
          if(parseInt($scope.entitiesIndexModels.type, 10) !== 1)
            words = parseInt($scope.entitiesIndexModels.type, 10) === 3;

          EntityFactory.query({
            topicModel: $scope.rootModels.topicModel.id,
            skip: ($scope.entitiesIndexModels.page - 1) * $scope.entitiesIndexModels.limit,
            limit: $scope.entitiesIndexModels.limit,
            sort: ($scope.entitiesIndexModels.sortdir ? '' : '-') + $scope.entitiesIndexModels.sortkey,
            char: $scope.entitiesIndexModels.startChar,
            contains: $scope.entitiesIndexModels.contains,
            word: words,
            fields: 'id,entity,isWord'
          }, function(data, headers) {
            $scope.entities = data;
            $scope.entitiesTotal = headers("V-Total");
            $scope.maxPage = Math.ceil($scope.entitiesTotal / $scope.entitiesIndexModels.limit);
            $scope.loadingEntities = false;
          }, function() {
            $scope.loadingEntities = false;
          });
        };

        $scope.$watchGroup(['entitiesIndexModels.page', 'entitiesIndexModels.sortkey', 'entitiesIndexModels.sortdir', 'entitiesIndexModels.type'], function() {
          $scope.reloadEntities();
        });

        $scope.$watchGroup(['entitiesIndexModels.startChar', 'entitiesIndexModels.contains'], function() {
          if($scope.entitiesIndexModels.page !== 1)
            $scope.entitiesIndexModels.page = 1;
          else
            $scope.reloadEntities();
        });
      });
    }
  ]);

  /**
   * Entities Show route
   */
  app.controller('EntitiesShowController', ['$scope', '$state', '$stateParams', 'EntityFactory',
    function($scope, $state, $stateParams, EntityFactory) {
      $scope.rootModels.title = 'Entity';

      $scope.checkTopicModel('entities.show', function() {
        EntityFactory.get({
          id: $stateParams.id,
          topicModel: $scope.rootModels.topicModel.id
        }, function(data) {
          $scope.entity = data;
          $scope.entityCreated = Vipra.formatDateTime($scope.entity.created);
          $scope.entityModified = Vipra.formatDateTime($scope.entity.modified);
          $scope.rootModels.title = $scope.entity.entity;
          $scope.types = Vipra.joinResources(data.types);
        }, function(e) {
          $scope.error(e.status);
        });
      });
    }
  ]);

  /**
   * Entities Articles route
   */
  app.controller('EntitiesArticlesController', ['$scope', '$state', '$stateParams', 'ArticleFactory',
    function($scope, $state, $stateParams, ArticleFactory) {

      $scope.checkTopicModel('entities.show.articles', function() {
        $scope.id = $stateParams.id;
        $scope.rootModels.title = $scope.id;

        $scope.entitiesArticlesModels = {
          sortkey: 'date',
          sortdir: true,
          page: 1,
          limit: 50
        };

        $scope.$watchGroup(['entitiesArticlesModels.page', 'entitiesArticlesModels.sortkey', 'entitiesArticlesModels.sortdir'], function() {
          $scope.loadingArticles = true;
          ArticleFactory.query({
            skip: ($scope.entitiesArticlesModels.page - 1) * $scope.entitiesArticlesModels.limit,
            limit: $scope.entitiesArticlesModels.limit,
            sort: ($scope.entitiesArticlesModels.sortdir ? '' : '-') + $scope.entitiesArticlesModels.sortkey,
            topicModel: $scope.rootModels.topicModel.id,
            entity: $scope.id
          }, function(data, headers) {
            $scope.articles = data;
            $scope.articlesTotal = headers("V-Total");
            $scope.maxPage = Math.ceil($scope.articlesTotal / $scope.entitiesArticlesModels.limit);
            $scope.loadingArticles = false;
          }, function() {
            $scope.loadingArticles = false;
          });
        });
      });
    }
  ]);

  /****************************************************************************
   * Word Controllers
   ****************************************************************************/

  /**
   * Words Index route
   */
  app.controller('WordsIndexController', ['$scope', '$state', 'WordFactory',
    function($scope, $state, WordFactory) {
      $scope.rootModels.title = 'Words';

      $scope.checkTopicModel('words', function() {
        $scope.wordsIndexModels = {
          sortkey: 'word',
          sortdir: true,
          type: 1,
          page: 1,
          limit: 50
        };

        $scope.reloadWords = function() {
          $scope.loadingWords = true;

          var entities = null;
          if(parseInt($scope.wordsIndexModels.type, 10) !== 1)
            entities = parseInt($scope.wordsIndexModels.type, 10) === 3;

          WordFactory.query({
            topicModel: $scope.rootModels.topicModel.id,
            skip: ($scope.wordsIndexModels.page - 1) * $scope.wordsIndexModels.limit,
            limit: $scope.wordsIndexModels.limit,
            sort: ($scope.wordsIndexModels.sortdir ? '' : '-') + $scope.wordsIndexModels.sortkey,
            char: $scope.wordsIndexModels.startChar,
            contains: $scope.wordsIndexModels.contains,
            entity: entities,
            fields: 'id,word,isEntity'
          }, function(data, headers) {
            $scope.words = data;
            $scope.wordsTotal = headers("V-Total");
            $scope.maxPage = Math.ceil($scope.wordsTotal / $scope.wordsIndexModels.limit);
            $scope.loadingWords = false;
          }, function() {
            $scope.loadingWords = false;
          });
        };

        $scope.$watchGroup(['wordsIndexModels.page', 'wordsIndexModels.sortkey', 'wordsIndexModels.sortdir', 'wordsIndexModels.type'], $scope.reloadWords);

        $scope.$watchGroup(['wordsIndexModels.startChar', 'wordsIndexModels.contains'], function() {
          if($scope.wordsIndexModels.page !== 1)
            $scope.wordsIndexModels.page = 1;
          else
            $scope.reloadWords();
        });
      });
    }
  ]);

  /**
   * Words Show route
   */
  app.controller('WordsShowController', ['$scope', '$state', '$stateParams', 'WordFactory',
    function($scope, $state, $stateParams, WordFactory) {
      $scope.rootModels.title = 'Word';

      $scope.checkTopicModel('words.show', function() {
        WordFactory.get({
          id: $stateParams.id,
          topicModel: $scope.rootModels.topicModel.id
        }, function(data) {
          $scope.word = data;
          $scope.rootModels.title = $scope.word.word;
        }, function(e) {
          $scope.error(e.status);
        });
      });
    }
  ]);

  /**
   * Words Topics route
   */
  app.controller('WordsTopicsController', ['$scope', '$state', '$stateParams', 'TopicFactory',
    function($scope, $state, $stateParams, TopicFactory) {

      $scope.checkTopicModel('words.show.topics', function() {
        $scope.word = $stateParams.id;
        $scope.rootModels.title = $scope.word;

        $scope.wordsTopicsModels = {
          sortkey: 'name',
          sortdir: true,
          page: 1,
          limit: 50
        };

        $scope.$watchGroup(['wordsTopicsModels.page', 'wordsTopicsModels.sortkey', 'wordsTopicsModels.sortdir'], function() {
          TopicFactory.query({
            topicModel: $scope.rootModels.topicModel.id,
            skip: ($scope.wordsTopicsModels.page - 1) * $scope.wordsTopicsModels.limit,
            limit: $scope.wordsTopicsModels.limit,
            sort: ($scope.wordsTopicsModels.sortdir ? '' : '-') + $scope.wordsTopicsModels.sortkey,
            word: $scope.word
          }, function(data, headers) {
            $scope.topics = data;
            $scope.topicsTotal = headers("V-Total");
            $scope.maxPage = Math.ceil($scope.topicsTotal / $scope.wordsTopicsModels.limit);
          });
        });
      });
    }
  ]);

  /**
   * Words Articles route
   */
  app.controller('WordsArticlesController', ['$scope', '$state', '$stateParams', 'ArticleFactory',
    function($scope, $state, $stateParams, ArticleFactory) {

      $scope.checkTopicModel('words.show.articles', function() {
        $scope.word = $stateParams.id;
        $scope.rootModels.title = $scope.word;

        $scope.wordsArticlesModels = {
          sortkey: 'date',
          sortdir: true,
          page: 1,
          limit: 50
        };

        $scope.$watchGroup(['wordsArticlesModels.page', 'wordsArticlesModels.sortkey', 'wordsArticlesModels.sortdir'], function() {
          ArticleFactory.query({
            skip: ($scope.wordsArticlesModels.page - 1) * $scope.wordsArticlesModels.limit,
            limit: $scope.wordsArticlesModels.limit,
            sort: ($scope.wordsArticlesModels.sortdir ? '' : '-') + $scope.wordsArticlesModels.sortkey,
            topicModel: $scope.rootModels.topicModel.id,
            word: $scope.word
          }, function(data, headers) {
            $scope.articles = data;
            $scope.articlesTotal = headers("V-Total");
            $scope.maxPage = Math.ceil($scope.articlesTotal / $scope.wordsArticlesModels.limit);
          });
        });
      });
    }
  ]);

  /****************************************************************************
   * Slides Controllers
   ****************************************************************************/

  /**
   * Slides route
   */
  app.controller('SlidesController', ['$scope',
    function($scope) {

      $scope.rootModels.title = 'Slides';
      $scope.current = 1;

      var folder = '//ftp.cochu.io/vipra/slides/';
      var prefix = 'Folie';
      var suffix = '.PNG';
      var slides = $('.slides');

      slides.css('background-image', 'url(' + folder + prefix + $scope.current + suffix + ')');

      $scope.go = function(next) {
        $('<img/>').attr('src', folder + prefix + next + suffix).load(function() {
          $scope.$apply(function() {
            $scope.current = next;
            slides.css('background-image', 'url(' + folder + prefix + $scope.current + suffix + ')');
          });
        });
      };
    }
  ]);

  /****************************************************************************
   * Error Controllers
   ****************************************************************************/

  /**
   * Error route
   */
  app.controller('ErrorsController', ['$scope', '$state', '$stateParams',
    function($scope, $state, $stateParams) {
      $scope.code = $stateParams.code;
      $scope.title = Vipra.statusMsg($scope.code);
      $scope.rootModels.title = 'Error: ' + $scope.code;
    }
  ]);

  /****************************************************************************
   * Directive Controllers
   ****************************************************************************/

  /**
   * Pagination
   */
  app.controller('PaginationController', ['$scope', '$location',
    function($scope, $location) {

      $scope.calculatePages = function() {
        var pages = [],
          max = Math.ceil($scope.total / $scope.limit * 1.0),
          pad = 2,
          end = Math.min(Math.max($scope.page + pad, Math.max($scope.page - pad, 1) + pad * 2), max),
          start = Math.max(Math.min($scope.page - pad, end - pad * 2), 1);
        for (var i = start; i <= end; i++) {
          pages.push(i);
        }
        $scope.pages = pages;
        $scope.maxPage = max;
      };

      $scope.$watchGroup(['total', 'page', 'limit'], function(newVal, oldVal) {
        if (!angular.equals(newVal, oldVal)) {
          $scope.calculatePages();
        }
      });

      $scope.calculatePages();

      $scope.changePage = function(page) {
        if(!page || page < 1 || page > $scope.maxPage || page === $scope.page) return;
        $scope.page = page;
        if($location.search().p !== page)
          $location.search('p', page === 1 ? null : page);
        window.scrollTo(0,0);
      };

      $scope.toPage = function() {
        var page = prompt("Enter a page number (between 1 and " + $scope.maxPage + ")");
        $scope.changePage(page);
      };

      /*
      $scope.changePage($location.search().p);

      $scope.$watch('page', function() {
        $location.search('p', $scope.page === 1 ? null : $scope.page);
      });
      */
    }
  ]);

  /*
  app.controller('CharSelectorController', ['$scope', '$location',
    function($scope, $location) {

      $scope.changeChar = function(char) {
        if(!char || $scope.ngModel === char) return;
        $scope.ngModel = char;
      };

      $scope.changeChar($location.search().c);

      $scope.$watch('ngModel', function() {
        $location.search('c', $scope.ngModel ? $scope.ngModel : null);
      });
    }
  ]);

  app.controller('SorterController', ['$scope', '$location',
    function($scope, $location) {

      $scope.changeSort = function(sort) {
        if(!sort || sort === $scope.ngModel) return;
        $scope.ngModel = sort;
      };

      $scope.changeSort($location.search().s);

      $scope.$watch('ngModel', function() {
        $location.search('s', $scope.ngModel ? $scope.ngModel : null);
      });
    }
  ]);

  app.controller('SortDirController', ['$scope', '$location',
    function($scope, $location) {

      $scope.changeSortDir = function(dir) {
        if(!dir || dir === $scope.ngModel || (dir === 'asc') === $scope.ngModel) return;
        $scope.ngModel = dir === 'asc';
      };

      $scope.changeSortDir($location.search().d);

      $scope.$watch('ngModel', function() {
        if(typeof $scope.ngModel !== 'boolean')
          $location.search('d', null);
        else
          $location.search('d', $scope.ngModel === true ? 'asc' : 'desc');
      });
    }
  ]);
  */

  /**
   * Word Evolution
   */
  app.controller('WordEvolutionController', ['$scope',
    function($scope) {

      $scope.chartId = $scope.chartId || Vipra.randomId();
      $scope.wordSeqstyle = 'absolute';
      $scope.wordChartstyle = 'spline';

      $scope.resetWordZoom = function() {
        if (!$scope.wordsSelected) return;
        var highcharts = $('#' + $scope.chartId).highcharts();
        if (!highcharts) return;

        highcharts.xAxis[0].setExtremes(null, null);
      };

      $scope.redrawWordEvolutionChart = function() {
        var evolutions = [];
        if ($scope.topic && $scope.topic.words && $scope.topic.sequences) {

          // create series
          for (var i = 0, word, probs; i < $scope.topic.words.length; i++) {
            word = $scope.topic.words[i];
            if (!word.selected) continue;
            probs = [];
            for (var j = 0, prob; j < word.sequenceProbabilities.length; j++) {
              prob = $scope.wordSeqstyle === 'relative' ? word.sequenceProbabilitiesChange[j] : word.sequenceProbabilities[j];
              probs.push([new Date($scope.topic.sequences[j].window.startDate).getTime(), prob]);
            }
            evolutions.push({
              id: word.word,
              name: word.word,
              color: word.color,
              data: probs
            });
          }
        }

        $scope.wordEvolution = areaRelevanceChart(evolutions, 'Word Evolution', $scope.wordChartstyle);
        $scope.wordsSelected = evolutions.length;
      };

      $scope.highlightSeries = function(id, toggle) {
        if (!$scope.wordsSelected) return;
        var highcharts = $('#' + $scope.chartId).highcharts();
        if (!highcharts) return;
        var series = highcharts.get(id);
        if (!series) return;

        if (toggle) {
          series.onMouseOver();
          series.group.zIndexOrig = series.group.zIndex;
          series.group.zIndexSetter(9999, 'zIndex');
          series.graph.attr('stroke', '#000000').attr('stroke-dasharray', '5,5');
        } else {
          series.onMouseOut();
          series.group.zIndexSetter(series.group.zIndexOrig, 'zIndex');
          series.graph.attr('stroke', series.color).attr('stroke-dasharray', '');
        }
      };

      $scope.$watchGroup(['wordSeqstyle', 'wordChartstyle', 'topic'], $scope.redrawWordEvolutionChart);

      $scope.$watch('topic', function() {
        if($scope.topic) {
          var colors = randomColor({
            count: $scope.topic.words.length
          });

          for (var i = 0; i < $scope.topic.words.length; i++) {
            $scope.topic.words[i].color = colors[i];
          }
        }

        $scope.redrawWordEvolutionChart();
      });
    }
  ]);

  /****************************************************************************
   * Shared Highcharts configurations
   ****************************************************************************/

  function areaRelevanceChart(series, title, chartType, chartStack, clickCallback) {
    return {
      chart: {
        type: (chartType || 'areaspline'),
        zoomType: 'x',
        spacingLeft: 0,
        spacingRight: 0
      },
      title: {
        text: title
      },
      xAxis: {
        type: 'datetime',
        title: {
          text: 'Sequence'
        }
      },
      yAxis: {
        title: {
          text: 'Relevance'
        }
      },
      tooltip: {
        headerFormat: '<b>{series.name}</b><br>',
        pointFormat: '{point.x:%Y}: {point.y:.4f}'
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      navigator: {
        enabled: true
      },
      plotOptions: {
        areaspline: {
          fillOpacity: 0.5,
        },
        series: {
          stacking: (chartStack === 'none' ? null : chartStack),
          animation: false,
          cursor: 'pointer',
          allowPointSelect: true,
          states: {
            hover: {
              color: '#ff0000',
              lineWidth: 4,
              halo: {
                size: 9,
                attributes: {
                  fill: Highcharts.getOptions().colors[2],
                  'stroke-width': 2,
                  stroke: Highcharts.getOptions().colors[1]
                }
              }
            }
          },
          point: {
            events: {
              click: clickCallback
            }
          }
        }
      },
      noData: {
        style: {
          fontSize: "14px",
          color: "#777",
          fontWeight: "normal"
        }
      },
      series: series
    };
  }

  function topicShareChart(series) {
    return {
      chart: {
        type: 'pie',
        spacingBottom: 0,
        spacingTop: 0,
        spacingLeft: 0,
        spacingRight: 0
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        pie: {
          size: '100%',
          dataLabels: {
            enabled: false
          },
          allowPointSelect: true
        }
      },
      title: {
        text: ''
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      noData: {
        style: {
          fontSize: "14px",
          color: "#777",
          fontWeight: "normal"
        }
      },
      series: series
    };
  }

  function itemCountChart(series, categories) {
    return {
      chart: {
        type: 'column',
        spacingLeft: 0,
        spacingRight: 0,
        height: 300
      },
      title: {
        text: null
      },
      credits: {
        enabled: false
      },
      xAxis: {
        categories: categories
      },
      yAxis: {
        title: null
      },
      tooltip: {
        headerFormat: '',
        pointFormat: '{series.name}: <b>{point.y}</b>'
      },
      legend: {
        enabled: false
      },
      noData: {
        style: {
          fontSize: "14px",
          color: "#777",
          fontWeight: "normal"
        }
      },
      series: series
    };
  }

})();