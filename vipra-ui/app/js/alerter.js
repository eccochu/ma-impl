/******************************************************************************
 * Vipra Application
 * Alerter library
 ******************************************************************************/
/* globals $, Alerter */
;(function(g) {
  'use strict';

  if(g.Alerter) return;

  g.Alerter = function(area, create, classes, style) {
    this.area = area || 'alerts';
    this.alerts = [];
    this.queue = [];
    this.settings = $.extend({}, Alerter.defaultSettings);
    this.alertDefaults = $.extend({}, Alerter.alertDefaults);
    if(create)
      this.createAlertArea(classes, style);
    Alerter.instances.push(this);
  };

  Alerter.instances = [];

  Alerter.defaultSettings = {
    prepend: true,
    maxAlerts: 0,
    queue: true,
    merge: false
  };

  Alerter.alertDefaults = {
    dismissible: true,
    fade: true,
    fadeTime: 300,
    fadeAfter: 5000
  };

  Alerter.prototype.setSettings = function(s, opts) {
    if(s)
      this.settings = $.extend({}, this.settings, s);
    if(opts)
      this.setAlertDefaults(opts);
    return this;
  };

  Alerter.prototype.getSettings = function() {
    return this.settings;
  };

  Alerter.prototype.setAlertDefaults = function(opts) {
    this.alertDefaults = $.extend({}, this.alertDefaults, opts);
    return this;
  };

  Alerter.prototype.showAlert = function(type, title, msg, opts) {
    return this.showAlerts(type, [{title: title, msg: msg}], opts);
  };

  Alerter.prototype.showAlerts = function(type, body, opts) {
    opts = $.extend({}, this.alertDefaults, opts);

    if(this.settings.merge && this.mergeAlerts(type, body, opts)) {
      return;
    }

    var id = '_' + Math.random().toString(36).substr(2, 9),
      c = {
        instance: this,
        id: id,
        type: type,
        opts: opts,
        time: new Date().getTime(),
        body: body
      };

    if(this.settings.maxAlerts > 0 && Object.keys(this.alerts).length >= this.settings.maxAlerts) {
      if(this.settings.queue) {
        this.queue.push(c);
        return;
      } else {
        this.removeOldestAlert();
      }
    }

    var html = this.createAlert(c),
      el = c.el = this.settings.prepend ? $(html).prependTo('.' + this.area) : $(html).appendTo('.' + this.area);

    this.alerts[id] = c;

    if(opts.fade) {
      el.hide().fadeIn(opts.fadeTime, function() {
        this.restartAlert(id);
      }.bind(this));
    } else {
      this.restartAlert(id);
    }

    return c;
  };

  Alerter.prototype.showSuccess = function() {
    return this.$alert('success', arguments);
  };

  Alerter.prototype.showInfo = function() {
    return this.$alert('info', arguments);
  };

  Alerter.prototype.showWarning = function() {
    return this.$alert('warning', arguments);
  };

  Alerter.prototype.showDanger = function() {
    return this.$alert('danger', arguments);
  };

  Alerter.prototype.showPrimary = function() {
    return this.$alert('primary', arguments);
  };

  Alerter.prototype.unqueueAlert = function() {
    if(this.queue.length) {
      var c = this.queue.shift();
      this.showAlerts(c.type, c.body, c.opts);
    }
    return this;
  };

  Alerter.prototype.mergeAlert = function(type, title, msg, opts) {
    this.mergeAlerts(type, [{title: title, msg: msg}], opts);
    return this;
  };

  Alerter.prototype.mergeAlerts = function(type, body, opts) {
    for(var id in this.alerts) {
      if(this.alerts[id].type === type) {
        return this.editAlerts(id, body, opts, true);
      }
    }
    if(this.settings.queue) {
      for(var i = 0; i < this.queue.length; i++) {
        if(this.queue[i].type === type) {
          return this.editAlerts(this.queue[i].id, body, opts, false);
        }
      }
    }
    return false;
  };

  Alerter.prototype.getAlert = function(id) {
    var c = this.alerts[id];
    if(c) return c;
    if(this.settings.queue) {
      for(var i = 0; i < this.queue.length; i++) {
        if(this.queue[i].id === id) {
          return this.queue[i];
        }
      }
    }
    return null;
  };

  Alerter.prototype.editAlert = function(id, title, msg, opts, restart) {
    this.editAlerts(id, [{title: title, msg: msg}], restart);
    return this;
  };

  Alerter.prototype.editAlerts = function(id, body, opts, restart) {
    var c = this.getAlert(id);
    if(c) {
      c.body.push.apply(c.body, body);
      if(opts) {
        for(var key in opts) {
          if(key !== 'body') {
            c[key] = opts[key];
          }
        }
      }
      if(c.el) {
        var elBody = c.el.find('.alert-body');
        for(var i = 0; i < body.length; i++) {
          elBody.append(this.createAlertBody(body[i].title, body[i].msg));
        }
      }
      if(restart) {
        this.restartAlert(id);
      }
      return true;
    }
    return false;
  };

  Alerter.prototype.restartAlert = function(id) {
    var c = this.getAlert(id);
    if(c) {
      clearTimeout(c.timeout);
      if(c.opts.fadeAfter && c.opts.fadeAfter > 0) {
        c.timeout = setTimeout(function() {
          this.removeAlert(id);
        }.bind(this), c.opts.fadeAfter);
      }
    }
    return this;
  };

  Alerter.prototype.removeAlert = function(id) {
    var c = this.getAlert(id);
    if(c) {
      if(c.opts.fade) {
        c.el.fadeOut(c.opts.fadeTime, function() {
          $(this).remove();
          delete this.alerts[id];
          this.unqueueAlert();
        }.bind(this));
      } else {
        c.el.remove();
        delete this.alerts[id];
        this.unqueueAlert();
      }
    }
    return this;
  };

  Alerter.prototype.removeAlerts = function() {
    this.queue = [];
    for(var id in this.alerts)
      this.removeAlert(id);
    return this;
  };

  Alerter.prototype.getOldestAlert = function() {
    var oldest = null;
    for(var id in this.alerts) {
      if(!oldest || this.alerts[id].time < oldest.time)
        oldest = this.alerts[id];
    }
    return oldest;
  };

  Alerter.prototype.removeOldestAlert = function() {
    var oldest = this.getOldestAlert();
    if(oldest) {
      this.removeAlert(oldest.id);
    }
    return this;
  };

  Alerter.prototype.createAlert = function(c) {
    var msgs = "";
    for(var i = 0; i < c.body.length; i++)
      msgs += this.createAlertBody(c.body[i].title, c.body[i].msg);
    return '<div id="' + c.id + '" class="alert alert-' + c.type + ' ' + (c.opts.dismissible ? 'alert-dismissible' : '') +
      '" role="alert">' + (c.opts.dismissible ? '<button onclick="Alerter.removeAlertById(\'' + 
      c.id + '\')" type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' : '') +
      '<div class="alert-body">' + msgs + '</div></div>';
  };

  Alerter.prototype.createAlertBody = function(title, msg) {
    return '<div><strong>' + (title ? title : '') + '</strong> ' + msg + '</div>';
  };

  Alerter.prototype.createAlertArea = function(classes, style) {
    $('<div class="' + this.area + ' ' + classes + '" style="' + style + '"></div>').appendTo('body');
    return this;
  };

  /**
   * Private functions
   */

  Alerter.prototype.$alert = function(type, args) {
    if(!args || !args.length)
      throw 'not enough arguments';
    if(args && args.length == 1)
      return this.showAlert(type, null, args[0]);
    if(args && args.length == 2) {
      if(typeof args[1] === 'object')
        return this.showAlert(type, null, args[0], args[1]);
      else
        return this.showAlert(type, args[0], args[1]);
    }
    return this.showAlert(type, args[0], args[1], args[2]);
  };

  /**
   * Singleton
   */

  var _A = new Alerter('alerts');
  Alerter.instances.push(_A);

  Alerter.setSettings = _A.setSettings.bind(_A);
  Alerter.getSettings = _A.getSettings.bind(_A);
  Alerter.setAlertDefaults = _A.setAlertDefaults.bind(_A);
  Alerter.showAlert = _A.showAlert.bind(_A);
  Alerter.showAlerts = _A.showAlerts.bind(_A);
  Alerter.showSuccess = _A.showSuccess.bind(_A);
  Alerter.showInfo = _A.showInfo.bind(_A);
  Alerter.showWarning = _A.showWarning.bind(_A);
  Alerter.showDanger = _A.showDanger.bind(_A);
  Alerter.showPrimary = _A.showPrimary.bind(_A);
  Alerter.unqueueAlert = _A.unqueueAlert.bind(_A);
  Alerter.mergeAlert = _A.mergeAlert.bind(_A);
  Alerter.mergeAlerts = _A.mergeAlerts.bind(_A);
  Alerter.getAlert = _A.getAlert.bind(_A);
  Alerter.editAlert = _A.editAlert.bind(_A);
  Alerter.editAlerts = _A.editAlerts.bind(_A);
  Alerter.restartAlert = _A.restartAlert.bind(_A);
  Alerter.removeAlert = _A.removeAlert.bind(_A);
  Alerter.removeAlerts = _A.removeAlerts.bind(_A);
  Alerter.getOldestAlert = _A.getOldestAlert.bind(_A);
  Alerter.removeOldestAlert = _A.removeOldestAlert.bind(_A);
  Alerter.createAlert = _A.createAlert.bind(_A);
  Alerter.createAlertBody = _A.createAlertBody.bind(_A);
  Alerter.createAlertArea = _A.createAlertArea.bind(_A);

  Alerter.removeAlertById = function(id) {
    for(var i = 0; i < Alerter.instances.length; i++) {
      Alerter.instances[i].removeAlert(id);
    }
  };

  Alerter.removeAllAlerts = function() {
    for(var i = 0; i < Alerter.instances.length; i++) {
      Alerter.instances[i].removeAlerts();
    }
  };

})(this);