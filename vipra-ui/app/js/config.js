/******************************************************************************
 * Vipra Application
 * Configuration
 ******************************************************************************/
/* globals Vipra */
(function() {

  "use strict";

  window.Vipra = window.Vipra || {};

  Vipra.config = {
    /*
     * Point this URL to the backend REST servlet. The default is /rest, which expects the servlet
     * to be deployed in the root context. If the servlet is deployed under another context, prepend
     * it to this URL. Test the URL by entering this into a browser:
     *
     *    http://yourserver[/rest]/application.wadl
     *                      ^^^^^ 
     *                      this is the restUrl
     */
    restUrl: '/rest',
  };

})();