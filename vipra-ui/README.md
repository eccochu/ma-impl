# Vipra UI

The frontend UI project. Built with AngularJS and many other Javascript and CSS frameworks and libraries:

* [AngularJS](https://github.com/angular/angular.js) by Google (MIT)
* [Angular Hotkeys](https://github.com/chieffancypants/angular-hotkeys) by Wes Cruver (MIT)
* [AngularUI Router](https://github.com/angular-ui/ui-router) by The AngularUI Team, Karsten Sperling (MIT)
* [jQuery](https://github.com/jquery/jquery) by the jQuery Foundation and other contributors (MIT)
* [Bootstrap](https://github.com/twbs/bootstrap) by Twitter (MIT)
* [bootstrap-datetimepicker](https://github.com/Eonasdan/bootstrap-datetimepicker) by Jonathan Peterson (MIT)
* [nya-bootstrap-select](https://github.com/lordfriend/nya-bootstrap-select) by Nyasoft (MIT)
* [Bootbox.js](https://github.com/makeusabrew/bootbox) by Nick Payne (MIT)
* [Highcharts](http://www.highcharts.com/) by Highsoft (CC-BY-NC)
* [vis.js](https://github.com/almende/vis) by Almende B.V. (MIT)
* [Moment.js](https://github.com/moment/moment) by Tim Wood, Iskren Chernev, Moment.js contributors (MIT)
* [randomColor](https://github.com/davidmerfield/randomColor) by David Merfield (CC0 1.0)
* [FontAwesome](https://github.com/FortAwesome/Font-Awesome) by Dave Gandy (SIL OFL 1.1/MIT)
* [Bower](https://github.com/bower/bower) by Twitter and other contributors (MIT)
* [Reconnecting Websocket](https://github.com/joewalnes/reconnecting-websocket) by Joe Walnes (MIT)
* [Awesome Bootstrap Checkbox](https://github.com/flatlogic/awesome-bootstrap-checkbox) by flatlogic.com (MIT)
* [Gulp](https://github.com/gulpjs/gulp) by Fractal (MIT)
* [gulp-less](https://github.com/plus3network/gulp-less) by Plus 3 Network (MIT)
* [gulp-gzip](https://github.com/jstuckey/gulp-gzip) by Jeremy Stuckey (MIT)
* [gulp-concat](https://github.com/contra/gulp-concat) by Fractal (MIT)
* [gulp-uglify](https://github.com/terinjokes/gulp-uglify) by Terin Stock (MIT)
* [gulp-clean-css](https://github.com/scniro/gulp-clean-css) by scniro (MIT)
* [gulp-webserver](https://github.com/schickling/gulp-webserver) by Johannes Schickling (MIT)
* [gulp-ng-annotate](https://github.com/Kagami/gulp-ng-annotate) by Kagami Hiiragi and contributors (CC0 1.0)
* [gulp-angular-templatecache](https://github.com/miickel/gulp-angular-templatecache) by Mickel (MIT)

## Installation

### Base path

Simply put all files into a static web server document root.

If the application is not deployed into a web root (/), change the base tag in the file `index.html`
```html
<base href="/somepath" />
```
the href value has to be the same under which the application should be available, e.g. http://someserver/somepath/index.html.

### Backend connection

The backend is expected to be deployed to a Java application server. To change the default REST URL, edit the file `js/config.js`