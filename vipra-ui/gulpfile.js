/* jshint ignore: start */

var gulp = require('gulp'),
  gzip = require('gulp-gzip'),
  less = require('gulp-less'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  cleancss = require('gulp-clean-css'),
  webserver = require('gulp-webserver'),
  ngannotate = require('gulp-ng-annotate'),
  templatecache = require('gulp-angular-templatecache');

var assets = {
  js: [
    'bower_components/jquery/dist/jquery.min.js',
    'bower_components/jquery-ui/ui/core.js',
    'bower_components/jquery-ui/ui/widget.js',
    'bower_components/jquery-ui/ui/mouse.js',
    'bower_components/jquery-ui/ui/draggable.js',
    'bower_components/angular/angular.min.js',
    'bower_components/angular-resource/angular-resource.min.js',
    'bower_components/angular-sanitize/angular-sanitize.min.js',
    'bower_components/angular-animate/angular-animate.min.js',
    'bower_components/angular-hotkeys/build/hotkeys.min.js',
    'bower_components/angular-ui-router/release/angular-ui-router.min.js',
    'bower_components/bootstrap/dist/js/bootstrap.min.js',
    'bower_components/highcharts/highstock.js',
    'bower_components/highcharts/modules/no-data-to-display.js',
    'bower_components/vis/dist/vis.min.js',
    'bower_components/moment/min/moment-with-locales.min.js',
    'bower_components/randomcolor/randomColor.js',
    'bower_components/bootbox.js/bootbox.js',
    'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
    'bower_components/angulartics/dist/angulartics.min.js',
    'bower_components/angulartics-google-analytics/dist/angulartics-google-analytics.min.js',
    'bower_components/jquery-layout/source/stable/jquery.layout.min.js',
    'bower_components/angular-loading-bar/build/loading-bar.min.js'
  ],
  css: [
    'bower_components/bootstrap/dist/css/bootstrap.min.css',
    'bower_components/font-awesome/css/font-awesome.min.css',
    'bower_components/vis/dist/vis.min.css',
    'bower_components/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
    'bower_components/angular-hotkeys/build/hotkeys.min.css',
    'bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
    'bower_components/angular-loading-bar/build/loading-bar.min.css'
  ],
  fonts: [
    'bower_components/bootstrap/dist/fonts/*',
    'bower_components/font-awesome/fonts/*'
  ],
  img: []
};

var gzip_options = {
  threshold: 1400,
  gzipOptions: {
    level: 6
  }
};

gulp.task('less', function() {
  gulp.src('app/less/**/*.less')
    .pipe(concat('app.css'))
    .pipe(less())
    .pipe(cleancss())
    .pipe(gulp.dest('public/css'));
});

gulp.task('js', function() {
  gulp.src(['app/js/**/*.js', '!app/js/config.js'])
    .pipe(concat('app.js'))
    .pipe(ngannotate())
    .pipe(gulp.dest('public/js'));
  gulp.src('app/js/config.js')
    .pipe(gulp.dest('public/js'));
});

gulp.task('js-rel', function() {
  gulp.src(['app/js/**/*.js', '!app/js/config.js', '!app/js/dev.js'])
    .pipe(concat('app.js'))
    .pipe(ngannotate())
    .pipe(uglify())
    .pipe(gulp.dest('public/js'));
  gulp.src('app/js/config.js')
    .pipe(uglify())
    .pipe(gulp.dest('public/js'));
});

gulp.task('html', function() {
  gulp.src('app/index.html')
    .pipe(gulp.dest('public'));
  gulp.src(['app/html/**/*.html'])
    .pipe(gulp.dest('public/html'));
  gulp.src(['app/html/**/*.html'])
    .pipe(templatecache({
      module: 'vipra.templates',
      standalone: true
    }))
    .pipe(ngannotate())
    .pipe(uglify())
    .pipe(gulp.dest('public/js'));
});

gulp.task('img', function() {
  gulp.src('app/img/**/*.*')
    .pipe(gulp.dest('public/img'));
});

gulp.task('public', function() {
  gulp.src('app/public/**/*')
    .pipe(gulp.dest('public'));
});

gulp.task('assets', function() {
  gulp.src(assets.js)
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('public/js'));
  gulp.src(assets.css)
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest('public/css'));
  gulp.src(assets.fonts)
    .pipe(gulp.dest('public/fonts'));
  gulp.src(assets.img)
    .pipe(gulp.dest('public/img'));
});

gulp.task('assets-rel', function() {
  gulp.src(assets.js)
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/js'));
  gulp.src(assets.css)
    .pipe(cleancss())
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest('public/css'));
  gulp.src(assets.fonts)
    .pipe(gulp.dest('public/fonts'));
  gulp.src(assets.img)
    .pipe(gulp.dest('public/img'));
});

gulp.task('compress', function() {
  gulp.src('public/css/**/*')
    .pipe(gzip(gzip_options))
    .pipe(gulp.dest('public/css'));
  gulp.src('public/fonts/**/*')
    .pipe(gzip(gzip_options))
    .pipe(gulp.dest('public/fonts'));
  gulp.src('public/html/**/*')
    .pipe(gzip(gzip_options))
    .pipe(gulp.dest('public/html'));
  gulp.src('public/js/**/*')
    .pipe(gzip(gzip_options))
    .pipe(gulp.dest('public/js'));
  gulp.src('public/*')
    .pipe(gzip(gzip_options))
    .pipe(gulp.dest('public'));
});

gulp.task('build-dev', ['less', 'js', 'html', 'img', 'public', 'assets']);

gulp.task('build', ['less', 'js-rel', 'html', 'img', 'public', 'assets-rel']);

gulp.task('watch', function() {
  gulp.watch('app/less/**/*.less', ['less']);
  gulp.watch('app/js/**/*.js', ['js']);
  gulp.watch(['app/index.html', 'app/html/**/*.html'], ['html']);
  gulp.watch('app/public/**/*', ['public']);
});

gulp.task('server', function() {
  gulp.src('public')
    .pipe(webserver({
      open: true,
      port: 4200,
      fallback: 'index.html'
    }));
});