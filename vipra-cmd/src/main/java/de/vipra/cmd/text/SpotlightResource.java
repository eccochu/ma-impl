package de.vipra.cmd.text;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SpotlightResource {

	@JsonProperty("@URI")
	private String uri;

	@JsonProperty("@support")
	private int support;

	@JsonProperty("@types")
	private List<String> types;

	@JsonProperty("@surfaceForm")
	private String surfaceForm;

	@JsonProperty("@offset")
	private int offset;

	@JsonProperty("@similarityScore")
	private double similarityScore;

	@JsonProperty("@percentageOfSecondRank")
	private double percentageOfSecondRank;

	public String getUri() {
		return uri;
	}

	public void setUri(final String uri) {
		this.uri = uri;
	}

	public int getSupport() {
		return support;
	}

	public void setSupport(final int support) {
		this.support = support;
	}

	public List<String> getTypes() {
		return types;
	}

	public void setTypes(final List<String> types) {
		this.types = types;
	}

	@JsonSetter("@types")
	public void setTypes(final String types) {
		this.types = Arrays.asList(types.split(","));
	}

	public String getSurfaceForm() {
		return surfaceForm;
	}

	public void setSurfaceForm(final String surfaceForm) {
		this.surfaceForm = surfaceForm;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(final int offset) {
		this.offset = offset;
	}

	public double getSimilarityScore() {
		return similarityScore;
	}

	public void setSimilarityScore(final double similarityScore) {
		this.similarityScore = similarityScore;
	}

	public double getPercentageOfSecondRank() {
		return percentageOfSecondRank;
	}

	public void setPercentageOfSecondRank(final double percentageOfSecondRank) {
		this.percentageOfSecondRank = percentageOfSecondRank;
	}

}
