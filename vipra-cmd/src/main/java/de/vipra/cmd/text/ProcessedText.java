package de.vipra.cmd.text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.vipra.util.CountMap;
import de.vipra.util.model.ArticleWord;

public class ProcessedText {

	private final String[] words;
	private final long originalWordCount;
	private final long reducedWordCount;
	private final double reductionRatio;
	private final CountMap<String> wordCounts;
	private final List<ArticleWord> articleWords;
	private final Map<String, String> lemmas;

	public ProcessedText(final String text, final long wordCount, final Map<String, String> lemmas) {
		final String[] allWords = text.trim().split("\\s+");
		final List<String> wordList = new ArrayList<>(allWords.length);
		final Map<String, String> cleanedLemmas = new HashMap<>(lemmas.size());
		for (final String word : allWords) {
			if (word != null && !word.trim().isEmpty())
				wordList.add(word.toLowerCase().trim());
			final String lemma = lemmas.get(word);
			if (lemma != null)
				cleanedLemmas.put(lemma, word);
		}
		words = wordList.toArray(new String[allWords.length]);
		originalWordCount = wordCount;
		reducedWordCount = words.length;
		reductionRatio = 1 - ((double) reducedWordCount / wordCount);

		this.lemmas = cleanedLemmas;

		wordCounts = new CountMap<>();
		for (final String word : words)
			wordCounts.count(word);

		articleWords = new ArrayList<>(wordCounts.size());
		for (final Entry<String, Integer> entry : wordCounts.entrySet())
			articleWords.add(new ArticleWord(entry.getKey(), entry.getValue()));
		Collections.sort(articleWords, Comparator.reverseOrder());
	}

	public String[] getWords() {
		return words;
	}

	public long getWordCount() {
		return originalWordCount;
	}

	public long getReducedWordCount() {
		return reducedWordCount;
	}

	public double getReductionRatio() {
		return reductionRatio;
	}

	public List<ArticleWord> getArticleWords() {
		return articleWords;
	}

	public CountMap<String> getWordCounts() {
		return wordCounts;
	}

	public Map<String, String> getLemmas() {
		return lemmas;
	}

}
