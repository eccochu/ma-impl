package de.vipra.cmd.text;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import edu.stanford.nlp.ling.CoreAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.Annotator;

public class StopwordsAnnotator implements Annotator, CoreAnnotation<Boolean> {

	public static final String NAME = "stopwords";

	private final Set<String> stopWords;

	public StopwordsAnnotator(final String input, final Properties props) {
		stopWords = new HashSet<>(Arrays.asList(props.getProperty(NAME).split(" ")));
		stopWords.addAll(Arrays.asList("-lrb-", "-rrb-", "-lsb-", "-rsb-", "-lcb-", "-rcb-"));
	}

	@Override
	public void annotate(final Annotation annotation) {
		final List<CoreLabel> tokens = annotation.get(TokensAnnotation.class);
		for (final CoreLabel token : tokens) {
			if (stopWords.contains(token.word().toLowerCase().trim()))
				token.set(StopwordsAnnotator.class, true);
		}
	}

	@Override
	public Set<Requirement> requirementsSatisfied() {
		return Collections.singleton(new Requirement(NAME));
	}

	@Override
	public Set<Requirement> requires() {
		return TOKENIZE_AND_SSPLIT;
	}

	@Override
	public Class<Boolean> getType() {
		return Boolean.class;
	}

}