package de.vipra.cmd.text;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import de.vipra.util.Config;
import de.vipra.util.URLUtils;
import de.vipra.util.ex.ConfigException;
import de.vipra.util.model.TopicModelConfig;

public class SpotlightAnalyzer {

	private final URL spotlightUrl;
	private final TopicModelConfig modelConfig;

	public SpotlightAnalyzer(final TopicModelConfig modelConfig) throws MalformedURLException, ConfigException {
		final Config config = Config.getConfig();
		spotlightUrl = new URL(URLUtils.concat(config.getSpotlightUrl(), "/rest/annotate"));
		this.modelConfig = modelConfig;
	}

	public SpotlightResponse analyze(String text) throws IOException {
		text = "confidence=" + modelConfig.getSpotlightConfidence() + "&support=" + modelConfig.getSpotlightSupport() + "&text="
				+ URLEncoder.encode(text, "UTF-8");

		final HttpURLConnection connection = (HttpURLConnection) spotlightUrl.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Length", Integer.toString(text.getBytes().length));
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestProperty("Accept", "application/json");
		connection.setUseCaches(false);
		connection.setDoOutput(true);

		final DataOutputStream out = new DataOutputStream(connection.getOutputStream());
		out.writeBytes(text);
		out.close();

		final BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		final StringBuilder result = new StringBuilder();
		String line = null;
		while ((line = in.readLine()) != null)
			result.append(line);

		return Config.mapper.readValue(result.toString(), SpotlightResponse.class);
	}

}
