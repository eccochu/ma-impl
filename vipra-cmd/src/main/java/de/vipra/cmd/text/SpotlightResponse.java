package de.vipra.cmd.text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.vipra.util.CountMap;
import de.vipra.util.model.TextEntity;
import de.vipra.util.model.TextEntityCount;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SpotlightResponse {

	@JsonProperty("Resources")
	private List<SpotlightResource> resources;

	public List<SpotlightResource> getResources() {
		return resources;
	}

	public void setResources(final List<SpotlightResource> resources) {
		this.resources = resources;
	}

	public List<TextEntityCount> getEntities() {
		if (resources == null)
			return new ArrayList<>();
		final CountMap<String> textEntitiesCount = new CountMap<>(resources.size());
		final Set<TextEntity> textEntities = new HashSet<>(resources.size());

		// get entities and count
		for (final SpotlightResource resource : resources) {
			final Set<String> types = new HashSet<>(resource.getTypes().size());
			for (final String type : resource.getTypes()) {
				if (!type.isEmpty()) {
					final String[] parts;
					if (type.contains("/"))
						parts = type.split("/");
					else
						parts = type.split(":");
					if (parts.length > 0)
						types.add(parts[parts.length - 1]);
				}
			}

			final TextEntity textEntity = new TextEntity(resource.getSurfaceForm(), resource.getUri());
			textEntity.setTypes(new ArrayList<>(types));

			textEntities.add(textEntity);
			textEntitiesCount.count(resource.getSurfaceForm());
		}

		// insert count
		final List<TextEntityCount> textEntitiesCountList = new ArrayList<>(textEntities.size());
		for (final TextEntity textEntity : textEntities) {
			final TextEntityCount textEntityCount = new TextEntityCount();
			textEntityCount.setEntity(textEntity);
			textEntityCount.setCount(textEntitiesCount.get(textEntity.getEntity()));
			textEntitiesCountList.add(textEntityCount);
		}

		Collections.sort(textEntitiesCountList, Comparator.reverseOrder());

		return textEntitiesCountList;
	}

}
