package de.vipra.cmd.text;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.zeroturnaround.zip.commons.IOUtils;

import de.ruedigermoeller.serialization.FSTConfiguration;
import de.vipra.util.FileUtils;

public class HypernymAnalyzer {

	private static HypernymAnalyzer instance;

	private final Map<String, String> hypernyms;

	@SuppressWarnings("unchecked")
	private HypernymAnalyzer() throws IOException, ClassNotFoundException {
		final InputStream in = FileUtils.getResource("hypernyms.ser");
		final byte[] barray = IOUtils.toByteArray(in);
		final FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
		hypernyms = (Map<String, String>) conf.asObject(barray);
	}

	public boolean containsEntity(final String entity) {
		return hypernyms.containsKey(StringEscapeUtils.unescapeJava(entity));
	}

	public String getHypernym(final String entity) {
		return hypernyms.get(StringEscapeUtils.unescapeJava(entity));
	}

	public String getURL(final String hypernym) {
		return "http://dbpedia.org/resource/" + hypernym;
	}

	public static HypernymAnalyzer getInstance() throws IOException, ClassNotFoundException {
		if (instance == null)
			instance = new HypernymAnalyzer();
		return instance;
	}
}
