package de.vipra.cmd.text;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DBPediaAnalyzer {

	public static final String DBPEDIA_SPARQL_ENDPOINT = "http://dbpedia.org/sparql";

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String getAbstract(final String resourceURL, final boolean retry) throws Exception {

		final String query = "select ?abstract where {<" + resourceURL
				+ "> <http://dbpedia.org/ontology/abstract> ?abstract filter(langMatches(lang(?abstract),\"en\"))}";

		int retries = 0;
		while (++retries <= 2) {
			try {
				final String strUrl = DBPEDIA_SPARQL_ENDPOINT + "?default-graph-uri=" + URLEncoder.encode("http://dbpedia.org", "UTF-8")
						+ "&format=application%2Fsparql-results%2Bjson&CXML_redir_for_subjs=121&CXML_redir_for_hrefs=&timeout=30000&debug=on&query="
						+ URLEncoder.encode(query, "UTF-8");

				final URL url = new URL(strUrl);
				final String result = IOUtils.toString(url.openStream());

				if (result != null) {
					final ObjectMapper mapper = new ObjectMapper();
					final Map<String, Object> map = mapper.readValue(result, new TypeReference<HashMap>() {});

					Object o = map.get("results");
					if (o != null && o instanceof Map) {
						o = ((Map<String, Object>) o).get("bindings");
						if (o != null && o instanceof List) {
							final List<Object> l = (List<Object>) o;
							for (int i = 0; i < l.size(); i++) {
								o = l.get(i);
								if (o != null && o instanceof Map) {
									o = ((Map<String, Object>) o).get("abstract");
									if (o != null && o instanceof Map) {
										o = ((Map<String, Object>) o).get("value");
										if (o != null && o instanceof String)
											return (String) o;
									}
								}
							}
						}
					}
				}
				return null;
			} catch (final IOException e) {
				try {
					Thread.sleep(100);
				} catch (final InterruptedException e1) {}
			}
		}

		throw new Exception("dbpedia query error");
	}

}
