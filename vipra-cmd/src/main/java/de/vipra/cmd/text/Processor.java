package de.vipra.cmd.text;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import de.vipra.util.Constants;
import de.vipra.util.model.TopicModelConfig;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.StringUtils;

public class Processor {

	private final StanfordCoreNLP nlp;

	public Processor() {
		final Properties props = new Properties();
		props.setProperty("customAnnotatorClass.stopwords", StopwordsAnnotator.class.getCanonicalName());
		props.setProperty("customAnnotatorClass.frequency", FrequencyAnnotator.class.getCanonicalName());
		// tokenize: transform words to tokens
		// ssplit: split by and group into sentences
		// stopwords: mark stopwords
		// frequency: count word frequency
		// pos: mark word position
		// lemma: lemmatize words
		props.setProperty("annotators", "tokenize, ssplit, stopwords, pos, lemma, frequency");
		props.setProperty("stopwords", StringUtils.join(Constants.STOPWORDS));

		nlp = new StanfordCoreNLP(props);
	}

	public ProcessedText process(final TopicModelConfig modelConfig, final String input) throws ProcessorException {
		final Annotation doc = new Annotation(input);
		nlp.annotate(doc);
		final StringBuilder sb = new StringBuilder();
		final Map<String, String> lemmas = new HashMap<>();
		long wordCount = 0;
		// loop sentences
		for (final CoreMap sentence : doc.get(SentencesAnnotation.class)) {
			final List<CoreLabel> words = sentence.get(TokensAnnotation.class);
			// count words
			wordCount += words.size();
			// loop words
			for (final CoreLabel word : words) {
				// filter out stopwords
				final Boolean b = word.get(StopwordsAnnotator.class);
				if (b == null || !b) {
					// filter out infrequent words
					final Long count = word.get(FrequencyAnnotator.class);
					if (count != null && count >= modelConfig.getDocumentMinimumWordFrequency()) {
						final String lemma = word.get(LemmaAnnotation.class);
						if (lemma != null) {
							// collect unique words
							sb.append(lemma).append(" ");

							if (!lemma.equals(word.word()))
								lemmas.put(lemma, word.word());
						}
					}
				}
			}
		}

		final String text = clean(sb.toString());
		return new ProcessedText(text, wordCount, lemmas);
	}

	public static String clean(final String in) {
		return in.replaceAll(Constants.REGEX_EMAIL, "").replaceAll(Constants.REGEX_URL, "").replaceAll(Constants.REGEX_NUMBER, "")
				.replaceAll(Constants.CHARS_DISALLOWED, "").replaceAll(Constants.REGEX_SINGLECHAR, "").replaceAll("\\s+", " ").trim();
	}

}
