package de.vipra.cmd.text;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import edu.stanford.nlp.ling.CoreAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.Annotator;

public class FrequencyAnnotator implements Annotator, CoreAnnotation<Long> {

	public static final String NAME = "frequency";

	@Override
	public void annotate(final Annotation annotation) {
		final List<CoreLabel> tokens = annotation.get(TokensAnnotation.class);
		final Map<String, Long> words = tokens.stream().collect(Collectors.groupingBy(p -> p.get(LemmaAnnotation.class), Collectors.counting()));
		for (final CoreLabel token : tokens) {
			token.set(FrequencyAnnotator.class, words.get(token.get(LemmaAnnotation.class)));
		}
	}

	@Override
	public Set<Requirement> requirementsSatisfied() {
		return Collections.singleton(new Requirement(NAME));
	}

	@Override
	public Set<Requirement> requires() {
		return TOKENIZE_SSPLIT_POS_LEMMA;
	}

	@Override
	public Class<Long> getType() {
		return Long.class;
	}

}
