package de.vipra.cmd.text;

public class ProcessorException extends Exception {

	private static final long serialVersionUID = 1L;

	public ProcessorException(final String msg) {
		super(msg);
	}

	public ProcessorException(final Exception e) {
		super(e);
	}

}
