package de.vipra.cmd;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CommandLineOptions {

	public static final Option ERASE = Option.builder("e").longOpt("erase").desc("erase the database and models").build();
	public static final Option DEBUG = Option.builder("d").longOpt("debug").desc("show debug information").build();
	public static final Option HELP = Option.builder("h").longOpt("help").desc("show this help").build();
	public static final Option LIST = Option.builder("l").longOpt("list").desc("list available models").build();
	public static final Option SILENT = Option.builder("s").longOpt("silent").desc("suppress all output").build();
	public static final Option TEST = Option.builder("t").longOpt("test").desc("test database connections").build();
	public static final Option ALL = Option.builder("A").longOpt("all").desc("select all models (-S all)").build();
	public static final Option VERSION = Option.builder("v").longOpt("version").desc("print the version number").build();
	public static final Option PERF = Option.builder("p").longOpt("perf").desc("generate performance statistics").hasArg().optionalArg(true)
			.argName("file").build();
	public static final Option INDEX = Option.builder("i").longOpt("index").desc("create index for models").hasArgs().argName("[models...]")
			.optionalArg(true).build();
	public static final Option REREAD = Option.builder("r").longOpt("reread").desc("reread generated models").hasArgs().argName("[models...]")
			.optionalArg(true).build();
	public static final Option CREATE = Option.builder("C").longOpt("create").desc("create new models").hasArgs().argName("[models...]")
			.optionalArg(true).build();
	public static final Option DELETE = Option.builder("D").longOpt("delete").desc("delete existing models").hasArgs().argName("[models...]")
			.optionalArg(true).build();
	public static final Option EDIT = Option.builder("E").longOpt("edit").desc("edit config of selected models").hasArgs().argName("[models...]")
			.optionalArg(true).build();
	public static final Option PRINT = Option.builder("m").longOpt("print").desc("print model configuration").hasArgs().argName("[models...]")
			.optionalArg(true).build();
	public static final Option IMPORT = Option.builder("I").longOpt("import").desc("import data for models").hasArgs().argName("[models...]")
			.optionalArg(true).build();
	public static final Option MODEL = Option.builder("M").longOpt("model").desc("generate topics on models").hasArgs().argName("[models...]")
			.optionalArg(true).build();
	public static final Option RENAME = Option.builder("n").longOpt("rename").desc("rename selected models").hasArg().argName("[models...]")
			.optionalArg(true).build();
	public static final Option CLEAR = Option.builder("c").longOpt("clear").desc("clear selected models").hasArg().argName("[models...]")
			.optionalArg(true).build();
	public static final Option SELECT = Option.builder("S").longOpt("select").desc("select models").hasArgs().argName("models...").build();
	public static final Option BACKUP = Option.builder("B").longOpt("backup").desc("backup data/filebase").hasArg().argName("path").build();
	public static final Option RESTORE = Option.builder("R").longOpt("restore").desc("restore data/filebase").hasArg().argName("file").build();
	public static final Option LOG = Option.builder("o").longOpt("out").desc("print output to file").hasArg().optionalArg(true).argName("file")
			.build();
	public static final Option CONFIG = Option.builder("f").longOpt("config").desc("choose a custom config file").hasArg().argName("file/name")
			.build();

	private final Options options;
	private CommandLine cmd;
	private final String cmdName = "vipra";

	public CommandLineOptions() {
		final Option[] optionsArray = { ERASE, DEBUG, HELP, INDEX, LIST, REREAD, SILENT, TEST, ALL, CREATE, DELETE, EDIT, PRINT, IMPORT, MODEL,
				RENAME, CLEAR, SELECT, BACKUP, RESTORE, VERSION, PERF, LOG, CONFIG };
		options = new Options();
		for (final Option option : optionsArray)
			options.addOption(option);
	}

	public void parse(final String[] args) throws ParseException {
		cmd = new DefaultParser().parse(options, split(args));
	}

	private String[] split(final String[] args) {
		final List<String> args2 = new ArrayList<>();
		for (final String arg : args) {
			if (arg.startsWith("-") && arg.length() > 2) {
				for (final char c : arg.substring(1, arg.length()).toCharArray())
					args2.add("-" + c);
			} else {
				args2.add(arg);
			}
		}
		return args2.toArray(new String[args2.size()]);
	}

	public boolean hasOption(final Option opt) {
		return cmd.hasOption(opt.getOpt());
	}

	public String getOptionValue(final Option opt) {
		return cmd.getOptionValue(opt.getOpt());
	}

	public String[] getOptionValues(final Option opt) {
		return cmd.getOptionValues(opt.getOpt());
	}

	public boolean isErase() {
		return hasOption(ERASE);
	}

	public boolean isDebug() {
		return hasOption(DEBUG) && !hasOption(SILENT);
	}

	public boolean isHelp() {
		return hasOption(HELP);
	}

	public void printHelp() {
		final HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(cmdName, options, true);
	}

	public boolean isIndex() {
		return hasOption(INDEX);
	}

	public String[] modelsToIndex() {
		final String[] models = getOptionValues(INDEX);
		if (models != null && models.length > 0)
			return stripGroups(models);
		return stripGroups(selectedModels());
	}

	public boolean isList() {
		return hasOption(LIST);
	}

	public boolean isReread() {
		return !hasOption(MODEL) && hasOption(REREAD);
	}

	public String[] modelsToReread() {
		final String[] models = getOptionValues(REREAD);
		if (models != null && models.length > 0)
			return stripGroups(models);
		return stripGroups(selectedModels());
	}

	public boolean isSilent() {
		return hasOption(SILENT);
	}

	public boolean isTest() {
		return hasOption(TEST);
	}

	public boolean isCreate() {
		return hasOption(CREATE);
	}

	public boolean isAll() {
		return hasOption(ALL);
	}

	public String[] modelsToCreate() {
		final String[] models = getOptionValues(CREATE);
		if (models != null && models.length > 0)
			return models;
		return selectedModels();
	}

	public boolean isDelete() {
		return hasOption(DELETE);
	}

	public String[] modelsToDelete() {
		final String[] models = getOptionValues(DELETE);
		if (models != null && models.length > 0)
			return stripGroups(models);
		return stripGroups(selectedModels());
	}

	public boolean isEdit() {
		return hasOption(EDIT);
	}

	public String[] modelsToEdit() {
		final String[] models = getOptionValues(EDIT);
		if (models != null && models.length > 0)
			return stripGroups(models);
		return stripGroups(selectedModels());
	}

	public boolean isPrint() {
		return hasOption(PRINT);
	}

	public String[] modelsToPrint() {
		final String[] models = getOptionValues(PRINT);
		if (models != null && models.length > 0)
			return stripGroups(models);
		return stripGroups(selectedModels());
	}

	public boolean isImport() {
		return hasOption(IMPORT);
	}

	public String[] modelsForImport() {
		return stripGroups(selectedModels());
	}

	public String[] filesToImport() {
		return getOptionValues(IMPORT);
	}

	public boolean isModel() {
		return hasOption(MODEL);
	}

	public String[] modelsToModel() {
		final String[] models = getOptionValues(MODEL);
		if (models != null && models.length > 0)
			return stripGroups(models);
		return stripGroups(selectedModels());
	}

	public boolean isSelect() {
		return hasOption(SELECT);
	}

	public String[] selectedModels() {
		String[] models = null;
		if (isAll())
			models = new String[] { "all" };
		else if (isSelect())
			models = getOptionValues(SELECT);

		if (models != null && models.length > 0)
			return models;

		throw new RuntimeException("select at least one model");
	}

	public boolean isBackup() {
		return hasOption(BACKUP);
	}

	public String backupPath() {
		return getOptionValue(BACKUP);
	}

	public boolean isRestore() {
		return hasOption(RESTORE);
	}

	public String restorePath() {
		return getOptionValue(RESTORE);
	}

	public boolean isPerf() {
		return hasOption(PERF);
	}

	public String perfPath() {
		return getOptionValue(PERF);
	}

	public boolean isVersion() {
		return hasOption(VERSION);
	}

	public boolean isRename() {
		return hasOption(RENAME);
	}

	public String[] modelsToRename() {
		final String[] models = getOptionValues(RENAME);
		if (models != null && models.length > 0)
			return stripGroups(models);
		return stripGroups(selectedModels());
	}

	public boolean isClear() {
		return hasOption(CLEAR);
	}

	public String[] modelsToClear() {
		final String[] models = getOptionValues(CLEAR);
		if (models != null && models.length > 0)
			return stripGroups(models);
		return stripGroups(selectedModels());
	}

	public boolean isLog() {
		return hasOption(LOG);
	}

	public String logPath() {
		return getOptionValue(LOG);
	}

	public boolean isConfig() {
		return hasOption(CONFIG);
	}

	public String configString() {
		return getOptionValue(CONFIG);
	}

	private static String[] stripGroups(final String[] names) {
		if (names == null)
			return null;
		for (int i = 0; i < names.length; i++) {
			final String[] parts = names[i].split(":");
			if (parts.length > 1)
				names[i] = parts[1];
		}
		return names;
	}

}
