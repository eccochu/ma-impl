package de.vipra.cmd;

public enum ErrorCodes {
	NONE,
	CANNOT_LOCK,
	CANNOT_UNLOCK
}
