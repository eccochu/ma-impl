package de.vipra.cmd.lda;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.function.Consumer;

import org.bson.types.ObjectId;

import de.vipra.cmd.Main;
import de.vipra.cmd.file.FilebaseIDDateIndex;
import de.vipra.cmd.file.FilebaseIDDateIndexEntry;
import de.vipra.cmd.file.FilebaseWindowIndex;
import de.vipra.cmd.file.FilebaseWordIndex;
import de.vipra.util.ArrayUtils;
import de.vipra.util.ColorUtils;
import de.vipra.util.CompareMap;
import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.CountMatrix;
import de.vipra.util.MongoUtils;
import de.vipra.util.StringUtils;
import de.vipra.util.ex.ConfigException;
import de.vipra.util.ex.DatabaseException;
import de.vipra.util.model.Article;
import de.vipra.util.model.ArticleFull;
import de.vipra.util.model.Sequence;
import de.vipra.util.model.SequenceFull;
import de.vipra.util.model.SequenceWord;
import de.vipra.util.model.SimilarArticle;
import de.vipra.util.model.Topic;
import de.vipra.util.model.TopicFull;
import de.vipra.util.model.TopicModel;
import de.vipra.util.model.TopicModelConfig;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.model.TopicShare;
import de.vipra.util.model.TopicWord;
import de.vipra.util.service.MongoService;
import de.vipra.util.service.QueryBuilder;

public class Analyzer {

	private final Config config;
	private final File dataDir;
	private final File dtmBinary;
	private final MongoService<ArticleFull, ObjectId> dbArticles;
	private final MongoService<TopicFull, ObjectId> dbTopics;
	private final MongoService<SequenceFull, ObjectId> dbSequences;
	private final MongoService<TopicModelFull, ObjectId> dbTopicModels;

	public Analyzer() throws AnalyzerException, ConfigException {
		config = Config.getConfig();
		dataDir = config.getDataDirectory();
		dbArticles = MongoService.getDatabaseService(config, ArticleFull.class);
		dbTopics = MongoService.getDatabaseService(config, TopicFull.class);
		dbSequences = MongoService.getDatabaseService(config, SequenceFull.class);
		dbTopicModels = MongoService.getDatabaseService(config, TopicModelFull.class);

		// check for binary
		if (config.getDtmPath() == null || config.getDtmPath().isEmpty())
			throw new AnalyzerException("dtm path not configured");
		dtmBinary = new File(config.getDtmPath());
		if (dtmBinary == null || !dtmBinary.exists())
			throw new AnalyzerException("dtm binary not found at path: " + config.getDtmPath() + ", check config key 'tm.dtmpath'");
	}

	public void analyze(final TopicModelConfig modelConfig, final boolean reread, final Consumer<Double> progressCallback)
			throws AnalyzerException, DatabaseException, ParseException, IOException, InterruptedException {

		final File modelDir = modelConfig.getModelDir(dataDir);
		final File outDir = new File(modelDir, "out");
		final File outDirSeq = new File(outDir, "lda-seq");

		final String[] parameters = {
				// number of topics
				"--ntopics=" + modelConfig.getkTopics(),
				// topc modeling mode
				"--mode=fit",
				// random seed (0 for pseudo random)
				"--rng_seed=0",
				// initialize model with lda
				"--initialize_lda=true",
				// top chain var (default 0.005)
				"--top_chain_var=0.005",
				// alpha (default 0.01)
				"--alpha=0.01",
				// minimum number if iterations
				"--lda_sequence_min_iter=" + modelConfig.getDynamicMinIterations(),
				// maximum number of iterations
				"--lda_sequence_max_iter=" + modelConfig.getDynamicMaxIterations(),
				// em iter (default 20)
				"--lda_max_em_iter=" + modelConfig.getStaticIterations(),
				// input file prefix
				"--corpus_prefix=" + modelDir.getAbsolutePath() + File.separator + "dtm",
				// output directory
				"--outname=" + outDir.getAbsolutePath() };

		final String command = dtmBinary.getAbsolutePath() + " " + StringUtils.join(parameters, " ");

		BufferedReader in;

		final FilebaseWordIndex wordIndex = new FilebaseWordIndex(modelDir);
		final FilebaseIDDateIndex idDateIndex = new FilebaseIDDateIndex(modelDir);
		final FilebaseWindowIndex windowIndex = new FilebaseWindowIndex(modelDir, modelConfig.getWindowResolution());

		if (!reread) {
			recalcWindows(idDateIndex, windowIndex);

			final Process p = Runtime.getRuntime().exec(command, null);
			if (!p.isAlive())
				throw new AnalyzerException("dtm process is dead");

			// read from process output
			in = new BufferedReader(new InputStreamReader(p.getErrorStream()));

			String line;
			int iteration = 0;
			final int maxIterationsLength = Integer.toString(modelConfig.getDynamicMaxIterations()).length();

			long lastStarted = System.nanoTime();
			long lastDuration = 0;
			double avgDuration = 0;
			final double smoothingFactor = 0.1;
			int lastLength = printProgress(0, 0, iteration, maxIterationsLength, 0, modelConfig, 0);
			int nextUpdate = 10;
			
			List<Long> durations = new ArrayList<>(modelConfig.getDynamicMaxIterations());
			Main.stats.start("dtm-iter");

			while ((line = in.readLine()) != null) {
				if (line.contains("EM iter")) {
					Main.stats.stop("dtm-iter");
					Main.stats.start("dtm-iter");
					iteration++;

					// calculate progress
					final double progress = (double) iteration / modelConfig.getDynamicMaxIterations() * 100.0;
					final int tenthPercent = (int) (progress - progress % 10) / 10;

					// calculate remaining duration
					final long currentTime = System.nanoTime();
					lastDuration = currentTime - lastStarted;
					durations.add(lastDuration);
					lastStarted = currentTime;
					if (avgDuration == 0)
						avgDuration = lastDuration;
					avgDuration = smoothingFactor * lastDuration + (1 - smoothingFactor) * avgDuration;
					final long remainingDuration = (long) avgDuration * (modelConfig.getDynamicMaxIterations() - iteration);

					if (progress <= 100) {
						lastLength = printProgress(tenthPercent, progress, iteration, maxIterationsLength, remainingDuration, modelConfig,
								lastLength);

						if (progress >= nextUpdate) {
							progressCallback.accept(progress);
							nextUpdate += 10;
						}
					}
				}
			}
			Main.stats.stop("dtm-iter");

			ConsoleUtils.clearLine();

			in.close();
			p.waitFor();
		}

		final int topicCount = modelConfig.getkTopics();
		assert topicCount > 0;

		final int windowCount = windowIndex.size();
		assert windowCount > 0;

		final int articleCount = idDateIndex.size();
		final int wordCount = wordIndex.size();

		// read topic distributions

		final File gamFile = new File(outDirSeq, "gam.dat");
		if (!gamFile.exists())
			throw new AnalyzerException("file not found: " + gamFile.getAbsolutePath());
		in = new BufferedReader(new InputStreamReader(new FileInputStream(gamFile)));

		final double[][] topicDistributions = new double[articleCount][topicCount];
		for (int idxArticle = 0; idxArticle < articleCount; idxArticle++) {
			// read distributions into matrix and sum
			double topicDistributionSum = 0;
			for (int idxTopic = 0; idxTopic < topicCount; idxTopic++) {
				final double topicDistribution = Double.parseDouble(in.readLine());
				topicDistributions[idxArticle][idxTopic] = topicDistribution;
				topicDistributionSum += topicDistribution;
			}
			// normalize distributions by sum
			for (int idxTopic = 0; idxTopic < topicCount; idxTopic++) {
				topicDistributions[idxArticle][idxTopic] /= topicDistributionSum;
			}
		}

		in.close();

		// read topic definition files and create topics

		final TopicModelFull topicModel = dbTopicModels.getSingle(QueryBuilder.builder().eq("name", modelConfig.getName()));
		if (topicModel == null)
			throw new AnalyzerException("unknwon topic model: '" + modelConfig.getName() + "'");

		final List<SequenceFull> newSequences = new ArrayList<>(topicCount * windowCount);
		final List<TopicFull> newTopics = new ArrayList<>(topicCount);
		if (topicModel.getColorSeed() == null)
			topicModel.setColorSeed(new Random().nextLong());
		final List<Color> newColors = ColorUtils.generateRandomColors(topicCount, topicModel.getColorSeed());

		final boolean seqRelativeCutoff = modelConfig.getMinRelativeProbability() > 0;

		// for each topic
		for (int idxTopic = 0; idxTopic < topicCount; idxTopic++) {
			final File seqFile = new File(outDirSeq, "topic-" + StringUtils.padNumber(idxTopic, 3) + "-var-e-log-prob.dat");
			if (!seqFile.exists()) {
				in.close();
				throw new AnalyzerException("file not found: " + seqFile.getAbsolutePath());
			}

			// create new topic
			final TopicFull newTopic = new TopicFull();
			newTopic.setTopicModel(new TopicModel(topicModel.getId()));
			newTopic.setColor(ColorUtils.toHexString(newColors.get(idxTopic)));
			newTopics.add(newTopic);

			in = new BufferedReader(new InputStreamReader(new FileInputStream(seqFile)));

			// read file lines into word x sequence matrix
			// gather maximum probability per sequence and per word
			final double[][] probabilities = new double[wordCount][windowCount];
			for (int idxWord = 0; idxWord < wordCount; idxWord++) {
				for (int idxSeq = 0; idxSeq < windowCount; idxSeq++) {
					probabilities[idxWord][idxSeq] = Math.exp(Double.parseDouble(in.readLine()));
				}
			}

			in.close();

			// find maximum
			final double[] maxSeqProbabilities = ArrayUtils.findColMaximum(probabilities);

			// collect topic words per sequence with highest probability
			final CompareMap<SequenceWord> topicWords = new CompareMap<>();

			final double[] relevances = new double[windowCount];
			double relevanceSum = 0;
			double prevRelevance = 0;

			// for each sequence
			final List<Sequence> newTopicSequences = new ArrayList<>(windowCount);
			for (int idxSeq = 0, sequenceOffset = 0; idxSeq < windowCount; idxSeq++) {
				// calculate relative cutoff probability
				final double maxSeqProbability = maxSeqProbabilities[idxSeq];
				final double minRelativeSeqProbability = modelConfig.getMinRelativeProbability() * Math.abs(maxSeqProbability);

				// collect words
				List<SequenceWord> newSequenceWords = new ArrayList<>(wordCount);
				for (int idxWord = 0; idxWord < wordCount; idxWord++) {
					final double probability = probabilities[idxWord][idxSeq];
					// check if word acceptable
					if (!seqRelativeCutoff || (maxSeqProbability >= 0 && probability >= minRelativeSeqProbability)
							|| (maxSeqProbability < 0 && Math.abs(probability) >= minRelativeSeqProbability)) {
						final String word = wordIndex.word(idxWord);
						final SequenceWord sequenceWord = new SequenceWord(word, probability);
						newSequenceWords.add(sequenceWord);
					}
				}

				if (!newSequenceWords.isEmpty()) {
					Collections.sort(newSequenceWords, Comparator.reverseOrder());
					newSequenceWords = newSequenceWords.subList(0, Math.min(newSequenceWords.size(), modelConfig.getkTopWords()));
					topicWords.addAll(newSequenceWords);
				}

				// calculate topic sequence relevance
				final int sequenceSize = windowIndex.windowSize(idxSeq);
				double seqTopicDistributionSum = 0;
				for (int idxArticle = sequenceOffset; idxArticle < sequenceOffset + sequenceSize; idxArticle++)
					seqTopicDistributionSum += topicDistributions[idxArticle][idxTopic];
				final double relevance = seqTopicDistributionSum / sequenceSize;

				// create sequence
				final SequenceFull newSequenceFull = new SequenceFull();
				newSequenceFull.setWindow(windowIndex.getWindow(idxSeq));
				newSequenceFull.setWords(newSequenceWords);
				newSequenceFull.setRelevance(relevance);
				newSequenceFull.setRelevanceChange(relevance - prevRelevance);
				newSequenceFull.setTopic(new Topic(newTopic.getId()));
				newSequenceFull.setTopicModel(new TopicModel(topicModel.getId()));
				newSequences.add(newSequenceFull);
				newTopicSequences.add(new Sequence(newSequenceFull.getId()));

				// sequence offset is current position in list of sequences
				// of this topic
				sequenceOffset += sequenceSize;

				// relevance is summed up to calculate average later on
				relevanceSum += relevance;

				// relevances are gathered to calculate variance and
				// rising/falling relevance
				relevances[idxSeq] = relevance;

				// previous relevance is remembered to calculate difference
				// to next relevance
				prevRelevance = relevance;
			}
			newTopic.setSequences(newTopicSequences);

			// sort topic words descending and generate topic name
			List<SequenceWord> topTopicWordsList = new ArrayList<>(topicWords.values());
			Collections.sort(topTopicWordsList, Comparator.reverseOrder());
			topTopicWordsList = topTopicWordsList.subList(0, Math.min(topTopicWordsList.size(), modelConfig.getkTopWords()));
			newTopic.setName(TopicFull.getNameFromWords(modelConfig.getTopicAutoNamingWords(), topTopicWordsList));

			// set topic words
			final List<TopicWord> newTopicWords = new ArrayList<>(modelConfig.getkTopWords());
			for (final SequenceWord sequenceWord : topTopicWordsList) {
				final TopicWord newTopicWord = new TopicWord();
				newTopicWord.setWord(sequenceWord.getWord());
				final List<Double> sequenceProbabilities = new ArrayList<>(windowCount);
				final List<Double> sequenceProbabilitiesChange = new ArrayList<>(windowCount);
				double prevProbability = 0;
				for (final double probability : probabilities[wordIndex.index(sequenceWord.getWord())]) {
					sequenceProbabilities.add(probability);
					sequenceProbabilitiesChange.add(probability - prevProbability);
					prevProbability = probability;
				}
				newTopicWord.setSequenceProbabilities(sequenceProbabilities);
				newTopicWord.setSequenceProbabilitiesChange(sequenceProbabilitiesChange);
				newTopicWords.add(newTopicWord);
			}
			Collections.sort(newTopicWords, Comparator.reverseOrder());
			newTopic.setWords(newTopicWords);

			// calculate average
			final double average = relevanceSum / windowCount;
			newTopic.setAvgRelevance(average);

			// calculate variance
			double variance = 0;
			for (final double relevance : relevances)
				variance += Math.pow(relevance - average, 2);
			newTopic.setVarRelevance(variance / windowCount);

			// calculate rising/falling/rising-decay relevances
			double risingRelevance = 0;
			double fallingRelevance = 0;
			double risingDecayRelevance = 0;
			prevRelevance = relevances[0];
			for (int idxSeq2 = 1; idxSeq2 < relevances.length; idxSeq2++) {
				final double relevanceDiff = relevances[idxSeq2] - prevRelevance;
				if (relevanceDiff > 0) {
					risingRelevance += relevanceDiff;
				} else {
					fallingRelevance += Math.abs(relevanceDiff);
				}
				risingDecayRelevance += Math.exp(-modelConfig.getRisingDecayLambda() * (windowCount - idxSeq2 + 1)) * relevanceDiff;
			}
			newTopic.setRisingRelevance(risingRelevance);
			newTopic.setFallingRelevance(fallingRelevance);
			newTopic.setRisingDecayRelevance(risingDecayRelevance);
		}

		// create topic references, get document and topic similarities

		final CountMatrix<ObjectId, ObjectId> topicShareMatrix = new CountMatrix<>();
		int idxArticle = -1;

		for (final FilebaseIDDateIndexEntry entry : idDateIndex) {
			idxArticle++;

			final double[] topicDistribution = topicDistributions[idxArticle];

			// create topic references

			double reducedShare = 0;
			final List<TopicShare> newTopicRefs = new ArrayList<>(topicCount);
			for (int idxTopic = 0; idxTopic < topicCount; idxTopic++) {
				if (topicDistribution[idxTopic] >= modelConfig.getMinTopicShare()) {
					reducedShare += topicDistribution[idxTopic];
					final TopicShare newTopicRef = new TopicShare();
					final TopicFull topicFull = newTopics.get(idxTopic);
					final Integer articlesCount = topicFull.getArticlesCount();
					topicFull.setArticlesCount(articlesCount == null ? 1 : articlesCount + 1);
					newTopicRef.setTopic(new Topic(topicFull.getId()));
					newTopicRef.setShare(topicDistribution[idxTopic]);
					newTopicRefs.add(newTopicRef);
				}
			}

			// count topic/topic share per article

			for (final TopicShare topicRef1 : newTopicRefs)
				for (final TopicShare topicRef2 : newTopicRefs)
					topicShareMatrix.count(topicRef1.getTopic().getId(), topicRef2.getTopic().getId());

			// calculate article divergences

			final List<SimilarArticle> similarArticles = new ArrayList<>(articleCount - 1);

			for (int idxArticle2 = 0; idxArticle2 < articleCount; idxArticle2++) {
				if (idxArticle == idxArticle2)
					continue;

				final double divergence = ArrayUtils.jsDivergence(topicDistributions[idxArticle], topicDistributions[idxArticle2]);
				if (divergence > modelConfig.getMaxSimilarDocumentsDivergence())
					continue;

				final SimilarArticle similarArticle = new SimilarArticle();
				similarArticle.setArticle(new Article(MongoUtils.objectId(idDateIndex.get(idxArticle2).getId())));
				similarArticle.setDivergence(divergence);
				similarArticles.add(similarArticle);
			}

			Collections.sort(similarArticles);

			if (similarArticles.size() > modelConfig.getMaxSimilarDocuments())
				similarArticles.subList(modelConfig.getMaxSimilarDocuments(), similarArticles.size()).clear();

			// update article

			if (!newTopicRefs.isEmpty()) {
				// renormalize share
				for (final TopicShare newTopicRef : newTopicRefs)
					newTopicRef.setShare(newTopicRef.getShare() / reducedShare);

				Collections.sort(newTopicRefs, Comparator.reverseOrder());

				// update article with topic references (partial update)
				final ArticleFull article = new ArticleFull();
				article.setId(entry.getId());
				article.setTopicModel(new TopicModel(topicModel.getId()));
				article.setTopics(newTopicRefs);
				article.setSimilarArticles(similarArticles);

				try {
					dbArticles.updateSingle(article, "topicModel", "topics", "topicsCount", "similarArticles");
				} catch (final DatabaseException e) {
					ConsoleUtils.error(e);
				}
			}
		}

		// remove unreferenced topics

		for (final ListIterator<TopicFull> iter = newTopics.listIterator(); iter.hasNext();) {
			final TopicFull topic = iter.next();
			final Integer articlesCount = topic.getArticlesCount();
			if (articlesCount == null || articlesCount == 0)
				iter.remove();
		}

		// calculate topic similarities

		final int topicMinCount = (int) Math.ceil(topicCount * (1 - modelConfig.getMaxSimilarTopicsDivergence()));

		for (final TopicFull topic1 : newTopics) {
			final List<TopicShare> similarTopics = new ArrayList<>();
			for (final TopicFull topic2 : newTopics) {
				if (!topic1.getId().equals(topic2.getId())) {
					final Integer count = topicShareMatrix.get(topic1.getId(), topic2.getId());
					if (count != null && count >= topicMinCount) {
						final TopicShare newTopicShare = new TopicShare();
						newTopicShare.setTopic(new Topic(topic2.getId()));
						newTopicShare.setShare((double) count / topicCount);
						similarTopics.add(newTopicShare);
					}
				}
			}

			Collections.sort(similarTopics);

			if (similarTopics.size() > modelConfig.getMaxSimilarTopics())
				similarTopics.subList(modelConfig.getMaxSimilarTopics(), similarTopics.size()).clear();

			topic1.setSimilarTopics(similarTopics);
		}

		// recreate entities

		final QueryBuilder builder = QueryBuilder.builder().eq("topicModel", new TopicModel(topicModel.getId()));

		dbSequences.deleteMultiple(builder);
		dbTopics.deleteMultiple(builder);

		dbSequences.createMultiple(newSequences);
		dbTopics.createMultiple(newTopics);

		topicModel.setWordCount((long) wordCount);
		topicModel.setWindowCount((long) windowCount);
		topicModel.setArticleCount((long) articleCount);
		topicModel.setTopicCount((long) newTopics.size());
		topicModel.setLastGenerated(new Date());
		dbTopicModels.replaceSingle(topicModel);
	}

	private int printProgress(final int tenthPercent, final double progress, final int iteration, final int maxIterationsLength,
			final long remainingNanos, final TopicModelConfig modelConfig, final int lastLength) {
		String msg = " [" + StringUtils.repeat("#", tenthPercent) + StringUtils.repeat(" ", 10 - tenthPercent) + "] "
				+ StringUtils.pad(Integer.toString((int) Math.floor(progress)), 3, true) + "% ("
				+ StringUtils.pad(Integer.toString(iteration), maxIterationsLength, true) + "/" + modelConfig.getDynamicMinIterations() + "-"
				+ modelConfig.getDynamicMaxIterations() + ") " + StringUtils.timeString(remainingNanos, false, true, false);

		// add padding if shorter than last message to clear rest of line
		if (msg.length() < lastLength)
			msg += StringUtils.repeat(" ", lastLength - msg.length());

		// add carriage return to rewrite next line
		msg += "\r";

		ConsoleUtils.infoNOLF(msg);
		return msg.length() - 1;
	}

	private void recalcWindows(final FilebaseIDDateIndex idDateIndex, final FilebaseWindowIndex windowIndex) throws IOException {
		windowIndex.clear();
		for (final FilebaseIDDateIndexEntry date : idDateIndex)
			windowIndex.add(date.getDate());
		windowIndex.sync();
	}

}
