package de.vipra.cmd.lda;

public class AnalyzerException extends Exception {

	private static final long serialVersionUID = 1L;

	public AnalyzerException(final String msg) {
		super(msg);
	}

	public AnalyzerException(final Exception e) {
		super(e);
	}

}
