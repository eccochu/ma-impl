package de.vipra.cmd.option;

import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.model.TopicModelConfig;

public class PrintModelCommand implements Command {

	private final String[] names;
	private Config config;

	public PrintModelCommand(final String[] names) {
		this.names = names;
	}

	private void printModelConfig(final String name, final TopicModelConfig modelConfig) {
		ConsoleUtils.info("model: " + name + "\n" + modelConfig.toPrettyString());
	}

	@Override
	public void run() throws Exception {
		config = Config.getConfig();

		for (final String name : names) {
			printModelConfig(name, config.getTopicModelConfig(name));
		}
	}

	@Override
	public boolean requiresLock() {
		return false;
	}

	@Override
	public String getName() {
		return "print";
	}

}
