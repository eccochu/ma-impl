package de.vipra.cmd.option;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.function.Consumer;

import de.vipra.cmd.Main;
import de.vipra.cmd.lda.Analyzer;
import de.vipra.cmd.lda.AnalyzerException;
import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.StringUtils;
import de.vipra.util.Timer;
import de.vipra.util.ex.ConfigException;
import de.vipra.util.ex.DatabaseException;
import de.vipra.util.model.TopicModelConfig;

public class ModelingCommand implements Command {

	private final String[] models;
	private final boolean reread;

	public ModelingCommand(final String[] models, final boolean reread) {
		this.models = models;
		this.reread = reread;
	}

	private void modelForModel(final TopicModelConfig modelConfig, final Consumer<Double> progressCallback)
			throws AnalyzerException, ConfigException, DatabaseException, ParseException, IOException, InterruptedException {
		Main.stats.start("modeling." + modelConfig.getName());
		if (reread)
			ConsoleUtils.info("rereading model: " + modelConfig.getName());
		else
			ConsoleUtils.info("generating model: " + modelConfig.getName());
		ConsoleUtils.flush();

		final Analyzer analyzer = new Analyzer();

		final Timer timer = new Timer();
		timer.restart();

		/*
		 * do topic modeling
		 */
		analyzer.analyze(modelConfig, reread, progressCallback);
		timer.lap("topic modeling");

		/*
		 * run information
		 */
		ConsoleUtils.info("done in " + StringUtils.timeString(timer.total()));
		Main.stats.stop("modeling." + modelConfig.getName());
	}

	@Override
	public void run() throws Exception {
		final Config config = Config.getConfig();
		if (config.getDtmPath() == null || config.getDtmPath().isEmpty())
			throw new Exception("dtm path not configured, set config variable 'dtmPath'");
		if (!new File(config.getDtmPath()).exists())
			throw new Exception("dtm not found at specified path: '" + config.getDtmPath() + "'");

		for (final TopicModelConfig modelConfig : config.getTopicModelConfigs(models))
			modelForModel(modelConfig, (progress) -> {});
	}

	@Override
	public boolean requiresLock() {
		return true;
	}

	@Override
	public String getName() {
		return "modeling";
	}

}
