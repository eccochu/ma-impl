package de.vipra.cmd.option;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import de.vipra.cmd.Main;
import de.vipra.cmd.file.Filebase;
import de.vipra.cmd.file.FilebaseException;
import de.vipra.cmd.file.FilebaseWindowIndex;
import de.vipra.cmd.file.FilebaseWordIndex;
import de.vipra.cmd.text.DBPediaAnalyzer;
import de.vipra.cmd.text.HypernymAnalyzer;
import de.vipra.cmd.text.ProcessedText;
import de.vipra.cmd.text.Processor;
import de.vipra.cmd.text.ProcessorException;
import de.vipra.cmd.text.SpotlightAnalyzer;
import de.vipra.cmd.text.SpotlightResponse;
import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.Constants;
import de.vipra.util.CountMap;
import de.vipra.util.StringUtils;
import de.vipra.util.Timer;
import de.vipra.util.ex.ConfigException;
import de.vipra.util.ex.DatabaseException;
import de.vipra.util.model.ArticleFull;
import de.vipra.util.model.ArticleStats;
import de.vipra.util.model.ArticleWord;
import de.vipra.util.model.TextEntity;
import de.vipra.util.model.TextEntityCount;
import de.vipra.util.model.TextEntityFull;
import de.vipra.util.model.TopicModel;
import de.vipra.util.model.TopicModelConfig;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.model.Window;
import de.vipra.util.model.WindowFull;
import de.vipra.util.model.WordFull;
import de.vipra.util.service.MongoService;
import de.vipra.util.service.QueryBuilder;

public class ImportCommand implements Command {

	public static class ArticleBuffer {

		private final MongoService<ArticleFull, ObjectId> dbArticles;
		private final List<ArticleFull> articles = new ArrayList<>(Constants.IMPORT_BUFFER_MAX);

		public ArticleBuffer(final MongoService<ArticleFull, ObjectId> dbArticles) {
			this.dbArticles = dbArticles;
		}

		public void add(final ArticleFull article) throws DatabaseException {
			articles.add(article);
			if (articles.size() >= Constants.IMPORT_BUFFER_MAX) {
				save();
			}
		}

		public synchronized void save() throws DatabaseException {
			if (!articles.isEmpty()) {
				dbArticles.createMultiple(articles);
				articles.clear();
			}
		}
	}

	private final String[] models;
	private final List<File> files = new ArrayList<>();
	private final JSONParser parser = new JSONParser();
	private Config config;

	private MongoService<ArticleFull, ObjectId> dbArticles;
	private MongoService<TopicModelFull, ObjectId> dbTopicModels;
	private MongoService<WordFull, String> dbWords;
	private MongoService<TextEntityFull, String> dbEntities;
	private MongoService<WindowFull, String> dbWindows;

	private TopicModelFull topicModelFull;
	private TopicModel topicModel;
	private TopicModelConfig modelConfig;
	private SpotlightAnalyzer spotlightAnalyzer;
	private Filebase filebase;
	private FilebaseWindowIndex windowIndex;
	private Processor processor;
	private ArticleBuffer buffer;
	private Set<TextEntityFull> newTextEntities;
	private Set<WordFull> wordEntities;

	/**
	 * Import command to import articles into the database, do topic modeling
	 * and save everything. Use {@link ImportCommand#run()} to execute this
	 * command.
	 *
	 * @param paths
	 *            Paths to all *.json files containing articles or folders
	 *            containing *.json files. Not recursive.
	 */
	public ImportCommand(final String[] models, final String[] paths) {
		this.models = models;
		addPaths(paths);
	}

	private void addPaths(final String[] paths) {
		for (final String path : paths) {
			addPath(new File(path));
		}
	}

	private void addPaths(final File[] paths) {
		for (final File path : paths) {
			addPath(path);
		}
	}

	private void addPath(final File file) {
		if (file.isFile()) {
			files.add(file);
		} else if (file.isDirectory()) {
			final File[] files = file.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(final File dir, final String name) {
					return dir.isFile() && dir.exists();
				}
			});

			addPaths(files);
		}
	}

	private ArticleFull articleFromJSON(final JSONObject obj) {
		final ArticleFull article = new ArticleFull();
		article.setId(new ObjectId());
		if (obj.containsKey("title"))
			article.setTitle(obj.get("title").toString());
		if (obj.containsKey("text"))
			article.setText(obj.get("text").toString());
		if (obj.containsKey("url"))
			article.setUrl(obj.get("url").toString());
		if (obj.containsKey("date"))
			article.setDate(obj.get("date").toString());
		return article;
	}

	/**
	 * import a single article into the database and filebase
	 *
	 * @param object
	 * @return
	 * @throws Exception
	 */
	private void importArticle(final JSONObject object, final int current, final int max) {
		final ArticleFull article = articleFromJSON(object);

		try {
			// preprocess text
			final ProcessedText processedText = processor.process(modelConfig, article.getText());

			if (processedText.getReducedWordCount() < modelConfig.getDocumentMinimumLength()) {
				ConsoleUtils.info(ConsoleUtils.positionString(current, max) + "  skipped \"" + object.get("title") + "\" ("
						+ processedText.getReducedWordCount() + ")");
			} else {
				Set<String> blockedWords = new HashSet<>(0);
				final CountMap<String> wordCounts = processedText.getWordCounts();
				final List<ArticleWord> articleWords = processedText.getArticleWords();
				String articleText = article.getText();

				// spotlight analysis
				if (spotlightAnalyzer != null) {
					final SpotlightResponse spotlightResponse = spotlightAnalyzer.analyze(article.getText());

					List<TextEntityCount> textEntitiesCounts = spotlightResponse.getEntities();
					if (textEntitiesCounts != null) {
						// replace entities with hypernyms
						if (modelConfig.isProcessorUseHypernyms()) {
							final HypernymAnalyzer hypernymAnalyzer = HypernymAnalyzer.getInstance();
							final Map<String, TextEntityCount> replacedTextEntities = new HashMap<>(textEntitiesCounts.size());
							for (final TextEntityCount textEntityCount : textEntitiesCounts) {
								final TextEntity textEntity = textEntityCount.getEntity();
								final String hypernym = hypernymAnalyzer.getHypernym(textEntity.realEntity());
								if (hypernym != null) {
									textEntity.setUrl(hypernymAnalyzer.getURL(hypernym));
									textEntity.setIsHypernym(true);
								}
								final TextEntityCount replacedTextEntity = replacedTextEntities.get(textEntity.getUrl());
								if (replacedTextEntity != null)
									replacedTextEntity.setCount(replacedTextEntity.getCount() + textEntityCount.getCount());
								else
									replacedTextEntities.put(textEntity.getUrl(), textEntityCount);
							}
							textEntitiesCounts = new ArrayList<>(replacedTextEntities.values());
						}

						blockedWords = new HashSet<>(textEntitiesCounts.size());

						// insert entities into text
						for (final TextEntityCount textEntityCount : textEntitiesCounts) {
							// get new text entity
							final TextEntityFull newTextEntity = new TextEntityFull(textEntityCount.getEntity(), topicModel);
							if (newTextEntity.getEntity() == null || newTextEntity.getEntity().isEmpty())
								continue;

							newTextEntities.add(newTextEntity);

							// insert entity into text
							articleText = articleText.replaceAll(
									"(?i)\\b(" + Pattern.quote(textEntityCount.getEntity().getEntity()) + ")\\b(?![^<]*>|[^<>]*</)",
									newTextEntity.aTag("$1"));

							// set to ignore entity later when replacing words
							blockedWords.add(textEntityCount.getEntity().getEntity().toLowerCase());

							// replace entity surface form by resource form
							textEntityCount.getEntity().setEntity(newTextEntity.getEntity());

							if (newTextEntity.getIsWord()) {
								// mark word as entity
								final WordFull wordEntity = new WordFull(newTextEntity.getEntity(), topicModel);
								wordEntity.setIsEntity(true);
								wordEntities.add(wordEntity);

								// add entities as words if not already words
								if (!wordCounts.contains(newTextEntity.getEntity())) {
									final ArticleWord newArticleWord = new ArticleWord(newTextEntity.getEntity(), textEntityCount.getCount());
									articleWords.add(newArticleWord);
								}
							}
						}

						article.setEntities(textEntitiesCounts);
					}
				}

				// insert word links
				for (final ArticleWord word : processedText.getArticleWords()) {
					if (blockedWords.contains(word.getWord()))
						continue;
					articleText = articleText.replaceAll("(?i)\\b(" + word.getWord() + ")\\b(?![^<]*>|[^<>]*</)", word.aTag("$1"));
				}

				// insert more word links
				for (final Map.Entry<String, String> entry : processedText.getLemmas().entrySet()) {
					if (blockedWords.contains(entry.getKey().toLowerCase()))
						continue;
					articleText = articleText.replaceAll("(?i)\\b(" + entry.getKey() + ")\\b(?![^<]*>|[^<>]*</)",
							ArticleWord.aTag(entry.getValue(), "$1"));
				}

				article.setProcessedText(processedText.getWords());
				article.setWords(articleWords);
				article.setTopicModel(new TopicModel(topicModelFull.getId()));
				article.setWindow(windowIndex.getWindow(article.getDate()));
				article.setText(articleText);

				// generate article stats
				final ArticleStats stats = new ArticleStats();
				stats.setWordCount(processedText.getWordCount());
				stats.setProcessedWordCount(processedText.getReducedWordCount());
				stats.setReductionRatio(processedText.getReductionRatio());
				article.setStats(stats);

				// add article to data- and filebase
				buffer.add(article);
				filebase.add(article);

				ConsoleUtils.info(ConsoleUtils.positionString(current, max) + " imported \"" + object.get("title") + "\"");
			}
		} catch (final ProcessorException e) {
			ConsoleUtils.error("could not preprocess text of article '" + article.getTitle() + "'");
		} catch (final DatabaseException e) {
			ConsoleUtils.error("could not save processed article in the database '" + article.getTitle() + "'");
		} catch (final FilebaseException e) {
			ConsoleUtils.error("could not save processed article in the filebase '" + article.getTitle() + "'");
		} catch (final IOException e) {
			ConsoleUtils.error("io error: " + e.getMessage());
		} catch (final ClassNotFoundException e) {
			ConsoleUtils.error("error: " + e.getMessage());
		}
	}

	/**
	 * Imports a file into the database and the filebase
	 *
	 * @param file
	 * @throws ParseException
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws Exception
	 */
	private int importFile(final File file) throws FileNotFoundException, IOException, ParseException {
		final Object data = parser.parse(new FileReader(file));
		int imported = 0;

		ConsoleUtils.info("file \"" + file.getAbsolutePath() + "\"");

		if (data instanceof JSONArray) {
			final JSONArray objects = (JSONArray) data;
			final int size = objects.size();
			imported += size;
			for (int i = 0; i < objects.size(); i++)
				importArticle((JSONObject) objects.get(i), i + 1, size);
		} else if (data instanceof JSONObject) {
			importArticle((JSONObject) data, 1, 1);
			imported++;
		} else {
			ConsoleUtils.error("unknown data format");
		}

		return imported;
	}

	private int importFiles(final List<File> files) throws FileNotFoundException, IOException, ParseException {
		int imported = 0;
		for (final File file : files) {
			imported += importFile(file);
		}
		return imported;
	}

	private void importForModel(final TopicModelConfig modelConfig) throws Exception {
		Main.stats.start("import." + modelConfig.getName());
		ConsoleUtils.info("importing for model: " + modelConfig.getName());

		this.modelConfig = modelConfig;

		if (config.getSpotlightUrl() != null)
			spotlightAnalyzer = new SpotlightAnalyzer(modelConfig);

		if ((modelConfig.isProcessorUseEntities() || modelConfig.isProcessorUseEntityTypes() || modelConfig.isProcessorUseHypernyms())
				&& spotlightAnalyzer == null)
			throw new ConfigException("spotlight url is empty, but processor requires entity informations");

		buffer = new ArticleBuffer(dbArticles);
		filebase = new Filebase(modelConfig, config.getDataDirectory());
		windowIndex = filebase.getWindowIndex();
		topicModelFull = dbTopicModels.getSingle(QueryBuilder.builder().eq("name", modelConfig.getName()));
		if (topicModelFull == null)
			throw new Exception("Topic model with name '" + modelConfig.getName() + "' not found.");
		topicModel = new TopicModel(topicModelFull.getId());
		newTextEntities = new HashSet<>();
		wordEntities = new HashSet<>();

		final Timer timer = new Timer();
		timer.restart();

		/*
		 * import files into database and filebase
		 */
		importFiles(files);
		buffer.save();

		/*
		 * write filebase
		 */
		filebase.sync();

		/*
		 * add new words
		 */
		final FilebaseWordIndex wordIndex = filebase.getWordIndex();
		final List<WordFull> newWords = new ArrayList<>(wordIndex.getNewWords().size());
		for (final String word : wordIndex.getNewWords()) {
			final WordFull newWord = new WordFull(word, topicModel);
			newWords.add(newWord);
		}
		dbWords.createMultiple(newWords);

		/*
		 * mark words as entities
		 */
		dbWords.updateMultiple(wordEntities, "isEntity", true);

		/*
		 * add new windows
		 */
		final List<Window> windows = filebase.getWindowIndex().getWindows();
		final List<WindowFull> newWindows = new ArrayList<>(windows.size());
		for (final Window window : windows) {
			final WindowFull newWindow = new WindowFull(window, topicModel);
			newWindows.add(newWindow);
		}
		dbWindows.createMultiple(newWindows);

		/*
		 * add new entities
		 */
		if (modelConfig.isQueryEntityDescriptions()) {
			ConsoleUtils.info("querying descriptions of new entities");
			for (final TextEntityFull textEntity : newTextEntities) {
				try {
					// get description from dbpedia
					textEntity.setDescription(DBPediaAnalyzer.getAbstract(textEntity.getUrl(), true));
				} catch (final Exception e) {
					ConsoleUtils.error("could not query description for entity: '" + textEntity.getEntity() + "'");
				}
			}
		}
		dbEntities.createMultiple(newTextEntities);

		/*
		 * update topic model
		 */
		topicModelFull.setWindows(filebase.getWindowIndex().getWindows());
		topicModelFull.setArticleCount((long) filebase.getIdDateIndex().size());
		topicModelFull.setWordCount((long) filebase.getWordIndex().size());
		topicModelFull.setEntityCount(dbEntities.count(QueryBuilder.builder().eq("topicModel", topicModel)));
		dbTopicModels.replaceSingle(topicModelFull);

		/*
		 * run information
		 */
		ConsoleUtils.info("done in " + StringUtils.timeString(timer.total()));
		Main.stats.stop("import." + modelConfig.getName());
	}

	@Override
	public void run() throws java.text.ParseException, IOException, ParseException, InterruptedException, DatabaseException, Exception {
		config = Config.getConfig();
		dbArticles = MongoService.getDatabaseService(config, ArticleFull.class);
		dbTopicModels = MongoService.getDatabaseService(config, TopicModelFull.class);
		dbEntities = MongoService.getDatabaseService(config, TextEntityFull.class);
		dbWords = MongoService.getDatabaseService(config, WordFull.class);
		dbWindows = MongoService.getDatabaseService(config, WindowFull.class);
		processor = new Processor();
		for (final TopicModelConfig modelConfig : config.getTopicModelConfigs(models))
			importForModel(modelConfig);
	}

	@Override
	public boolean requiresLock() {
		return true;
	}

	@Override
	public String getName() {
		return "import";
	}

}