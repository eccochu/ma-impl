package de.vipra.cmd.option;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import org.bson.types.ObjectId;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.IndexNotFoundException;

import de.vipra.cmd.Main;
import de.vipra.cmd.file.FilebaseIDDateIndex;
import de.vipra.cmd.file.FilebaseIDDateIndexEntry;
import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.ESClient;
import de.vipra.util.ESSerializer;
import de.vipra.util.MongoUtils;
import de.vipra.util.StringUtils;
import de.vipra.util.Timer;
import de.vipra.util.ex.ConfigException;
import de.vipra.util.ex.DatabaseException;
import de.vipra.util.model.ArticleFull;
import de.vipra.util.model.TopicModelConfig;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.service.MongoService;
import de.vipra.util.service.QueryBuilder;

public class IndexingCommand implements Command {

	private final String[] models;
	private Config config;
	private MongoService<TopicModelFull, ObjectId> dbTopicModels;
	private MongoService<ArticleFull, ObjectId> dbArticles;
	private Client elasticClient;
	private ESSerializer<ArticleFull> elasticSerializer;

	public IndexingCommand(final String[] models) {
		this.models = models;
	}

	private void indexForModel(final TopicModelConfig modelConfig) throws ParseException, IOException, ConfigException, DatabaseException {
		Main.stats.start("indexing." + modelConfig.getName());
		ConsoleUtils.info("indexing for model: " + modelConfig.getName());

		final Timer timer = new Timer();
		timer.restart();

		final FilebaseIDDateIndex index = new FilebaseIDDateIndex(modelConfig.getModelDir(config.getDataDirectory()));
		final String indexName = modelConfig.getName() + "-articles";

		try {
			// clear index
			elasticClient.admin().indices().prepareDelete(indexName).get();
		} catch (final IndexNotFoundException e) {}

		final int max = index.size();
		int current = 0;

		for (final FilebaseIDDateIndexEntry entry : index) {
			++current;

			// get article from database
			final ArticleFull article = dbArticles.getSingle(MongoUtils.objectId(entry.getId()), "_all");
			if (article == null) {
				ConsoleUtils.error(ConsoleUtils.positionString(current, max) + " skipped \"" + entry.getId() + "\", unknown id");
				continue;
			}

			// index article
			final Map<String, Object> source = elasticSerializer.serialize(article);
			elasticClient.prepareIndex(indexName, "article", article.getId().toString()).setSource(source).get();
			ConsoleUtils.info(ConsoleUtils.positionString(current, max) + " indexed \"" + article.getTitle() + "\"");
		}

		// update indexed date
		final TopicModelFull topicModel = dbTopicModels.getSingle(QueryBuilder.builder().eq("name", modelConfig.getName()));
		topicModel.setLastIndexed(new Date());
		dbTopicModels.updateSingle(topicModel, "lastIndexed");

		// run information
		ConsoleUtils.info("done in " + StringUtils.timeString(timer.total()));
		Main.stats.stop("indexing." + modelConfig.getName());
	}

	@Override
	public void run() throws ParseException, IOException, DatabaseException, Exception {
		config = Config.getConfig();
		dbTopicModels = MongoService.getDatabaseService(config, TopicModelFull.class);
		dbArticles = MongoService.getDatabaseService(config, ArticleFull.class);
		elasticClient = ESClient.getClient(config);
		elasticSerializer = new ESSerializer<>(ArticleFull.class);
		for (final TopicModelConfig modelConfig : config.getTopicModelConfigs(models))
			indexForModel(modelConfig);
		elasticClient.close();
	}

	@Override
	public boolean requiresLock() {
		return true;
	}

	@Override
	public String getName() {
		return "indexing";
	}

}
