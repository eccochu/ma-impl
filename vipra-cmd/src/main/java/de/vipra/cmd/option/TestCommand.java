package de.vipra.cmd.option;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.bson.types.ObjectId;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.elasticsearch.client.transport.TransportClient;
import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Color;

import de.vipra.cmd.Main;
import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.ESClient;
import de.vipra.util.model.Article;
import de.vipra.util.service.MongoService;

public class TestCommand implements Command {

	@Override
	public void run() throws Exception {
		try {
			Main.stats.start("test command");
			ConsoleUtils.info("testing system");

			// test if configuration readable
			ConsoleUtils.infoNOLF(" " + ConsoleUtils.PATH_T + " reading configuration...");
			final Config config = Config.getConfig(false);
			ConsoleUtils.print(Ansi.ansi().fg(Color.GREEN).a("OK").reset().toString());

			// test if dtm is accessible
			ConsoleUtils.infoNOLF(" " + ConsoleUtils.PATH_T + " testing dtm binary...");
			if (config.getDtmPath() == null || config.getDtmPath().isEmpty())
				throw new Exception("dtm binary not configured, set 'dtmPath' in config.json");
			final File dtm = new File(config.getDtmPath());
			if (!dtm.exists())
				throw new FileNotFoundException("dtm binary not available at path: '" + config.getDtmPath() + "'");
			ConsoleUtils.print(Ansi.ansi().fg(Color.GREEN).a("OK").reset().toString());

			// test if database is accessible
			ConsoleUtils.infoNOLF(" " + ConsoleUtils.PATH_T + " testing mongodb connection...");
			final MongoService<Article, ObjectId> dbArticles = MongoService.getDatabaseService(config, Article.class);
			dbArticles.count(null);
			ConsoleUtils.print(Ansi.ansi().fg(Color.GREEN).a("OK").reset().toString());

			// test if elasticsearch is accessible
			ConsoleUtils.infoNOLF(
					" " + (config.isSpotlightEnabled() ? ConsoleUtils.PATH_T : ConsoleUtils.PATH_L) + " testing elasticsearch connection...");
			final TransportClient esclient = ESClient.getClient(config);
			if (esclient.connectedNodes().isEmpty())
				throw new NoNodeAvailableException("no elasticsearch nodes available");
			ConsoleUtils.print(Ansi.ansi().fg(Color.GREEN).a("OK").reset().toString());

			// test if spotlight is accessible
			if (config.isSpotlightEnabled()) {
				ConsoleUtils.infoNOLF(" " + ConsoleUtils.PATH_L + " testing spotlight connection...");
				final URL url = new URL(config.getSpotlightUrl() + "/rest/application.wadl");
				final HttpURLConnection huc = (HttpURLConnection) url.openConnection();
				huc.setRequestMethod("HEAD");
				if (huc.getResponseCode() != 200)
					throw new Exception("spotlight server not available");
				ConsoleUtils.print(Ansi.ansi().fg(Color.GREEN).a("OK").reset().toString());
			}

			ConsoleUtils.info("all tests passed");
		} catch (final Exception e) {
			ConsoleUtils.print(Ansi.ansi().fg(Color.RED).a("FAILED").reset().toString());
			throw e;
		} finally {
			Main.stats.stop("test command");
		}
	}

	@Override
	public boolean requiresLock() {
		return false;
	}

	@Override
	public String getName() {
		return "test";
	}

}
