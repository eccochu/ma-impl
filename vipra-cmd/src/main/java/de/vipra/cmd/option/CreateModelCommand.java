package de.vipra.cmd.option;

import java.io.File;
import java.util.Random;

import org.bson.types.ObjectId;

import de.vipra.cmd.Main;
import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.model.TopicModelConfig;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.service.MongoService;

public class CreateModelCommand implements Command {

	private final String[] names;
	private Config config;
	private MongoService<TopicModelFull, ObjectId> dbTopicModels;
	private Random random;

	public CreateModelCommand(final String[] names) {
		this.names = names;
	}

	private TopicModelFull createModel(final String name, final TopicModelConfig modelConfig) throws Exception {
		final TopicModelFull topicModel = new TopicModelFull(name, modelConfig);
		topicModel.setColorSeed(random.nextLong());
		modelConfig.setName(topicModel.getName());
		modelConfig.setGroup(topicModel.getGroup());
		dbTopicModels.createSingle(topicModel);
		config.getTopicModelConfigs().put(topicModel.getName(), modelConfig);

		final File modelDir = new File(config.getDataDirectory(), topicModel.getName());

		if (!modelDir.mkdirs())
			throw new Exception("could not create model directory: " + modelDir.getAbsolutePath());

		modelConfig.saveToFile(modelDir);

		return topicModel;
	}

	@Override
	public void run() throws Exception {
		if (names.length == 0)
			return;

		config = Config.getConfig();
		dbTopicModels = MongoService.getDatabaseService(config, TopicModelFull.class);
		random = new Random();

		final TopicModelConfig modelConfig;

		if (config.getModelConfigTemplate() != null) {
			modelConfig = new TopicModelConfig(config.getModelConfigTemplate());
		} else {
			modelConfig = new TopicModelConfig();
		}

		for (final String name : names) {
			Main.stats.start("create." + name);
			final String msg = config.validModelName(name);
			if (msg != null)
				throw new Exception(msg);

			final TopicModelFull topicModel = createModel(name, modelConfig);
			ConsoleUtils
					.info("model created: " + topicModel.getName() + (topicModel.getGroup() == null ? "" : " in group: " + topicModel.getGroup()));
			Main.stats.stop("create." + name);
		}
	}

	@Override
	public boolean requiresLock() {
		return false;
	}

	@Override
	public String getName() {
		return "create";
	}

}
