package de.vipra.cmd.option;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import de.vipra.cmd.Main;
import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.Constants.WindowResolution;
import de.vipra.util.ex.ConfigException;
import de.vipra.util.ex.DatabaseException;
import de.vipra.util.model.TopicModelConfig;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.service.MongoService;
import de.vipra.util.service.QueryBuilder;

public class EditModelCommand implements Command {

	private final String[] names;
	private Config config;
	private MongoService<TopicModelFull, ObjectId> dbTopicModels;

	public EditModelCommand(final String[] names) {
		this.names = names;
	}

	private void editModel(final TopicModelFull topicModel)
			throws DatabaseException, JsonGenerationException, JsonMappingException, IOException, ConfigException {
		Main.stats.start("edit." + topicModel.getName());
		ConsoleUtils.info("editing model: " + topicModel.getName());

		final TopicModelConfig topicModelConfig = topicModel.getModelConfig();
		topicModel.setGroup(ConsoleUtils.readString(" " + ConsoleUtils.PATH_T + " group", topicModel.getGroup(), true));
		topicModelConfig.setDescription(
				ConsoleUtils.readString(" " + ConsoleUtils.PATH_T + " description (↲ to skip)", topicModelConfig.getDescription(), true));
		topicModelConfig.setkTopics(ConsoleUtils.readInt(" " + ConsoleUtils.PATH_T + " k topics", topicModelConfig.getkTopics(), 1, null, true));
		topicModelConfig.setDynamicMinIterations(ConsoleUtils.readInt(" " + ConsoleUtils.PATH_T + " dynamic min iterations",
				topicModelConfig.getDynamicMinIterations(), 1, null, true));
		topicModelConfig.setDynamicMaxIterations(ConsoleUtils.readInt(" " + ConsoleUtils.PATH_T + " dynamic max iterations",
				topicModelConfig.getDynamicMaxIterations(), topicModelConfig.getDynamicMinIterations(), null, true));
		topicModelConfig.setStaticIterations(
				ConsoleUtils.readInt(" " + ConsoleUtils.PATH_T + " static iterations", topicModelConfig.getStaticIterations(), 1, null, true));
		topicModelConfig.setTopicAutoNamingWords(ConsoleUtils.readInt(" " + ConsoleUtils.PATH_T + " topic auto naming words",
				topicModelConfig.getTopicAutoNamingWords(), 1, null, true));
		topicModelConfig.setMaxSimilarDocuments(
				ConsoleUtils.readInt(" " + ConsoleUtils.PATH_T + " max similar documents", topicModelConfig.getMaxSimilarDocuments(), 0, null, true));
		topicModelConfig.setDocumentMinimumLength(
				ConsoleUtils.readInt(" " + ConsoleUtils.PATH_T + " document min length", topicModelConfig.getDocumentMinimumLength(), 0, null, true));
		topicModelConfig.setDocumentMinimumWordFrequency(ConsoleUtils.readInt(" " + ConsoleUtils.PATH_T + " document min word frequency",
				topicModelConfig.getDocumentMinimumWordFrequency(), 0, null, true));
		topicModelConfig.setSpotlightSupport(
				ConsoleUtils.readInt(" " + ConsoleUtils.PATH_T + " spotlight support", topicModelConfig.getSpotlightSupport(), 0, null, true));
		topicModelConfig.setSpotlightConfidence(ConsoleUtils.readDouble(" " + ConsoleUtils.PATH_T + " spotlight confidence",
				topicModelConfig.getSpotlightConfidence(), 0.0, 1.0, true));
		topicModelConfig.setMinRelativeProbability(ConsoleUtils.readDouble(" " + ConsoleUtils.PATH_T + " min relative probability",
				topicModelConfig.getMinRelativeProbability(), 0.0, 1.0, true));
		topicModelConfig.setRisingDecayLambda(
				ConsoleUtils.readDouble(" " + ConsoleUtils.PATH_T + " rising decay lambda", topicModelConfig.getRisingDecayLambda()));
		topicModelConfig.setMaxSimilarDocumentsDivergence(ConsoleUtils.readDouble(" " + ConsoleUtils.PATH_T + " max similar documents divergence",
				topicModelConfig.getMaxSimilarDocumentsDivergence(), 0.0, 1.0, true));
		topicModelConfig.setMaxSimilarTopicsDivergence(ConsoleUtils.readDouble(" " + ConsoleUtils.PATH_T + " max similar topics divergence",
				topicModelConfig.getMaxSimilarTopicsDivergence(), 0.0, 1.0, true));
		topicModelConfig.setProcessorUseText(
				ConsoleUtils.readBoolean(" " + ConsoleUtils.PATH_T + " processor use text", topicModelConfig.isProcessorUseText()));
		topicModelConfig.setProcessorUseEntities(
				ConsoleUtils.readBoolean(" " + ConsoleUtils.PATH_T + " processor use entities", topicModelConfig.isProcessorUseEntities()));
		topicModelConfig.setProcessorUseEntityTypes(
				ConsoleUtils.readBoolean(" " + ConsoleUtils.PATH_T + " processor use entity types", topicModelConfig.isProcessorUseEntityTypes()));
		topicModelConfig.setProcessorUseHypernyms(
				ConsoleUtils.readBoolean(" " + ConsoleUtils.PATH_T + " processor use hypernyms", topicModelConfig.isProcessorUseHypernyms()));
		topicModelConfig.setQueryEntityDescriptions(
				ConsoleUtils.readBoolean(" " + ConsoleUtils.PATH_T + " query entity types", topicModelConfig.isQueryEntityDescriptions()));
		topicModelConfig.setWindowResolution(ConsoleUtils.readEnum(WindowResolution.class, " " + ConsoleUtils.PATH_L + " window resolution",
				topicModelConfig.getWindowResolution()));

		dbTopicModels.replaceSingle(topicModel);
		topicModelConfig.saveToFile(topicModelConfig.getModelDir(config.getDataDirectory()));
		config.setTopicModelConfig(topicModelConfig);
		Main.stats.stop("edit." + topicModel.getName());
	}

	@Override
	public void run() throws Exception {
		config = Config.getConfig();
		dbTopicModels = MongoService.getDatabaseService(config, TopicModelFull.class);

		final List<TopicModelFull> topicModels = dbTopicModels.getMultiple(QueryBuilder.builder().in("name", Arrays.asList(names)).allFields());

		for (final TopicModelFull topicModel : topicModels) {
			editModel(topicModel);
		}
	}

	@Override
	public boolean requiresLock() {
		return true;
	}

	@Override
	public String getName() {
		return "edit";
	}

}
