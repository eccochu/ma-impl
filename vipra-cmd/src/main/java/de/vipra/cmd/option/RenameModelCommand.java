package de.vipra.cmd.option;

import java.io.File;

import org.bson.types.ObjectId;

import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.model.TopicModelConfig;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.service.MongoService;
import de.vipra.util.service.QueryBuilder;

public class RenameModelCommand implements Command {

	private final String[] models;
	private Config config;
	private MongoService<TopicModelFull, ObjectId> dbTopicModels;

	public RenameModelCommand(final String[] models) {
		this.models = models;
	}

	private void renameModel(final String name) throws Exception {
		String newName;
		while (true) {
			final File oldModelDir = new File(config.getDataDirectory(), name);
			final TopicModelFull topicModel = dbTopicModels.getSingle(QueryBuilder.builder().eq("name", name));
			if (!oldModelDir.exists() || topicModel == null) {
				ConsoleUtils.error("unknwon topic model: '" + name + "'");
				return;
			}

			newName = ConsoleUtils.readString("enter new name for model '" + name + "'");
			final String msg = config.validModelName(newName);
			if (msg != null) {
				ConsoleUtils.error(msg);
				continue;
			}
			final TopicModelConfig modelConfig = config.getTopicModelConfig(name);
			modelConfig.setName(newName);
			modelConfig.saveToFile(oldModelDir);

			final File newModelDir = new File(config.getDataDirectory(), newName);
			oldModelDir.renameTo(newModelDir);

			topicModel.setName(newName);
			topicModel.getModelConfig().setName(newName);
			dbTopicModels.replaceSingle(topicModel);

			ConsoleUtils.info("model '" + name + "' renamed to '" + newName + "'");
			break;
		}
	}

	@Override
	public void run() throws Exception {
		config = Config.getConfig();
		dbTopicModels = MongoService.getDatabaseService(config, TopicModelFull.class);
		for (final String model : models)
			renameModel(model);
	}

	@Override
	public boolean requiresLock() {
		return false;
	}

	@Override
	public String getName() {
		return "rename";
	}

}
