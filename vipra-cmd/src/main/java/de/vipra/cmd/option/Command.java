package de.vipra.cmd.option;

public interface Command {

	public void run() throws Exception;

	public boolean requiresLock();

	public String getName();

}
