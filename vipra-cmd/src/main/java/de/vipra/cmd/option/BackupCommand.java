package de.vipra.cmd.option;

import java.io.File;
import java.util.Date;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Color;
import org.zeroturnaround.zip.ZipUtil;

import de.vipra.cmd.Main;
import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.FileUtils;
import de.vipra.util.StringUtils;
import de.vipra.util.Timer;

public class BackupCommand implements Command {

	private final String path;

	public BackupCommand(final String path) {
		this.path = path;
	}

	@Override
	public void run() throws Exception {
		try {
			Main.stats.start("backup");
			final Timer timer = new Timer();

			final Config config = Config.getConfig();
			final File tmpTarget = FileUtils.getTempFile("vipra-dump");
			org.apache.commons.io.FileUtils.deleteDirectory(tmpTarget);

			Main.stats.start("backup.database");
			ConsoleUtils.infoNOLF(" " + ConsoleUtils.PATH_T + " backup database...");
			final Process p = Runtime.getRuntime().exec("mongodump -d " + config.getDatabaseName() + " -h " + config.getDatabaseHost() + " --port "
					+ config.getDatabasePort() + " -o " + new File(tmpTarget, "db"));
			p.waitFor();
			ConsoleUtils.print(Ansi.ansi().fg(Color.GREEN).a("OK").reset().toString());

			Main.stats.next("backup.database", "backup.filebase");
			ConsoleUtils.infoNOLF(" " + ConsoleUtils.PATH_T + " backup filebase...");
			org.apache.commons.io.FileUtils.copyDirectory(config.getDataDirectory(), new File(tmpTarget, "fb"));
			ConsoleUtils.print(Ansi.ansi().fg(Color.GREEN).a("OK").reset().toString());

			Main.stats.next("backup.filebase", "backup.configuration");
			ConsoleUtils.infoNOLF(" " + ConsoleUtils.PATH_T + " backup configuration...");
			org.apache.commons.io.FileUtils.copyDirectory(Config.getGenericConfigDir(), new File(tmpTarget, "config"));
			ConsoleUtils.print(Ansi.ansi().fg(Color.GREEN).a("OK").reset().toString());

			Main.stats.next("backup.configuration", "backup.compress");
			ConsoleUtils.infoNOLF(" " + ConsoleUtils.PATH_L + " compressing...");
			File target = new File(path);
			if (target.exists() && target.isDirectory())
				target = new File(target, "vipra-" + new Date().getTime() + ".zip");
			ZipUtil.pack(tmpTarget, target);
			org.apache.commons.io.FileUtils.deleteDirectory(tmpTarget);
			ConsoleUtils.print(Ansi.ansi().fg(Color.GREEN).a("OK").reset().toString());

			ConsoleUtils.info("saved to file: " + target.getAbsolutePath());
			ConsoleUtils.info("done in " + StringUtils.timeString(timer.total()));
			Main.stats.stop("backup.compress");
		} catch (final Exception e) {
			ConsoleUtils.print(Ansi.ansi().fg(Color.RED).a("FAILED").reset().toString());
			if (e.getMessage().contains("mongodump")) {
				ConsoleUtils.error("mongodump not installed");
				return;
			} else {
				throw e;
			}
		}
	}

	@Override
	public boolean requiresLock() {
		return true;
	}

	@Override
	public String getName() {
		return "backup";
	}

}
