package de.vipra.cmd.option;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.bson.types.ObjectId;

import de.vipra.cmd.Main;
import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.model.ArticleFull;
import de.vipra.util.model.SequenceFull;
import de.vipra.util.model.TextEntityFull;
import de.vipra.util.model.TopicFull;
import de.vipra.util.model.TopicModel;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.model.WindowFull;
import de.vipra.util.model.WordFull;
import de.vipra.util.service.MongoService;
import de.vipra.util.service.QueryBuilder;

public class ClearModelCommand implements Command {

	private final String[] names;

	public ClearModelCommand(final String[] names) {
		this.names = names;
	}

	@Override
	public void run() throws Exception {
		final Config config = Config.getConfig();
		final MongoService<TopicModelFull, ObjectId> dbTopicModels = MongoService.getDatabaseService(config, TopicModelFull.class);
		final MongoService<ArticleFull, ObjectId> dbArticles = MongoService.getDatabaseService(config, ArticleFull.class);
		final MongoService<TopicFull, ObjectId> dbTopics = MongoService.getDatabaseService(config, TopicFull.class);
		final MongoService<SequenceFull, ObjectId> dbSequences = MongoService.getDatabaseService(config, SequenceFull.class);
		final MongoService<WordFull, String> dbWords = MongoService.getDatabaseService(config, WordFull.class);
		final MongoService<TextEntityFull, String> dbEntities = MongoService.getDatabaseService(config, TextEntityFull.class);
		final MongoService<WindowFull, String> dbWindows = MongoService.getDatabaseService(config, WindowFull.class);

		final List<TopicModelFull> topicModels = dbTopicModels.getMultiple(QueryBuilder.builder().in("name", Arrays.asList(names)).allFields());

		for (final TopicModelFull topicModel : topicModels) {
			Main.stats.start("clear." + topicModel.getName());
			final File modelDir = new File(config.getDataDirectory(), topicModel.getName());

			final QueryBuilder builder = QueryBuilder.builder().eq("topicModel", new TopicModel(topicModel.getId()));

			dbArticles.deleteMultiple(builder);
			dbTopics.deleteMultiple(builder);
			dbSequences.deleteMultiple(builder);
			dbWords.deleteMultiple(builder);
			dbEntities.deleteMultiple(builder);
			dbWindows.deleteMultiple(builder);

			if (modelDir.exists()) {
				FileUtils.cleanDirectory(modelDir);
				topicModel.getModelConfig().saveToFile(modelDir);
			}

			topicModel.setArticleCount(0L);
			topicModel.setTopicCount(0L);
			topicModel.setEntityCount(0L);
			topicModel.setWordCount(0L);

			ConsoleUtils.info("model cleared: " + topicModel.getName());
			Main.stats.stop("clear." + topicModel.getName());
		}

		dbTopicModels.replaceMultiple(topicModels);
	}

	@Override
	public boolean requiresLock() {
		return true;
	}

	@Override
	public String getName() {
		return "clear";
	}

}
