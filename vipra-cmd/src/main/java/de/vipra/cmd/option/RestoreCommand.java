package de.vipra.cmd.option;

import java.io.File;
import java.io.FileNotFoundException;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Color;
import org.zeroturnaround.zip.ZipUtil;

import de.vipra.cmd.Main;
import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.FileUtils;
import de.vipra.util.StringUtils;
import de.vipra.util.Timer;

public class RestoreCommand implements Command {

	private final String path;

	public RestoreCommand(final String path) {
		this.path = path;
	}

	@Override
	public void run() throws Exception {
		try {
			final Timer timer = new Timer();
			ConsoleUtils.info("restoring from file");

			Main.stats.start("restore.unzip");
			final File zip = new File(path);
			if (!zip.isFile())
				throw new FileNotFoundException(path);
			final File tmpTarget = FileUtils.getTempFile("vipra-dump");
			ZipUtil.unpack(zip, tmpTarget);
			final Config config = Config.getConfig();

			Main.stats.next("restore.unzip", "restore.database");
			ConsoleUtils.infoNOLF(" " + ConsoleUtils.PATH_T + " restore database...");
			final Process p = Runtime.getRuntime().exec(
					"mongorestore --drop -h " + config.getDatabaseHost() + " --port " + config.getDatabasePort() + " " + new File(tmpTarget, "db"));
			p.waitFor();
			ConsoleUtils.print(Ansi.ansi().fg(Color.GREEN).a("OK").reset().toString());

			Main.stats.next("restore.database", "restore.filebase");
			ConsoleUtils.infoNOLF(" " + ConsoleUtils.PATH_T + " restore filebase...");
			org.apache.commons.io.FileUtils.copyDirectory(new File(tmpTarget, "fb"), config.getDataDirectory());
			ConsoleUtils.print(Ansi.ansi().fg(Color.GREEN).a("OK").reset().toString());

			Main.stats.next("restore.filebase", "restore.configuration");
			ConsoleUtils.infoNOLF(" " + ConsoleUtils.PATH_L + " restore configuration...");
			org.apache.commons.io.FileUtils.copyDirectory(new File(tmpTarget, "config"), Config.getGenericConfigDir());
			ConsoleUtils.print(Ansi.ansi().fg(Color.GREEN).a("OK").reset().toString());

			Main.stats.stop("restore.configuration");
			org.apache.commons.io.FileUtils.deleteDirectory(tmpTarget);

			ConsoleUtils.info("restored");
			ConsoleUtils.info("done in " + StringUtils.timeString(timer.total()));
		} catch (final Exception e) {
			ConsoleUtils.print(Ansi.ansi().fg(Color.RED).a("FAILED").reset().toString());
			if (e.getMessage().contains("mongorestore")) {
				ConsoleUtils.error("mongorestore not installed");
				return;
			} else {
				throw e;
			}
		}
	}

	@Override
	public boolean requiresLock() {
		return true;
	}

	@Override
	public String getName() {
		return "restore";
	}

}
