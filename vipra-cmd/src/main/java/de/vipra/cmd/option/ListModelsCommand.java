package de.vipra.cmd.option;

import java.util.Map.Entry;

import org.fusesource.jansi.Ansi;

import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.model.TopicModelConfig;
import edu.stanford.nlp.util.StringUtils;

public class ListModelsCommand implements Command {

	@Override
	public void run() throws Exception {
		ConsoleUtils.info("existing models:");
		final Config config = Config.getConfig();
		int longestModelName = 0;
		for (final Entry<String, TopicModelConfig> entry : config.getTopicModelConfigs().entrySet())
			longestModelName = Math.max(longestModelName, entry.getValue().getName().length());
		for (final Entry<String, TopicModelConfig> entry : config.getTopicModelConfigs().entrySet())
			ConsoleUtils.info(Ansi.ansi().a(Ansi.Attribute.INTENSITY_BOLD).a(StringUtils.pad(entry.getValue().getName(), longestModelName)).reset()
					+ " " + entry.getValue().toString());
	}

	@Override
	public boolean requiresLock() {
		return false;
	}

	@Override
	public String getName() {
		return "list";
	}

}
