package de.vipra.cmd.option;

import java.io.InputStream;
import java.util.Properties;

import de.vipra.util.ConsoleUtils;
import de.vipra.util.Constants;
import de.vipra.util.FileUtils;

public class VersionCommand implements Command {

	@Override
	public void run() throws Exception {
		final InputStream in = FileUtils.getResource(Constants.BUILDNUMBER_FILE);
		final Properties props = new Properties();
		props.load(in);

		ConsoleUtils.info("VIPRA CMD tool");
		ConsoleUtils.info(" " + ConsoleUtils.PATH_T + " Version: " + props.getProperty("git-tag"));
		ConsoleUtils.info(" " + ConsoleUtils.PATH_T + " Commit : " + props.getProperty("git-sha-1"));
		ConsoleUtils.info(" " + ConsoleUtils.PATH_L + " From   : " + props.getProperty("builddate"));
	}

	@Override
	public boolean requiresLock() {
		return false;
	}

	@Override
	public String getName() {
		return "version";
	}

}
