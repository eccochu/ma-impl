package de.vipra.cmd.option;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.elasticsearch.client.Client;

import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.ESClient;
import de.vipra.util.service.MongoService;

public class EraseCommand implements Command {

	private Config config;
	private Client elasticClient;

	private void clear() throws Exception {
		ConsoleUtils.info("erasing database");

		config = Config.getConfig();
		elasticClient = ESClient.getClient(config);
		MongoService.dropDatabase(config);
		elasticClient.admin().indices().prepareDelete("_all").get();

		try {
			final File dataDir = config.getDataDirectory();
			if (dataDir.exists() && dataDir.isDirectory()) {
				FileUtils.deleteDirectory(dataDir);
			}
		} catch (final IOException e) {
			ConsoleUtils.warn("could not delete data directory: " + config.getDataDirectory().getAbsolutePath());
		}

		config.clearTopicModelConfigs();

		ConsoleUtils.info("erased");
	}

	@Override
	public void run() throws Exception {
		clear();
	}

	@Override
	public boolean requiresLock() {
		return true;
	}

	@Override
	public String getName() {
		return "erase";
	}

}
