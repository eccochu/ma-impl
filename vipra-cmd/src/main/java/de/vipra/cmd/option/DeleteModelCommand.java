package de.vipra.cmd.option;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;

import de.vipra.cmd.Main;
import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.model.ArticleFull;
import de.vipra.util.model.SequenceFull;
import de.vipra.util.model.TextEntityFull;
import de.vipra.util.model.TopicFull;
import de.vipra.util.model.TopicModel;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.model.WindowFull;
import de.vipra.util.model.WordFull;
import de.vipra.util.service.MongoService;
import de.vipra.util.service.QueryBuilder;

public class DeleteModelCommand implements Command {

	private final String[] names;

	public DeleteModelCommand(final String[] names) {
		this.names = names;
	}

	@Override
	public void run() throws Exception {
		final Config config = Config.getConfig();
		final MongoService<TopicModelFull, ObjectId> dbTopicModels = MongoService.getDatabaseService(config, TopicModelFull.class);
		final MongoService<ArticleFull, ObjectId> dbArticles = MongoService.getDatabaseService(config, ArticleFull.class);
		final MongoService<TopicFull, ObjectId> dbTopics = MongoService.getDatabaseService(config, TopicFull.class);
		final MongoService<SequenceFull, ObjectId> dbSequences = MongoService.getDatabaseService(config, SequenceFull.class);
		final MongoService<WordFull, String> dbWords = MongoService.getDatabaseService(config, WordFull.class);
		final MongoService<TextEntityFull, String> dbEntities = MongoService.getDatabaseService(config, TextEntityFull.class);
		final MongoService<WindowFull, String> dbWindows = MongoService.getDatabaseService(config, WindowFull.class);

		final List<TopicModelFull> topicModels = dbTopicModels.getMultiple(QueryBuilder.builder().in("name", Arrays.asList(names)));

		for (final TopicModelFull topicModel : topicModels) {
			Main.stats.start("delete." + topicModel.getName());
			final File modelDir = new File(config.getDataDirectory(), topicModel.getName());

			final QueryBuilder builder = QueryBuilder.builder().eq("topicModel", new TopicModel(topicModel.getId()));

			// delete from database
			long deleted = 0;
			deleted += dbArticles.deleteMultiple(builder);
			deleted += dbTopics.deleteMultiple(builder);
			deleted += dbSequences.deleteMultiple(builder);
			deleted += dbWords.deleteMultiple(builder);
			deleted += dbEntities.deleteMultiple(builder);
			deleted += dbWindows.deleteMultiple(builder);

			// delete from filebase
			if (modelDir.exists()) {
				org.apache.commons.io.FileUtils.deleteDirectory(modelDir);
				deleted++;
			}

			// delete from config
			config.deleteTopicModelConfig(topicModel.getName());

			if (deleted > 0)
				ConsoleUtils.info("model deleted: " + topicModel.getName());
			Main.stats.stop("delete." + topicModel.getName());
		}

		dbTopicModels.deleteMultiple(topicModels.stream().map((t) -> t.getId()).collect(Collectors.toList()));
	}

	@Override
	public boolean requiresLock() {
		return true;
	}

	@Override
	public String getName() {
		return "delete";
	}

}
