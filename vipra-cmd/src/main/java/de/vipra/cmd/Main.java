package de.vipra.cmd;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.mongodb.morphia.logging.MorphiaLoggerFactory;
import org.mongodb.morphia.logging.slf4j.SLF4JLoggerImplFactory;

import de.vipra.cmd.option.BackupCommand;
import de.vipra.cmd.option.ClearModelCommand;
import de.vipra.cmd.option.Command;
import de.vipra.cmd.option.CreateModelCommand;
import de.vipra.cmd.option.DeleteModelCommand;
import de.vipra.cmd.option.EditModelCommand;
import de.vipra.cmd.option.EraseCommand;
import de.vipra.cmd.option.ImportCommand;
import de.vipra.cmd.option.IndexingCommand;
import de.vipra.cmd.option.ListModelsCommand;
import de.vipra.cmd.option.ModelingCommand;
import de.vipra.cmd.option.PrintModelCommand;
import de.vipra.cmd.option.RenameModelCommand;
import de.vipra.cmd.option.RestoreCommand;
import de.vipra.cmd.option.TestCommand;
import de.vipra.cmd.option.VersionCommand;
import de.vipra.util.Config;
import de.vipra.util.ConsoleUtils;
import de.vipra.util.LockFile;
import de.vipra.util.Statistics;
import de.vipra.util.ex.LockFileException;

public class Main {

	private static final LockFile lockFile = new LockFile();
	public static Statistics stats = new Statistics();

	static {
		// set morphia log level
		MorphiaLoggerFactory.registerLogger(SLF4JLoggerImplFactory.class);
		// set corenlp log level, close stderr to mute corenlp messages
		System.err.close();
	}

	private static void lock() {
		try {
			lockFile.lock();
		} catch (final LockFileException e1) {
			ConsoleUtils.error("Cannot acquire lock. Is vipra already running? If not, delete the file '" + lockFile.getPath() + "'");
			System.exit(ErrorCodes.CANNOT_LOCK.ordinal());
		}
	}

	private static void unlock() {
		try {
			lockFile.unlock();
		} catch (final LockFileException e) {
			ConsoleUtils.error("Cannot delete lock file. Delete the file '" + lockFile.getPath() + "'");
			System.exit(ErrorCodes.CANNOT_UNLOCK.ordinal());
		}
	}

	public static void main(final String[] args) {

		final CommandLineOptions opts = new CommandLineOptions();
		try {
			opts.parse(args);
		} catch (final Exception e) {
			ConsoleUtils.error(e.getMessage());
			opts.printHelp();
			return;
		}

		if (opts.isHelp()) {
			opts.printHelp();
			return;
		}

		if (opts.isPerf())
			stats.begin();

		// logger configuration

		ConsoleUtils.setSilent(opts.isSilent());

		try {
			if (opts.isLog()) {
				final String file = opts.logPath();
				if (file == null)
					ConsoleUtils.setLogFile(Config.getGenericLogFile());
				else
					ConsoleUtils.setLogFile(new File(file));
			}
		} catch (final IOException e) {
			ConsoleUtils.error("cannot use log file: " + e.getMessage());
			return;
		}

		// config

		Config.configString = opts.configString();

		// commands

		final List<Command> commands = new ArrayList<>();

		try {
			if (opts.isVersion())
				commands.add(new VersionCommand());

			if (opts.isTest())
				commands.add(new TestCommand());

			if (opts.isBackup())
				commands.add(new BackupCommand(opts.backupPath()));

			if (opts.isRestore())
				commands.add(new RestoreCommand(opts.restorePath()));

			if (opts.isErase())
				commands.add(new EraseCommand());

			if (opts.isClear())
				commands.add(new ClearModelCommand(opts.modelsToClear()));

			if (opts.isDelete())
				commands.add(new DeleteModelCommand(opts.modelsToDelete()));

			if (opts.isCreate())
				commands.add(new CreateModelCommand(opts.modelsToCreate()));

			if (opts.isRename())
				commands.add(new RenameModelCommand(opts.modelsToRename()));

			if (opts.isList())
				commands.add(new ListModelsCommand());

			if (opts.isEdit())
				commands.add(new EditModelCommand(opts.modelsToEdit()));

			if (opts.isPrint())
				commands.add(new PrintModelCommand(opts.modelsToPrint()));

			if (opts.isImport())
				commands.add(new ImportCommand(opts.modelsForImport(), opts.filesToImport()));

			if (opts.isModel() || opts.isReread())
				commands.add(new ModelingCommand(opts.modelsToModel(), opts.isReread()));

			if (opts.isIndex())
				commands.add(new IndexingCommand(opts.modelsToIndex()));
		} catch (final Exception e) {
			ConsoleUtils.error(e.getMessage());
		}

		// run

		if (commands.size() > 0) {
			boolean locked = false;
			for (final ListIterator<Command> it = commands.listIterator(); it.hasNext();) {
				final Command c = it.next();
				if (c.requiresLock() && !locked) {
					lock();
					locked = true;
				}
				try {
					Main.stats.start(c.getName());
					c.run();
				} catch (final Exception e) {
					final Throwable cause = e.getCause();
					if (cause != null)
						ConsoleUtils.error(cause.getMessage());
					else
						ConsoleUtils.error(e.getMessage());
					if (opts.isDebug() && !opts.isSilent())
						e.printStackTrace(System.out);
				} finally {
					Main.stats.stop(c.getName());
				}
			}
		} else {
			opts.printHelp();
		}

		unlock();

		if (opts.isPerf()) {
			try {
				stats.finish(opts.perfPath());
			} catch (final IOException e) {
				ConsoleUtils.error("could not write statistics: " + e.getMessage());
			}
		}
	}

}
