package de.vipra.cmd.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import de.vipra.util.Constants;

public class FilebaseIDDateIndex implements Iterable<FilebaseIDDateIndexEntry> {

	public static final String FILE_NAME = "iddate.idx";

	private boolean sorted = true;
	private boolean dirty = false;
	private final File file;
	private final List<FilebaseIDDateIndexEntry> entries;

	public FilebaseIDDateIndex(final File modelDir) throws ParseException, IOException {
		file = new File(modelDir, FILE_NAME);
		entries = new ArrayList<>();
		if (file.exists()) {
			final BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			String line = null;
			while ((line = in.readLine()) != null) {
				final String[] parts = line.split(",");
				entries.add(new FilebaseIDDateIndexEntry(parts[0], new Date(Long.parseLong(parts[1])), false));
			}
			in.close();
		}
	}

	public void sync() throws IOException {
		if (!dirty)
			return;
		if (!sorted)
			sort();
		final BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, false)));
		for (final FilebaseIDDateIndexEntry entry : entries) {
			out.write(entry.getId());
			out.write(",");
			out.write(Long.toString(entry.getDate().getTime()));
			out.write(Constants.LINE_SEP);
		}
		out.close();
		dirty = false;
	}

	public void add(final String id, final Date date) {
		entries.add(new FilebaseIDDateIndexEntry(id, date, true));
		sorted = false;
		dirty = true;
	}

	public FilebaseIDDateIndexEntry get(final int index) {
		return entries.get(index);
	}

	public void sort() {
		Collections.sort(entries);
		sorted = true;
	}

	public int size() {
		return entries.size();
	}

	@Override
	public Iterator<FilebaseIDDateIndexEntry> iterator() {
		if (!sorted)
			sort();
		return entries.iterator();
	}

}
