package de.vipra.cmd.file;

import java.util.Date;

public class FilebaseIDDateIndexEntry implements Comparable<FilebaseIDDateIndexEntry> {

	private final String id;
	private final Date date;
	private final boolean isNew;

	public FilebaseIDDateIndexEntry(final String id, final Date date, final boolean isNew) {
		this.id = id;
		this.date = date;
		this.isNew = isNew;
	}

	public String getId() {
		return id;
	}

	public Date getDate() {
		return date;
	}

	public boolean isNew() {
		return isNew;
	}

	@Override
	public int compareTo(final FilebaseIDDateIndexEntry o) {
		return date.compareTo(o.getDate());
	}

}
