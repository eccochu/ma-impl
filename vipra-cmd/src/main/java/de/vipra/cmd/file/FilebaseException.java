package de.vipra.cmd.file;

import java.io.IOException;

public class FilebaseException extends IOException {

	private static final long serialVersionUID = 1L;

	public FilebaseException(final String msg) {
		super(msg);
	}

	public FilebaseException(final Exception e) {
		super(e);
	}

	public FilebaseException(final String msg, final Exception e) {
		super(msg, e);
	}

}
