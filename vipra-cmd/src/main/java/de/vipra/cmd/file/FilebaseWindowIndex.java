package de.vipra.cmd.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;

import de.vipra.util.Constants;
import de.vipra.util.Constants.WindowResolution;
import de.vipra.util.CountMap;
import de.vipra.util.model.Window;

public class FilebaseWindowIndex {

	public static final String MULT_FILE_NAME = "dtm-mult.dat";
	public static final String SEQ_FILE_NAME = "dtm-seq.dat";
	public static final String WIN_FILE_NAME = "windows.idx";

	private boolean seqDirty = false;
	private boolean winDirty = false;
	private final File modelDir;
	private final File seqFile;
	private final File winFile;
	private final WindowResolution windowResolution;
	private List<Integer> windowSizes;
	private List<Date> windowDates;
	private final CountMap<Date> windowMap;

	public FilebaseWindowIndex(final File modelDir, final WindowResolution windowResolution) throws NumberFormatException, IOException {
		this.modelDir = modelDir;
		seqFile = new File(modelDir, SEQ_FILE_NAME);
		winFile = new File(modelDir, WIN_FILE_NAME);
		this.windowResolution = windowResolution;
		windowSizes = new ArrayList<>();
		windowDates = new ArrayList<>();
		windowMap = new CountMap<>();
		if (winFile.exists()) {
			final BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(winFile)));
			String line = null;
			while ((line = in.readLine()) != null) {
				final String[] parts = line.split(",");
				final Date date = new Date(Long.parseLong(parts[0]));
				final int count = Integer.parseInt(parts[1]);
				windowSizes.add(count);
				windowDates.add(date);
				windowMap.count(date, count);
			}
			in.close();
		}
	}

	public void sync() throws IOException {
		if (!winDirty)
			return;

		if (!modelDir.exists() && !modelDir.mkdirs())
			throw new FilebaseException("could not create model directory: " + modelDir.getAbsolutePath());

		final BufferedWriter winOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(winFile, false)));
		final BufferedWriter seqOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(seqFile, false)));

		final List<Date> dates = new ArrayList<>(windowMap.keySet());
		Collections.sort(dates);

		seqOut.write(Integer.toString(windowMap.size()));
		seqOut.write(Constants.LINE_SEP);

		for (final Date date : dates) {
			final int windowSize = windowMap.get(date);
			winOut.write(Long.toString(date.getTime()));
			winOut.write(",");
			winOut.write(Integer.toString(windowSize));
			winOut.write(Constants.LINE_SEP);
			seqOut.write(Integer.toString(windowSize));
			seqOut.write(Constants.LINE_SEP);
		}

		winOut.close();
		seqOut.close();

		winDirty = false;
	}

	public void add(final Date date) {
		windowMap.count(windowResolution.startDate(date));
		winDirty = true;
		seqDirty = true;
	}

	public int size() {
		if (seqDirty)
			resizeWindows();
		return windowSizes.size();
	}

	public int windowSize(final int index) {
		if (seqDirty)
			resizeWindows();
		return windowSizes.get(index);
	}

	public Date startDate(final int index) {
		if (seqDirty)
			resizeWindows();
		return windowResolution.startDate(windowDates.get(index));
	}

	public Date startDate(final Date date) {
		return windowResolution.startDate(date);
	}

	public Date endDate(final int index) {
		if (seqDirty)
			resizeWindows();
		return windowResolution.endDate(windowDates.get(index));
	}

	public Date endDate(final Date date) {
		return windowResolution.endDate(date);
	}

	public void copy(final File modelFile) throws IOException {
		FileUtils.copyFile(modelFile, new File(modelDir, MULT_FILE_NAME));
	}

	public List<Window> getWindows() {
		final List<Window> windows = new ArrayList<>(size());
		for (int i = 0; i < size(); i++)
			windows.add(getWindow(i));
		return windows;
	}

	public Window getWindow(final int index) {
		final Window window = new Window();
		window.setStartDate(startDate(index));
		window.setEndDate(endDate(index));
		window.setWindowResolution(windowResolution);
		return window;
	}

	public Window getWindow(final Date date) {
		final Window window = new Window();
		window.setStartDate(startDate(date));
		window.setEndDate(endDate(date));
		window.setWindowResolution(windowResolution);
		return window;
	}

	public void clear() {
		windowSizes.clear();
		windowDates.clear();
		windowMap.clear();
	}

	public void resizeWindows() {
		final List<Date> dates = new ArrayList<>(windowMap.keySet());
		Collections.sort(dates);
		windowSizes = new ArrayList<>(windowMap.size());
		windowDates = new ArrayList<>(windowMap.size());
		for (final Date date : dates) {
			windowSizes.add(windowMap.get(date));
			windowDates.add(date);
		}
		seqDirty = false;
	}

}
