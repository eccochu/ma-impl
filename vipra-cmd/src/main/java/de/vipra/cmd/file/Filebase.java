package de.vipra.cmd.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import de.vipra.util.Constants;
import de.vipra.util.ex.ConfigException;
import de.vipra.util.model.ArticleFull;
import de.vipra.util.model.TopicModelConfig;

public class Filebase {

	public static final String FILE_NAME = "dtm-mult.dat";

	private final TopicModelConfig modelConfig;
	private final File modelDir;
	private final File file;
	private final Map<String, ArticleFull> newArticles;
	private final FilebaseIDDateIndex idDateIndex;
	private final FilebaseWordIndex wordIndex;
	private final FilebaseWindowIndex windowIndex;

	public Filebase(final TopicModelConfig modelConfig, final File dataDir) throws ParseException, IOException {
		this.modelConfig = modelConfig;
		modelDir = new File(dataDir, modelConfig.getName());
		file = new File(modelDir, FILE_NAME);
		newArticles = new HashMap<>();
		idDateIndex = new FilebaseIDDateIndex(modelDir);
		wordIndex = new FilebaseWordIndex(modelDir);
		windowIndex = new FilebaseWindowIndex(modelDir, modelConfig.getWindowResolution());
	}

	public void add(final ArticleFull article) throws FilebaseException {
		newArticles.put(article.getId().toString(), article);
		idDateIndex.add(article.getId().toString(), article.getDate());
	}

	public void sync() throws IOException, ConfigException {
		if (newArticles.isEmpty())
			return;
		if (!modelDir.exists() && !modelDir.mkdirs())
			throw new FilebaseException("could not create data directory: " + modelDir.getAbsolutePath());

		// delete tmp file if exists
		final File tmpFile = new File(modelDir, FILE_NAME + ".tmp");
		if (tmpFile.exists() && !tmpFile.delete())
			throw new FilebaseException("could not delete tmp file: " + tmpFile.getAbsolutePath());

		// old model reader
		BufferedReader in = null;
		if (file.exists())
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

		// tmp model writer
		final BufferedWriter outModel = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tmpFile, false)));

		// merge existing model and new entries
		for (final FilebaseIDDateIndexEntry entry : idDateIndex) {
			if (entry.isNew()) {
				final ArticleFull newArticle = newArticles.get(entry.getId());
				if (modelConfig.isProcessorUseText())
					outModel.write(wordIndex.transform(newArticle.getProcessedText()));

				outModel.write(" ");

				if (modelConfig.isProcessorUseEntities())
					outModel.write(wordIndex.transform(newArticle.entities()));
				else if (modelConfig.isProcessorUseHypernyms())
					outModel.write(wordIndex.transform(newArticle.hypernyms()));

				outModel.write(" ");

				if (modelConfig.isProcessorUseEntityTypes())
					outModel.write(wordIndex.transform(newArticle.types()));

				outModel.write(Constants.LINE_SEP);
			} else {
				if (in == null) {
					outModel.close();
					throw new FilebaseException("filebase inconsistency: missing article with id: " + entry.getId());
				}
				outModel.write(in.readLine());
				outModel.write(Constants.LINE_SEP);
			}

			windowIndex.add(entry.getDate());
		}

		// close buffers
		if (in != null)
			in.close();
		outModel.close();

		// move tmp file
		if (file.exists() && !file.delete())
			throw new FilebaseException("could not delete model file: " + file.getAbsolutePath());
		if (!tmpFile.renameTo(file))
			throw new FilebaseException("could not rename tmp file: " + tmpFile.getAbsolutePath());

		// sync indexes
		idDateIndex.sync();
		wordIndex.sync();
		windowIndex.sync();
	}

	public FilebaseIDDateIndex getIdDateIndex() {
		return idDateIndex;
	}

	public FilebaseWordIndex getWordIndex() {
		return wordIndex;
	}

	public FilebaseWindowIndex getWindowIndex() {
		return windowIndex;
	}

}
