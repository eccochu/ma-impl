# Vipra-CMD

This application is used to maintenance the Vipra database.

## Build JAR

To use this application, it has to be packaged into an executable jar file. Download dependencies and run the build script:

* `mvn install`
* `ant`
