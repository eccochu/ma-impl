#!/bin/bash

VERSION=$(git describe --tags --abbrev=0 2> /dev/null)
if [ $? -ne 0 ]; then
	VERSION=$(git rev-parse --short HEAD 2> /dev/null)
fi

if [ -z $VERSION ]; then
	echo "could not get version"
	exit 1
fi

DIR="$(dirname "$(readlink -f "$0")")"
REL_NAME="vipra-$VERSION"
REL="$DIR/$REL_NAME"

rm -rf $REL vipra*.tar.gz
mkdir -p $REL/vipra-backend $REL/vipra-cmd $REL/vipra-ui
cp -r $DIR/dtm_release $REL/dtm_release
find $REL -type f -name "*.o" -exec rm -f {} \;
rm -f $REL/dtm_release/dtm/main
cp $DIR/vipra-backend/target/vipra.war $REL/vipra-backend
cp $DIR/vipra-cmd/target/*.jar $REL/vipra-cmd/vipra.jar
cp -r $DIR/vipra-cmd/target/lib $REL/vipra-cmd/lib
cp -r $DIR/vipra-ui/public $REL/vipra-ui
cp $DIR/vipra $REL/vipra-cmd/vipra
cp $DIR/vipra $REL/vipra
cp $DIR/LICENSE $DIR/README.md $REL
cp $DIR/scripts/* $REL

tar zcf $REL_NAME.tar.gz $REL_NAME
rm -rf $REL

echo "$REL_NAME.tar.gz"
exit 0