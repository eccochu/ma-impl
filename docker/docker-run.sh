#!/bin/sh

#######################################################################################
# CONFIGURATION

# set host machine ports
HOST_NGINX=80
HOST_ELASTICSEARCH_REST=9200
HOST_ELASTICSEARCH_API=9300
HOST_MONGODB=27017

# set to 1 to override built in webapps and webroot
REPLACE_WEBAPPS=1
REPLACE_WEBROOT=1

#######################################################################################

DIR="$(dirname "$(readlink -f "$0")")"
PORT_MAPPING="-p $HOST_NGINX:80 -p $HOST_ELASTICSEARCH_REST:9200 -p $HOST_ELASTICSEARCH_API:9300 -p $HOST_MONGODB:27017"

if [ $REPLACE_WEBAPPS -eq 1 ]; then
	VOLUME_WEBAPPS="-v $DIR/webapps:/tomcat/webapps"
fi

if [ $REPLACE_WEBROOT -eq 1 ]; then
	VOLUME_WEBROOT="-v $DIR/webroot:/webroot"
fi

docker run -d $PORT_MAPPING $VOLUME_WEBAPPS $VOLUME_WEBROOT -v $DIR/logs:/tomcat/logs eikecochu/vipra
