#!/bin/sh

p=$OLDPWD
wget -O /tmp/vipra.tar.gz ftp://cochu.io/vipra/vipra-current.tar.gz
tar zxf /tmp/vipra.tar.gz -C /tmp
rm /tmp/vipra.tar.gz
cd /tmp/vipra*
sudo ./install.sh >> /dev/null
sudo yum install -y gsl-devel
sudo make -C /opt/vipra/dtm_release/dtm
cd $p
rm -rf /tmp/vipra*