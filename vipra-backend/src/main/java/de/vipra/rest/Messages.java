package de.vipra.rest;

public class Messages {

	public static final String NOT_FOUND = "%s with id %s was not found";
	public static final String BAD_REQUEST = "bad request: %s";

}
