package de.vipra.rest.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import de.vipra.util.StringUtils;

public class ResponseWrapper<T> {

	private List<APIError> errors;
	private Map<String, Object> headers;

	public List<APIError> getErrors() {
		return errors;
	}

	public void addError(final APIError error) {
		if (errors == null)
			errors = new ArrayList<>();
		errors.add(error);
	}

	public void addHeader(final String name, final Object value) {
		if (headers == null)
			headers = new HashMap<>();
		headers.put("V-" + StringUtils.capitalize(name), value);
	}

	public boolean hasErrors() {
		return errors != null && errors.size() > 0;
	}

	private void addHeaders(final ResponseBuilder builder) {
		if (headers == null || headers.size() == 0)
			return;
		for (final Entry<String, Object> entry : headers.entrySet())
			builder.header(entry.getKey(), entry.getValue());
	}

	/**
	 * Status 200
	 */
	public Response ok(final T data) {
		final ResponseBuilder builder = Response.ok().entity(data);
		addHeaders(builder);
		return builder.build();
	}

	/**
	 * Status 201
	 */
	public Response created(final T data, final URI loc) {
		final ResponseBuilder builder = Response.created(loc);
		if (data != null)
			builder.entity(data);
		addHeaders(builder);
		return builder.build();
	}

	/**
	 * Status 204
	 */
	public Response noContent() {
		final ResponseBuilder builder = Response.noContent();
		addHeaders(builder);
		return builder.build();
	}

	/**
	 * Status 400
	 */
	public Response badRequest() {
		final ResponseBuilder builder = Response.status(Status.BAD_REQUEST).entity(errors);
		addHeaders(builder);
		return builder.build();
	}

	/**
	 * Status 404
	 */
	public Response notFound() {
		final ResponseBuilder builder = Response.status(Status.NOT_FOUND).entity(errors);
		addHeaders(builder);
		return builder.build();
	}

	/**
	 * Status 500
	 */
	public Response serverError() {
		final ResponseBuilder builder = Response.serverError().entity(errors);
		addHeaders(builder);
		return builder.build();
	}

}
