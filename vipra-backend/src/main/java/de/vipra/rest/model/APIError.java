package de.vipra.rest.model;

import javax.ws.rs.core.Response.Status;

public class APIError {

	private String status;
	private String code;
	private String title;
	private String detail;

	public APIError() {}

	public APIError(final String status, final String code, final String title, final String detail) {
		this.status = status;
		this.code = code;
		this.title = title;
		this.detail = detail;
	}

	public APIError(final Status status, final String title, final String detail) {
		this(Integer.toString(status.getStatusCode()), status.getReasonPhrase(), title, detail);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(final String detail) {
		this.detail = detail;
	}

}
