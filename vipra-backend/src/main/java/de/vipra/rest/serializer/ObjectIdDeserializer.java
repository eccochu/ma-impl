package de.vipra.rest.serializer;

import java.io.IOException;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import de.vipra.util.MongoUtils;

public class ObjectIdDeserializer extends JsonDeserializer<ObjectId> {

	@Override
	public ObjectId deserialize(final JsonParser p, final DeserializationContext ctxt) throws IOException, JsonProcessingException {
		return MongoUtils.objectId(p.getValueAsString());
	}

}
