package de.vipra.rest.resource;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;

import de.vipra.rest.Messages;
import de.vipra.rest.model.APIError;
import de.vipra.rest.model.ResponseWrapper;
import de.vipra.util.Config;
import de.vipra.util.StringUtils;
import de.vipra.util.ex.ConfigException;
import de.vipra.util.model.TopicModel;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.model.WordFull;
import de.vipra.util.service.MongoService;
import de.vipra.util.service.QueryBuilder;

@Path("words")
public class WordResource {

	final MongoService<WordFull, String> dbWords;
	final MongoService<TopicModelFull, ObjectId> dbTopicModels;

	public WordResource(@Context final ServletContext servletContext) throws ConfigException, IOException {
		final Config config = Config.getConfig();
		dbWords = MongoService.getDatabaseService(config, WordFull.class);
		dbTopicModels = MongoService.getDatabaseService(config, TopicModelFull.class);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWords(@QueryParam("topicModel") final String topicModel, @QueryParam("skip") final Integer skip,
			@QueryParam("limit") final Integer limit, @QueryParam("sort") @DefaultValue("id") final String sortBy,
			@QueryParam("fields") final String fields, @QueryParam("char") final String startChar, @QueryParam("contains") final String contains,
			@QueryParam("entity") final Boolean isEntity) {
		final ResponseWrapper<List<WordFull>> res = new ResponseWrapper<>();

		if (topicModel == null || topicModel.trim().isEmpty()) {
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Topic model is empty",
					String.format(Messages.BAD_REQUEST, "topic model cannot be empty")));
			return res.badRequest();
		}

		try {
			final QueryBuilder query = QueryBuilder.builder().skip(skip).limit(limit).sortBy(sortBy);
			if (fields != null && !fields.isEmpty())
				query.fields(true, StringUtils.getFields(fields));

			final ObjectId topicModelId;
			if (ObjectId.isValid(topicModel)) {
				topicModelId = new ObjectId(topicModel);
			} else {
				final TopicModelFull topicModelFull = dbTopicModels.getSingle(QueryBuilder.builder().eq("name", topicModel));
				if (topicModelFull == null)
					throw new Exception("Topic model with name '" + topicModel + "' not found");
				topicModelId = topicModelFull.getId();
			}
			query.eq("topicModel", new TopicModel(topicModelId));

			if (startChar != null && !startChar.isEmpty())
				query.startsWith("word", startChar, true);

			if (contains != null && !contains.isEmpty())
				query.contains("word", contains, true);

			if (isEntity != null)
				query.eq("isEntity", isEntity);

			final List<WordFull> words = dbWords.getMultiple(query);

			if ((skip != null && skip > 0) || (limit != null && limit > 0))
				res.addHeader("total", dbWords.count(query));
			else
				res.addHeader("total", words.size());

			return res.ok(words);
		} catch (final Exception e) {
			e.printStackTrace();
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Error", e.getMessage()));
			return res.badRequest();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response getWord(@PathParam("id") String id, @QueryParam("topicModel") final String topicModel,
			@QueryParam("fields") final String fields) {
		final ResponseWrapper<WordFull> res = new ResponseWrapper<>();
		if (id == null || id.trim().isEmpty()) {
			res.addError(new APIError(Response.Status.BAD_REQUEST, "ID is empty", String.format(Messages.BAD_REQUEST, "id cannot be empty")));
			return res.badRequest();
		}
		if (topicModel == null || topicModel.trim().isEmpty()) {
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Topic model is empty",
					String.format(Messages.BAD_REQUEST, "topic model cannot be empty")));
			return res.badRequest();
		}
		try {
			id = URLDecoder.decode(id, "UTF-8");
		} catch (final UnsupportedEncodingException e1) {
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Invalid ID", String.format(Messages.BAD_REQUEST, "id could not be decoded")));
			return res.badRequest();
		}

		WordFull word;
		try {
			final ObjectId topicModelId;
			if (ObjectId.isValid(topicModel)) {
				topicModelId = new ObjectId(topicModel);
			} else {
				final TopicModelFull topicModelFull = dbTopicModels.getSingle(QueryBuilder.builder().eq("name", topicModel));
				if (topicModelFull == null)
					throw new Exception("Topic model with name '" + topicModel + "' not found");
				topicModelId = topicModelFull.getId();
			}

			word = dbWords.getSingle(id.toLowerCase() + "-" + topicModelId, StringUtils.getFields(fields));
		} catch (final Exception e) {
			e.printStackTrace();
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Error", e.getMessage()));
			return res.badRequest();
		}

		if (word != null) {
			return res.ok(word);
		} else {
			res.addError(new APIError(Response.Status.NOT_FOUND, "Resource not found", String.format(Messages.NOT_FOUND, "word", id)));
			return res.notFound();
		}
	}

}
