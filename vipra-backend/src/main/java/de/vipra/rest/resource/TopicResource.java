package de.vipra.rest.resource;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;

import de.vipra.rest.Messages;
import de.vipra.rest.model.APIError;
import de.vipra.rest.model.ResponseWrapper;
import de.vipra.util.Config;
import de.vipra.util.MongoUtils;
import de.vipra.util.StringUtils;
import de.vipra.util.ex.ConfigException;
import de.vipra.util.ex.DatabaseException;
import de.vipra.util.model.ArticleFull;
import de.vipra.util.model.Topic;
import de.vipra.util.model.TopicFull;
import de.vipra.util.model.TopicModel;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.model.WordFull;
import de.vipra.util.service.MongoService;
import de.vipra.util.service.QueryBuilder;

@Path("topics")
public class TopicResource {

	final MongoService<TopicFull, ObjectId> dbTopics;
	final MongoService<ArticleFull, ObjectId> dbArticles;
	final MongoService<WordFull, String> dbWords;
	final MongoService<TopicModelFull, ObjectId> dbTopicModels;

	public TopicResource(@Context final ServletContext servletContext) throws ConfigException, IOException {
		final Config config = Config.getConfig();
		dbTopics = MongoService.getDatabaseService(config, TopicFull.class);
		dbArticles = MongoService.getDatabaseService(config, ArticleFull.class);
		dbWords = MongoService.getDatabaseService(config, WordFull.class);
		dbTopicModels = MongoService.getDatabaseService(config, TopicModelFull.class);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopics(@QueryParam("topicModel") final String topicModel, @QueryParam("skip") final Integer skip,
			@QueryParam("limit") final Integer limit, @QueryParam("sort") @DefaultValue("name") final String sortBy,
			@QueryParam("fields") final String fields, @QueryParam("word") final String word, @QueryParam("char") final String startChar,
			@QueryParam("contains") final String contains) {
		final ResponseWrapper<List<TopicFull>> res = new ResponseWrapper<>();

		if (topicModel == null || topicModel.trim().isEmpty()) {
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Topic model is empty",
					String.format(Messages.BAD_REQUEST, "topic model cannot be empty")));
			return res.badRequest();
		}

		try {
			final QueryBuilder query = QueryBuilder.builder().skip(skip).limit(limit).sortBy(sortBy);
			if (fields != null && !fields.isEmpty())
				query.fields(true, StringUtils.getFields(fields));

			final ObjectId topicModelId;
			if (ObjectId.isValid(topicModel)) {
				topicModelId = new ObjectId(topicModel);
			} else {
				final TopicModelFull topicModelFull = dbTopicModels.getSingle(QueryBuilder.builder().eq("name", topicModel));
				if (topicModelFull == null)
					throw new Exception("Topic model with name '" + topicModel + "' not found");
				topicModelId = topicModelFull.getId();
			}
			query.eq("topicModel", new TopicModel(topicModelId));

			if (word != null && !word.isEmpty())
				query.eq("words.word", word);

			if (startChar != null && !startChar.isEmpty())
				query.startsWith("name", startChar, true);

			if (contains != null && !contains.isEmpty())
				query.contains("name", contains, true);

			final List<TopicFull> topics = dbTopics.getMultiple(query);

			if ((skip != null && skip > 0) || (limit != null && limit > 0))
				res.addHeader("total", dbTopics.count(query));
			else
				res.addHeader("total", topics.size());

			return res.ok(topics);
		} catch (final Exception e) {
			e.printStackTrace();
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Error", e.getMessage()));
			return Response.status(Response.Status.BAD_REQUEST).entity(res).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response getTopic(@PathParam("id") String id, @QueryParam("fields") final String fields) throws ConfigException, IOException {
		final ResponseWrapper<TopicFull> res = new ResponseWrapper<>();
		if (id == null || id.trim().length() == 0) {
			res.addError(new APIError(Response.Status.BAD_REQUEST, "ID is empty", String.format(Messages.BAD_REQUEST, "id cannot be empty")));
			return res.badRequest();
		}
		try {
			id = URLDecoder.decode(id, "UTF-8");
		} catch (final UnsupportedEncodingException e1) {
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Invalid ID", String.format(Messages.BAD_REQUEST, "id could not be decoded")));
			return res.badRequest();
		}

		TopicFull topic;
		try {
			topic = dbTopics.getSingle(MongoUtils.objectId(id), StringUtils.getFields(fields));
		} catch (final Exception e) {
			e.printStackTrace();
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Error", e.getMessage()));
			return res.badRequest();
		}

		if (topic != null) {
			return res.ok(topic);
		} else {
			res.addError(new APIError(Response.Status.NOT_FOUND, "Resource not found", String.format(Messages.NOT_FOUND, "topic", id)));
			return res.notFound();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}/articles")
	public Response getArticles(@PathParam("id") final String id, @QueryParam("skip") final Integer skip, @QueryParam("limit") final Integer limit,
			@QueryParam("sort") @DefaultValue("title") final String sortBy, @QueryParam("fields") final String fields,
			@QueryParam("from") final Long fromDate, @QueryParam("to") final Long toDate) {
		final ResponseWrapper<List<ArticleFull>> res = new ResponseWrapper<>();
		try {
			final Topic topic = new Topic(MongoUtils.objectId(id));
			final QueryBuilder query = QueryBuilder.builder().eq("topics.topic", topic).skip(skip).limit(limit).sortBy(sortBy);

			if (fields != null && !fields.isEmpty())
				query.fields(true, StringUtils.getFields(fields));

			if (fromDate != null) {
				final Date d = new Date(fromDate);
				query.gte("date", d);
			}

			if (toDate != null) {
				final Date d = new Date(toDate);
				query.lte("date", d);
			}

			final List<ArticleFull> articles = dbArticles.getMultiple(query);

			if ((skip != null && skip > 0) || (limit != null && limit > 0))
				res.addHeader("total", dbArticles.count(query));
			else
				res.addHeader("total", articles.size());

			return res.ok(articles);
		} catch (final Exception e) {
			e.printStackTrace();
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Error", e.getMessage()));
			return res.badRequest();
		}
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response renameTopic(@PathParam("id") final String id, final TopicFull topic) {
		final ResponseWrapper<TopicFull> res = new ResponseWrapper<>();
		final TopicFull topic2 = new TopicFull();
		topic2.setId(topic.getId());
		topic2.setName(topic.getName());

		try {
			dbTopics.updateSingle(topic2, "name");
			return res.ok(topic);
		} catch (final DatabaseException e) {
			e.printStackTrace();
			res.addError(new APIError(Response.Status.INTERNAL_SERVER_ERROR, "item could not be updated",
					"item could not be updated due to an internal server error"));
			return res.serverError();
		}
	}

}
