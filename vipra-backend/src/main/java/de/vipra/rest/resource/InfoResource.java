package de.vipra.rest.resource;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;

import de.vipra.rest.model.ResponseWrapper;
import de.vipra.util.BuildInfo;
import de.vipra.util.Config;
import de.vipra.util.Constants;
import de.vipra.util.NestedMap;
import de.vipra.util.StringUtils;
import de.vipra.util.model.Article;
import de.vipra.util.model.Topic;
import de.vipra.util.service.MongoService;

@Path("info")
public class InfoResource {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInfo() {
		final ResponseWrapper<NestedMap> res = new ResponseWrapper<>();
		final NestedMap info = new NestedMap();

		try {
			final RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();
			final Runtime rt = Runtime.getRuntime();
			final BuildInfo buildInfo = new BuildInfo();
			final Config config = Config.getConfig();
			final MongoService<Article, ObjectId> dbArticles = MongoService.getDatabaseService(config, Article.class);
			final MongoService<Topic, ObjectId> dbTopics = MongoService.getDatabaseService(config, Topic.class);

			// vm info
			info.put("vm.starttime", rb.getStartTime());
			info.put("vm.uptime", rb.getUptime());
			info.put("vm.args", StringUtils.join(rb.getInputArguments()));
			info.put("vm.java.vendor", System.getProperty("java.vendor"));
			info.put("vm.java.version", System.getProperty("java.version"));

			// host info
			info.put("host.cores", rt.availableProcessors());
			info.put("host.memory", rt.maxMemory());
			info.put("host.os.name", System.getProperty("os.name"));
			info.put("host.os.arch", System.getProperty("os.arch"));
			info.put("host.os.version", System.getProperty("os.version"));

			// app info
			info.put("app.gittag", buildInfo.getGitTag());
			info.put("app.gitsha1", buildInfo.getGitSHA1());
			info.put("app.version", buildInfo.getVersion());
			info.put("app.builddate", buildInfo.getBuildDate());

			// database info
			info.put("db.articles", dbArticles.count(null));
			info.put("db.topics", dbTopics.count(null));

			// constants
			info.put("const.windowres", Constants.WINDOW_RESOLUTION);
			info.put("const.proctext", Constants.PROCESSOR_USE_TEXT);
			info.put("const.procentity", Constants.PROCESSOR_USE_ENTITIES);
			info.put("const.proctype", Constants.PROCESSOR_USE_ENTITY_TYPES);
			info.put("const.prochypernym", Constants.PROCESSOR_USE_HYPERNYMS);
			info.put("const.importbuf", Constants.IMPORT_BUFFER_MAX);
			info.put("const.esboosttopics", Constants.ES_BOOST_TOPICS);
			info.put("const.esboosttitles", Constants.ES_BOOST_TITLES);
			info.put("const.topicautoname", Constants.TOPIC_AUTO_NAMING_WORDS);
			info.put("const.ktopics", Constants.K_TOPICS);
			info.put("const.ktopwords", Constants.K_TOP_WORDS);
			info.put("const.decaylambda", Constants.RISING_DECAY_LAMBDA);
			info.put("const.mintopicshare", Constants.MIN_TOPIC_SHARE);
			info.put("const.minrelprob", Constants.MIN_RELATIVE_PROB);
			info.put("const.maxsimdocs", Constants.MAX_SIMILAR_DOCUMENTS);
			info.put("const.maxsimtopics", Constants.MAX_SIMILAR_TOPICS);
			info.put("const.maxdocdiv", Constants.MAX_SIMILAR_DOCUMENTS_DIVERGENCE);
			info.put("const.maxtopicdiv", Constants.MAX_SIMILAR_TOPICS_DIVERGENCE);
			info.put("const.dynminiter", Constants.DYNAMIC_MIN_ITER);
			info.put("const.dynmaxiter", Constants.DYNAMIC_MAX_ITER);
			info.put("const.statiter", Constants.STATIC_ITER);
			info.put("const.docminlength", Constants.DOCUMENT_MIN_LENGTH);
			info.put("const.docminwordfreq", Constants.DOCUMENT_MIN_WORD_FREQ);
			info.put("const.spotsupport", Constants.SPOTLIGHT_SUPPORT);
			info.put("const.spotconf", Constants.SPOTLIGHT_CONFIDENCE);
			info.put("const.charsdisallow", Constants.CHARS_DISALLOWED);
			info.put("const.regexemail", Constants.REGEX_EMAIL);
			info.put("const.regexurl", Constants.REGEX_URL);
			info.put("const.regexnumber", Constants.REGEX_NUMBER);
			info.put("const.regexchar", Constants.REGEX_SINGLECHAR);
			info.put("const.excerptlength", Constants.EXCERPT_LENGTH);
			info.put("const.dateformat", Constants.DATETIME_FORMAT);

		} catch (final Exception e) {
			info.put("error", e.getMessage());
		}

		return res.ok(info);
	}
}
