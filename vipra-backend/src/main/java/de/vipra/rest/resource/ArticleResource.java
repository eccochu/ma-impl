package de.vipra.rest.resource;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.bson.types.ObjectId;

import de.vipra.rest.Messages;
import de.vipra.rest.model.APIError;
import de.vipra.rest.model.ResponseWrapper;
import de.vipra.util.Config;
import de.vipra.util.Constants;
import de.vipra.util.MongoUtils;
import de.vipra.util.StringUtils;
import de.vipra.util.ex.ConfigException;
import de.vipra.util.model.ArticleFull;
import de.vipra.util.model.TextEntityFull;
import de.vipra.util.model.Topic;
import de.vipra.util.model.TopicModel;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.model.WordFull;
import de.vipra.util.service.MongoService;
import de.vipra.util.service.QueryBuilder;

@Path("articles")
public class ArticleResource {

	@Context
	UriInfo uri;

	final MongoService<ArticleFull, ObjectId> dbArticles;
	final MongoService<WordFull, String> dbWords;
	final MongoService<TextEntityFull, String> dbEntities;
	final MongoService<TopicModelFull, ObjectId> dbTopicModels;

	public ArticleResource(@Context final ServletContext servletContext) throws ConfigException, IOException {
		final Config config = Config.getConfig();
		dbArticles = MongoService.getDatabaseService(config, ArticleFull.class);
		dbWords = MongoService.getDatabaseService(config, WordFull.class);
		dbEntities = MongoService.getDatabaseService(config, TextEntityFull.class);
		dbTopicModels = MongoService.getDatabaseService(config, TopicModelFull.class);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getArticles(@QueryParam("topicModel") final String topicModel, @QueryParam("skip") final Integer skip,
			@QueryParam("limit") final Integer limit, @QueryParam("sort") @DefaultValue("date") final String sortBy,
			@QueryParam("fields") final String fields, @QueryParam("word") final String word, @QueryParam("entity") final String entity,
			@QueryParam("excerpt") final String excerpt, @QueryParam("char") final String startChar, @QueryParam("contains") final String contains,
			@QueryParam("from") final Long fromDate, @QueryParam("to") final Long toDate, @QueryParam("topics") final List<String> topics) {
		return processArticles(topicModel, skip, limit, sortBy, fields, word, entity, excerpt, startChar, contains, fromDate, toDate, topics);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response postArticles(@FormParam("topicModel") final String topicModel, @FormParam("skip") final Integer skip,
			@FormParam("limit") final Integer limit, @FormParam("sort") @DefaultValue("date") final String sortBy,
			@FormParam("fields") final String fields, @FormParam("word") final String word, @FormParam("entity") final String entity,
			@FormParam("excerpt") final String excerpt, @FormParam("char") final String startChar, @FormParam("contains") final String contains,
			@FormParam("from") final Long fromDate, @FormParam("to") final Long toDate, @FormParam("topics") final List<String> topics) {
		return processArticles(topicModel, skip, limit, sortBy, fields, word, entity, excerpt, startChar, contains, fromDate, toDate, topics);
	}

	private Response processArticles(final String topicModel, final Integer skip, final Integer limit, final String sortBy, final String fields,
			final String word, final String entity, final String excerpt, final String startChar, final String contains, final Long fromDate,
			final Long toDate, final List<String> topics) {
		final ResponseWrapper<List<ArticleFull>> res = new ResponseWrapper<>();

		if (topicModel == null || topicModel.trim().isEmpty()) {
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Topic model is empty",
					String.format(Messages.BAD_REQUEST, "topic model cannot be empty")));
			return res.badRequest();
		}

		try {
			final QueryBuilder query = QueryBuilder.builder().skip(skip).limit(limit).sortBy(sortBy);
			if (fields != null && !fields.isEmpty())
				query.fields(true, StringUtils.getFields(fields));

			final ObjectId topicModelId;
			if (ObjectId.isValid(topicModel)) {
				topicModelId = new ObjectId(topicModel);
			} else {
				final TopicModelFull topicModelFull = dbTopicModels.getSingle(QueryBuilder.builder().eq("name", topicModel));
				if (topicModelFull == null)
					throw new Exception("Topic model with name '" + topicModel + "' not found");
				topicModelId = topicModelFull.getId();
			}
			query.eq("topicModel", new TopicModel(topicModelId));

			if (word != null && !word.isEmpty())
				query.eq("words.word", word);

			if (entity != null && !entity.isEmpty())
				query.eq("entities.entity.entity", entity);

			if (startChar != null && !startChar.isEmpty())
				query.startsWith("title", startChar, true);

			if (contains != null && !contains.isEmpty())
				query.contains("title", contains, true);

			if (fromDate != null)
				query.gte("date", new Date(fromDate));

			if (toDate != null)
				query.lte("date", new Date(toDate));

			if (topics != null && !topics.isEmpty()) {
				final List<Topic> ids = new ArrayList<>(topics.size());
				for (final String s : topics)
					if (ObjectId.isValid(s))
						ids.add(new Topic(new ObjectId(s)));
				query.in("topics.topic", ids);
			}

			final List<ArticleFull> articles = dbArticles.getMultiple(query);

			if ((skip != null && skip > 0) || (limit != null && limit > 0))
				res.addHeader("total", dbArticles.count(query));
			else
				res.addHeader("total", articles.size());

			if (excerpt != null) {
				int length = Constants.EXCERPT_LENGTH;
				try {
					length = Integer.parseInt(excerpt);
				} catch (final NumberFormatException e) {}
				for (final ArticleFull article : articles)
					article.setText(article.serializeExcerpt(length));
			}

			return res.ok(articles);
		} catch (final Exception e) {
			e.printStackTrace();
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Error", e.getMessage()));
			return res.badRequest();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response getArticle(@PathParam("id") String id, @QueryParam("fields") final String fields, @QueryParam("excerpt") final String excerpt) {
		final ResponseWrapper<ArticleFull> res = new ResponseWrapper<>();
		if (id == null || id.trim().length() == 0) {
			res.addError(new APIError(Response.Status.BAD_REQUEST, "ID is empty", String.format(Messages.BAD_REQUEST, "id cannot be empty")));
			return res.badRequest();
		}
		try {
			id = URLDecoder.decode(id, "UTF-8");
		} catch (final UnsupportedEncodingException e1) {
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Invalid ID", String.format(Messages.BAD_REQUEST, "id could not be decoded")));
			return res.badRequest();
		}

		ArticleFull article;
		try {
			article = dbArticles.getSingle(MongoUtils.objectId(id), StringUtils.getFields(fields));
		} catch (final Exception e) {
			e.printStackTrace();
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Error", e.getMessage()));
			return res.badRequest();
		}

		if (article != null) {
			if (excerpt != null) {
				int length = Constants.EXCERPT_LENGTH;
				try {
					length = Integer.parseInt(excerpt);
				} catch (final NumberFormatException e) {}
				article.setText(article.serializeExcerpt(length));
			}

			return res.ok(article);
		} else {
			res.addError(new APIError(Response.Status.NOT_FOUND, "Resource not found", String.format(Messages.NOT_FOUND, "article", id)));
			return res.notFound();
		}
	}

}
