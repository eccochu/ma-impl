package de.vipra.rest.resource;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;

import de.vipra.rest.Messages;
import de.vipra.rest.model.APIError;
import de.vipra.rest.model.ResponseWrapper;
import de.vipra.util.Config;
import de.vipra.util.StringUtils;
import de.vipra.util.ex.ConfigException;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.service.MongoService;
import de.vipra.util.service.QueryBuilder;

@Path("topicmodels")
public class TopicModelResource {

	final MongoService<TopicModelFull, ObjectId> dbTopicModels;

	public TopicModelResource(@Context final ServletContext servletContext) throws ConfigException, IOException {
		final Config config = Config.getConfig();
		dbTopicModels = MongoService.getDatabaseService(config, TopicModelFull.class);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopicModels(@QueryParam("skip") final Integer skip, @QueryParam("limit") final Integer limit,
			@QueryParam("sort") @DefaultValue("id") final String sortBy, @QueryParam("fields") final String fields) {
		final ResponseWrapper<List<TopicModelFull>> res = new ResponseWrapper<>();

		try {
			final QueryBuilder query = QueryBuilder.builder().skip(skip).limit(limit).sortBy(sortBy);
			if (fields != null && !fields.isEmpty())
				query.fields(true, StringUtils.getFields(fields));

			final List<TopicModelFull> topicModels = dbTopicModels.getMultiple(query);

			if ((skip != null && skip > 0) || (limit != null && limit > 0))
				res.addHeader("total", dbTopicModels.count(query));
			else
				res.addHeader("total", topicModels.size());

			return res.ok(topicModels);
		} catch (final Exception e) {
			e.printStackTrace();
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Error", e.getMessage()));
			return Response.status(Response.Status.BAD_REQUEST).entity(res).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response getTopicModel(@PathParam("id") String id, @QueryParam("fields") final String fields) {
		final ResponseWrapper<TopicModelFull> res = new ResponseWrapper<>();
		if (id == null || id.trim().length() == 0) {
			res.addError(new APIError(Response.Status.BAD_REQUEST, "ID is empty", String.format(Messages.BAD_REQUEST, "id cannot be empty")));
			return res.badRequest();
		}
		try {
			id = URLDecoder.decode(id, "UTF-8");
		} catch (final UnsupportedEncodingException e1) {
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Invalid ID", String.format(Messages.BAD_REQUEST, "id could not be decoded")));
			return res.badRequest();
		}

		TopicModelFull topicModel;
		try {
			topicModel = dbTopicModels.getSingle(new ObjectId(id), StringUtils.getFields(fields));
			if (topicModel == null)
				topicModel = dbTopicModels.getSingle(QueryBuilder.builder().eq("name", id).fields(StringUtils.getFields(fields)));
		} catch (final Exception e) {
			e.printStackTrace();
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Error", e.getMessage()));
			return res.badRequest();
		}

		if (topicModel != null) {
			return res.ok(topicModel);
		} else {
			res.addError(new APIError(Response.Status.NOT_FOUND, "Resource not found", String.format(Messages.NOT_FOUND, "topicmodel", id)));
			return res.notFound();
		}
	}

}
