package de.vipra.rest.resource;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;

import de.vipra.rest.Messages;
import de.vipra.rest.model.APIError;
import de.vipra.rest.model.ResponseWrapper;
import de.vipra.util.Config;
import de.vipra.util.Constants;
import de.vipra.util.ESClient;
import de.vipra.util.MongoUtils;
import de.vipra.util.ex.ConfigException;
import de.vipra.util.model.ArticleFull;
import de.vipra.util.model.TopicModelFull;
import de.vipra.util.service.MongoService;

@Path("search")
public class SearchResource {

	final TransportClient client;
	final MongoService<TopicModelFull, ObjectId> dbTopicModels;

	public SearchResource(@Context final ServletContext servletContext) throws ConfigException, IOException {
		final Config config = Config.getConfig();
		client = ESClient.getClient(config);
		dbTopicModels = MongoService.getDatabaseService(config, TopicModelFull.class);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response doSearch(@QueryParam("topicModel") String topicModel, @QueryParam("skip") Integer skip, @QueryParam("limit") Integer limit,
			@QueryParam("fields") final String fields, @QueryParam("query") final String query, @QueryParam("from") final Long fromDate,
			@QueryParam("to") final Long toDate) {
		final ResponseWrapper<List<ArticleFull>> res = new ResponseWrapper<>();

		if (topicModel == null || topicModel.trim().isEmpty()) {
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Topic model is empty",
					String.format(Messages.BAD_REQUEST, "topic model cannot be empty")));
			return res.badRequest();
		}

		if (skip == null || skip < 0)
			skip = 0;

		if (limit == null || limit < 0)
			limit = 20;

		if (query == null || query.isEmpty() || limit == 0)
			return res.noContent();

		String indexName = "_all";
		try {
			if (ObjectId.isValid(topicModel)) {
				final TopicModelFull topicModelFull = dbTopicModels.getSingle(new ObjectId(topicModel));
				if (topicModelFull == null)
					throw new Exception("Topic model with name '" + topicModel + "' not found");
				topicModel = topicModelFull.getName();
			}
			indexName = topicModel + "-articles";
		} catch (final Exception e) {
			e.printStackTrace();
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Error", e.getMessage()));
			return res.badRequest();
		}

		SearchResponse response = null;

		org.elasticsearch.index.query.QueryBuilder qb = QueryBuilders.multiMatchQuery(query, "topics^" + Constants.ES_BOOST_TOPICS,
				"title^" + Constants.ES_BOOST_TITLES, "_all");

		if (fromDate != null || toDate != null) {
			final RangeQueryBuilder rqb = QueryBuilders.rangeQuery("date");
			final SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATETIME_FORMAT);
			if (fromDate != null) {
				final String date = sdf.format(new Date(fromDate));
				rqb.from(date);
			}
			if (toDate != null) {
				final String date = sdf.format(new Date(toDate));
				rqb.to(date);
			}
			rqb.format(Constants.DATETIME_FORMAT);
			qb = QueryBuilders.boolQuery().must(qb).must(rqb);
		}

		try {
			response = client.prepareSearch(indexName).setQuery(qb).setFrom(skip).setSize(limit).execute().actionGet();
		} catch (final Exception e) {
			e.printStackTrace();
			res.addError(new APIError(Response.Status.BAD_REQUEST, "Error", e.getMessage()));
			return res.badRequest();
		}

		Set<String> allowedFields = null;
		if (fields != null) {
			allowedFields = new HashSet<>(Arrays.asList(fields.split(",")));
		}

		final SearchHits hits = response.getHits();
		final List<ArticleFull> articles = new ArrayList<>(10);
		for (final SearchHit hit : hits) {
			final Map<String, Object> source = hit.getSource();
			final ArticleFull article = new ArticleFull();
			article.setId(MongoUtils.objectId(hit.getId()));
			if (allowedFields == null || allowedFields.contains("title"))
				article.setTitle(source.get("title").toString());
			if (allowedFields == null || allowedFields.contains("date"))
				article.setDate(source.get("date").toString());
			if (allowedFields == null || allowedFields.contains("excerpt"))
				article.setText(source.get("excerpt").toString());
			article.addMeta("score", hit.getScore());
			articles.add(article);
		}

		res.addHeader("total", hits.getTotalHits());

		return res.ok(articles);
	}

}
