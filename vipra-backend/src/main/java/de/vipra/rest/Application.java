package de.vipra.rest;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.message.GZipEncoder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.EncodingFilter;

import de.vipra.rest.provider.CORSResponseFilter;
import de.vipra.rest.provider.ObjectMapperProvider;

public class Application extends ResourceConfig {

	public Application() {
		// register rest application
		packages("de.vipra.rest.resource");
		register(JacksonFeature.class);
		register(CORSResponseFilter.class);
		register(ObjectMapperProvider.class);
		EncodingFilter.enableFor(this, GZipEncoder.class);
	}

}
