package de.vipra.rest.provider;

import java.text.SimpleDateFormat;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;

import de.vipra.rest.serializer.ObjectIdDeserializer;
import de.vipra.rest.serializer.ObjectIdSerializer;
import de.vipra.util.Constants;

@Provider
public class ObjectMapperProvider implements ContextResolver<ObjectMapper> {

	public static final Logger log = LogManager.getLogger(ObjectMapperProvider.class);
	final ObjectMapper defaultObjectMapper;

	public ObjectMapperProvider() {
		defaultObjectMapper = createDefaultMapper();
		log.info("object mapper provider registered");
	}

	@Override
	public ObjectMapper getContext(final Class<?> type) {
		return defaultObjectMapper;
	}

	public static ObjectMapper createDefaultMapper() {
		final SimpleModule module = new SimpleModule();

		module.addSerializer(ObjectId.class, new ObjectIdSerializer());
		module.addDeserializer(ObjectId.class, new ObjectIdDeserializer());

		final ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.setSerializationInclusion(Include.NON_EMPTY);
		mapper.setDateFormat(new SimpleDateFormat(Constants.DATETIME_FORMAT));
		mapper.registerModule(module);
		return mapper;
	}

}
