package de.vipra.rest.provider;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Provider
public class CORSResponseFilter implements ContainerResponseFilter {

	public static final Logger log = LogManager.getLogger(CORSResponseFilter.class);

	public CORSResponseFilter() {
		log.info("cors filter registered");
	}

	@Override
	public void filter(final ContainerRequestContext request, final ContainerResponseContext response) throws IOException {
		response.getHeaders().add("Access-Control-Allow-Origin", "*");
		response.getHeaders().add("Access-Control-Allow-Headers", "Origin, Content-Type, Accept, Authorization");
		response.getHeaders().add("Access-Control-Allow-Credentials", "true");
		response.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD");
		response.getHeaders().add("Access-Control-Expose-Headers", "V-Total");
	}
}