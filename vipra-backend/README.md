# Backend Service

This project contains the Vipra backend services. These services include:

* REST service (`de.vipra.rest` packages)
* WebSocket (`de.vipra.ws` packages)

## REST Service

The REST service is used by the frontend UI project to query and modify data in the database and search index. It offers multiple REST conform resources for CRUD operations. A full specification in the form of a WADL file can be found under: [application.wadl](http://localhost:8080/vipra/rest/application.wadl)

## WebSocket

The WebSocket is used by the frontend UI project to obtain instant updates on changed data.