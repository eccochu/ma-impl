package de.vipra.hypernym;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import de.ruedigermoeller.serialization.FSTConfiguration;

public class HypernymReader {

	public static final String IN_FILE_PATH = "/home/eike/Downloads/en.lhd.extension.2015-10.nt";
	public static final String OUT_FILE_PATH = "/home/eike/Downloads/hypernyms.ser";

	public static final Pattern LINE_PATTERN = Pattern
			.compile("<http://dbpedia.org/resource/([^>]+)>\\s+<([^>]+)>\\s+<http://dbpedia.org/resource/([^>]+)>");

	public static void main(String[] args) throws Exception {
		final File inFile = new File(IN_FILE_PATH);
		if (!inFile.exists() || inFile.isDirectory())
			throw new Exception("input file does not exist or is a directory");

		final BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(inFile)));
		String line;

		// skip first line
		in.readLine();

		int lineCount = countLines(inFile);
		final Map<String, String> hypernyms = new HashMap<>(lineCount);

		int lineNum = 1;
		while ((line = in.readLine()) != null) {
			lineNum++;
			if (line.isEmpty())
				continue;
			Matcher m = LINE_PATTERN.matcher(line);
			if (!m.find()) {
				System.err.println("pattern does not match line " + lineNum);
				continue;
			}

			hypernyms.put(StringEscapeUtils.unescapeJava(m.group(1)), StringEscapeUtils.unescapeJava(m.group(3)));
		}

		in.close();

		FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
		byte barray[] = conf.asByteArray((Serializable) hypernyms);
		FileUtils.writeByteArrayToFile(new File(OUT_FILE_PATH), barray);
	}

	public static int countLines(final File file) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(file));
		try {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			return (count == 0 && !empty) ? 1 : count;
		} finally {
			is.close();
		}
	}

}
