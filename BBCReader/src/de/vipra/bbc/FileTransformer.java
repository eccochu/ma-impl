package de.vipra.bbc;

import java.io.File;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class FileTransformer {

	public static final String BBC_SOURCE_PATH = "/home/eike/Downloads/bbc";
	public static final String BBC_TARGET_PATH = "/home/eike/Downloads/bbc-transformed";

	public static final String BBC_SEARCH_URL = "http://www.bbc.co.uk/search?filter=news&q=";

	public static final Pattern TITLE_TEXT_REGEX = Pattern.compile("(.+?)\\n\\n(.+)$", Pattern.DOTALL);

	public static final SimpleDateFormat sdfBBC = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	public static final SimpleDateFormat sdfVIPRA = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	public static void main(String[] args) throws Exception {
		final File sourceDir = new File(BBC_SOURCE_PATH);
		if (!sourceDir.exists() || !sourceDir.isDirectory())
			throw new Exception("Source directory not found or not a directory");

		final File bbcTarget = new File(BBC_TARGET_PATH);
		if (bbcTarget.exists())
			FileUtils.deleteDirectory(bbcTarget);
		bbcTarget.mkdirs();

		final List<BBCArticle> articles = new ArrayList<>();

		for (final File topicDir : sourceDir.listFiles((d) -> d.isDirectory())) {
			final String topic = topicDir.getName();
			int i = 1;
			for (final File file : topicDir.listFiles()) {
				System.out.println(file.getAbsolutePath());
				try {
					articles.add(getArticle(topic, file, i++));
				} catch (Exception e) {
					System.err.println(e.getMessage() + " (" + file.getAbsolutePath() + ")");
				}
			}
		}

		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.writeValue(new File(bbcTarget, "bbc.json"), articles);
	}

	public static final BBCArticle getArticle(final String topic, final File file, final int number) throws Exception {
		final BBCArticle article = new BBCArticle();
		final String content = FileUtils.readFileToString(file, Charset.defaultCharset());
		final Matcher matcher = TITLE_TEXT_REGEX.matcher(content);
		if (!matcher.find())
			throw new Exception("Invalid article structure");

		article.setTitle(matcher.group(1));
		article.setText(matcher.group(2));

		getBBCData(article);

		return article;
	}

	public static final void getBBCData(final BBCArticle article) throws Exception {
		final String src = BBC_SEARCH_URL + URLEncoder.encode(article.getTitle(), "UTF-8");
		final Document doc = Jsoup.connect(src).get();
		final Elements results = doc.select("article");
		if (results.size() == 0)
			throw new Exception("not found: " + article.getTitle() + ", no search results");

		for (Element el : results) {
			String strDate = el.select(".display-date").attr("datetime");
			if(strDate == null)
				continue;
			Date date = sdfBBC.parse(strDate);
			final Calendar c = new GregorianCalendar();
			c.setTime(date);
			if (c.get(Calendar.YEAR) != 2004 && c.get(Calendar.YEAR) != 2005)
				continue;

			article.setDate(sdfVIPRA.format(date));

			Elements elTitle = el.select("h1 > a");
			String title = elTitle.text();
			if (!article.getTitle().toLowerCase().trim().equals(title.toLowerCase().trim()))
				continue;

			String url = elTitle.attr("href");
			if(url == null)
				continue;
			article.setUrl(url);
			return;
		}
		
		throw new Exception("not found: " + article.getTitle() + ", no matching search result");
	}

}
